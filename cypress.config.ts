import { defineConfig } from 'cypress';

const startTime: any = new Date();

export default defineConfig({
    e2e: {
        baseUrl: `http://localhost:5173${process.env.VITE_APP_ROUTE_BASE_URL}`,
        viewportWidth: 1920,
        viewportHeight: 932,
        setupNodeEvents(on, config) {
            const { baseUrl } = config;
            const endTime: any = new Date();
            const elapsedTimeInSeconds: any = endTime - startTime;

            on('before:run', () => {
                console.clear();
                console.log(`\x1B[32m➜  CYPRESS v${require('cypress/package.json').version} \x1B[37m ready in ${elapsedTimeInSeconds}ms`);
                console.log();
                console.log(`\x1B[32m➜  E2E Local:\t\x1B[36m${baseUrl}`);
                console.log('\x1B[32m➜  Network:\t\x1B[37muse --host to expose');
                console.log();
                console.log(`You can now view ${require('./package.json').name} in the browser, and view tests result.`);
            });

            on('after:run', () => {
                console.log('E2E Tests Exit');
            });
        }
    }
});
