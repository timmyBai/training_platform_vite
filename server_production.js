const express = require('express');

const app = express();
const port = 5200;

app.use('/TrainingBackstage', express.static('build'));

app.use('/', (req, res) => {
    res.redirect('/TrainingBackstage');
});

app.listen(port);
console.log('Server started at http://localhost:' + port);