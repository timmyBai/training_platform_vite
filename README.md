# Training Platform Vite

<div>
    <img src="https://img.shields.io/badge/react-18.2.0-blue">
    <img src="https://img.shields.io/badge/react dom-18.2.0-blue">
    <img src="https://img.shields.io/badge/@reduxjs/toolkit-1.9.5-blue">
    <img src="https://img.shields.io/badge/react router dom-6.3.0-blue">
    <img src="https://img.shields.io/badge/vite-4.4.5-blue">
</div>

## 簡介

Training Platform Vite 是一個等速肌力訓練後端解決方案，他基於 react、react-dom、react-router-dom、@reduxjs/toolkit、vite 實現，他使用最新前端技術，內置了登入權限解決方案，提供多環境建置環境，快速建置開發服務器、提供強大擴展性，快速最佳化靜態資源，提供了許多前端處理方法，他可以幫助你快速搭建前端原型，可以幫助你解決許多需求。本專案針對等速肌力專案釋出相對應前端所需功能，提供後台需求。

## 功能

```textile
- 首頁
- 登入
- 學員清單
- 身體組成與體適能評價
- 運動處方
- 儀器設定值
- 訓練排程
- 檢視報表
- 列印
- 登出
```

## 開發

```git
# 克隆項目
git clone http://192.168.18.72/emgtech/training_platform_vite.git

# 進入目錄
cd training_platform_vite

# 安裝套件
npm install
或
yarn install

# development(開發機)
npm run start:dev

# staging(demo 機)
npm run start:stag
或
yarn start:stag

# production(正式機)
npm run start:prod
或
yarn start:prod

# ship(出貨機)
npm run start:ship
或
yarn start:ship
```

## 打包

```git
# development(開發機)
npm run build
或
yarn build

# staging(demo 機)
npm run build:stag
或
yarn build:stag

# production(正式機)
npm run build:prod
或
yarn build:prod

# ship(出貨機)
npm run build:ship
或
yarn build:ship
```

## 打包 docker

```docker
# development(開發機)
docker-compose -f docker-compose.development.yml build && docker-compose -f docker-compose.development.yml up -d

# staging(demo 機)
docker-compose -f docker-compose.staging.yml build && docker-compose -f docker-compose.staging.yml up -d

# production(正式機)
docker-compose -f docker-compose.production.yml build && docker-compose -f docker-compose.production.yml up -d

# ship(出貨機)
docker-compose -f docker-compose.ship.yml build && docker-compose -f docker-compose.ship.yml up -d
```

## 佈署伺服器

| 環境          | 自動化佈署 | 必須設定 SSH | 必須安裝 docker | 必須安裝 docker-compose |
| ----------- | ----- | -------- | ----------- | ------------------- |
| development | √     | √        | √           | √                   |
| staging     | √     | √        | √           | √                   |
| production  | √     | √        | √           | √                   |
| ship        | √     | √        | √           | √                   |

## 自動化佈署流程

| 階段名稱                    | 工作名稱                              | 作用                          |
| ----------------------- | --------------------------------- | --------------------------- |
| testing                 | unit-tests                        | 單元測試方法                      |
| testing                 | test-build                        | 測試打包                        |
| build_development_image | build-to-docker-development-image | 打包 development docker image |
| deploy_development      | deploy-to-development             | 佈署開發機                       |
| build_staging_image     | build-to-docker-staging-image     | 打包 staging docker image     |
| deploy_staging          | deploy-to-staging                 | 佈署 demo 機                   |
| build_production_image  | build-to-docker-production-image  | 打包 production docker image  |
| deploy_production       | deploy-to-production              | 佈署正式機                       |
| build_ship_image        | build-to-docker-ship-image        | 打包 ship docker image        |
| deploy_ship             | deploy-to-docker-build-ship       | 佈署出貨機                       |

## GitLab Commit 格式

| 標籤       | 更改類型                        | 範例                                     |
| -------- | --------------------------- | -------------------------------------- |
| feat     | (feature)                   | feat(page): add page                   |
| fix      | (bug fix)                   | fix(page): add page bug                |
| docs     | (documentation)             | docs(documentation): add documentation |
| style    | (formatting, css, sass)     | style(sass): add page style            |
| refactor | (refactor)                  | refactor(page): refactor page          |
| test     | (when adding missing tests) | test(function): add function test      |
| chore    | (maintain)                  | chore(style): modify sass chore        |
