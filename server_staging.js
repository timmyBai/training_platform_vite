const express = require('express');

const app = express();
const port = 5100;

app.use('/Stag/TrainingBackstage', express.static('build'));

app.use('/', (req, res) => {
    res.redirect('/Stag/TrainingBackstage');
});

app.listen(port);
console.log('Server started at http://localhost:' + port);