import { defineConfig, loadEnv } from 'vite';
// import react from '@vitejs/plugin-react-swc';
import { swcReactRefresh } from 'vite-plugin-swc-react-refresh';
import path from 'path';
import eslintPlugin from 'vite-plugin-eslint';

// https://vitejs.dev/config/
export default ({ mode }) => {
    process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };

    return defineConfig({
        appType: 'spa',
        base: process.env.VITE_APP_ROUTE_BASE_URL,
        plugins: [swcReactRefresh(), eslintPlugin()],
        server: {
            open: true
        },
        esbuild: {
            jsx: 'automatic'
        },
        resolve: {
            alias: {
                '@': path.resolve(__dirname, 'src')
            }
        },
        css: {
            preprocessorOptions: {
                scss: {
                    additionalData: '@use @/styles/colors.sass',
                    javascriptEnabled: true
                }
            }
        },
        build: {
            outDir: 'build',
            assetsDir: 'static',
            target: 'es2015'
        }
    });
};
