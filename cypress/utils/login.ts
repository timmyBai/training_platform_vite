export const homeProccess = (): void => {
    // 進入首頁
    cy.visit('/home/');

    // 檢查首頁有無 '等速肌力訓練後台管理' 文字
    cy.get('h1[class=\'title\']')
        .should('exist')
        .contains('等速肌力訓練後台管理');

    // 檢查首頁有無 '銀髮族專用智慧型運動復健器材' 文字
    cy.get('h3[class=\'sub_title\']')
        .should('exist')
        .contains('銀髮族專用智慧型運動器材');

    // 檢查是否有按進登入頁面肌肉圖片
    cy.get('img[class=\'homeArrow\']')
        .should('exist')
        .click();
};

/**
 * 登入流程
 */
export const loginProccess = (): void => {
    // 檢查點擊肌肉圖片是否現在在登入路徑
    cy.url()
        .should('include', '/login/');

    cy.get('h1.title').should('exist').contains('等速肌力訓練後台管理');
    cy.get('h3.subMenu').should('exist').contains('銀髮專用智慧型運動器材');

    // 輸入管理者帳號
    cy.get('input[class=\'account\']')
        .should('exist')
        .type('jason');

    // 輸入管理者密碼
    cy.get('input[class=\'password\']')
        .should('exist')
        .type('jason');

    // 按下登入按鈕
    cy.get('button[class=\'submit\']').click();

    // 確認現在是否在學員清單路由
    cy.url()
        .should('include', '/studentList/');
};