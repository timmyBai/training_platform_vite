/**
 * 檢查滲透後，所彈出的錯誤訊息
 */
const inspectDialogErrorMessage = (): void => {
    cy.get('.dialog.visibleInfoDialog')
        .get('h1[class=\'title\']')
        .should('exist')
        .contains('錯誤');

    cy.get('.dialog.visibleInfoDialog')
        .get('.dialogTitle')
        .get('h1[class=\'title\']')
        .should('exist')
        .contains('錯誤');

    cy.get('.dialog.visibleInfoDialog')
        .get('.dialogBody')
        .get('.content')
        .get('p')
        .should('exist')
        .contains('無此使用者');

    cy.get('.dialog.visibleInfoDialog')
        .get('.dialogFooter')
        .get('button[class=\'btnDialogConfirm\'')
        .should('exist')
        .click();
};

/**
 * 檢查滲透後，是否為進入儀錶板
 */
const inspectAttackNotTransferPath = (): void => {
    let pathname: string = '';

    cy.location().then((location) => {
        pathname = location.pathname;
    });

    cy.wrap(pathname).should('not.eq', '/studentList');
};

/**
 * 滲透測試強行奪取權限
 * @param {string} account 帳號
 * @param {string} password 密碼
 */
const penetrationTestAccountSecret = (account: string, password: string): void => {
    cy.get('input[class=\'account\']')
        .should('exist')
        .clear()
        .type(account);

    cy.get('input[class=\'password\']')
        .should('exist')
        .clear()
        .type(password);

    cy.get('button[class=\'submit\']')
        .should('exist')
        .click();
};

describe('test login page', (): void => {
    it('test login fail', (): void => {
        cy.visit('/login/');

        cy.get('h1').should('exist').contains('等速肌力訓練後台管理');
        cy.get('h3').should('exist').contains('銀髮專用智慧型運動器材');

        cy.get('input[class=\'account\']').should('exist').type('abc123');
        cy.get('input[class=\'password\']').should('exist').type('123456');
        cy.get('button[class=\'submit\']').should('exist').click();

        cy.get('.dialog.visibleInfoDialog')
            .get('h1[class=\'title\']')
            .should('exist')
            .contains('錯誤');

        cy.get('.dialog.visibleInfoDialog')
            .get('.dialogTitle')
            .get('h1[class=\'title\']')
            .should('exist')
            .contains('錯誤');

        cy.get('.dialog.visibleInfoDialog')
            .get('.dialogBody')
            .get('.content')
            .get('p')
            .should('exist')
            .contains('無此使用者');

        cy.get('.dialog.visibleInfoDialog')
            .get('.dialogFooter')
            .get('button[class=\'btnDialogConfirm\'')
            .should('exist')
            .click();
    });

    it('test login success', (): void => {
        cy.visit('/login/');

        cy.get('input[class=\'account\']').should('exist').type('jason');
        cy.get('input[class=\'password\']').should('exist').type('jason');
        cy.get('button[class=\'submit\']').should('exist').click();

        cy.url().should('include', '/studentList/');
    });

    it('test login not sql injection', (): void => {
        cy.visit('/login/');

        penetrationTestAccountSecret('jason \' or 1=1 -- ', 'sdfadskj');
        inspectDialogErrorMessage();
        inspectAttackNotTransferPath();

        cy.wait(2000);

        penetrationTestAccountSecret('\' or 1=1 -- ', 'sdfadskj');
        inspectDialogErrorMessage();
        inspectAttackNotTransferPath();

        cy.wait(2000);

        penetrationTestAccountSecret('jason \' (or) (1=1) -- ', 'sdfadskj');
        inspectDialogErrorMessage();
        inspectAttackNotTransferPath();

        cy.wait(2000);

        penetrationTestAccountSecret('jason \' or \'1\'=\'1\' -- ', 'sdfadskj');
        inspectDialogErrorMessage();
        inspectAttackNotTransferPath();
    });
});