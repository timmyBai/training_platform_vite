import { homeProccess } from '../utils/login';

describe('test home page', () => {
    it('test home page element', () => {
        homeProccess();

        cy.url().should('include', '/login/');
    });
});