import { homeProccess, loginProccess } from '../utils/login';


const randomAccountSecret = (): string => {
    let result: string = '';

    const randomAccountSecretKey: string = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (let i = 0; i < 9; i++) {
        const randomAccountSecretIndex = Math.floor(Math.random() * randomAccountSecretKey.length);

        result = result + randomAccountSecretKey[randomAccountSecretIndex];
    }

    // 如果沒有任何數字手動塞入數字
    const isPassAccountSecret = /[0-9]+/.test(result);
    
    if (!isPassAccountSecret) {
        result = result + randomAccountSecretKey[1];
    }

    return result;
};

const randomName = (): string => {
    const randomNameKey: string = '白王方劉陳林周洪官藍吳呂蔡黃';

    return `${randomNameKey[Math.floor(Math.random() * randomName.length)]}測試`;
};

const randomPhone = (): string => {
    let result: string = '09';

    for (let i = 0; i < 8; i++) {
        result = result + Math.floor(Math.random() * 8).toString();
    }

    return result;
};

const randomNext = (start: number, end: number = undefined): number => {
    if (end === undefined) {
        return Math.floor(Math.random() * start);
    }

    return Math.floor(Math.random() * (end - start + 1)) + start;
};

/**
 * 新增學員
 */
const addStudentPage = () => {
    /**
     * 輸入學員基本資料
     */
    const account: string = randomAccountSecret(); // 亂數帳號
    const name: string = randomName(); // 亂數名稱
    const email: string = `${account}@gmail.com`; // 信箱
    const address: string = '台中市'; // 地址
    const password: string = account; // 亂數帳號

    const currentYear: number = new Date().getFullYear() - 21;
    const randomYear: number = randomNext(1911, currentYear);
    const randomMonth: string = randomNext(1, 12).toString().padStart(2, '0');
    let randomDay: string = randomNext(1, 30).toString().padStart(2, '0');

    if ((randomYear % 4 === 0 && randomYear % 100 !== 0) || (randomYear % 400 === 0)) {
        if (parseInt(randomMonth) === 2 && parseInt(randomDay) === 28) {
            randomDay = '29';
        }
    }

    // 測試 "帳號" 是否正確跳出錯誤訊息
    cy.get('input[name=\'account\']').should('exist').type(account).clear();
    cy.get('input[name=\'account\'] + p').should('exist').contains('必須輸入帳號');

    cy.get('input[name=\'account\']').should('exist').type(account.substring(2));
    cy.get('input[name=\'account\'] + p').should('exist').contains('帳號最短長度大於 8 個字，最長 50 個字');

    cy.get('input[name=\'account\']').should('exist').clear().type(account);
    cy.get('input[name=\'account\'] + p').should('not.exist');

    // 測試 "姓名" 是否跳出正確錯誤訊息
    cy.get('input[name=\'name\']').should('exist').type(`${name}`).clear();
    cy.get('input[name=\'name\'] + p').should('exist').contains('必須輸入姓名');

    cy.get('input[name=\'name\']').should('exist').type(name);
    cy.get('input[name=\'name\'] + p').should('not.exist');

    // 測試 '性別' 是否跳出正確錯誤訊息
    cy.get('input[name=\'gender\']').should('exist').eq(Math.floor(Math.random() * 2)).check();
    cy.get('input[name=\'gender\'] + p').should('not.exist');

    // 測試 '生日' 是否跳出正確錯誤訊息
    cy.get('input[name=\'birthday\']').should('exist').type(`${randomYear}-${randomMonth}-${randomDay}`).clear();
    cy.get('input[name=\'birthday\'] + p').should('exist').contains('必須輸入生日');

    cy.get('input[name=\'birthday\']').should('exist').type(`${randomYear}-${randomMonth}-${randomDay}`);
    cy.get('input[name=\'birthday\'] + p').should('not.exist');

    // 測試 '身高' 是否跳出正確錯誤訊息
    cy.get('input[name=\'height\']').should('exist').type(`${randomNext(300.5)}`).clear();
    cy.get('input[name=\'height\'] + p').should('exist').contains('必須輸入身高');

    cy.get('input[name=\'height\']').should('exist').type(`${randomNext(300.5)}abc`);
    cy.get('input[name=\'height\'] + p').should('exist').contains('請輸入數字');

    cy.get('input[name=\'height\']').should('exist').clear().type(`${randomNext(300.5)}`);
    cy.get('input[name=\'height\'] + p').should('not.exist');

    // 測試 '體重' 是否跳出正確錯誤訊息
    cy.get('input[name=\'weight\']').should('exist').type(`${randomNext(300.5)}`).clear();
    cy.get('input[name=\'weight\'] + p').should('exist').contains('必須輸入體重');

    cy.get('input[name=\'weight\']').should('exist').type(`${randomNext(300.5)}abc`);
    cy.get('input[name=\'weight\'] + p').should('exist').contains('請輸入數字');

    cy.get('input[name=\'weight\']').should('exist').clear().type(`${randomNext(300.5)}`);
    cy.get('input[name=\'weight\'] + p').should('not.exist');

    // 測試 '電話' 是否跳出正確錯誤訊息
    cy.get('input[name=\'phone\']').should('exist').type(randomPhone()).clear();
    cy.get('input[name=\'phone\'] + p').should('exist').contains('必須輸入電話');

    cy.get('input[name=\'phone\']').should('exist').type(`${randomPhone()}abc`);
    cy.get('input[name=\'phone\'] + p').should('exist').contains('請輸入數字');

    cy.get('input[name=\'phone\']').should('exist').clear().type(`${randomPhone()}.12`);
    cy.get('input[name=\'phone\'] + p').should('exist').contains('請輸入數字');

    cy.get('input[name=\'phone\']').should('exist').clear().type(`${randomPhone()}`);
    cy.get('input[name=\'phone\'] + p').should('not.exist');

    // 測試 '信箱' 是否跳出正確錯誤訊息
    cy.get('input[name=\'email\']').should('exist').type(email).clear();
    cy.get('input[name=\'email\'] + p').should('not.exist');

    cy.get('input[name=\'email\']').should('exist').type(email.replace('@gmail', ''));
    cy.get('input[name=\'email\'] + p').should('exist').contains('e-mail 格式錯誤');

    cy.get('input[name=\'email\']').should('exist').clear().type(email);
    cy.get('input[name=\'email\'] + p').should('not.exist');

    // 測試 '地址' 是否跳出正確錯誤訊息
    cy.get('input[name=\'address\']').should('exist').type(address).clear();
    cy.get('input[name=\'address\'] + p').should('not.exist');

    cy.get('input[name=\'address\']').should('exist').type(address.substring(2));
    cy.get('input[name=\'address\'] + p').should('exist').contains('地址最短長度 3 個字，最長 50 個字');

    cy.get('input[name=\'address\']').should('exist').clear().type(address);
    cy.get('input[name=\'address\'] + p').should('not.exist');

    // 測試 '密碼' 是否跳出正確錯誤訊息
    cy.get('input[name=\'password\']').should('exist').type(password).clear();
    cy.get('input[name=\'password\'] + p').should('exist').contains('必輸輸入密碼');

    cy.get('input[name=\'password\']').should('exist').type(password.substring(2));
    cy.get('input[name=\'password\'] + p').should('exist').contains('密碼最短長度大於 8 個字，最長 50 個字');

    cy.get('input[name=\'password\']').should('exist').clear().type(password);
    cy.get('input[name=\'password\'] + p').should('not.exist');

    // 測試確認密碼
    cy.get('input[name=\'confirmPassword\']').should('exist').type(password).clear();
    cy.get('input[name=\'confirmPassword\'] + p').should('exist').contains('必輸輸入確認密碼');

    cy.get('input[name=\'confirmPassword\']').should('exist').type(password.substring(2));
    cy.get('input[name=\'confirmPassword\'] + p').should('exist').contains('確認密碼最短長度大於 8 個字，最長 50 個字');

    cy.get('input[name=\'confirmPassword\']').should('exist').type(`${password}abc`);
    cy.get('input[name=\'confirmPassword\'] + p').should('not.exist');

    // 按下確定按鈕，檢查密碼與確認密碼是否不相同
    cy.get('button[class=\'confirm\']').should('exist').click();

    cy.get('input[name=\'account\'] + p').should('not.exist' || '');

    // cy.get('input[name=\'account\'] + p').then(async($element) => {
    //     if ($element.text().includes('帳號必須要有英文大寫或小寫及數字組合')) {
    //         addStudentPage();
    //     }
    // });

    // cy.get('input[name=\'password\'] + p').then(async($element) => {
    //     if ($element.text().includes('密碼必須要有英文大寫或小寫及數字組合')) {
    //         addStudentPage();
    //     }
    // });

    // cy.get('input[name=\'confirmPassword\'] + p').then(async($element) => {
    //     if ($element.text().includes('密碼必須要有英文大寫或小寫及數字組合')) {
    //         addStudentPage();
    //     }
    // });

    cy.get('input[name=\'password\'] + p').should('exist').contains('密碼與確認密碼不相同');
    cy.get('input[name=\'confirmPassword\'] + p').should('exist').contains('密碼與確認密碼不相同');

    // 再次輸入密碼與確認密碼
    cy.get('input[name=\'password\']').should('exist').clear().type(password);
    cy.get('input[name=\'confirmPassword\']').should('exist').clear().type(password);

    // 按下確定按鈕，是否成功進入第一部份問卷頁面
    cy.get('button[class=\'confirm\']').should('exist').click();

    cy.get('h2.title').should('exist').contains('身體活動準備問卷 - 第一部分');
};

describe('test student page', () => {
    it('test student list view', (): void => {
        homeProccess();
        loginProccess();

        // 檢查是否有新增學員按鈕
        cy.get('img.add_student').should('exist');

        // 檢查是否有搜尋學員按鈕
        cy.get('img.search_student').should('exist');

        // 檢查是否有學員清單
        cy.get('table.studentTable').should('exist');

        // 檢查是否資料學員資料是否有大於 0
        const listIsLargerThan = cy.get('tr.item')
            .should('exist')
            .its('length')
            .should('be.gte', 0);

        if (listIsLargerThan) {
            cy.get('tr.item')
                .should('exist')
                .first()
                .click();

            cy.get('.confirm')
                .should('exist')
                .first()
                .click();

            cy.url()
                .should('include', '/bodyCompositionAndPhysicalFitness/');

            cy.wait(2000);

            cy.get('.menuItem').first().click();
        }
    });

    it('test student list search', (): void => {
        homeProccess();
        loginProccess();

        // 檢查是否有新增學員按鈕
        cy.get('img.add_student').should('exist');

        // 檢查是否有搜尋學員按鈕
        cy.get('img.search_student').should('exist');

        // 檢查是否有學員清單
        cy.get('table.studentTable').should('exist');

        const studentName: string[] = [];

        cy.get('tr.item td:first-child').should('exist').each((item) => {
            cy.wrap(item).invoke('text').then((text) => {
                studentName.push(text);
            });
        }).then(() => {
            if (studentName.length > 1) {
                // 點擊搜尋按鈕
                cy.get('.search_student')
                    .should('exist')
                    .click();

                // 搜尋框輸入姓名不加空白
                cy.get('input[class=\'dialogInput\']')
                    .should('exist')
                    .type(studentName[0].substring(0, 1));

                // 點擊搜尋彈窗按鈕
                cy.get('button[class=\'dialogBtnBtnYes\']')
                    .should('exist')
                    .click();

                // 檢查搜尋後是否大於一筆
                cy.get('tr.item')
                    .should('exist')
                    .its('length')
                    .should('be.gte', 0);

                cy.wait(2000);

                // 點擊搜尋彈窗按鈕
                cy.get('.search_student')
                    .should('exist')
                    .click();

                // 搜尋框輸入姓名加空白
                cy.get('input[class=\'dialogInput\']')
                    .should('exist')
                    .type(`${studentName[1].substring(0, 1)} `);

                // 點擊搜尋彈窗按鈕
                cy.get('button[class=\'dialogBtnBtnYes\']')
                    .should('exist')
                    .click();

                // 檢查搜尋後是否大於一筆
                cy.get('tr.item')
                    .should('exist')
                    .its('length')
                    .should('be.gte', 0);

                cy.wait(2000);

                // 點擊搜尋彈窗按鈕
                cy.get('.search_student')
                    .should('exist')
                    .click();

                // 測試什麼都未輸入按下確定按鈕，還原全部清單
                cy.get('button[class=\'dialogBtnBtnYes\']')
                    .should('exist')
                    .click();

                // 檢查是否清單大於一筆
                cy.get('tr.item')
                    .should('exist')
                    .its('length')
                    .should('be.gte', 1);
            }
            else {
                cy.log('stop search student test');
            }
        });
    });

    it('test register student', (): void => {
        homeProccess();
        loginProccess();

        // 檢查是否有搜尋學員按鈕
        cy.get('img.search_student').should('exist');

        // 檢查是否有學員清單
        cy.get('table.studentTable').should('exist');

        // 檢查是否有新增學員按鈕
        cy.get('img.add_student').should('exist').click();

        addStudentPage();

        /****************************************************************************************************
         *                                            身體活動準備問卷                                        *
         *************************************************************************************************** */

        //     for(let i = 0; i < 7; i++) {
        //         cy.get(`input[name='questionnaire${i + 1}']`).eq(Math.floor(Math.random() * 2)).check();

        //         cy.wait(1000);
        //     }

        //     cy.get('button[class=\'next\']').should('exist').click();
    });
});