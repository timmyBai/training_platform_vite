/**
 * 體適能-心肺適能 顏色判斷
 */
export const physicalFitnessCardiorespiratoryFitnessColor = (comment: string | undefined): string => {
    if (comment === '-') {
        return '';
    }
    else if (comment === '異常') {
        return 'red';
    }
    else if (comment === '稍差') {
        return 'orange';
    }
    else if (comment === '標準') {
        return 'green';
    }
    else if (comment === '尚好') {
        return 'green';
    }
    
    return 'green';
};

/**
 * 體適能-肌力與肌耐力 顏色判斷
 */
export const physicalFitnessMuscleStrengthAndMuscleEnduranceColor = (comment: string | undefined): string => {
    if (comment === '-') {
        return '';
    }
    else if (comment === '異常') {
        return 'red';
    }
    else if (comment === '稍差') {
        return 'orange';
    }
    else if (comment === '標準') {
        return 'green';
    }
    else if (comment === '尚好') {
        return 'green';
    }
    
    return 'green';
};

/**
 * 體適能-柔軟度 顏色判斷
 */
export const physicalFitnessSoftnessColor = (comment: string | undefined): string => {
    if (comment === '-') {
        return '';
    }
    else if (comment === '異常') {
        return 'red';
    }
    else if (comment === '稍差') {
        return 'orange';
    }
    else if (comment === '標準') {
        return 'green';
    }
    else if (comment === '尚好') {
        return 'green';
    }
    
    return 'green';
};

/**
 * 體適能-平衡 顏色判斷
 */
export const physicalFitnessBalanceColor = (comment: string | undefined, age: number) => {
    if (age >= 21 && age <= 65) {
        if (comment === '-') {
            return '-';
        }
        else if (comment === '稍差') {
            return 'red';
        }
        else if (comment === '普通') {
            return 'green';
        }
        else {
            return 'green';
        }
    }
    else {
        if (comment === '-') {
            return '';
        }
        else if (comment === '異常') {
            return 'red';
        }
        else if (comment === '稍差') {
            return 'orange';
        }
        else if (comment === '標準') {
            return 'green';
        }
        else if (comment === '尚好') {
            return 'green';
        }
        else {
            return 'green';
        }
    }
};