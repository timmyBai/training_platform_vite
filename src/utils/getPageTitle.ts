import defaultSettings from '@/settings';

const title: string = defaultSettings.title || '等速肌力訓練後台管理';

export const getPageTitle = (pageTitle: string): string => {
    if (pageTitle) {
        return `${pageTitle} - ${title}`;
    }

    return title;
};
