/**
 * bmi 顏色判斷
 */
export const bmiColor = (comment: string | undefined): string => {
    if (comment === '-') {
        return '';
    }
    else if (comment === '過輕') {
        return 'red';
    }
    else if (comment === '稍輕') {
        return 'orange';
    }
    else if (comment === '適當') {
        return 'green';
    }
    else if (comment === '稍重') {
        return 'orange';
    }

    return 'red';
};

/**
 * 體脂肪 顏色判斷
 */
export const bodyFatColor = (comment: string | undefined): string => {
    if (comment === '-') {
        return '';
    }
    else if (comment === '非常好'){
        return 'green';
    }
    else if (comment === '很好') {
        return 'green';
    }
    else if (comment === '普通') {
        return 'green';
    }
    else if (comment === '過重') {
        return 'orange';
    }

    return 'red';
};

/**
 *  內臟脂肪 顏色判斷
 */
export const visceralFatColor = (comment: string | undefined): string => {
    if (comment === '-') {
        return '';
    }
    else if (comment === '標準') {
        return 'green';
    }
    else if (comment === '過量') {
        return 'orange';
    }

    return 'red';
};

/**
 * 骨骼肌率 顏色判斷
 */
export const skeletalMuscleRateColor = (comment: string | undefined): string => {
    if (comment === '-') {
        return '';
    }
    else if (comment === '低') {
        return 'red';
    }
    else if (comment === '標準') {
        return 'green';
    }
    else if (comment === '偏高') {
        return 'green';
    }

    return 'green';
};