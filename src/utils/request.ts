import axios, { AxiosError } from 'axios';
import NProgress from 'nprogress';

// store
import store from '@/store/index';

// utils
import { getToken } from './auth';

const service = axios.create({
    baseURL: import.meta.env.VITE_APP_BASE_URL,
    timeout: 5000
});

//使用 axios 攔截器，攔截 request 回傳所有結果
service.interceptors.request.use(
    config => {
        store.dispatch({ type: 'loading/changeLoading', payload: true });
        NProgress.start();

        if (store.getState().user.token) {
            if (!config?.headers) {
                throw new Error('config header not defaied');
            }

            config.headers['Authorization'] = `Bearer ${getToken()}`;
        }

        return config;
    },
    error => {
        console.log(error);
        store.dispatch({ type: 'loading/changeLoading', payload: false });
        NProgress.done();

        return Promise.reject(error.response);
    }
);

//使用 axios 攔截器，攔截 response 回傳所有結果
service.interceptors.response.use(
    response => {
        store.dispatch({ type: 'loading/changeLoading', payload: false });
        NProgress.done();

        return Promise.resolve(response);
    },
    error => {
        store.dispatch({ type: 'loading/changeLoading', payload: false });
        NProgress.done();

        return Promise.reject(error);
    }
);

export const axiosErrorCode = <ErrorCode>(e: any) => {
    const error: AxiosError<{ message: ErrorCode }> = e;
    const errorCode = error?.response?.data?.message || error?.response?.status || error.message.match(/[0-9]/g)?.join('');
    
    return errorCode;
};

export default service;
