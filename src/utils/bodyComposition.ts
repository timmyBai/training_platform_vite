/** 
 * 身體組成 bmi 評語 轉換 分數
 */
export const bodyCompositionBmiCommentConvertFraction = (comment: string): number => {
    if (comment === '-') {
        return 0;
    }
    else if (comment === '過輕') {
        return 20;
    }
    else if (comment === '稍輕') {
        return 40;
    }
    else if (comment === '適當') {
        return 60;
    }
    else if (comment === '稍重') {
        return 80;
    }

    return 100;
};

/**
 * 身體組成 體脂肪 評語 轉換 分數
 */
export const bodyCompositionBodyFatCommentConvertFraction = (comment: string): number => {
    if (comment === '-') {
        return 0;
    }
    else if (comment === '非常好') {
        return 20;
    }
    else if (comment === '很好') {
        return 40;
    }
    else if (comment === '普通') {
        return 60;
    }
    else if (comment === '過重') {
        return 80;
    }

    return 100;
};

/**
 * 身體組成 內臟脂肪 評語 轉換 分數
 */
export const bodyCompositionVisceralFatCommentConvertFraction = (comment: string): number => {
    if (comment === '-') {
        return 0;
    }
    else if (comment === '標準') {
        return 33;
    }
    else if (comment === '過量') {
        return 66;
    }

    return 100;
};

/** 
 * 身體組成 骨骼機率 評語 轉換 分數
 */
export const bodyCompositioSkeletalMuscleRateCommentConvertFraction = (comment: string): number => {
    if (comment === '-') {
        return 0;
    }
    else if (comment === '低') {
        return 25;
    }
    else if (comment === '標準') {
        return 50;
    }
    else if (comment === '偏高') {
        return 75;
    }

    return 100;
};