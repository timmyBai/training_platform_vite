/**
 * 運動處方-心肺適能訓練項目
 */
export const cardiorespiratoryFitnessSportsOptions = [
    {
        value: '有氧操',
        text: '有氧操'
    },
    {
        value: '自行車',
        text: '自行車'
    },
    {
        value: '慢跑',
        text: '慢跑'
    },
    {
        value: '游泳',
        text: '游泳'
    },
    {
        value: '跳繩',
        text: '跳繩'
    },
    {
        value: '飛輪',
        text: '飛輪'
    },
    {
        value: '原地登階',
        text: '原地登階'
    },
    {
        value: '健走',
        text: '健走'
    }
];

/**
 * 運動處方-心肺適能訓練強度
 */
export const cardiorespiratoryFitnessExerciseIntensityOptions = [
    {
        value: '1',
        text: '低強度運動'
    },
    {
        value: '2',
        text: '中強度運動'
    },
    {
        value: '3',
        text: '高強度運動'
    }
];

/**
 * 運動處方-心肺適能訓練頻率(天/週)
 */
export const cardiorespiratoryFitnessExerciseFrequencyOptions = [
    {
        value: '1',
        text: '每週 1 天'
    },
    {
        value: '2',
        text: '每週 2 天'
    },
    {
        value: '3',
        text: '每週 3 天'
    },
    {
        value: '4',
        text: '每週 4 天'
    },
    {
        value: '5',
        text: '每週 5 天'
    },
    {
        value: '6',
        text: '每週 6 天'
    },
    {
        value: '7',
        text: '每週 7 天'
    }
];

/**
 * 運動處方-心肺適能訓練頻率(分鐘/次)
 */
export const cardiorespiratoryFitnessTrainingUnitMinuteOptions = [
    {
        value: '5',
        text: '5 分鐘/次'
    },
    {
        value: '10',
        text: '10 分鐘/次'
    },
    {
        value: '15',
        text: '15 分鐘/次'
    },
    {
        value: '20',
        text: '20 分鐘/次'
    },
    {
        value: '25',
        text: '25 分鐘/次'
    },
    {
        value: '30',
        text: '30 分鐘/次'
    },
    {
        value: '35',
        text: '35 分鐘/次'
    },
    {
        value: '40',
        text: '40 分鐘/次'
    },
    {
        value: '45',
        text: '45 分鐘/次'
    },
    {
        value: '50',
        text: '50 分鐘/次'
    }
];

/**
 * 運動處方-肌力與肌耐力訓練強度
 */
export const muscleStrengthAndMuscleEnduranceExerciseIntensityOptions = [
    {
        value: '1',
        text: '低強度運動'
    },
    {
        value: '2',
        text: '中強度運動'
    },
    {
        value: '3',
        text: '高強度運動'
    }
];

/**
 * 運動處方-肌力與肌耐力訓練頻率(單位: 天/週)
 */
export const muscleStrengthAndMuscleEnduranceExerciseFrequencyOptions = [
    {
        value: '1',
        text: '每週 1 天'
    },
    {
        value: '2',
        text: '每週 2 天'
    },
    {
        value: '3',
        text: '每週 3 天'
    },
    {
        value: '4',
        text: '每週 4 天'
    },
    {
        value: '5',
        text: '每週 5 天'
    },
    {
        value: '6',
        text: '每週 6 天'
    },
    {
        value: '7',
        text: '每週 7 天'
    }
];

/**
 * 運動處方-肌力與肌耐力訓練頻率(單位: 台/組)
 */
export const muscleStrengthAndMuscleEnduranceTrainingUnitGroupOptions = [
    {
        value: '2',
        text: '每天 2 組'
    },
    {
        value: '4',
        text: '每週 4 組'
    },
    {
        value: '6',
        text: '每週 6 組'
    },
    {
        value: '8',
        text: '每週 8 組'
    },
    {
        value: '10',
        text: '每週 10 組'
    }
];

/**
 * 運動處方-肌力與肌耐力訓練頻率(單位: 次/組)
 */
export const muscleStrengthAndMuscleEnduranceTrainingUnitNumberOptions = [
    {
        value: '4',
        text: '每天 4 次/組'
    },
    {
        value: '6',
        text: '每天 6 次/組'
    },
    {
        value: '8',
        text: '每天 8 次/組'
    },
    {
        value: '10',
        text: '每天 10 組'
    },
    {
        value: '12',
        text: '每天 12 組'
    },
    {
        value: '14',
        text: '每天 14 組'
    },
    {
        value: '16',
        text: '每天 16 組'
    },
    {
        value: '18',
        text: '每天 18 組'
    }
];

/**
 * 運動處方-柔軟度訓練項目
 */
export const softnessSportsOptions = [
    {
        value: '瑜珈',
        text: '瑜珈'
    },
    {
        value: '伸展操',
        text: '伸展操'
    }
];

/**
 * 運動處方-柔軟度訓練強度
 */
export const softnessExerciseIntensityOptions = [
    {
        value: '1',
        text: '低強度運動'
    },
    {
        value: '2',
        text: '中強度運動'
    },
    {
        value: '3',
        text: '高強度運動'
    }
];

/**
 * 運動處方-柔軟度訓練項目(天/週)
 */
export const softnessExerciseFrequencyOptions = [
    {
        value: '1',
        text: '每週 1 天'
    },
    {
        value: '2',
        text: '每週 2 天'
    },
    {
        value: '3',
        text: '每週 3 天'
    },
    {
        value: '4',
        text: '每週 4 天'
    },
    {
        value: '5',
        text: '每週 5 天'
    },
    {
        value: '6',
        text: '每週 6 天'
    },
    {
        value: '7',
        text: '每週 7 天'
    }
];

/**
 * 運動處方-柔軟度訓練單位(分鐘/次)
 */
export const softnessTrainingUnitMinuteOptions = [
    {
        value: '5',
        text: '5 分鐘'
    },
    {
        value: '10',
        text: '10 分鐘'
    },
    {
        value: '15',
        text: '15 分鐘'
    },
    {
        value: '20',
        text: '20 分鐘'
    },
    {
        value: '25',
        text: '25 分鐘'
    },
    {
        value: '30',
        text: '30 分鐘'
    }
];

/**
 * 運動處方-平衡運動項目
 */
export const balanceSportsOptions = [
    {
        value: '靜態平衡訓練',
        text: '靜態平衡訓練'
    },
    {
        value: '動態平衡訓練',
        text: '動態平衡訓練'
    },
    {
        value: '太極拳',
        text: '太極拳'
    }
];

/**
 * 運動處方-平衡運動強度
 */
export const balanceExerciseIntensityOptions = [
    {
        value: '1',
        text: '低強度運動'
    },
    {
        value: '2',
        text: '中強度運動'
    },
    {
        value: '3',
        text: '高強度運動'
    }
];

/**
 * 運動處方-平衡訓練頻率(單位: 天/周)
 */
export const balanceExerciseFrequencyOptions = [
    {
        value: '1',
        text: '每週 1 天'
    },
    {
        value: '2',
        text: '每週 2 天'
    },
    {
        value: '3',
        text: '每週 3 天'
    },
    {
        value: '4',
        text: '每週 4 天'
    },
    {
        value: '5',
        text: '每週 5 天'
    },
    {
        value: '6',
        text: '每週 6 天'
    },
    {
        value: '7',
        text: '每週 7 天'
    }
];

/**
 * 運動處方-平衡訓練單位(分鐘/次)
 */
export const balanceTrainingUnitMinuteOptions = [
    {
        value: '5',
        text: '5 分鐘'
    },
    {
        value: '10',
        text: '10 分鐘'
    },
    {
        value: '15',
        text: '15 分鐘'
    },
    {
        value: '20',
        text: '20 分鐘'
    },
    {
        value: '25',
        text: '25 分鐘'
    },
    {
        value: '30',
        text: '30 分鐘'
    }
];