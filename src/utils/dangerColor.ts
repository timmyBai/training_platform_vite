/**
 * 危險族群顏色判斷
 * @param {string} dangerColor 
 */
export const dangerColor = (dangerStr: string): string => {
    if (dangerStr === '高危險群') {
        return 'red';
    }
    else if (dangerStr === '中危險群') {
        return 'yellow';
    }
    else {
        return 'green';
    }
};