export const isStudentGreaterThan65 = (age: number) => {
    if (age >= 21 && age <= 65) {
        return true;
    }
    
    return false;
};