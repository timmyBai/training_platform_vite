import colors from '@/styles/colors.module.sass';
import machinaryCodeOne from '@/assets/images/machinaryCode/machinaryCodeOne.png';
import machinaryCodeTwo from '@/assets/images/machinaryCode/machinaryCodeTwo.png';
import machinaryCodeThree from '@/assets/images/machinaryCode/machinaryCodeThree.png';
import machinaryCodeFour from '@/assets/images/machinaryCode/machinaryCodeFour.png';
import machinaryCodeFive from '@/assets/images/machinaryCode/machinaryCodeFive.png';
import machinaryCodeSix from '@/assets/images/machinaryCode/machinaryCodeSix.png';
import machinaryCodeSeven from '@/assets/images/machinaryCode/machinaryCodeSeven.png';
import machinaryCodeEight from '@/assets/images/machinaryCode/machinaryCodeEight.png';
import machinaryCodeNine from '@/assets/images/machinaryCode/machinaryCodeNine.png';
import machinaryCodeTen from '@/assets/images/machinaryCode/machinaryCodeTen.png';
import machinaryCodeOneMachinaryMuscleDistribution from '@/assets/images/machinaryMuscleDistribution/machinaryCodeOneMachinaryMuscleDistribution.svg';
import machinaryCodeTwoMachinaryMuscleDistribution from '@/assets/images/machinaryMuscleDistribution/machinaryCodeTwoMachinaryMuscleDistribution.svg';
import machinaryCodeThreeMachinaryMuscleDistribution from '@/assets/images/machinaryMuscleDistribution/machinaryCodeThreeMachinaryMuscleDistribution.svg';
import machinaryCodeFourMachinaryMuscleDistribution from '@/assets/images/machinaryMuscleDistribution/machinaryCodeFourMachinaryMuscleDistribution.svg';
import machinaryCodeFiveMachinaryMuscleDistribution from '@/assets/images/machinaryMuscleDistribution/machinaryCodeFiveMachinaryMuscleDistribution.svg';
import machinaryCodeSixMachinaryMuscleDistribution from '@/assets/images/machinaryMuscleDistribution/machinaryCodeSixMachinaryMuscleDistribution.svg';
import machinaryCodeSevenMachinaryMuscleDistribution from '@/assets/images/machinaryMuscleDistribution/machinaryCodeSevenMachinaryMuscleDistribution.svg';
import machinaryCodeEightMachinaryMuscleDistribution from '@/assets/images/machinaryMuscleDistribution/machinaryCodeEightMachinaryMuscleDistribution.svg';
import machinaryCodeNineMachinaryMuscleDistribution from '@/assets/images/machinaryMuscleDistribution/machinaryCodeNineMachinaryMuscleDistribution.svg';
import machinaryCodeTenMachinaryMuscleDistribution from '@/assets/images/machinaryMuscleDistribution/machinaryCodeTenMachinaryMuscleDistribution.svg';
import consciousEffortLevel_1_2 from '@/assets/images/consciousEffort/consciousEffortLevel_1_2.png';
import consciousEffortLevel_3_4 from '@/assets/images/consciousEffort/consciousEffortLevel_3_4.png';
import consciousEffortLevel_5_6 from '@/assets/images/consciousEffort/consciousEffortLevel_5_6.png';
import consciousEffortLevel_7_8 from '@/assets/images/consciousEffort/consciousEffortLevel_7_8.png';
import consciousEffortLevel_9_10 from '@/assets/images/consciousEffort/consciousEffortLevel_9_10.png';

/**
 * 機器功率外框樣式
 */
export const totalPercentageStyle = (totalPower: number) => {
    const angle = 0;
    const powerPercentage = totalPower / 20000 * 100;
    
    if (powerPercentage < 25) {
        return `linear-gradient(${angle}deg, ${colors.color_green}, ${colors.color_yellow})`;
    }
    else if (powerPercentage > 25 && powerPercentage < 50) {
        return `linear-gradient(${angle}deg, ${colors.color_yellow}, ${colors.color_orange})`;
    }
    else if (powerPercentage > 50 && powerPercentage < 75) {
        return `linear-gradient(${angle}deg, ${colors.color_orange}, ${colors.color_red})`;
    }
    else {
        return `linear-gradient(${angle}deg, ${colors.color_red}, ${colors.color_purple})`;
    }
};

// 功率內原顏色
export const powerInnerCircleStyle = (power: number | undefined) => {
    if (power !== undefined) {
        if (power < 250) {
            return '#607e33';
        }
        else if (power > 250 && power < 500) {
            return '#8d4b2e';
        }
        else if (power > 500 && power < 750) {
            return '#8b282c';
        }
        else {
            return '#66225d';
        }
    }
};

// 功率外原顏色
export const powerOuterCircleStyle = (power: number | undefined) => {
    const angle = 0;

    if (power !== undefined) {
        if (power < 250) {
            return `linear-gradient(${angle}deg, ${colors.color_green}, ${colors.color_yellow})`;
        }
        else if (power > 250 && power < 500) {
            return `linear-gradient(${angle}deg, ${colors.color_yellow}, ${colors.color_orange})`;
        }
        else if (power > 500 && power < 750) {
            return `linear-gradient(${angle}deg, ${colors.color_orange}, ${colors.color_red})`;
        }
        else {
            return `linear-gradient(${angle}deg, ${colors.color_red}, ${colors.color_purple})`;
        }
    }
};


/**
 * 肌肉分佈圖
 */
export const targetMuscleDistribution = (machinaryCode: number) => {
    switch (machinaryCode) {
        case 1:
            return machinaryCodeOneMachinaryMuscleDistribution;
        case 2:
            return machinaryCodeTwoMachinaryMuscleDistribution;
        case 3:
            return machinaryCodeThreeMachinaryMuscleDistribution;
        case 4:
            return machinaryCodeFourMachinaryMuscleDistribution;
        case 5:
            return machinaryCodeFiveMachinaryMuscleDistribution;
        case 6:
            return machinaryCodeSixMachinaryMuscleDistribution;
        case 7:
            return machinaryCodeSevenMachinaryMuscleDistribution;
        case 8:
            return machinaryCodeEightMachinaryMuscleDistribution;
        case 9:
            return machinaryCodeNineMachinaryMuscleDistribution;
        case 10:
            return machinaryCodeTenMachinaryMuscleDistribution;
        default:
            return;
    }
};

// 取得機器圖片
export const targetMachinaryPicture = (machinaryCode: number) => {
    switch (machinaryCode) {
        case 1:
            return machinaryCodeOne;
        case 2:
            return machinaryCodeTwo;
        case 3:
            return machinaryCodeThree;
        case 4:
            return machinaryCodeFour;
        case 5:
            return machinaryCodeFive;
        case 6:
            return machinaryCodeSix;
        case 7:
            return machinaryCodeSeven;
        case 8:
            return machinaryCodeEight;
        case 9:
            return machinaryCodeNine;
        case 10:
            return machinaryCodeTen;
        default:
            return;
    }
};

export const targetConsciousEffort = (consciousEffortLevel: number) => {
    switch(consciousEffortLevel) {
        case 1:
        case 2:
            return consciousEffortLevel_1_2;
        case 3:
        case 4:
            return consciousEffortLevel_3_4;
        case 5:
        case 6:
            return consciousEffortLevel_5_6;
        case 7:
        case 8:
            return consciousEffortLevel_7_8;
        case 9:
        case 10:
            return consciousEffortLevel_9_10;
        default:
            return;
    }
};

export const getMachinaryName = (machinaryCode: number) => {
    switch(machinaryCode) {
        case 1:
            return '屈腿伸腿訓練機';
        case 2:
            return '推胸拉背訓練機 ';
        case 3:
            return '髖關節外展內收訓練機';
        case 4:
            return '蹬腿訓練機';
        case 5:
            return '上斜推拉訓練機';
        case 6:
            return '蝴蝶訓練機';
        case 7:
            return '坐式上臂訓練機';
        case 8:
            return '坐式轉體訓練機';
        case 9:
            return '深蹲訓練機';
        case 10:
            return '坐式腹背訓練機';
        default:
            return;
    }
};