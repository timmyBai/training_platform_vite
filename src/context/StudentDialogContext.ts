import { createContext } from 'react';

type StudentDialogContextType = {
    isShow: boolean;
    handleStudentDialogShow: (isShow: boolean) => void
}

const StudentDialogContext = createContext<StudentDialogContextType>({
    isShow: false,
    handleStudentDialogShow: () => {}
});

export default StudentDialogContext;