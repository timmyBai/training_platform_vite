import React, { Fragment, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import NProgress from 'nprogress';


// store
import { getUserInfo } from './store/modules/user';

// hook
import { useAppDispatch, useAppSelector } from './hook/store';

const Permission: React.FC = () => {
    const location = useLocation();
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const storeGetters = useAppSelector((state) => {
        return {
            token: state.user.token,
            id: state.user.id,
            identity: state.user.identity,
            addRoutes: state.permission.addRoutes
        };
    });

    const handlePermission = async () => {
        NProgress.configure({
            showSpinner: false
        });

        const whiteList = ['/home/', '/login/', '/404/'];

        NProgress.start();

        if (storeGetters.token) {
            if (location.pathname === '/login/') {
                navigate('/redirect/');
                NProgress.done();
            }
            else {
                const hasRoutes = storeGetters.addRoutes && storeGetters.addRoutes.length > 0;

                if (hasRoutes) {
                    navigate(location.pathname);
                }
                else {
                    dispatch(getUserInfo()).unwrap().then(async (res) => {
                        await dispatch({ type: 'user/changeUserInfo', payload: res.data });

                        const accessRoutes = await dispatch({ type: 'permission/generateRoutes', payload: res.data._role });

                        const interval = setInterval(() => {
                            if (accessRoutes.payload.length > 0) {
                                // 如果身分是 '業主'
                                if (res.data.identity === 'A') {
                                    navigate('/ownerList/');
                                }
                                else {
                                    // 否則為 '物理治療師'、'復健師'、'體適能指導員'
                                    navigate('/studentList/');
                                }

                                clearInterval(interval);
                            }
                        }, 100);
                    }).catch(() => {
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home/');
                        NProgress.done();
                    });
                }
            }
        }
        else {
            if (whiteList.indexOf(location.pathname) !== -1) {
                navigate(location.pathname);
            }
            else {
                navigate('/home/');
                NProgress.done();
            }
        }

        NProgress.done();
    };

    useEffect(() => {
        handlePermission();

        return () => {
            NProgress.done();
        };
    }, [location.pathname, storeGetters.token, storeGetters.addRoutes]);

    return (
        <Fragment></Fragment>
    );
};

export default Permission;