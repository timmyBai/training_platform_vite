import { AxiosResponse } from 'axios';
import request from '@/utils/request';

// 取得體適能報表(心肺適能、柔軟度、平衡)
export const getInspectionReportPhysicalFitness = (studentId: number, trainingType: number): Promise<AxiosResponse<GetInspectionReportPhysicalFitnessResponseType, any>> => {
    return request.get<GetInspectionReportPhysicalFitnessResponseType>(`api/inspectionReport/${studentId}/physicalFitness/${trainingType}/report`);
};

// 取得正式訓練學員訓練各台儀器總功率清單
export const getStudentEachMachinaryTotalPowerList = (studentId: number): Promise<AxiosResponse<GetStudentEachMachinaryTotalPowerListResponseType, any>> => {
    return request.get<GetStudentEachMachinaryTotalPowerListResponseType>(`api/${studentId}/eachMachinary/totalPower/list`);
};

// 取得學員使用機器總功率
export const getStudentMachinaryTotalPower = (studentId: number, machinaryCode: number): Promise<AxiosResponse<GetStudentMachinaryTotalPowerResponseType, any>> => {
    return request.get<GetStudentMachinaryTotalPowerResponseType>(`api/${studentId}/machinary/totalPower/${machinaryCode}/fetch`);
};

// 取得過去 20 次正式訓練狀況報表
export const getStudentFormalTrainingStatusReport = (studentId: number, machinaryCode: number): Promise<AxiosResponse<GetStudentFormalTrainingStatusReportResponseType, any>> => {
    return request.get<GetStudentFormalTrainingStatusReportResponseType>(`api/${studentId}/formatTraining/status/${machinaryCode}/report`);
};

// 取得過去 20 次正式訓練狀況-功率
export const getStudentTrainingPowerReport = (studentId: number, machinaryCode: number): Promise<AxiosResponse<GetStudentTrainingPowerReportResponseType, any>> => {
    return request.get<GetStudentTrainingPowerReportResponseType>(`api/${studentId}/formalTraining/power/${machinaryCode}/report`);
};

/**
 * 取得單日報表
 */
export const getStudentSingleDayReport = (studentId: number, data: GetStudentSingleDayReportRequestType): Promise<AxiosResponse<GetStudentSingleDayReportResponseType, any>> => {
    return request.post<GetStudentSingleDayReportResponseType>(`api/${studentId}/singleDay/report`, data);
};

/**
 * 取得學員機器訓練總表
 */
export const getStudentTrainingMachinarySummaryList = (studentId: number, machinaryCode: number): Promise<AxiosResponse<GetStudentTrainingMachinarySummaryListResponseType, any>> => {
    return request.get<GetStudentTrainingMachinarySummaryListResponseType>(`api/${studentId}/trainingMachinary/${machinaryCode}/summary/list`);
};

/**
 * 新增或更新學員正式機器訓練當天備註
 */
export const replaceStudentFormalTrainingMark = (studentId: number, machinaryCode: number, data: ReplaceStudentTrainingMachinarySummaryRequestType): Promise<AxiosResponse<ReplaceStudentTrainingMachinarySummaryResponseType, any>> => {
    return request.put<ReplaceStudentTrainingMachinarySummaryResponseType>(`api/${studentId}/formalTraining/${machinaryCode}/mark/replace`, data);
};

/**
 * 刪除學員正式機器訓練當天備註
 */
export const deleteStudentFormalTrainingMark = (studentId: number, machinaryCode: number, data: DeleteStudentTrainingMachinarySummaryRequestType): Promise<AxiosResponse<DeleteStudentTrainingMachinarySummaryResponseType, any>> => {
    return request.delete<DeleteStudentTrainingMachinarySummaryResponseType>(`api/${studentId}/formalTraining/${machinaryCode}/mark/delete`, {
        data
    });
};