import { AxiosResponse } from 'axios';
import request from '@/utils/request';

/**
 * 新增或更新 體適能-心肺適能
 */
// export const physicalFitnessCardiorespiratoryFitnessMark = (studentId: number, data: PhysicalFitnessCardiorespiratoryFitnessMarkRequestType) => {
//     return request.put<PhysicalFitnessCardiorespiratoryFitnessMarkResponseType>(`api/physicalFitnessCardiorespiratoryFitness/${studentId}/mark/replace`, data);
// };

/**
 * 新增或更新 體適能-肌力與肌耐力
 */
// export const physicalFitnessMuscleStrengthAndMuscleEnduranceMark = (studentId: number, data: PhysicalFitnessMuscleStrengthAndMuscleEnduranceMarkRequestType) => {
//     return request.put<PhysicalFitnessMuscleStrengthAndMuscleEnduranceMarkResponseType>(`api/physicalFitnessMuscleStrengthAndMuscleEndurance/${studentId}/mark/replace`, data);
// };

/**
 * 新增或更新 體適能-柔軟度
 */
// export const physicalFitnessSoftnessMarkMark = (studentId: number, data: PhysicalFitnessSoftnessMarkRequestType) => {
//     return request.put<PhysicalFitnessSoftnessMarkResponseType>(`api/physicalFitnessSoftness/${studentId}/mark/replace`, data);
// };

/**
 * 新增或更新 體適能-平衡
 */
// export const physicalFitnessBalanceMark = (studentId: number, data: PhysicalFitnessBalanceMarkRequestType) => {
//     return request.put<PhysicalFitnessBalanceMarkResponseType>(`api/physicalFitnessBalance/${studentId}/mark/replace`, data);
// };

/**
 * 新增或更新體適能-心肺適能 登階測驗與備註
 */
export const replacePhysicalFitnessCardiorespiratoryFitnessStepTestAndMark = (studentId: number, data: ReplacePhysicalFitnessCardiorespiratoryFitnessStepTestAndMarkRequestType): Promise<AxiosResponse<ReplacePhysicalFitnessCardiorespiratoryFitnessStepTestAndMarkResponseType, any>> => {
    return request.put<ReplacePhysicalFitnessCardiorespiratoryFitnessStepTestAndMarkResponseType>(`api/physicalFitnessCardiorespiratoryFitness/${studentId}/stepTest/mark/replace`, data);
};

/**
 * 新增或更新體適能-心肺適能 原地站立抬膝與備註
 */
export const replacePhysicalFitnessCardiorespiratoryFitnessStandingKneeRaise = (studentId: number, data: ReplacePhysicalFitnessCardiorespiratoryFitnessStandingKneeRaiseRequestType): Promise<AxiosResponse<ReplacePhysicalFitnessCardiorespiratoryFitnessStandingKneeRaiseResponseType, any>> => {
    return request.put<ReplacePhysicalFitnessCardiorespiratoryFitnessStandingKneeRaiseResponseType>(`api/physicalFitnessCardiorespiratoryFitness/${studentId}/kneeLiftFrequency/mark/replace`, data);
};

/**
 * 新增或更新學員體適能-肌力與肌耐力 屈膝仰臥起坐與備註
 */
export const replacePhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequency = (studentId: number, data: ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyRequestType): Promise<AxiosResponse<ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyResponseType, any>> => {
    return request.put<ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyResponseType>(`api/physicalFitnessMuscleStrengthAndMuscleEndurance/${studentId}/kneeCrunchesFrequency/mark/replace`, data);
};

/**
 * 新增或更新學員體適能-肌力與肌耐力 手臂彎舉(上肢)與備註
 */
export const replacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequency = (studentId: number, data: ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyRequestType): Promise<AxiosResponse<ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyResponseType, any>> => {
    return request.put<ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyResponseType>(`api/physicalFitnessMuscleStrengthAndMuscleEndurance/${studentId}/armCurlUpperBodyFrequency/replace`, data);
};

/**
 * 新增或更新學員體適能-肌力與肌耐力 手臂彎舉(下肢)與備註
 */
export const replacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequency = (studentId: number, data: ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyRequestType): Promise<AxiosResponse<ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyResponseType, any>> => {
    return request.put<ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyResponseType>(`api/physicalFitnessMuscleStrengthAndMuscleEndurance/${studentId}/armCurlLowerBodyFrequency/mark/replace`, data);
};

/**
 * 新增或更新學員體適能-柔軟度 坐姿體前彎與備註
 */
export const replacePhysicalFitnessSoftnessSeatedForwardBend = (studentId: number, data: ReplacePhysicalFitnessSoftnessSeatedForwardBendRequestType): Promise<AxiosResponse<ReplacePhysicalFitnessSoftnessSeatedForwardBendResponseType, any>> => {
    return request.put<ReplacePhysicalFitnessSoftnessSeatedForwardBendResponseType>(`api/physicalFitnessSoftness/${studentId}/seatedForwardBend/mark/replace`, data);
};

/**
 * 新增或更新學員體適能-柔軟度 椅子坐姿體前彎與備註
 */
export const replacePhysicalFitnessSoftnessChairSeatedForwardBend = (studentId: number, data: ReplacePhysicalFitnessSoftnessChairSeatedForwardBendRequestType): Promise<AxiosResponse<ReplacePhysicalFitnessSoftnessChairSeatedForwardBendResponseType, any>> => {
    return request.put<ReplacePhysicalFitnessSoftnessChairSeatedForwardBendResponseType>(`api/physicalFitnessSoftness/${studentId}/chairSeatedForwardBend/mark/replace`, data);
};

/**
 * 新增或更新學員體適能-柔軟度 拉背側驗與備註
 */
export const replacePhysicalFitnessSoftnessPullBackTest = (studentId: number, data: ReplacePhysicalFitnessSoftnessPullBackTestRequestType): Promise<AxiosResponse<ReplacePhysicalFitnessSoftnessPullBackTestResponseType, any>> => {
    return request.put<ReplacePhysicalFitnessSoftnessPullBackTestResponseType>(`api/physicalFitnessSoftness/${studentId}/chairSeatedForwardBend/mark/replace`, data);
};

/**
 * 新增或更新學員體適能-平衡 壯年開眼單足立與備註
 */
export const replacePhysicalFitnessBalanceAdultEyeOpeningMonopodSecond = (studentId: number, data: ReplacePhysicalFitnessBalanceAdultEyeOpeningMonopodSecondRequestType): Promise<AxiosResponse<ReplacePhysicalFitnessBalanceAdultEyeOpeningMonopodSecondResponseType, any>> => {
    return request.put<ReplacePhysicalFitnessBalanceAdultEyeOpeningMonopodSecondResponseType>(`api/physicalFitnessBalance/${studentId}/adultEyeOpeningMonopodSecond/mark/replace`, data);
};

/**
 * 新增或更新學員體適能-平衡 老年開眼單足立與備註
 */
export const replacePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecond = (studentId: number, data: ReplacePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondRequestType): Promise<AxiosResponse<ReplacePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondResponseType, any>> => {
    return request.put<ReplacePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondResponseType>(`api/physicalFitnessBalance/${studentId}/elderlyEyeOpeningMonopodSecond/mark/replace`, data);
};

/**
 * 新增或更新學員體適能-平衡 坐立繞物第一次秒數與備註
 */
export const replacePhysicalFitnessBalanceSittingAroundOneSecond = (studentId: number, data: ReplacePhysicalFitnessBalanceSittingAroundOneSecondRequestType): Promise<AxiosResponse<ReplacePhysicalFitnessBalanceSittingAroundOneSecondResponseType, any>> => {
    return request.put<ReplacePhysicalFitnessBalanceSittingAroundOneSecondResponseType>(`api/physicalFitnessBalance/${studentId}/sittingAroundOneSecond/mark/replace`, data);
};

/**
 * 新增或更新學員體適能-平衡 坐立繞物第二次秒數與備註
 */
export const replacePhysicalFitnessBalanceSittingAroundTwoSecond = (studentId: number, data: ReplacePhysicalFitnessBalanceSittingAroundTwoSecondRequestType): Promise<AxiosResponse<ReplacePhysicalFitnessBalanceSittingAroundTwoSecondResponseType, any>> => {
    return request.put<ReplacePhysicalFitnessBalanceSittingAroundTwoSecondResponseType>(`api/physicalFitnessBalance/${studentId}/sittingAroundTwoSecond/mark/replace`, data);
};
