import { AxiosResponse } from 'axios';
import request from '@/utils/request';

/**
 * 會員登入
 */
export const authLogin = (data: ApiAuthLoginRequest): Promise<AxiosResponse<ApiAuthLoginResponse, any>> => {
    return request.post<ApiAuthLoginResponse>('/api/auth/login', data);
};

/**
 * 取得會員資訊
 */
export const memberInfo = (): Promise<AxiosResponse<ApiUserInfoResponse, any>> => {
    return request.post<ApiUserInfoResponse>('/api/member/information');
};

/**
 * 會員註冊
 */
export const authRegister = (data: AuthRegisterRequestType): Promise<AxiosResponse<ApiAuthRegisterResponseType, any>> => {
    return request.post<ApiAuthRegisterResponseType>('api/auth/register', data);
};