import { AxiosResponse } from 'axios';
import request from '@/utils/request';

/**
 * 取得學員肌力訓練報表
 */
export const printTrainingReport = (studentId: number, data: PrintTrainingReportRequestType): Promise<AxiosResponse<Blob, any>> => {
    return request.post<Blob>(`api/print/training/${studentId}/pdf/report`, data, {
        responseType: 'blob'
    });
};