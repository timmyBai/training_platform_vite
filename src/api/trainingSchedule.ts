import { AxiosResponse } from 'axios';
import request from '@/utils/request';

/**
 * 檢查訓練排程排程天數是否有誤差發放通知
 */
export const trainingScheduleNotify = (studentId: number): Promise<AxiosResponse<TrainingScheduleNotifyResponseType, any>> => {
    return request.get<TrainingScheduleNotifyResponseType>(`api/trainingSchedule/${studentId}/notify`);
};

/**
 * 取得訓練排程-心肺適能這週剩餘排程天數
 */
export const getThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequency = (studentId: number): Promise<AxiosResponse<GetThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequencyResponseType, any>> => {
    return request.get<GetThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequencyResponseType>(`api/thisWeek/trainingSchedule/${studentId}/cardiorespiratoryFitness/remainingFrequency`);
};

/**
 * 取得訓練排程-肌力與肌耐力這週剩餘排程天數
 */
export const getThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequency = (studentId: number): Promise<AxiosResponse<GetThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequencyResponseType, any>> => {
    return request.get<GetThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequencyResponseType>(`api/thisWeek/trainingSchedule/${studentId}/muscleStrengthAndMuscleEndurance/remainingFrequency`);
};

/**
 * 取得訓練排程-柔軟度這週剩餘排程天數
 */
export const getThisWeekTrainingScheduleSoftnessRemainingFrequency = (studentId: number): Promise<AxiosResponse<GetThisWeekTrainingScheduleSoftnessRemainingFrequencyResponseType, any>> => {
    return request.get<GetThisWeekTrainingScheduleSoftnessRemainingFrequencyResponseType>(`api/thisWeek/trainingSchedule/${studentId}/softness/remainingFrequency`);
};

/**
 * 取得訓練排程-平衡這週剩餘排程天數
 */
export const getThisWeekTrainingScheduleBalanceRemainingFrequency = (studentId: number): Promise<AxiosResponse<GetThisWeekTrainingScheduleBalanceRemainingFrequencyResponseType, any>> => {
    return request.get<GetThisWeekTrainingScheduleBalanceRemainingFrequencyResponseType>(`api/thisWeek/trainingSchedule/${studentId}/balance/remainingFrequency`);
};

/**
 * 新增訓練排程
 */
export const addTrainingScheduleWeek = (studentId: number, data: AddTrainingScheduleWeekRequest): Promise<AxiosResponse<AddTrainingScheduleWeekResponseType, any>> => {
    return request.post<AddTrainingScheduleWeekResponseType>(`api/${studentId}/trainingSchedule/add`, data);
};

/**
 * 更新訓練排程實際完成分鐘
 */
export const updateTrainingSchedule = (studentId: number, data: UpdateTrainingScheduleRequestType): Promise<AxiosResponse<UpdateTrainingScheduleResponseType, any>> => {
    return request.patch<UpdateTrainingScheduleResponseType>(`api/trainingSchedule/${studentId}/actualCompletion/replace`, data);
};