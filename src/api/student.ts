import { AxiosResponse } from 'axios';
import request from '@/utils/request';

/**
 * 取得學員清單
 */
export const getStudentList = (): Promise<AxiosResponse<GetStudentListResponseType, any>> => {
    return request.get<GetStudentListResponseType>('api/student/list');
};

/**
 * 停用學員
 * @param {number} studentId 學員 id
 */
export const disabledStudent = (studentId: number): Promise<AxiosResponse<DisabledStudentResponseType, any>> => {
    return request.patch<DisabledStudentResponseType>(`api/student/disabled/${studentId}/replace`);
};

/**
 * 更新某學員基本資訊
 */
export const updateStudentBaseInfo = (studentId: number, data: StudentBaseInfoRequestType): Promise<AxiosResponse<StudentBaseInfoResponseType, any>> => {
    return request.put<StudentBaseInfoResponseType>(`api/base/${studentId}/info/replace`, data);
};

/**
 * 取得某學員最新基本資
 */
export const getOnlyStudentBaseInfo = (studentId: number): Promise<AxiosResponse<GetOnlyStudentBaseInfoResponseType, any>> => {
    return request.get<GetOnlyStudentBaseInfoResponseType>(`api/base/${studentId}/info`);
};