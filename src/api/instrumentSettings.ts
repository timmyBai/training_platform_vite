import { AxiosResponse } from 'axios';
import request from '@/utils/request';

/**
 * 取得機器設定運動表現清單
 * @param {number} studentId 學員 id
 */
export const instrumentSettingsSportsPerformanceList = (studentId: number): Promise<AxiosResponse<InstrumentSettingsListResponseType, any>> => {
    return request.get<InstrumentSettingsListResponseType>(`api/instrumentSettings/${studentId}/sportsPerformance/list`);
};

/**
 * 更新儀器設定運動表現備註
 * @param {number} studentId 學員 id
 * @param {number} machinaryCode 機器編號
 * @param {InstrumentSettingsSportsPerformanceRequestType} data
 */
export const instrumentSettingsSportsPerformanceReplace = (studentId: number, machinaryCode: number | undefined, data: InstrumentSettingsSportsPerformanceRequestType): Promise<AxiosResponse<InstrumentSettingsSportsPerformanceResponseType, any>> => {
    return request.put<InstrumentSettingsSportsPerformanceResponseType>(`api/${studentId}/sportsPerformance/${machinaryCode}/mark/replace`, data);
};
