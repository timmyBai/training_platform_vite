import { AxiosResponse } from 'axios';
import request from '@/utils/request';

/**
 * 取得學員運動處方清單
 */
export const exercisePrescriptionList = (studentId: number): Promise<AxiosResponse<ExercisePrescriptionListResponseType, any>> => {
    return request.get<ExercisePrescriptionListResponseType>(`api/exercisePrescription/${studentId}/fetch`);
};

/**
 * 取得最新訓練階段
 */
export const exercisePrescriptionTrainingStage = (studentId: number): Promise<AxiosResponse<ExercisePrescriptionTrainingStageResponseType, any>> => {
    return request.get<ExercisePrescriptionTrainingStageResponseType>(`api/exercisePrescriptionTrainingStage/${studentId}/latest`);
};

/**
 * 新增或更新運動處方訓練階段
 */
export const exercisePrescriptionTrainingStageReplace = (studentId: number, data: ExercisePrescriptionTrainingStageReplaceRequestType): Promise<AxiosResponse<ExercisePrescriptionTrainingStageReplaceResponseType, any>> => {
    return request.put<ExercisePrescriptionTrainingStageReplaceResponseType>(`api/trainingStage/${studentId}/replace`, data);
};

/**
 * 新增或更新運動處方 心肺適能
 */
export const exercisePrescriptionCardiorespiratoryFitness = (studentId: number, data: ExercisePrescriptionCardiorespiratoryFitnessRequestType): Promise<AxiosResponse<ExercisePrescriptionCardiorespiratoryFitnessResponseType, any>> => {
    return request.put<ExercisePrescriptionCardiorespiratoryFitnessResponseType>(`api/exercisePrescriptionCardiorespiratoryFitness/${studentId}/mark/replace`, data);
};

/**
 * 新增或更新運動處方 肌力與肌耐力
 */
export const exercisePrescriptionMuscleStrengthAndMuscleEndurance = (studentId: number, data: ExercisePrescriptionMuscleStrengthAndMuscleEnduranceRequestType): Promise<AxiosResponse<ExercisePrescriptionMuscleStrengthAndMuscleEnduranceResponseType, any>> => {
    return request.put<ExercisePrescriptionMuscleStrengthAndMuscleEnduranceResponseType>(`api/exercisePrescriptionMuscleStrengthAndMuscleEndurance/${studentId}/mark/replace`, data);
};

/**
 * 新增或更新運動處方 柔軟度
 */
export const exercisePrescriptionSoftness = (studentId: number, data: ExercisePrescriptionSoftnessRequestType): Promise<AxiosResponse<ExercisePrescriptionSoftnessResponseType, any>> => {
    return request.put<ExercisePrescriptionSoftnessResponseType>(`api/exercisePrescriptionSoftness/${studentId}/mark/replace`, data);
};

/**
 * 新增或更新 平衡
 */
export const exercisePrescriptionBalance = (studentId: number, data: ExercisePrescriptionBalanceRequestType): Promise<AxiosResponse<ExercisePrescriptionBalanceResponseType, any>> => {
    return request.put<ExercisePrescriptionBalanceResponseType>(`api/exercisePrescriptionBalance/${studentId}/mark/replace`, data);
};