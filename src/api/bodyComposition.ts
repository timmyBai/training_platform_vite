import { AxiosResponse } from 'axios';
import request from '@/utils/request';

/**
 * 新增或更新身體組成 bmi
 */
export const bodyCompositionBmiAndMarkReplace = (studentId: number, data: BodyCompositionBmiAndMarkRequestType): Promise<AxiosResponse<BodyCompositionBmiAndMarkResponseType, any>> => {
    return request.put<BodyCompositionBmiAndMarkResponseType>(`api/bodyCompositionRecord/${studentId}/bmi/replace`, data);
};

/**
 * 新增或更新身體組成 體脂肪
 */
export const bodyCompositionBodyFatAndMarkReplace = (studentId: number, data: BodyCompositionBodyFatAndMarkRequestType): Promise<AxiosResponse<BodyCompositionBodyFatAndMarkResponseType, any>> => {
    return request.put<BodyCompositionBodyFatAndMarkResponseType>(`api/bodyCompositionRecord/${studentId}/bodyFat/replace`, data);
};

/**
 * 新增或更新身體組成 內臟脂肪
 */
export const bodyCompositionVisceralFatAndMarkReplace = (studentId: number, data: BodyCompositionVisceralFatAndMarkRequestType): Promise<AxiosResponse<BodyCompositionVisceralFatAndMarkResponseType, any>> => {
    return request.put<BodyCompositionVisceralFatAndMarkResponseType>(`api/bodyCompositionRecord/${studentId}/visceralFat/replace`, data);
};

/**
 * 新增或更新身體組成 骨骼肌率
 */
export const bodyCompositionSkeletalMuscleRateAndMarkReplace = (studentId: number, data: BodyCompositionSkeletalMuscleRateAndMarkRequestType): Promise<AxiosResponse<BodyCompositionSkeletalMuscleRateAndMarkResponseType, any>> => {
    return request.put<BodyCompositionSkeletalMuscleRateAndMarkResponseType>(`api/bodyCompositionRecord/${studentId}/skeletalMuscleRate/replace`, data);
};
