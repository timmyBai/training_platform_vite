import { AxiosResponse } from 'axios';
import request from '@/utils/request';

/**
 * 取得學員身體組成清單與體適能清單
 */
export const bodyMassAndPhysicalFitnessList = (studentId: number): Promise<AxiosResponse<BodyMassAadPhysicalFitnessListResponseType, any>> => {
    return request.get<BodyMassAadPhysicalFitnessListResponseType>(`api/studentBodyCompositionAndPhysicalFitness/${studentId}/fetch`);
};
