import React from 'react';

// css
import './index.sass';

type PageTitleBarPropsType = {
    className?: string;
    title: string;
}

const PageTitleBar: React.FC<PageTitleBarPropsType> = ({ className, title }) => {
    return (
        <div className={`page_title ${className}`}>
            <div className='flex flex-wrap'>
                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                    <div className='card'>
                        <h2 className='title'>{title}</h2>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PageTitleBar;