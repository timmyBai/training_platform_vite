import React, { useContext, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import validate from 'validate.js';

// css
import './index.sass';

// utils
import { dangerColor } from '@/utils/dangerColor';
import { axiosErrorCode } from '@/utils/request';

// context
import StudentDialogContext from '@/context/StudentDialogContext';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

// api
import { getOnlyStudentBaseInfo, updateStudentBaseInfo } from '@/api/student';

// component
import Dialog from '../Dialog';

// image
import btn_edit from '@/assets/images/btn_edit.svg';
import btn_edit_save from '@/assets/images/btn_edit_save.svg';
import user_logo from '@/assets/images/user_photo.svg';

const StudentInfoBar: React.FC = () => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const studentDialogContext = useContext(StudentDialogContext);
    const [isShowInput, setIsShowInput] = useState<boolean>(false);
    const [studnetDialogTitle, setStudnetDialogTitle] = useState<string>('');
    const [studentDialogMessage, setStudentDialogMessage] = useState<string>('');
    const [inputName, setInputName] = useState<string>('');
    const [inputPosture, setInputPosture] = useState<string>('');
    const [inputPhone, setInputPhone] = useState<string>('');
    const [inputAddress, setInputAddress] = useState<string>('');
    const studentBaseInfoFormRef = useRef<HTMLFormElement>(null);
    const formStudentInfo = {
        'name': {
            length: {
                maximum: 10,
                message: '名子最大 10 個字'
            }
        },
        'phone': {
            format: {
                pattern: '^[0-9]{8,10}$',
                message: '電話號碼 8 到 10 個字'
            }
        },
        'posture': {
            format: {
                pattern: '^([0-9]+)\\/([0-9]+)$',
                message: '身高與體重輸入格是錯誤'
            }
        },
        'address': {
            length: {
                maximum: 50,
                message: '地址最長 50 字'
            }
        }
    };

    const storeGetters = useAppSelector((state) => {
        return {
            student_logo: state.studentInfo.student_logo,
            studentId: state.studentInfo.studentId,
            name: state.studentInfo.name,
            gender: state.studentInfo.gender,
            age: state.studentInfo.age,
            posture: state.studentInfo.posture,
            phone: state.studentInfo.phone,
            address: state.studentInfo.address,
            medicalHistory: state.studentInfo.medicalHistory,
            dangerGrading: state.studentInfo.dangerGrading,
            mark: state.studentInfo.mark
        };
    });

    const handleShowEditInput = () => {
        setIsShowInput(true);
        setInputName(storeGetters.name);
        setInputPhone(storeGetters.phone);
        setInputPosture(storeGetters.posture);
        setInputAddress(storeGetters.address);
    };

    const handleUpdateStudentBaseInfo = () => {
        const isValidate = validate(studentBaseInfoFormRef.current, formStudentInfo);
        
        if (isValidate !== undefined) {
            setStudnetDialogTitle('錯誤');
            setStudentDialogMessage('輸入格式錯誤');
            studentDialogContext.handleStudentDialogShow(true);
        }
        else {
            const data: StudentBaseInfoRequestType = {
                name: inputName,
                phone: inputPhone,
                posture: inputPosture,
                address: inputAddress
            };

            updateStudentBaseInfo(storeGetters.studentId, data).then(() => {
                setInputName(storeGetters.name);
                setInputPhone(storeGetters.phone);
                setInputPosture(storeGetters.posture);
                setInputAddress(storeGetters.address);

                handleGetOnlyStudentBaseInfo();
                setStudnetDialogTitle('資訊');
                setStudentDialogMessage('修改學員資料成功');
                setIsShowInput(false);
                studentDialogContext.handleStudentDialogShow(true);
            }).catch((error) => {
                const errorCode = axiosErrorCode<StudentBaseInfoErrorCodeType>(error);

                studentDialogContext.handleStudentDialogShow(true);
                setStudnetDialogTitle('錯誤');

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setStudentDialogMessage('格式輸入錯誤');
                        break;
                    case 'StudentDoesNotExist':
                        setStudentDialogMessage('找不到學員');
                        break;
                    case 'UserPhoneAlreadyRegistered':
                        setStudentDialogMessage('此手機已有人註冊');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setStudnetDialogTitle('伺服器錯誤');
                        break;
                }
            });
        }
    };

    const handleGetOnlyStudentBaseInfo = () => {
        getOnlyStudentBaseInfo(storeGetters.studentId).then((res) => {
            const { data } = res.data;

            dispatch({ type: 'studentInfo/changeStudentInfo', payload: data });
        }).catch((error) => {
            const errorCode = axiosErrorCode<GetOnlyStudentBaseInfoErrorCodeType>(error);

            studentDialogContext.handleStudentDialogShow(true);
            setStudnetDialogTitle('錯誤');

            switch (errorCode) {
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                default:
                    setStudnetDialogTitle('伺服器錯誤');
                    break;
            }
        });
    };

    const handleStudentDialogShow = () => {
        studentDialogContext.handleStudentDialogShow(false);
    };

    const handleChangeInputName = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInputName(e.target.value);
    };

    const handleChangeInputPosture = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInputPosture(e.target.value);
    };

    const handleChangeInputPhone = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInputPhone(e.target.value);
    };

    const handleChangeInputAddress = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInputAddress(e.target.value);
    };

    return (
        <form ref={studentBaseInfoFormRef}>
            <div className='studentInfoBar'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <img className='student_logo' src={storeGetters.student_logo || user_logo} />
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        {!isShowInput && <p>{storeGetters.name}</p>}
                        {
                            isShowInput && (
                                <input type='text' placeholder='請輸入姓名' defaultValue={storeGetters.name} onChange={handleChangeInputName} name='name' />
                            )
                        }
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <p>{storeGetters.gender}</p>
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <p>{storeGetters.age}歲</p>
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        {
                            !isShowInput && (
                                <p>{storeGetters.posture}</p>
                            )
                        }
                        {
                            isShowInput && (
                                <input type='text' placeholder='請輸入身高/體重' defaultValue={storeGetters.posture} onChange={handleChangeInputPosture} name='posture' />
                            )
                        }
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        {
                            !isShowInput && (
                                <img className='btn_edit_student' src={btn_edit} onClick={handleShowEditInput} />
                            )
                        }
                        {
                            isShowInput && (
                                <img className='btn_save_student' src={btn_edit_save} onClick={handleUpdateStudentBaseInfo} />
                            )
                        }
                    </div>
                    
                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <p>{storeGetters.medicalHistory}</p>
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <p className={`dangerGrading ${dangerColor(storeGetters.dangerGrading)}`}>{storeGetters.dangerGrading}</p>
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        {
                            !isShowInput && <p>{storeGetters.phone}</p>
                        }
                        {
                            isShowInput && (
                                <input type="text" placeholder='請輸入電話號碼' defaultValue={storeGetters.phone} onChange={handleChangeInputPhone} name='phone' />
                            )
                        }
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        {
                            !isShowInput && (
                                <p>{storeGetters.address}</p>
                            )
                        }
                        {
                            isShowInput && (
                                <input type="text" placeholder='請輸入地址' defaultValue={storeGetters.address} onChange={handleChangeInputAddress} name='address' />
                            )
                        }
                    </div>
                </div>

                {
                    studentDialogContext.isShow && (
                        <Dialog className='visibleStudentDialog'>
                            <Dialog.Title title={studnetDialogTitle}></Dialog.Title>

                            <Dialog.Body>
                                <div className='content'>
                                    <p>{studentDialogMessage}</p>
                                </div>
                            </Dialog.Body>

                            <Dialog.Footer>
                                <button className='dialogBtnConfirm' onClick={handleStudentDialogShow}>確定</button>
                            </Dialog.Footer>
                        </Dialog>
                    )
                }
            </div>
        </form>
    );
};

export default StudentInfoBar;