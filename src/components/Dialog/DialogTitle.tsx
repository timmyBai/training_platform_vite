import React from 'react';

// css
import './DialogTitle.sass';

type DialogTitleProps = {
    title: string
}

const DialogTitle: React.FC<DialogTitleProps> = ({ title }) => {
    return (
        <div className='dialogTitle'>
            <h1 className='title'>{title}</h1>
        </div>
    );
};

export default DialogTitle;