import React from 'react';

// css
import './DialogBody.sass';

type DialogBodyProps = {
    children: JSX.Element | JSX.Element[]
};

const DialogBody: React.FC<DialogBodyProps> = ({ children }) => {
    return (
        <div className='dialogBody'>
            {children}
        </div>
    );
};

export default DialogBody;