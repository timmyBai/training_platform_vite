import React from 'react';

// css
import './DialogFooter.sass';

type DialogFooterProps = {
    children: JSX.Element | JSX.Element[]
}

const DialogFooter: React.FC<DialogFooterProps> = ({ children }) => {
    return (
        <div className='dialogFooter'>
            {children}
        </div>
    );
};

export default DialogFooter;