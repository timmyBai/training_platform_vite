import React from 'react';

// css
import './Dialog.sass';

type DialogProps = {
    className?: string;
    children: JSX.Element | JSX.Element[];
};

const Dialog: React.FC<DialogProps> = ({ className, children }) => {
    return (
        <div className={`dialog ${className !== undefined ? className : ''}`}>
            <div className='container mx-auto'>
                {children}
            </div>
        </div>
    );
};

export default Dialog;