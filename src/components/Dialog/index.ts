import Dialog from './Dialog';
import DialogTitle from './DialogTitle';
import DialogBody from './DialogBody';
import DialogFooter from './DialogFooter';

export default Object.assign(Dialog, {
    Title: DialogTitle,
    Body: DialogBody,
    Footer: DialogFooter
});