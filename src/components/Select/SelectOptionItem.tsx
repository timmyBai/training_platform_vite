import React, { useEffect, useState } from 'react';

// css
import './SelectOptionItem.sass';
import colors from '@/styles/colors.module.sass';

type SelectItemPropsType = {
    index: number,
    removeIndexKey: string | undefined,
    optionItem: string[]
    text: string,
    handleAddOptionItem: (index: number) => void
    handleResetKey: () => void
}

const SelectOptionItem: React.FC<SelectItemPropsType> = ({ optionItem, removeIndexKey, index, text, handleAddOptionItem, handleResetKey }) => {
    const [isShow, setIsShow] = useState<boolean>(false);
    const [selectOptionItem, setSelectOptionItem] = useState<{ color: string }>();

    const handleClick = () => {
        handleAddOptionItem(index);

        if (isShow) {
            setIsShow(false);
            setSelectOptionItem({
                color: colors.color_black
            });
        }
        else {
            setIsShow(true);
            setSelectOptionItem({
                color: colors.color_orange
            });
        }
    };

    useEffect(() => {
        if (removeIndexKey === text) {
            if (isShow) {
                setIsShow(false);
                setSelectOptionItem({
                    color: colors.color_black
                });
            }
        }

        handleResetKey();
    }, [optionItem]);

    return (
        <div className={'option_item'} onClick={handleClick} style={selectOptionItem}>
            {text}
        </div>
    );
};

export default SelectOptionItem;