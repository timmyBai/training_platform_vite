export const handleDocumentEvent = (e: MouseEvent) => {
    if (e.target instanceof Element) {
        const isDropdownButton = e.target.matches('[data-dropdown-button]');

        if (!isDropdownButton && (e.target.closest('[data-dropdown]') !== null && e.target.parentElement?.closest('.select_tag:nth-child(1)') === null) && e.target.closest('span') === null) {
            return;
        }

        let currentDropdown: any = null;

        if (isDropdownButton) {
            currentDropdown = e.target.closest('[data-dropdown]');
            currentDropdown.classList.toggle('active');
        }
        
        if (!isDropdownButton) {
            if (e.target.parentElement?.parentElement?.closest('[data-dropdown]')) {
                currentDropdown = e.target.parentElement?.parentElement?.closest('[data-dropdown]');
                currentDropdown.classList.toggle('active');
            }
        }

        document.querySelectorAll('[data-dropdown].active').forEach(dropdown => {
            if (dropdown === currentDropdown) return;
            dropdown.classList.remove('active');
        });
    }
};

document.addEventListener('click', handleDocumentEvent);

document.addEventListener('submit', (e) => {
    if (e.target instanceof HTMLElement) {
        const isDropdownSubmitButton = e.target.querySelector('.btnConfirm');
        if (!isDropdownSubmitButton && e.target.querySelector('.dropdown') !== null) return;

        let currentDropdown: any = null;
        if (isDropdownSubmitButton) {
            currentDropdown = e.target.querySelector('.dropdown');
            currentDropdown.classList.remove('active');
        }
    }
});


