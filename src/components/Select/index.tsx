import React, { useCallback, useEffect, useRef, useState } from 'react';

// css
import './index.sass';

// utils
import './utils/selectUtils';

// components
import SelectOptionItem from './SelectOptionItem';

type SelectPropsType = {
    options: SelectOptionType[];
    name?: string;
    placeholder?: string;
    multiple?: boolean;
    handleSelectOption: (item: string[] | string) => void
}

type SelectOptionType = {
    value: string;
    text: string;
}

const Select: React.FC<SelectPropsType> = ({ options, multiple, placeholder, name, handleSelectOption }) => {
    const selectRef = useRef<HTMLDivElement>(null);
    const selectTagRef = useRef<HTMLDivElement>(null);
    const selectInputRef = useRef<HTMLInputElement>(null); // select input ref
    const dropdownMenuRef = useRef<HTMLDivElement>(null); // drop down menu ref
    const [inputLeft, setInputLeft] = useState<number>(0); // select input left position
    const [inputTop, setInputTop] = useState<number>(0); // select input top position
    const [inputHeight, setInputHeight] = useState<number>(0); // select input height

    const [height, setHeight] = useState<number | undefined>(undefined);
    const [optionItem, setOptionItem] = useState<string[]>([]);
    const [targetOption, setTargetOption] = useState<string>('');
    const [targetOptionValue, setTargetOptionValue] = useState<string>('');
    const [removeIndexKey, setRemoveIndexKey] = useState<string | undefined>(undefined);
    
    const handleAddOptionItem = (index: number): void => {
        if (multiple) {
            const optionItemIndex = optionItem.indexOf(options[index].text);
            
            if (optionItemIndex !== -1) {
                optionItem.splice(optionItemIndex, 1);
                setOptionItem([...optionItem]);
            }
            else {
                setOptionItem(state => [...state, options[index].text]);
            }
            
            handleSelectStyle();
        }
        else {
            setTargetOption(options[index].text);
            setTargetOptionValue(options[index].value);
            selectRef.current?.classList.remove('active');
        }
    };
    
    const handleResetKey = () => {
        setRemoveIndexKey(undefined);
    };
    
    const handleSelectStyle = async (): Promise<void> => {
        const selectTagRefCurrent = await selectTagRef.current;
        
        if (optionItem.length > 0) {
            if (selectTagRefCurrent?.clientHeight) {
                setHeight(selectTagRefCurrent.clientHeight + 10);
            }
        }
        else {
            setHeight(undefined);
        }
    };
    
    const [optionItemString, setOptionItemString] = useState<string>('');
    
    useEffect(() => {
        let result = '';
        
        if (multiple) {
            optionItem.forEach((item) => {
                result = result + item + ', ';
            });
            
            setOptionItemString(result.substring(0, result.length - 2));
        }
        else {
            setOptionItemString(targetOption);
        }
    }, [optionItem, targetOptionValue]);
    
    /**
     * 檢查下拉選單往外傳送選擇值
     */
    useEffect(() => {
        if (multiple) {
            handleSelectOption(optionItem);
        }
        else {
            handleSelectOption(targetOptionValue);
        }
    }, [optionItem, targetOptionValue]);
    
    

    const handleSelectPosition = () => {
        if (dropdownMenuRef.current && selectInputRef.current?.offsetWidth) {
            dropdownMenuRef.current.style.width = `${selectInputRef.current.offsetWidth}px`;

            const inputRect = selectInputRef.current.getBoundingClientRect();
            setInputLeft(inputRect.left);
            setInputTop(inputRect.top);
            setInputHeight(inputRect.height);
            
            requestAnimationFrame(handleSelectPosition);
        }
    };

    const handleSelectOptionPosition = useCallback(() => {
        if (dropdownMenuRef.current) {
            dropdownMenuRef.current.style.left = `${inputLeft}px`;
            dropdownMenuRef.current.style.top = `${inputTop + inputHeight}px`;
            dropdownMenuRef.current.style.transform = `translate(${inputLeft}, ${inputTop})`;
        }
    }, [inputLeft, inputTop, inputHeight]);

    useEffect(() => {
        handleSelectPosition();
        handleSelectOptionPosition();
    }, [handleSelectOptionPosition]);
    
    return (
        <div className='select' data-dropdown ref={selectRef}>
            <input
                type='text'
                readOnly
                autoComplete='off'
                placeholder={optionItem.length > 0 || targetOption?.length > 0 ? '' : placeholder}
                style={height !== undefined ? { height: height } : {}}
                name={name}
                defaultValue={optionItemString}
                data-dropdown-button
                ref={selectInputRef}
            />

            <div className='select_tag'
                ref={selectTagRef}
                data-dropdown-button
            >
            </div>

            <div className='dropdownMenu' ref={dropdownMenuRef}>
                <div className='dropdownFlex'>
                    {
                        multiple ? (
                            options.map((item, index) => {
                                return (
                                    <SelectOptionItem
                                        key={index}
                                        optionItem={optionItem}
                                        removeIndexKey={removeIndexKey}
                                        index={index}
                                        text={item.text}
                                        handleAddOptionItem={() => handleAddOptionItem(index)}
                                        handleResetKey={handleResetKey}
                                    ></SelectOptionItem>
                                );
                            })
                        ) : (
                            options.map((item, index) => {
                                return (
                                    <div className='option_item' onClick={() => handleAddOptionItem(index)} key={index}>
                                        {item.text}
                                    </div>
                                );
                            })
                        )
                    }
                </div>
            </div>
        </div>
    );
};

export default Select;