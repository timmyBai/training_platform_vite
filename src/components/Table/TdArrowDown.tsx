import React, { Fragment } from 'react';

// css
import './TdArrowDown.sass';

// images
import arrow_down from '@/assets/images/arrow_down.svg';

type TdEditPropsType = {
    dropDownClassName: string;
    data: any
};

const TdArrowDown: React.FC<TdEditPropsType> = ({ dropDownClassName, data }) => {
    const handleDropDownClassName = () => {
        const displayType = document.getElementsByClassName(dropDownClassName);
        
        for(let i = 0; i < displayType.length; i++) {
            displayType[i].classList.toggle('active');
        }
    };

    return (
        <Fragment>
            <td className='icon_arrow_down'>
                { data?.length > 0 && <img src={arrow_down} onClick={handleDropDownClassName} /> }
            </td>
        </Fragment>
    );
};

export default TdArrowDown;