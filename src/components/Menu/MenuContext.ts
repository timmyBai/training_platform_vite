import { createContext } from 'react';

type MenuContextType = {
    mode?: string,
    defaultRoute: string
}

const MenuContext = createContext<MenuContextType>({
    mode: 'vertical',
    defaultRoute: '/studentList'
});

export default MenuContext;
