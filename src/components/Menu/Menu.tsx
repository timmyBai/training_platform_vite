import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';

// css
import './Menu.sass';
import MenuContext from './MenuContext';

type MenuProps = {
    className?: string,
    children: JSX.Element | JSX.Element[]
}

const Menu: React.FC<MenuProps> = ({ className, children }) => {
    const location = useLocation();
    const [defaultRoute, setDefaultRoute] = useState<string>(location.pathname + '/');
    
    useEffect(() => {
        setDefaultRoute(location.pathname);
    }, [location.pathname, defaultRoute]);

    return (
        <MenuContext.Provider
            value={{
                defaultRoute: defaultRoute
            }}
        >
            <div className={`menu ${className}`}>
                { children }
            </div>
        </MenuContext.Provider>
    );
};

Menu.defaultProps = {
    className: 'menu'
};

export default Menu;
