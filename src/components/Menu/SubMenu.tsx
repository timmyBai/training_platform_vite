import React, { useState } from 'react';

// css
import './SubMenu.sass';

// context
import SubMenuContext from './SubMenuContext';

type SubMenuProps = {
    title: string,
    children: JSX.Element | JSX.Element[]
}

const SubMenu: React.FC<SubMenuProps> = ({ title, children }) => {
    const [subMenuType] = useState('subMenu');

    return (
        <SubMenuContext.Provider
            value={{
                type: subMenuType
            }}
        >
            <li className='subMenu'>
                <span className='title'>{title}</span>
                <ol className='nestMenu'>
                    {children}
                </ol>
            </li>
        </SubMenuContext.Provider>
    );
};

export default SubMenu;
