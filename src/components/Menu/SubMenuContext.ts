import { createContext } from 'react';

type SubMenuContextType = {
    type: string
}

const SubMenuContext = createContext<SubMenuContextType>({
    type: ''
});

export default SubMenuContext;