import React from 'react';

// css
import './index.sass';

const Loading: React.FC = () => {
    return (
        <div className='loading'>
            <div className='circle'></div>
        </div>
    );
};

export default Loading;
