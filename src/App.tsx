import React from 'react';
import { ToastContainer } from 'react-toastify';

// css
import './App.sass';

// components
import Loading from '@/components/Loading/index';

// hook
import { useAppSelector } from './hook/store';

type AppProps = {
    children: JSX.Element | JSX.Element[]
}

const App: React.FC<AppProps> = ({ children }) => {
    const storeGetters = useAppSelector(state => {
        return {
            isLoading: state.loading.isLoading
        };
    });

    return (
        <div className='App'>
            {children}

            { storeGetters.isLoading && <Loading></Loading> }
            <ToastContainer></ToastContainer>
        </div>
    );
};

export default App;
