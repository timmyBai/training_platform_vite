import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import RouterWaiter, { OnRouteBeforeType, RoutesItemType } from 'react-router-waiter';

// hook
import { useAppSelector } from '@/hook/store';

// utils
import { getPageTitle } from '@/utils/getPageTitle';

// components
import Layout from '@/layout';

export interface RoutesTypeNew extends RoutesItemType {
    alwaysShow?: boolean
    hidden?: boolean
    children?: RoutesTypeNew[]
}

type RoutersProps = {
    children: JSX.Element | JSX.Element[]
};

// base routes
export let constantRoutes: RoutesTypeNew[] = [
    {
        path: '/',
        redirect: '/home/',
        hidden: true
    },
    {
        path: '/home',
        component: () => import('@/views/home/index'),
        index: true,
        meta: {
            title: '首頁'
        },
        hidden: true
    },
    {
        path: '/login',
        component: () => import('@/views/login/index'),
        meta: {
            title: '登入'
        },
        hidden: true
    },
    {
        path: '/redirect',
        component: () => import('@/views/redirect/index'),
        meta: {
            title: '重新導向'
        },
        hidden: true
    },

    {
        path: '/404',
        component: () => import('@/views/errorPage/404'),
        meta: {
            title: '404'
        },
        hidden: true
    },
    {
        path: '*',
        redirect: '/404',
        hidden: true
    }
];

/**
 * permission routes
 * counselor role 輔導員
 * owner: 業主
 */
export const asyncRoutes: RoutesTypeNew[] = [
    {
        path: '/studentList',
        element: <Layout />,
        children: [
            {
                path: '',
                component: () => import('@/views/studentList/index'),
                meta: {
                    title: '學員清單',
                    roles: ['業主', '物理治療師', '復健師', '體適能指導員']
                }
            }
        ]
    },
    {
        path: '/studentList/add',
        element: <Layout />,
        children: [
            {
                path: '',
                component: () => import('@/views/studentList/addStudentInfo/index'),
                meta: {
                    title: '新增學員資料',
                    roles: ['業主', '物理治療師', '復健師', '體適能指導員']
                }
            }
        ],
        hidden: true
    },
    {
        path: '/bodyCompositionAndPhysicalFitness',
        element: <Layout />,
        children: [
            {
                path: '',
                component: () => import('@/views/bodyCompositionAndPhysicalFitness/index'),
                meta: {
                    title: '身體組成與<br />體適能評價',
                    roles: ['物理治療師', '復健師', '體適能指導員'],
                    visableRoutes: false
                }
            }
        ]
    },
    {
        path: '/exercisePrescription',
        element: <Layout />,
        children: [
            {
                path: '',
                component: () => import('@/views/exercisePrescription/index'),
                meta: {
                    title: '運動處方',
                    roles: ['物理治療師', '復健師', '體適能指導員'],
                    visableRoutes: false
                }
            }
        ]
    },
    {
        path: '/instrumentSettings',
        element: <Layout />,
        children: [
            {
                path: '',
                component: () => import('@/views/instrumentSettings/index'),
                meta: {
                    title: '儀器設定值',
                    roles: ['物理治療師', '復健師', '體適能指導員'],
                    visableRoutes: false
                }
            }
        ]
    },
    {
        path: '/trainingSchedule',
        element: <Layout />,
        children: [
            {
                path: '',
                component: () => import('@/views/trainingSchedule/index'),
                meta: {
                    title: '訓練排程',
                    roles: ['物理治療師', '復健師', '體適能指導員'],
                    visableRoutes: false
                }
            }
        ]
    },
    {
        path: '/inspectionReport',
        element: <Layout />,
        children: [
            {
                path: '',
                component: () => import('@/views/inspectionReport/index'),
                meta: {
                    title: '檢視報表',
                    roles: ['物理治療師', '復健師', '體適能指導員'],
                    visableRoutes: false
                }
            }
        ]
    },
    {
        path: '/print',
        element: <Layout />,
        children: [
            {
                path: '',
                component: () => import('@/views/print/index'),
                meta: {
                    title: '列印',
                    roles: ['物理治療師', '復健師', '體適能指導員'],
                    visableRoutes: false
                }
            }
        ]
    },
    {
        path: '/ownerList',
        element: <Layout />,
        children: [
            {
                path: '',
                component: () => import('@/views/ownerList/index'),
                meta: {
                    title: '業主基本資料', roles: ['業主']
                }
            }
        ]
    },
    {
        path: '/counselorList',
        element: <Layout />,
        children: [
            {
                path: '',
                component: () => import('@/views/counselorList/index'),
                meta: {
                    title: '輔導員清單', roles: ['業主']
                }
            }
        ]
    },
    {
        path: '/deviceList',
        element: <Layout />,
        children: [
            {
                path: '',
                component: () => import('@/views/deviceList/index'),
                meta: {
                    title: '設備清單', roles: ['業主']
                }
            }
        ]
    },
    {
        path: '/logout',
        element: <Layout />,
        children: [
            {
                path: '',
                component: () => import('@/views/logout/index'),
                meta: {
                    title: '登出'
                }
            }
        ]
    }
];

let tempRoutes: RoutesTypeNew[] = [];

const Router: React.FC<RoutersProps> = ({ children }) => {
    const storeGetters = useAppSelector((state) => {
        return {
            routes: state.permission.routes,
            addRoutes: state.permission.addRoutes
        };
    });

    const onRouteBefore: OnRouteBeforeType = ({ meta }) => {
        document.title = getPageTitle(meta.title);

        tempRoutes = constantRoutes;
        constantRoutes = tempRoutes.concat(storeGetters.addRoutes);
    };
    
    return (
        <BrowserRouter basename={import.meta.env.VITE_APP_ROUTE_BASE_URL}>
            <RouterWaiter routes={constantRoutes} onRouteBefore={onRouteBefore}></RouterWaiter>
            {children}
        </BrowserRouter>
    );
};

export default Router;