import React from 'react';

// css
import './index.sass';

// package
import packages from '@/../package.json';

const Footer: React.FC = () => {
    const date = new Date();

    return (
        <div className='footer'>
            <div className='container mx-auto'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                        <div className='card'>
                            {
                                date.getFullYear() === 2023 ? (
                                    <p>Copyright EMG technology © {date.getFullYear()} v{packages.version}</p>
                                ) : (
                                    <p>Copyright EMG technology © 2023 - {date.getFullYear()} v{packages.version}</p>
                                )
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Footer;