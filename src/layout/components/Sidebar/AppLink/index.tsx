import React, { Fragment, useContext, useEffect } from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import parse from 'html-react-parser';

// css
import './index.sass';

// context
import MenuContext from '@/components/Menu/MenuContext';

type AppLinkProps = {
    to: string;
    title: string;
    isNest: boolean;
}

const AppLink: React.FC<AppLinkProps> = ({ to, title, isNest }) => {
    const menuContext = useContext(MenuContext);
    const location = useLocation();

    const handleMenuActive = () => {
        if (menuContext.defaultRoute === to) {
            return (
                <NavLink className={isNest ? 'nestLink' : 'link'} to={to}>
                    <span className='linkTitle active'>{parse(title)}</span>
                </NavLink>
            );
        }
        else {
            return (
                <NavLink className={isNest ? 'nestLink' : 'link'} to={to}>
                    <span className='linkTitle'>{parse(title)}</span>
                </NavLink>
            );
        }
    };

    useEffect(() => {
        handleMenuActive();
    }, [location.pathname]);

    return (
        <Fragment>
            {handleMenuActive()}
        </Fragment>
    );
};

AppLink.defaultProps = {
    to: '',
    title: '',
    isNest: false
};

export default AppLink;
