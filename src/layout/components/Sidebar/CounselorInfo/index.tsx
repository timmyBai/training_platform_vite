import React from 'react';

// css
import './index.sass';

// hook
import { useAppSelector } from '@/hook/store';

// images
import user_logo from '@/assets/images/user_photo.svg';

// 輔導員資訊
const CounselorInfo: React.FC = () => {
    const storeGetters = useAppSelector((state) => {
        return {
            imageUrl: state.user.imageUrl,
            name: state.user.name,
            gender: state.user.gender,
            age: state.user.age,
            role: state.user.role
        };
    });

    return (
        <div className='counselorInfo'>
            <div className='flex flex-wrap'>
                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                    <div className='card'>
                        <img className='user_photo' src={storeGetters.imageUrl || user_logo} />
                        <h2 className='user_role'>{storeGetters.role}</h2>
                        <p className='user_introduction'>
                            <span>{storeGetters.name}</span>
                            <span>{storeGetters.gender}</span>
                            <span>{storeGetters.age}歲</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CounselorInfo;