import React, { Fragment } from 'react';

// css
import './index.sass';

// router
import { RoutesTypeNew } from '@/router/index';

// utils
import { isExternal } from '@/utils/validate';

// components
import MenuItem from '@/components/Menu/MenuItem';
import SubMenu from '@/components/Menu/SubMenu';
import AppLink from '../AppLink/index';

let onlyOneChild: any = null;

const hasOneShowingChild = (children: RoutesTypeNew[], parent: any) => {
    const showingChildren = children.filter(item => {
        if (item.hidden) {
            return false;
        }
        else {
            onlyOneChild = item;
            return true;
        }
    });

    if (showingChildren.length === 1) {
        return true;
    }

    if (showingChildren.length === 0) {
        onlyOneChild = { ...parent, path: '', noShowingChildren: true };
        return true;
    }
    
    return false;
};

const resolvePath = (routePath: string, basePath: string) => {
    if (isExternal(routePath)) {
        return routePath;
    }

    if (isExternal(basePath)) {
        return basePath;
    }
    
    if (routePath.indexOf('/') !== -1) {
        return basePath + routePath;
    }
    else {
        return basePath + '/' + routePath;
    }
};

type SidebarItemProps = {
    basePath: string;
    item: RoutesTypeNew;
    isNest: boolean
}

const SidebarItem: React.FC<SidebarItemProps> = ({ basePath, item, isNest }) => {
    if (!item.hidden) {
        if (item.children && hasOneShowingChild(item.children, item) && (!onlyOneChild.children || onlyOneChild.noShowingChildren) && !item.alwaysShow) {
            if (onlyOneChild.meta && onlyOneChild.meta.visableRoutes === undefined) {
                return (
                    <MenuItem>
                        <AppLink
                            to={resolvePath(onlyOneChild.path, basePath)}
                            title={onlyOneChild.meta.title}
                            isNest={isNest}
                        ></AppLink>
                    </MenuItem>
                );
            }
            else {
                if (onlyOneChild.meta && onlyOneChild.meta.visableRoutes === true) {
                    return (
                        <MenuItem>
                            <AppLink
                                to={resolvePath(onlyOneChild.path, basePath)}
                                title={onlyOneChild.meta.title}
                                isNest={isNest}
                            ></AppLink>
                        </MenuItem>
                    );
                }
            }
        }
        else {
            if (item.meta?.title && item.children) {
                return (
                    <SubMenu title={item.meta.title}>
                        {
                            item.children.map((child: any) => {
                                return (
                                    <SidebarItem basePath={resolvePath(child.path, basePath)} item={child} isNest={true} key={child.path}></SidebarItem>
                                );
                            })
                        }
                    </SubMenu>
                );
            }
        }
    }

    return <Fragment></Fragment>;
};

export default SidebarItem;