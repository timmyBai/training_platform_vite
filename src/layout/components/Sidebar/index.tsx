import React from 'react';

// css
import './index.sass';

// hook
import { useAppSelector } from '@/hook/store';

// components
import CounselorInfo from './CounselorInfo';
import Menu from '@/components/Menu/Menu';
import SidebarItem from './SidebarItem';

const Sidebar: React.FC = () => {
    const storeGetters = useAppSelector((state) => {
        return {
            routes: state.permission.routes
        };
    });

    return (
        <div className='sidebar'>
            <div className='sidebar_container'>
                <CounselorInfo></CounselorInfo>
                
                <div className='scroll'>
                    <Menu>
                        {
                            storeGetters.routes.map((route: any) => {
                                return (
                                    <SidebarItem item={route} basePath={route.path} isNest={false} key={route.path}></SidebarItem>
                                );
                            })
                        }
                    </Menu>
                </div>
            </div>
        </div>
    );
};

export default Sidebar;