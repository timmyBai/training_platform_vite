import React, { useEffect, useState } from 'react';
import { Outlet, useLocation } from 'react-router-dom';
import 'animate.css';

// css
import './index.sass';

const AppMain: React.FC = () => {
    const location = useLocation();
    const [animated, setAnimated] = useState<string>('');

    useEffect(() => {
        const timeOut = setTimeout(() => {
            setAnimated('animate__fadeIn');
            clearTimeout(timeOut);
        }, 100);

        setAnimated('');
    }, [location.pathname]);

    return (
        <div className={`appMain animate__animated ${animated}`}>
            <Outlet></Outlet>
        </div>
    );
};

export default AppMain;