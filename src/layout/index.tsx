import React, { useState } from 'react';

// css
import './index.sass';

// components
import AppMain from './components/AppMain';
import Sidebar from './components/Sidebar';
import StudentDialogContext from '@/context/StudentDialogContext';
import Footer from './components/Footer';

const Layout: React.FC = () => {
    const [studentDialogShow, setStudentDialogShow] = useState<boolean>(false);

    const handleStudentDialogShow = (isShow: boolean) => {
        setStudentDialogShow(isShow);
    };

    return (
        <div className='appWapper'>
            <Sidebar></Sidebar>
            
            <StudentDialogContext.Provider value={{
                isShow: studentDialogShow,
                handleStudentDialogShow: handleStudentDialogShow
            }}>
                <div className='main_container'>
                    <AppMain></AppMain>
                    
                    <Footer></Footer>
                </div>
            </StudentDialogContext.Provider>
        </div>
    );
};

export default Layout;