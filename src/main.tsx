import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import 'normalize.css';
import 'react-toastify/dist/ReactToastify.css';
import 'nprogress/nprogress.css';

// css
import '@/styles/index.sass';

// js
// import reportWebVitals from './reportWebVitals';
import store from './store';

// components
import App from './App';
import Permission from './Permission';
import Router from './router';

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <Provider store={store}>
            <App>
                <Router>
                    <Permission></Permission>
                </Router>
            </App>
        </Provider>
    </React.StrictMode>
);

// reportWebVitals();
