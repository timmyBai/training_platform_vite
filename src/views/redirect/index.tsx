import React, { useEffect, Fragment, ReactNode } from 'react';
import { useNavigate } from 'react-router-dom';

// store
import { getUserInfo } from '@/store/modules/user';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

const Redirect: React.FC = (): ReactNode => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const storeGetters = useAppSelector((state) => {
        return {
            addRoutes: state.permission.addRoutes
        };
    });

    const handleUserIntroduction = async() => {
        dispatch(getUserInfo()).unwrap().then(async (res) => {
            await dispatch({ type: 'user/changeUserInfo', payload: res.data });

            await dispatch({ type: 'permission/generateRoutes', payload: res.data._role });
            
            // 如果身分是 '業主'
            if (res.data.identity === 'A') {
                navigate('/ownerList/');
            }
            else {
                // 否則為 '物理治療師'、'復健師'、'體適能指導員'
                navigate('/studentList/');
            }
        }).catch(() => {
            dispatch({ type: 'user/resetToken' });
            navigate('/home');
        });
    };

    useEffect(() => {
        handleUserIntroduction();
    }, [storeGetters.addRoutes]);

    return (
        <Fragment></Fragment>
    );
};

export default Redirect;