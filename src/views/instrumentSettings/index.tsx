import React, { useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import validate from 'validate.js';

// css
import './index.sass';

// api
import { instrumentSettingsSportsPerformanceList, instrumentSettingsSportsPerformanceReplace } from '@/api/instrumentSettings';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

// utils
import { axiosErrorCode } from '@/utils/request';

// images
import btn_edit from '@/assets/images/btn_edit.svg';
import machinaryCodeOne from '@/assets/images/machinaryCode/machinaryCodeOne.png';
import machinaryCodeTwo from '@/assets/images/machinaryCode/machinaryCodeTwo.png';
import machinaryCodeThree from '@/assets/images/machinaryCode/machinaryCodeThree.png';
import machinaryCodeFour from '@/assets/images/machinaryCode/machinaryCodeFour.png';
import machinaryCodeFive from '@/assets/images/machinaryCode/machinaryCodeFive.png';
import machinaryCodeSix from '@/assets/images/machinaryCode/machinaryCodeSix.png';
import machinaryCodeSeven from '@/assets/images/machinaryCode/machinaryCodeSeven.png';
import machinaryCodeEight from '@/assets/images/machinaryCode/machinaryCodeEight.png';
import machinaryCodeNine from '@/assets/images/machinaryCode/machinaryCodeNine.png';
import machinaryCodeTen from '@/assets/images/machinaryCode/machinaryCodeTen.png';

// components
import StudentInfoBar from '@/components/StudentInfoBar';
import PageTitleBar from '@/components/PageTitleBar';
import TdArrowDown from '@/components/Table/TdArrowDown';
import Dialog from '@/components/Dialog';

const InstrumentSettings: React.FC = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [machinaryCodeOneGroup, setMachinaryCodeOneGroup] = useState<InstrumentSettingsMachinaryCodeOneGroup>();
    const [machinaryCodeTwoGroup, setMachinaryCodeTwoGroup] = useState<InstrumentSettingsMachinaryCodeTwoGroup>();
    const [machinaryCodeThreeGroup, setMachinaryCodeThreeGroup] = useState<InstrumentSettingsMachinaryCodeThreeGroup>();
    const [machinaryCodeFourGroup, setMachinaryCodeFourGroup] = useState<InstrumentSettingsMachinaryCodeFourGroup>();
    const [machinaryCodeFiveGroup, setMachinaryCodeFiveGroup] = useState<InstrumentSettingsMachinaryCodeFiveGroup>();
    const [machinaryCodeSixGroup, setMachinaryCodeSixGroup] = useState<InstrumentSettingsMachinaryCodeSixGroup>();
    const [machinaryCodeSevenGroup, setMachinaryCodeSevenGroup] = useState<InstrumentSettingsMachinaryCodeSevenGroup>();
    const [machinaryCodeEightGroup, setMachinaryCodeEightGroup] = useState<InstrumentSettingsMachinaryCodeEightGroup>();
    const [machinaryCodeNineGroup, setMachinaryCodeNineGroup] = useState<InstrumentSettingsMachinaryCodeNineGroup>();
    const [machinaryCodeTenGroup, setMachinaryCodeTenGroup] = useState<InstrumentSettingsMachinaryCodeTenGroup>();

    const instrumentSettingsFormRef = useRef<HTMLFormElement>(null);
    const [visibleInstrumentSettingsDialog, setVisibleInstrumentSettingsDialog] = useState<boolean>(false);
    const [instrumentSettingsMark, setIstrumentSettingsMark] = useState<string>('');
    const [instrumentSettingsMachinaryCode, setInstrumentSettingsMachinaryCode] = useState<number | undefined>(0);
    const [instrumentSettingsTrainingDate, setInstrumentSettingsTrainingDate] = useState<string | undefined>('');
    const [instrumentSettingsMarkMessage, setInstrumentSettingsMarkMessage] = useState<string>('');
    const formInstrumentSettingsMark = {
        'mark': {
            length: {
                maximum: 150,
                message: '備註最大 150 字'
            }
        }
    };

    const [visibleInfoDialog, setVisibleInfoDialog] = useState<boolean>(false); // 資訊彈窗
    const [visibleInfoDialogTitle, setVisibleInfoDialogTitle] = useState<string>('');
    const [visibleInfoDialogMessage, setVisibleInfoDialogMessage] = useState<string>('');

    const storeGetters = useAppSelector((state) => {
        return {
            studentId: state.studentInfo.studentId
        };
    });

    const handleInstrumentSettingsSportsPerformanceList = () => {
        instrumentSettingsSportsPerformanceList(storeGetters.studentId).then((res) => {
            const { data } = res.data;

            setMachinaryCodeOneGroup(data.machinaryCodeOneGroup);
            setMachinaryCodeTwoGroup(data.machinaryCodeTwoGroup);
            setMachinaryCodeThreeGroup(data.machinaryCodeThreeGroup);
            setMachinaryCodeFourGroup(data.machinaryCodeFourGroup);
            setMachinaryCodeFiveGroup(data.machinaryCodeFiveGroup);
            setMachinaryCodeSixGroup(data.machinaryCodeSixGroup);
            setMachinaryCodeSevenGroup(data.machinaryCodeSevenGroup);
            setMachinaryCodeEightGroup(data.machinaryCodeEightGroup);
            setMachinaryCodeNineGroup(data.machinaryCodeNineGroup);
            setMachinaryCodeTenGroup(data.machinaryCodeTenGroup);
        }).catch((error) => {
            const errorCode = axiosErrorCode<InstrumentSettingsListErrorCodeType>(error);

            switch (errorCode) {
                case '':
                    break;
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                default:
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    
    const handleInstrumentSettingsSportsPerformance = () => {
        const isValid = inspectInstrumentSettings();

        if (isValid) {
            const data: InstrumentSettingsSportsPerformanceRequestType = {
                mark: instrumentSettingsMark,
                trainingDate: instrumentSettingsTrainingDate
            };

            instrumentSettingsSportsPerformanceReplace(storeGetters.studentId, instrumentSettingsMachinaryCode, data).then(() => {
                closeVisibleInstrumentSettingsDialog();

                handleInstrumentSettingsSportsPerformanceList();
                setVisibleInfoDialog(true);
                setVisibleInfoDialogTitle('資訊');
                setVisibleInfoDialogMessage('更新成功');
            }).catch((error) => {
                const errorCode = axiosErrorCode<InstrumentSettingsSportsPerformanceErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('格式錯誤');
                        break;
                    case 'StudentDoesNotExist':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('查無學員');
                        break;
                    case 'DataDoesNotExist':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('查無資料');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    const inspectInstrumentSettings = () => {
        const isValid = validate(instrumentSettingsFormRef.current, formInstrumentSettingsMark);

        if (isValid !== undefined) {
            if (isValid.mark) {
                setInstrumentSettingsMarkMessage(isValid.mark[0].replace('Mark ', ''));
            }
            else {
                setInstrumentSettingsMarkMessage('');
            }

            return false;
        }
        else {
            setInstrumentSettingsMarkMessage('');

            return true;
        }
    };

    const handleIstrumentSettingsMark = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setIstrumentSettingsMark(e.target.value);
        inspectInstrumentSettings();
    };

    const showVisibleInstrumentSettingsDialog = (machinaryCode: number | undefined, trainingDate: string | undefined) => {
        setVisibleInstrumentSettingsDialog(true);

        setInstrumentSettingsMachinaryCode(machinaryCode);
        setInstrumentSettingsTrainingDate(trainingDate);
    };

    const closeVisibleInstrumentSettingsDialog = () => {
        setVisibleInstrumentSettingsDialog(false);

        setIstrumentSettingsMark('');
        setInstrumentSettingsMachinaryCode(0);
        setInstrumentSettingsTrainingDate('');
    };

    const closeInfoDialog = () => {
        setVisibleInfoDialog(false);
        setVisibleInfoDialogTitle('');
        setVisibleInfoDialogMessage('');
    };

    useEffect(() => {
        handleInstrumentSettingsSportsPerformanceList();
    }, []);

    return (
        <div className='instrumentSettings'>
            <StudentInfoBar></StudentInfoBar>

            <PageTitleBar title='儀器設定值'></PageTitleBar>

            <div className='instrument_settings_info'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                        <div className='card'>
                            <table>
                                <thead>
                                    <tr>
                                        <th colSpan={2}>
                                            <p>項目</p>
                                        </th>

                                        <th>
                                            <p>最適阻力等級</p>
                                        </th>

                                        <th>
                                            <p>活動範圍</p>
                                        </th>

                                        <th>
                                            <p>最大運動表現</p>
                                        </th>

                                        <th>
                                            <p>備註</p>
                                        </th>

                                        <th>
                                            <p>歷史紀錄</p>
                                        </th>

                                        <th>
                                            <p>編輯</p>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {/* 機器編號一 */}
                                    {
                                        machinaryCodeOneGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeOne} />
                                                </td>

                                                <td>{machinaryCodeOneGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeOneGroup?.level}</td>
                                                <td>{machinaryCodeOneGroup?.maxRange}</td>
                                                <td>{machinaryCodeOneGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeOneGroup?.mark}</td>

                                                <TdArrowDown dropDownClassName='machinaryCodeOneGroupChildren' data={machinaryCodeOneGroup?.children}></TdArrowDown>

                                                <td className='icon_btn_edit' onClick={() => showVisibleInstrumentSettingsDialog(machinaryCodeOneGroup?.machinaryCode, machinaryCodeOneGroup?.trainingDate)}>
                                                    <img src={btn_edit} />
                                                </td>
                                            </tr>
                                        )
                                    }
                                    {
                                        machinaryCodeOneGroup?.children.length !== 0 && (
                                            machinaryCodeOneGroup?.children.map((item, index) => {
                                                return (
                                                    <tr className='machinaryCodeOneGroupChildren' key={index}>
                                                        <td colSpan={2}>{item.trainingDate}</td>
                                                        <td>{item.level}</td>
                                                        <td>{item.maxRange}</td>
                                                        <td>{item.power}</td>
                                                        <td className='mark'>{item.mark}</td>
                                                    </tr>
                                                );
                                            })
                                        )
                                    }


                                    {/* 機器編號二 */}
                                    {
                                        machinaryCodeTwoGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeTwo} />
                                                </td>
                                                <td>{machinaryCodeTwoGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeTwoGroup?.level}</td>
                                                <td>{machinaryCodeTwoGroup?.maxRange}</td>
                                                <td>{machinaryCodeTwoGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeTwoGroup?.mark}</td>

                                                <TdArrowDown dropDownClassName='machinaryCodeTwoGroupChildren' data={machinaryCodeTwoGroup?.children}></TdArrowDown>

                                                <td className='icon_btn_edit' onClick={() => showVisibleInstrumentSettingsDialog(machinaryCodeTwoGroup?.machinaryCode, machinaryCodeTwoGroup?.trainingDate)}>
                                                    <img src={btn_edit} />
                                                </td>
                                            </tr>
                                        )
                                    }
                                    {
                                        machinaryCodeTwoGroup?.children.length !== 0 && (
                                            machinaryCodeTwoGroup?.children.map((item, index) => {
                                                return (
                                                    <tr className='machinaryCodeTwoGroupChildren' key={index}>
                                                        <td colSpan={2}>{item.trainingDate}</td>
                                                        <td>{item.level}</td>
                                                        <td>{item.maxRange}</td>
                                                        <td>{item.power}</td>
                                                        <td className='mark'>{item.mark}</td>
                                                    </tr>
                                                );
                                            })
                                        )
                                    }


                                    {/* 機器編號三 */}
                                    {
                                        machinaryCodeThreeGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeThree} />
                                                </td>
                                                <td>{machinaryCodeThreeGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeThreeGroup?.level}</td>
                                                <td>{machinaryCodeThreeGroup?.maxRange}</td>
                                                <td>{machinaryCodeThreeGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeThreeGroup?.mark}</td>

                                                <TdArrowDown dropDownClassName='machinaryCodeThreeGroupChildren' data={machinaryCodeThreeGroup?.children}></TdArrowDown>

                                                <td className='icon_btn_edit' onClick={() => showVisibleInstrumentSettingsDialog(machinaryCodeThreeGroup?.machinaryCode, machinaryCodeThreeGroup?.trainingDate)}>
                                                    <img src={btn_edit} />
                                                </td>
                                            </tr>
                                        )
                                    }
                                    {
                                        machinaryCodeThreeGroup?.children.length !== 0 && (
                                            machinaryCodeThreeGroup?.children.map((item, index) => {
                                                return (
                                                    <tr className='machinaryCodeThreeGroupChildren' key={index}>
                                                        <td colSpan={2}>{item.trainingDate}</td>
                                                        <td>{item.level}</td>
                                                        <td>{item.maxRange}</td>
                                                        <td>{item.power}</td>
                                                        <td className='mark'>{item.mark}</td>
                                                    </tr>
                                                );
                                            })
                                        )
                                    }


                                    {/* 機器編號四 */}
                                    {
                                        machinaryCodeFourGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeFour} />
                                                </td>
                                                <td>{machinaryCodeFourGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeFourGroup?.level}</td>
                                                <td>{machinaryCodeFourGroup?.maxRange}</td>
                                                <td>{machinaryCodeFourGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeFourGroup?.mark}</td>

                                                <TdArrowDown dropDownClassName='machinaryCodeFourGroupChildren' data={machinaryCodeFourGroup?.children}></TdArrowDown>

                                                <td className='icon_btn_edit' onClick={() => showVisibleInstrumentSettingsDialog(machinaryCodeFourGroup?.machinaryCode, machinaryCodeFourGroup?.trainingDate)}>
                                                    <img src={btn_edit} />
                                                </td>
                                            </tr>
                                        )
                                    }
                                    {
                                        machinaryCodeFourGroup?.children.length !== 0 && (
                                            machinaryCodeFourGroup?.children.map((item, index) => {
                                                return (
                                                    <tr className='machinaryCodeFourGroupChildren' key={index}>
                                                        <td colSpan={2}>{item.trainingDate}</td>
                                                        <td>{item.level}</td>
                                                        <td>{item.maxRange}</td>
                                                        <td>{item.power}</td>
                                                        <td className='mark'>{item.mark}</td>
                                                    </tr>
                                                );
                                            })
                                        )
                                    }


                                    {/* 機器編號五 */}
                                    {
                                        machinaryCodeFiveGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeFive} />
                                                </td>
                                                <td>{machinaryCodeFiveGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeFiveGroup?.level}</td>
                                                <td>{machinaryCodeFiveGroup?.maxRange}</td>
                                                <td>{machinaryCodeFiveGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeFiveGroup?.mark}</td>

                                                <TdArrowDown dropDownClassName='machinaryCodeFiveGroupChildren' data={machinaryCodeFiveGroup?.children}></TdArrowDown>

                                                <td className='icon_btn_edit' onClick={() => showVisibleInstrumentSettingsDialog(machinaryCodeFiveGroup?.machinaryCode, machinaryCodeFiveGroup?.trainingDate)}>
                                                    <img src={btn_edit} />
                                                </td>
                                            </tr>
                                        )
                                    }
                                    {
                                        machinaryCodeFiveGroup?.children.length !== 0 && (
                                            machinaryCodeFiveGroup?.children.map((item, index) => {
                                                return (
                                                    <tr className='machinaryCodeFiveGroupChildren' key={index}>
                                                        <td colSpan={2}>{item.trainingDate}</td>
                                                        <td>{item.level}</td>
                                                        <td>{item.maxRange}</td>
                                                        <td>{item.power}</td>
                                                        <td className='mark'>{item.mark}</td>
                                                    </tr>
                                                );
                                            })
                                        )
                                    }

                                    {/* 機器編號六 */}
                                    {
                                        machinaryCodeSixGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeSix} />
                                                </td>
                                                <td>{machinaryCodeSixGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeSixGroup?.level}</td>
                                                <td>{machinaryCodeSixGroup?.maxRange}</td>
                                                <td>{machinaryCodeSixGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeSixGroup?.mark}</td>

                                                <TdArrowDown dropDownClassName='machinaryCodeSixGroupChildren' data={machinaryCodeSixGroup?.children}></TdArrowDown>

                                                <td className='icon_btn_edit' onClick={() => showVisibleInstrumentSettingsDialog(machinaryCodeSixGroup?.machinaryCode, machinaryCodeSixGroup?.trainingDate)}>
                                                    <img src={btn_edit} />
                                                </td>
                                            </tr>
                                        )
                                    }
                                    {
                                        machinaryCodeSixGroup?.children.length !== 0 && (
                                            machinaryCodeSixGroup?.children.map((item, index) => {
                                                return (
                                                    <tr className='machinaryCodeSixGroupChildren' key={index}>
                                                        <td colSpan={2}>{item.trainingDate}</td>
                                                        <td>{item.level}</td>
                                                        <td>{item.maxRange}</td>
                                                        <td>{item.power}</td>
                                                        <td className='mark'>{item.mark}</td>
                                                    </tr>
                                                );
                                            })
                                        )
                                    }


                                    {/* 機器編號七 */}
                                    {
                                        machinaryCodeSevenGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeSeven} />
                                                </td>
                                                <td>{machinaryCodeSevenGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeSevenGroup?.level}</td>
                                                <td>{machinaryCodeSevenGroup?.maxRange}</td>
                                                <td>{machinaryCodeSevenGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeSevenGroup?.mark}</td>

                                                <TdArrowDown dropDownClassName='machinaryCodeSevenGroupChildren' data={machinaryCodeSevenGroup?.children}></TdArrowDown>

                                                <td className='icon_btn_edit' onClick={() => showVisibleInstrumentSettingsDialog(machinaryCodeSevenGroup?.machinaryCode, machinaryCodeSevenGroup?.trainingDate)}>
                                                    <img src={btn_edit} />
                                                </td>
                                            </tr>
                                        )
                                    }
                                    {
                                        machinaryCodeSevenGroup?.children.length !== 0 && (
                                            machinaryCodeSevenGroup?.children.map((item, index) => {
                                                return (
                                                    <tr className='machinaryCodeSevenGroupChildren' key={index}>
                                                        <td colSpan={2}>{item.trainingDate}</td>
                                                        <td>{item.level}</td>
                                                        <td>{item.maxRange}</td>
                                                        <td>{item.power}</td>
                                                        <td className='mark'>{item.mark}</td>
                                                    </tr>
                                                );
                                            })
                                        )
                                    }


                                    {/* 機器編號八 */}
                                    {
                                        machinaryCodeEightGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeEight} />
                                                </td>
                                                <td>{machinaryCodeEightGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeEightGroup?.level}</td>
                                                <td>{machinaryCodeEightGroup?.maxRange}</td>
                                                <td>{machinaryCodeEightGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeEightGroup?.mark}</td>

                                                <TdArrowDown dropDownClassName='machinaryCodeEightGroupChildren' data={machinaryCodeEightGroup?.children}></TdArrowDown>

                                                <td className='icon_btn_edit' onClick={() => showVisibleInstrumentSettingsDialog(machinaryCodeEightGroup?.machinaryCode, machinaryCodeEightGroup?.trainingDate)}>
                                                    <img src={btn_edit} />
                                                </td>
                                            </tr>
                                        )
                                    }
                                    {
                                        machinaryCodeEightGroup?.children.length !== 0 && (
                                            machinaryCodeEightGroup?.children.map((item, index) => {
                                                return (
                                                    <tr className='machinaryCodeEightGroupChildren' key={index}>
                                                        <td colSpan={2}>{item.trainingDate}</td>
                                                        <td>{item.level}</td>
                                                        <td>{item.maxRange}</td>
                                                        <td>{item.power}</td>
                                                        <td className='mark'>{item.mark}</td>
                                                    </tr>
                                                );
                                            })
                                        )
                                    }


                                    {/* 機器編號九 */}
                                    {
                                        machinaryCodeNineGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeNine} />
                                                </td>
                                                <td>{machinaryCodeNineGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeNineGroup?.level}</td>
                                                <td>{machinaryCodeNineGroup?.maxRange}</td>
                                                <td>{machinaryCodeNineGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeNineGroup?.mark}</td>

                                                <TdArrowDown dropDownClassName='machinaryCodeNineGroupChildren' data={machinaryCodeNineGroup?.children}></TdArrowDown>

                                                <td className='icon_btn_edit' onClick={() => showVisibleInstrumentSettingsDialog(machinaryCodeNineGroup?.machinaryCode, machinaryCodeNineGroup?.trainingDate)}>
                                                    <img src={btn_edit} />
                                                </td>
                                            </tr>
                                        )
                                    }
                                    {
                                        machinaryCodeNineGroup?.children.length !== 0 && (
                                            machinaryCodeNineGroup?.children.map((item, index) => {
                                                return (
                                                    <tr className='machinaryCodeNineGroupChildren' key={index}>
                                                        <td colSpan={2}>{item.trainingDate}</td>
                                                        <td>{item.level}</td>
                                                        <td>{item.maxRange}</td>
                                                        <td>{item.power}</td>
                                                        <td className='mark'>{item.mark}</td>
                                                    </tr>
                                                );
                                            })
                                        )
                                    }


                                    {/* 機器編號十 */}
                                    {
                                        machinaryCodeTenGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeTen} />
                                                </td>
                                                <td>{machinaryCodeTenGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeTenGroup?.level}</td>
                                                <td>{machinaryCodeTenGroup?.maxRange}</td>
                                                <td>{machinaryCodeTenGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeTenGroup?.mark}</td>

                                                <TdArrowDown dropDownClassName='machinaryCodeTenGroupChildren' data={machinaryCodeNineGroup?.children}></TdArrowDown>

                                                <td className='icon_btn_edit' onClick={() => showVisibleInstrumentSettingsDialog(machinaryCodeTenGroup?.machinaryCode, machinaryCodeTenGroup?.trainingDate)}>
                                                    <img src={btn_edit} />
                                                </td>
                                            </tr>
                                        )
                                    }
                                    {
                                        machinaryCodeTenGroup?.children.length !== 0 && (
                                            machinaryCodeTenGroup?.children.map((item, index) => {
                                                return (
                                                    <tr className='machinaryCodeTenGroupChildren' key={index}>
                                                        <td colSpan={2}>{item.trainingDate}</td>
                                                        <td>{item.level}</td>
                                                        <td>{item.maxRange}</td>
                                                        <td>{item.power}</td>
                                                        <td className='mark'>{item.mark}</td>
                                                    </tr>
                                                );
                                            })
                                        )
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            {
                visibleInstrumentSettingsDialog && (
                    <Dialog className='visibleInstrumentSettingsDialog'>
                        <Dialog.Title title='編輯'></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <form ref={instrumentSettingsFormRef}>
                                    <div className='group'>
                                        <h6>備註</h6>
                                        <textarea className='mark' cols={30} rows={5} placeholder='請輸入備註' defaultValue={instrumentSettingsMark} name='mark' onChange={handleIstrumentSettingsMark}></textarea>
                                        <p>{instrumentSettingsMarkMessage}</p>
                                    </div>
                                </form>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnBtnYes' onClick={handleInstrumentSettingsSportsPerformance}>確定</button>
                            <button className='dialogBtnBtnNo' onClick={closeVisibleInstrumentSettingsDialog}>取消</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }


            {/* 資訊彈窗 */}
            {
                visibleInfoDialog && (
                    <Dialog className='visibleInfoDialog'>
                        <Dialog.Title title={visibleInfoDialogTitle}></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <p>{visibleInfoDialogMessage}</p>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnConfirm' onClick={closeInfoDialog}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }
        </div>
    );
};

export default InstrumentSettings;