import React, { useEffect, Fragment, ReactNode } from 'react';
import { useNavigate } from 'react-router-dom';

// hook
import { useAppDispatch } from '@/hook/store';

const Logout: React.FC = (): ReactNode => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    useEffect(() => {
        dispatch({ type: 'user/resetToken' });
        navigate('/home/');
    }, []);
    
    return <Fragment></Fragment>;
};

export default Logout;