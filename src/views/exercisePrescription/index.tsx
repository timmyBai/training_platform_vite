import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import validate from 'validate.js';
import moment from 'moment';
import gsap from 'gsap';
import CountUp from 'react-countup';

// css
import './index.sass';

// utils
import { axiosErrorCode } from '@/utils/request';
import {
    cardiorespiratoryFitnessSportsOptions,
    cardiorespiratoryFitnessExerciseIntensityOptions,
    cardiorespiratoryFitnessExerciseFrequencyOptions,
    cardiorespiratoryFitnessTrainingUnitMinuteOptions,
    muscleStrengthAndMuscleEnduranceExerciseIntensityOptions,
    muscleStrengthAndMuscleEnduranceExerciseFrequencyOptions,
    muscleStrengthAndMuscleEnduranceTrainingUnitGroupOptions,
    muscleStrengthAndMuscleEnduranceTrainingUnitNumberOptions,
    softnessSportsOptions,
    softnessExerciseIntensityOptions,
    softnessExerciseFrequencyOptions,
    softnessTrainingUnitMinuteOptions,
    balanceSportsOptions,
    balanceExerciseIntensityOptions,
    balanceExerciseFrequencyOptions,
    balanceTrainingUnitMinuteOptions
} from '@/utils/exercisePrescription';

// api
import {
    exercisePrescriptionList,
    exercisePrescriptionTrainingStage,
    exercisePrescriptionTrainingStageReplace,
    exercisePrescriptionCardiorespiratoryFitness,
    exercisePrescriptionMuscleStrengthAndMuscleEndurance,
    exercisePrescriptionSoftness,
    exercisePrescriptionBalance
} from '@/api/exercisePrescription';

// hook
import { useAppSelector } from '@/hook/store';

// components
import StudentInfoBar from '@/components/StudentInfoBar';
import PageTitleBar from '@/components/PageTitleBar';
import TdArrowDown from '@/components/Table/TdArrowDown';
import Dialog from '@/components/Dialog';
import Select from '@/components/Select';

// images
import btn_edit from '@/assets/images/btn_edit.svg';
import training_stage_anchor from '@/assets/images/training_stage_anchor.svg';
import muscleDistribution from '@/assets/images/muscleDistribution.svg'; // 肌肉分佈圖
import muscleDistributionLowerBody from '@/assets/images/muscleDistributionLowerBody.svg'; // 肌肉分佈圖-下肢
import muscleDistributionUpperBody from '@/assets/images/muscleDistributionUpperBody.svg'; // 肌肉分佈圖-上肢
import muscleDistributionUpperBodyAndLowerBody from '@/assets/images/muscleDistributionUpperBodyAndLowerBody.svg'; // 肌肉分佈圖-上肢與下肢
import muscleDistributionCore from '@/assets/images/muscleDistributionCore.svg'; // 肌肉分佈圖-核心
import muscleDistributionUpperBodyAndCore from '@/assets/images/muscleDistributionUpperBodyAndCore.svg'; // 肌肉分佈圖-上肢與核心
import muscleDistributionLowerBodyAndCore from '@/assets/images/muscleDistributionLowerBodyAndCore.svg'; // 肌肉分佈圖-下肢與核心
import muscleDistributionAll from '@/assets/images/muscleDistributionAll.svg'; // 肌肉分佈圖-全

const ExercisePrescription: React.FC = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [trainingFirstProgress, setTrainingFirstProgress] = useState<number>(0); // 運動處方-訓練階段(開始階段)
    const [trainingMiddleProgress, setTrainingMiddleProgress] = useState<number>(0); // 運動處方-訓練階段(改善階段)
    const [trainingLastProgress, setTrainingLastProgress] = useState<number>(0); // 運動處方-訓練階段(維持階段)

    const [trainingStageResponse, setTrainingStageResponse] = useState<ExercisePrescriptionTrainingStageDataType>(); // 運動處處方-訓練階段資料

    const [exercisePrescriptionCardiorespiratoryFitnessGroup, setExercisePrescriptionCardiorespiratoryFitnessGroup] = useState<ExercisePrescriptionCardiorespiratoryFitnessGroupType>(); // 運動處方-心肺適能群組清單
    const [exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup, setExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup] = useState<ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupType>(); // 運動處方-肌力與肌耐力群組清單
    const [exercisePrescriptionSoftnessGroup, setExercisePrescriptionSoftnessGroup] = useState<ExercisePrescriptionSoftnessGroupType>(); // 運動處方-柔軟度群組清單
    const [exercisePrescriptionBalanceGroup, setExercisePrescriptionBalanceGroup] = useState<ExercisePrescriptionBalanceGroupType>(); // 運動處方-平衡群組清單

    const trainingStageRef = useRef<HTMLFormElement>(null);
    const [visibleTrainingStageDialog, setVisibleTrainingStageDialog] = useState<boolean>(false); // 運動處方-訓練項目彈窗
    const [trainingFirstStage, setTrainingFirstStage] = useState<string | undefined>(''); // 運動處方-訓練項目設定開始階段
    const [trainingMiddleStage, setTrainingMiddleStage] = useState<string | undefined>(''); // 運動處方-訓練項目設定改善階段
    const [trainingStartDate, setTrainingStartDate] = useState<string | undefined>(''); // 運動處方-訓練項目設定起始日期
    const [trainingFirstStageMessage, setTrainingFirstStageMessage] = useState<string>(''); // 運動處方-訓練階段開始階段錯誤訊息
    const [trainingMiddleStageMessage, setTrainingMiddleStageMessage] = useState<string>(''); // 運動處方-訓練資料改善階段錯誤訊息
    const [trainingStartDateMessage, setTrainingStartDateMessage] = useState<string>(''); // 運動處方-訓練資料設定起始錯誤訊息

    // 運動處方-訓練階段檢查物件
    const trainingStageForm = {
        'firstStage': {
            presence: {
                message: '必須輸入開始階段週數'
            },
            format: {
                pattern: '^[0-9]+$',
                message: '請輸入整數'
            }
        },
        'middleStage': {
            presence: {
                message: '必須輸入改善階段週數'
            },
            format: {
                pattern: '^[0-9]+$',
                message: '請輸入整數'
            }
        },
        'startDate': {
            presence: {
                message: '必須設定起始日期'
            }
        }
    };

    const [visibleInfoDialog, setVisibleInfoDialog] = useState<boolean>(false); // 運動處方-資訊彈窗
    const [visibleInfoDialogTitle, setVisibleInfoDialogTitle] = useState<string>(''); // 運動處方-資訊彈窗標題
    const [visibleInfoDialogMessage, setVisibleInfoDialogMessage] = useState<string>(''); // 運動處方-資訊彈窗訊息

    const exercisePrescriptionCardiorespiratoryFitnessFormRef = useRef<HTMLFormElement>(null);
    const [visibleCardiorespiratoryFitnessDialog, setVisibleCardiorespiratoryFitnessDialog] = useState<boolean>(false); // 運動處方-心肺適能彈窗
    const [cardiorespiratoryFitnessExerciseIntensity, setCardiorespiratoryFitnessExerciseIntensity] = useState<string | undefined>(''); // 運動處方-心肺適能運動強度
    const [cardiorespiratoryFitnessExerciseFrequency, setCardiorespiratoryFitnessExerciseFrequency] = useState<string | undefined>(''); // 運動處方-心肺適能訓練頻率(天/週)
    const [cardiorespiratoryFitnessTrainingUnitMinute, setCrdiorespiratoryFitnessTrainingUnitMinute] = useState<string | undefined>(''); // 運動處方-心肺適能訓練頻率(分鐘/次)
    const [cardiorespiratoryFitnessSports, setCardiorespiratoryFitnessSports] = useState<string[] | undefined>([]); // 運動處方-心肺適能訓練項目
    const [cardiorespiratoryFitnessMark, setCardiorespiratoryFitnessMark] = useState<string | undefined>(''); // 運動處方-心肺適能備註
    const [cardiorespiratoryFitnessExerciseIntensityMessage, setCardiorespiratoryFitnessExerciseIntensityMessage] = useState<string>(''); // 運動處方-心肺適能運動強度錯誤訊息
    const [cardiorespiratoryFitnessExerciseFrequencyMessage, setCardiorespiratoryFitnessExerciseFrequencyMessage] = useState<string>(''); // 運動處方-心肺適能訓練頻率錯誤訊息
    const [cardiorespiratoryFitnessTrainingUnitMinuteMessage, setCardiorespiratoryFitnessTrainingUnitMinuteMessage] = useState<string>(''); // 運動處方-心肺適能訓練頻率(天/週)錯誤訊息
    const [cardiorespiratoryFitnessSportsMessage, setCardiorespiratoryFitnessSportsMessage] = useState<string>(''); // 運動處方-心肺適能訓練頻率(分鐘/次)錯誤訊息
    const [cardiorespiratoryFitnessMarkMessage, setCardiorespiratoryFitnessMarkMessage] = useState<string>(''); // 運動處方-心肺適能備註錯誤訊息
    const exercisePrescriptionCardiorespiratoryFitnessForm = {
        'sports': {
        },
        'exerciseIntensity': {
            presence: {
                message: '必須輸入運動強度'
            }
        },
        'exerciseFrequency': {
            presence: {
                message: '必須訓練頻率(天/週)'
            }
        },
        'trainingUnitMinute': {
            presence: {
                message: '訓練頻率(分鐘/次)'
            }
        },
        'mark': {
            length: {
                maximum: 150,
                message: '備註最長 150 字'
            }
        }
    };

    const exercisePrescriptionMuscleStrengthAndMuscleEnduranceFormRef = useRef<HTMLFormElement>(null);
    const [visibleMuscleStrengthAndMuscleEnduranceDialog, setVisibleMuscleStrengthAndMuscleEnduranceDialog] = useState<boolean>(false); // 運動處方-肌力與肌耐力彈窗
    const [muscleStrengthAndMuscleEnduranceExerciseIntensity, setMuscleStrengthAndMuscleEnduranceExerciseIntensity] = useState<string>(''); // 運動處方-肌力與肌耐力運動強度
    const [muscleStrengthAndMuscleEnduranceExerciseFrequency, setMuscleStrengthAndMuscleEnduranceExerciseFrequency] = useState<string>(''); // 運動處方-肌力與肌耐力運練頻率(天/周)
    const [muscleStrengthAndMuscleEnduranceTrainingUnitGroup, setMuscleStrengthAndMuscleEnduranceTrainingUnitGroup] = useState<string>(''); // 運動處方-肌力與肌耐力訓練單位(組/台)
    const [muscleStrengthAndMuscleEnduranceTrainingUnitNumber, setMuscleStrengthAndMuscleEnduranceTrainingUnitNumber] = useState<string>(''); // 運動處方-肌力與肌耐力訓練單位(次/組)
    const [muscleStrengthAndMuscleEnduranceTrainingArea, setMuscleStrengthAndMuscleEnduranceTrainingArea] = useState<string[]>([]); // 運動處方-肌力與肌耐力加強部位
    const [muscleStrengthAndMuscleEnduranceMark, setMuscleStrengthAndMuscleEnduranceMark] = useState<string>(''); // 運動處方-肌力與肌耐力備註
    const [muscleStrengthAndMuscleEnduranceExerciseIntensityMessage, setMuscleStrengthAndMuscleEnduranceExerciseIntensityMessage] = useState<string>('');
    const [muscleStrengthAndMuscleEnduranceExerciseFrequencyMessage, setMuscleStrengthAndMuscleEnduranceExerciseFrequencyMessage] = useState<string>('');
    const [muscleStrengthAndMuscleEnduranceTrainingUnitGroupMessage, setMuscleStrengthAndMuscleEnduranceTrainingUnitGroupMessage] = useState<string>('');
    const [muscleStrengthAndMuscleEnduranceTrainingUnitNumberMessage, setMuscleStrengthAndMuscleEnduranceTrainingUnitNumberMessage] = useState<string>('');
    const [muscleStrengthAndMuscleEnduranceTrainingAreaMessage, setMuscleStrengthAndMuscleEnduranceTrainingAreaMessage] = useState<string>('');
    const [muscleStrengthAndMuscleEnduranceMarkMessage, setMuscleStrengthAndMuscleEnduranceMarkMessage] = useState<string>('');
    const exercisePrescriptionMuscleStrengthAndMuscleEnduranceForm = {
        'exerciseIntensity': {
            presence: {
                message: '必須輸入運動強度'
            }
        },
        'exerciseFrequency': {
            presence: {
                message: '必須輸入訓練頻率(天/週)'
            }
        },
        'trainingUnitGroup': {
            presence: {
                message: '必須輸入訓練頻率(台/組)'
            }
        },
        'trainingUnitNumber': {
            presence: {
                message: '必須輸入訓練頻率(分鐘/次)'
            }
        },
        'mark': {
            length: {
                maximum: 150,
                message: '備註最長 150 字'
            }
        }
    };

    const exercisePrescriptionSoftnessFormRef = useRef<HTMLFormElement>(null);
    const [visibleSoftness, setVisibleSoftness] = useState<boolean>(false); // 運動處方-柔軟度彈窗
    const [softnessSports, setSoftnessSports] = useState<string[]>([]); // 運動處方-柔軟度運動項目
    const [softnessExerciseIntensity, setSoftnessExerciseIntensity] = useState<string>(''); // 運動處方-柔軟度訓練頻率(天/週)
    const [softnessExerciseFrequency, setSoftnessExerciseFrequency] = useState<string>(''); // 運動處方-柔軟度訓練頻率(分鐘/次)
    const [softnessTrainingUnitMinute, setSoftnessTrainingUnitMinute] = useState<string>(''); // 運動處方-柔軟度訓練頻率(分鐘/次)
    const [softnessMark, setSoftnessMark] = useState<string>(''); // 運動處方-柔軟度備註
    const [softnessSportsMessage, setSoftnessSportsMessage] = useState<string>('');
    const [softnessExerciseIntensityMessage, setSoftnessExerciseIntensityMessage] = useState<string>('');
    const [softnessExerciseFrequencyMessage, setSoftnessExerciseFrequencyMessage] = useState<string>('');
    const [softnessTrainingUnitMinuteMessage, setSoftnessTrainingUnitMinuteMessage] = useState<string>('');
    const [softnessMarkMessage, setSoftnessMarkMessage] = useState<string>('');
    const exercisePrescriptionSoftnessForm = {
        'sports': {

        },
        'exerciseIntensity': {
            presence: {
                message: '必須輸入運動強度'
            }
        },
        'exerciseFrequency': {
            presence: {
                message: '必須輸入訓練頻率(天/週)'
            }
        },
        'trainingUnitMinute': {
            presence: {
                message: '必須輸入訓練頻率(分鐘/次)'
            }
        },
        'mark': {
            length: {
                maximum: 150,
                message: '備註最長 150 字'
            }
        }
    };

    const exercisePrescriptionBalanceFormRef = useRef<HTMLFormElement>(null);
    const [visibleBalanceDialog, setVisibleBalanceDialog] = useState<boolean>(false); // 運動處方-平衡彈窗
    const [balanceSports, setBalanceSports] = useState<string[]>([]); // 運動處方-平衡運動項目
    const [balanceExerciseIntensity, setBalanceExerciseIntensity] = useState<string>(''); // 運動處方-平衡運動強度
    const [balanceExerciseFrequency, setBalanceExerciseFrequency] = useState<string>(''); // 運動處方-平衡訓練頻率(天/週)
    const [balanceTrainingUnitMinute, setBalanceTrainingUnitMinute] = useState<string>(''); // 運動處方-平衡訓練頻率(分鐘/次)
    const [balanceMark, setBalanceMark] = useState<string>(''); // 運動處方-平衡備註
    const [balanceSportsMessage, setBalanceSportsMessage] = useState<string>('');
    const [balanceExerciseIntensityMessage, setBalanceExerciseIntensityMessage] = useState<string>('');
    const [balanceExerciseFrequencyMessage, setBalanceExerciseFrequencyMessage] = useState<string>('');
    const [balanceTrainingUnitMinuteMessage, setBalanceTrainingUnitMinuteMessage] = useState<string>('');
    const [balanceMarkMessage, setBalanceMarkMessage] = useState<string>('');
    const exercisePrescriptionBalanceForm = {
        'sports': {

        },
        'exerciseIntensity': {
            presence: {
                message: '必須輸入運動強度'
            }
        },
        'exerciseFrequency': {
            presence: {
                message: '必須輸入訓練頻率(天/週)'
            }
        },
        'trainingUnitMinute': {
            presence: {
                message: '必須輸入訓練頻率(分鐘/次)'
            }
        },
        'mark': {
            length: {
                maximum: 150,
                message: '備註最長 150 字'
            }
        }
    };

    const storeGetters = useAppSelector((state) => {
        return {
            studentId: state.studentInfo.studentId
        };
    });

    /**
     * 取得運動處方清單
     */
    const handleExercisePrescriptionList = () => {
        exercisePrescriptionList(storeGetters.studentId).then((res) => {
            const { data } = res.data;

            setExercisePrescriptionCardiorespiratoryFitnessGroup(data.exercisePrescriptionCardiorespiratoryFitnessGroup);
            setExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup(data.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup);
            setExercisePrescriptionSoftnessGroup(data.exercisePrescriptionSoftnessGroup);
            setExercisePrescriptionBalanceGroup(data.exercisePrescriptionBalanceGroup);
        }).catch((error) => {
            const errorCode = axiosErrorCode<ExercisePrescriptionListErrorCodeType>(error);

            switch (errorCode) {
                case 'StudentDoesNotExist':
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                default:
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    /**
     * 取得運動處方訓練階段
     */
    const handleLatestExercisePrescriptionTrainingStage = () => {
        exercisePrescriptionTrainingStage(storeGetters.studentId).then((res) => {
            const { data } = res.data;

            setTrainingStageResponse(data);

            if (data !== null) {
                drawTrainingStage(data);
                setTrainingFirstStage(data.firstStage.toString());
                setTrainingMiddleStage(data.middleStage.toString());
                setTrainingStartDate(data.startDate);
            }
        }).catch((error) => {
            const errorCode = axiosErrorCode<ExercisePrescriptionTrainingStageErrorCodeType>(error);

            switch (errorCode) {
                case 'StudentDoesNotExist':
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                default:
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    // 繪製運動處方階段圖表
    const drawTrainingStage = (data: ExercisePrescriptionTrainingStageDataType) => {
        const firstStage = data.firstStage;
        const middleStage = data.middleStage;
        const total = firstStage + middleStage + firstStage;
        const weekPercentage = data.week / total * 100;

        setTrainingFirstProgress(Math.round(firstStage / total * 100));
        setTrainingMiddleProgress(middleStage / total * 100);
        setTrainingLastProgress(firstStage / total * 100);

        if (data.week === 1) {
            gsap.to('.training_stage_anchor', {
                left: '-1%',
                ease: 'elastic.out(1, 0.3)',
                duration: 5
            });

            gsap.to('.training_stage_anchor_text', {
                left: '-4.2%',
                ease: 'elastic.out(1, 0.3)',
                duration: 5
            });
        }
        else if (data.week >= total) {
            gsap.to('.training_stage_anchor', {
                left: 'calc(100% - 18px)',
                ease: 'elastic.out(1, 0.3)',
                duration: 5
            });

            gsap.to('.training_stage_anchor_text', {
                left: 'calc(100% - 60px)',
                ease: 'elastic.out(1, 0.3)',
                duration: 5
            });
        }
        else {
            gsap.to('.training_stage_anchor', {
                left: `calc(${weekPercentage}% - 1.2%)`,
                ease: 'elastic.out(1, 0.3)',
                duration: 5
            });

            gsap.to('.training_stage_anchor_text', {
                left: `calc(${weekPercentage}% - 4.3%)`,
                ease: 'elastic.out(1, 0.3)',
                duration: 5
            });
        }
    };


    /** 顯示運動處方訓練階段彈窗 */
    const showTrainingStageDialog = () => {
        setVisibleTrainingStageDialog(true);

        if (trainingStageResponse) {
            setTrainingFirstStage(trainingStageResponse.firstStage.toString());
            setTrainingMiddleStage(trainingStageResponse.middleStage.toString());
            setTrainingStartDate(trainingStageResponse.startDate);
        }
    };

    /**
     * 關閉運動處方訓練階段彈窗
     */
    const closeTrainingStageDialog = () => {
        setVisibleTrainingStageDialog(false);
    };

    /**
     * 新增/更新運動處方訓練階段
     */
    const handleExercisePrescriptionTrainingStageReplace = async () => {
        const isValid = await inspectTrainingStage(trainingStartDate);

        if (isValid) {
            const data: ExercisePrescriptionTrainingStageReplaceRequestType = {
                firstStage: trainingFirstStage,
                middleStage: trainingMiddleStage,
                startDate: trainingStartDate
            };

            exercisePrescriptionTrainingStageReplace(storeGetters.studentId, data).then(() => {
                setVisibleTrainingStageDialog(false);

                setVisibleInfoDialog(true);
                setVisibleInfoDialogTitle('資訊');
                setVisibleInfoDialogMessage('更新資料成功');

                handleLatestExercisePrescriptionTrainingStage();
            }).catch((error) => {
                const errorCode = axiosErrorCode<ExercisePrescriptionTrainingStageReplaceErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('格式輸入錯誤');
                        break;
                    case 'LargerToday':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('日期不可超過今天');
                        break;
                    case 'StudentDoesNotExist':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('查無學員');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    // 檢查運動處方-訓練請求資料
    const inspectTrainingStage = (fullStartDate?: string) => {
        const isValid = validate(trainingStageRef.current, trainingStageForm);

        const today = parseInt(moment().format('YYYYMMDD'));
        const startDate = fullStartDate !== undefined ? parseInt(fullStartDate.replace(/-/g, '')) : undefined;

        if (isValid !== undefined) {
            if (isValid.firstStage) {
                setTrainingFirstStageMessage(isValid.firstStage[0].replace('First stage ', ''));
            }
            else {
                setTrainingFirstStageMessage('');
            }

            if (isValid.middleStage) {
                setTrainingMiddleStageMessage(isValid.middleStage[0].replace('Middle stage ', ''));
            }
            else {
                setTrainingMiddleStageMessage('');
            }

            if (isValid.startDate) {
                setTrainingStartDateMessage(isValid.startDate[0].replace('Start date ', ''));
            }
            else {
                setTrainingStartDateMessage('');
            }

            return false;
        }
        else if (startDate !== undefined && startDate === today) {
            setTrainingStartDateMessage('');

            return true;
        }
        else if (startDate !== undefined && startDate > today) {
            setTrainingStartDateMessage('日期不可超過今天');

            return false;
        }
        else {
            setTrainingFirstStageMessage('');
            setTrainingMiddleStageMessage('');
            setTrainingStartDateMessage('');

            return true;
        }
    };

    /**
     * 處理設定開始階段輸入
     */
    const handleTrainingFirstStage = (e: React.ChangeEvent<HTMLInputElement>) => {
        setTrainingFirstStage(e.target.value);
        inspectTrainingStage();
    };

    /**
     * 處理設定維持階段輸入
     */
    const handleTrainingMiddleStage = (e: React.ChangeEvent<HTMLInputElement>) => {
        setTrainingMiddleStage(e.target.value);
        inspectTrainingStage();
    };

    /**
     * 處理設定起始日期輸入
     */
    const handleTrainingStartDate = (e: React.ChangeEvent<HTMLInputElement>) => {
        setTrainingStartDate(e.target.value);
        inspectTrainingStage(e.target.value);
    };



    /**
     * 關閉資訊彈窗
     */
    const closeDialogInfo = () => {
        setVisibleInfoDialog(false);
    };

    /**
     * 顯示運動處方心肺適能彈窗
     */
    const showVisibleCardiorespiratoryFitnessDialog = () => {
        setVisibleCardiorespiratoryFitnessDialog(true);
    };

    /**
     * 關閉運動處方心肺適能彈窗
     */
    const closeVisibleCardiorespiratoryFitnessDialog = () => {
        setVisibleCardiorespiratoryFitnessDialog(false);

        setCardiorespiratoryFitnessExerciseIntensity('');
        setCardiorespiratoryFitnessExerciseFrequency('');
        setCrdiorespiratoryFitnessTrainingUnitMinute('');
        setCardiorespiratoryFitnessSports([]);
        setCardiorespiratoryFitnessMark('');
    };

    /**
     * 新增或更新運動處方-心肺適能
     */
    const handleCardiorespiratoryFitness = () => {
        const isValid = inspectCardiorespiratoryFitness();

        if (isValid) {
            const data: ExercisePrescriptionCardiorespiratoryFitnessRequestType = {
                exerciseIntensity: cardiorespiratoryFitnessExerciseIntensity,
                exerciseFrequency: cardiorespiratoryFitnessExerciseFrequency,
                trainingUnitMinute: cardiorespiratoryFitnessTrainingUnitMinute,
                sports: cardiorespiratoryFitnessSports,
                mark: cardiorespiratoryFitnessMark
            };

            exercisePrescriptionCardiorespiratoryFitness(storeGetters.studentId, data).then(() => {
                closeVisibleCardiorespiratoryFitnessDialog();

                setVisibleInfoDialog(true);
                setVisibleInfoDialogTitle('資訊');
                setVisibleInfoDialogMessage('更新資料成功');

                handleExercisePrescriptionList();
            }).catch((error) => {
                const errorCode = axiosErrorCode<ExercisePrescriptionCardiorespiratoryFitnessErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('格式輸入錯誤');
                        break;
                    case 'IllegalSports':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('非法運動項目');
                        break;
                    case 'StudentDoesNotExist':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('查無學員');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    /**
     * 檢查運動處方-心肺適能輸入欄位
     */
    const inspectCardiorespiratoryFitness = () => {
        const isValid = validate(exercisePrescriptionCardiorespiratoryFitnessFormRef.current, exercisePrescriptionCardiorespiratoryFitnessForm);

        if (isValid !== undefined) {
            if (isValid.exerciseFrequency) {
                setCardiorespiratoryFitnessExerciseFrequencyMessage(isValid.exerciseFrequency[0].replace('Exercise frequency ', ''));
            }
            else {
                setCardiorespiratoryFitnessExerciseFrequencyMessage('');
            }

            if (isValid.exerciseIntensity) {
                setCardiorespiratoryFitnessExerciseIntensityMessage(isValid.exerciseIntensity[0].replace('Exercise intensity ', ''));
            }
            else {
                setCardiorespiratoryFitnessExerciseIntensityMessage('');
            }

            if (isValid.sports) {
                setCardiorespiratoryFitnessSportsMessage(isValid.sports[0].replace('Sports ', ''));
            }
            else {
                setCardiorespiratoryFitnessSportsMessage('');
            }

            if (isValid.trainingUnitMinute) {
                setCardiorespiratoryFitnessTrainingUnitMinuteMessage(isValid.trainingUnitMinute[0].replace('Training unit minute ', ''));
            }
            else {
                setCardiorespiratoryFitnessTrainingUnitMinuteMessage('');
            }

            if (isValid.mark) {
                setCardiorespiratoryFitnessMarkMessage(isValid.mark[0].replace('Mark ', ''));
            }
            else {
                setCardiorespiratoryFitnessMarkMessage('');
            }

            return false;
        }
        else {
            setCardiorespiratoryFitnessExerciseIntensityMessage('');
            setCardiorespiratoryFitnessExerciseFrequencyMessage('');
            setCardiorespiratoryFitnessTrainingUnitMinuteMessage('');
            setCardiorespiratoryFitnessSportsMessage('');
            setCardiorespiratoryFitnessMarkMessage('');

            return true;
        }
    };

    /**
     * 處理運動處方-心肺適能運動強度
     */
    const handleCardiorespiratoryFitnessExerciseIntensitySelectOption = (item: string[] | string) => {
        if (typeof item === 'string') {
            setCardiorespiratoryFitnessExerciseIntensity(item);
            inspectCardiorespiratoryFitness();
        }
    };

    /**
     * 處理運動處方-心肺適能訓練頻率(天/週)
     */
    const handleCardiorespiratoryFitnessExerciseFrequencySelectOption = (item: string[] | string) => {
        if (typeof item === 'string') {
            setCardiorespiratoryFitnessExerciseFrequency(item);
            inspectCardiorespiratoryFitness();
        }
    };

    /**
     * 處理運動處方-心肺適能訓練頻率(分鐘/次)
     */
    const handleCardiorespiratoryFitnessTrainingUnitMinuteSelectOption = (item: string[] | string) => {
        if (typeof item === 'string') {
            setCrdiorespiratoryFitnessTrainingUnitMinute(item);
            inspectCardiorespiratoryFitness();
        }
    };

    /**
     * 處理運動處方-心肺適能運動項目
     */
    const handleCardiorespiratoryFitnessSportsOptions = (item: string[] | string) => {
        if (typeof item === 'object') {
            setCardiorespiratoryFitnessSports(item);
            inspectCardiorespiratoryFitness();
        }
    };

    /**
     * 處理運動處方-心肺適能備註
     */
    const handleCardiorespiratoryFitnessMark = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setCardiorespiratoryFitnessMark(e.target.value);
        inspectCardiorespiratoryFitness();
    };

    useEffect(() => {
        inspectCardiorespiratoryFitness();
    }, [cardiorespiratoryFitnessExerciseFrequency]);

    useEffect(() => {
        inspectCardiorespiratoryFitness();
    }, [cardiorespiratoryFitnessExerciseIntensity]);

    useEffect(() => {
        inspectCardiorespiratoryFitness();
    }, [cardiorespiratoryFitnessTrainingUnitMinute]);

    useEffect(() => {
        inspectCardiorespiratoryFitness();
    }, [cardiorespiratoryFitnessSports]);

    useEffect(() => {
        inspectCardiorespiratoryFitness();
    }, [cardiorespiratoryFitnessMark]);



    /**
     * 顯示運動處方-肌力與肌耐力彈窗
     */
    const showVisibleMuscleStrengthAndMuscleEnduranceDialog = () => {
        setVisibleMuscleStrengthAndMuscleEnduranceDialog(true);
    };

    /**
     * 關閉運動處方-肌力與肌耐力彈窗
     */
    const closeVisibleMuscleStrengthAndMuscleEnduranceDialog = () => {
        setVisibleMuscleStrengthAndMuscleEnduranceDialog(false);

        setMuscleStrengthAndMuscleEnduranceExerciseIntensity('');
        setMuscleStrengthAndMuscleEnduranceExerciseFrequency('');
        setMuscleStrengthAndMuscleEnduranceTrainingUnitGroup('');
        setMuscleStrengthAndMuscleEnduranceTrainingUnitNumber('');
        setMuscleStrengthAndMuscleEnduranceTrainingArea([]);
        setMuscleStrengthAndMuscleEnduranceMark('');
    };

    /**
     * 新增或更新運動處方-肌力與肌耐力
     */
    const handleMuscleStrengthAndMuscleEndurance = () => {
        const isValid = inspectMuscleStrengthAndMuscleEndurance();

        if (isValid) {
            const data: ExercisePrescriptionMuscleStrengthAndMuscleEnduranceRequestType = {
                exerciseIntensity: muscleStrengthAndMuscleEnduranceExerciseIntensity,
                exerciseFrequency: muscleStrengthAndMuscleEnduranceExerciseFrequency,
                trainingUnitGroup: muscleStrengthAndMuscleEnduranceTrainingUnitGroup,
                trainingUnitNumber: muscleStrengthAndMuscleEnduranceTrainingUnitNumber,
                trainingArea: muscleStrengthAndMuscleEnduranceTrainingArea,
                mark: muscleStrengthAndMuscleEnduranceMark
            };

            exercisePrescriptionMuscleStrengthAndMuscleEndurance(storeGetters.studentId, data).then(() => {
                closeVisibleMuscleStrengthAndMuscleEnduranceDialog();

                setVisibleInfoDialog(true);
                setVisibleInfoDialogTitle('資訊');
                setVisibleInfoDialogMessage('更新資料成功');

                handleExercisePrescriptionList();
            }).catch((error) => {
                const errorCode = axiosErrorCode<ExercisePrescriptionMuscleStrengthAndMuscleEnduranceErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('格式輸入錯誤');
                        break;
                    case 'IllegalTrainingArea':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('非法加強部位');
                        break;
                    case 'StudentDoesNotExist':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('查無學員');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    // 檢查運動處方-肌力與肌耐力輸入欄位
    const inspectMuscleStrengthAndMuscleEndurance = () => {
        const isValid = validate(exercisePrescriptionMuscleStrengthAndMuscleEnduranceFormRef.current, exercisePrescriptionMuscleStrengthAndMuscleEnduranceForm);

        if (isValid !== undefined) {
            if (isValid.exerciseFrequency) {
                setMuscleStrengthAndMuscleEnduranceExerciseFrequencyMessage(isValid.exerciseFrequency[0].replace('Exercise frequency ', ''));
            }
            else {
                setMuscleStrengthAndMuscleEnduranceExerciseFrequencyMessage('');
            }

            if (isValid.exerciseIntensity) {
                setMuscleStrengthAndMuscleEnduranceExerciseIntensityMessage(isValid.exerciseIntensity[0].replace('Exercise intensity ', ''));
            }
            else {
                setMuscleStrengthAndMuscleEnduranceExerciseIntensityMessage('');
            }

            if (isValid.trainingUnitGroup) {
                setMuscleStrengthAndMuscleEnduranceTrainingUnitGroupMessage(isValid.trainingUnitGroup[0].replace('Training unit group ', ''));
            }
            else {
                setMuscleStrengthAndMuscleEnduranceTrainingUnitGroupMessage('');
            }

            if (isValid.trainingUnitNumber) {
                setMuscleStrengthAndMuscleEnduranceTrainingUnitNumberMessage(isValid.trainingUnitNumber[0].replace('Training unit number ', ''));
            }
            else {
                setMuscleStrengthAndMuscleEnduranceTrainingUnitNumberMessage('');
            }

            if (isValid.mark) {
                setMuscleStrengthAndMuscleEnduranceMarkMessage(isValid.mark[0].replace('Mark ', ''));
            }
            else {
                setMuscleStrengthAndMuscleEnduranceMarkMessage('');
            }

            return false;
        }
        else {
            setMuscleStrengthAndMuscleEnduranceExerciseFrequencyMessage('');
            setMuscleStrengthAndMuscleEnduranceExerciseIntensityMessage('');
            setMuscleStrengthAndMuscleEnduranceTrainingUnitGroupMessage('');
            setMuscleStrengthAndMuscleEnduranceTrainingUnitNumberMessage('');
            setMuscleStrengthAndMuscleEnduranceMarkMessage('');
            setMuscleStrengthAndMuscleEnduranceTrainingAreaMessage('');

            return true;
        }
    };

    /**
     * 處理運動處方-肌力與肌耐力運動強度
     */
    const handleMuscleStrengthAndMuscleEnduranceExerciseIntensityOptions = (item: string[] | string) => {
        if (typeof item === 'string') {
            setMuscleStrengthAndMuscleEnduranceExerciseIntensity(item);
        }
    };

    /**
     * 處理運動處方-肌力與肌耐力訓練頻率(天/週)
     */
    const handleMuscleStrengthAndMuscleEnduranceExerciseFrequencyOptions = (item: string[] | string) => {
        if (typeof item === 'string') {
            setMuscleStrengthAndMuscleEnduranceExerciseFrequency(item);
        }
    };

    /**
     * 處理運動處方-肌力與肌耐力訓練單位(組/台)
     */
    const handleMuscleStrengthAndMuscleEnduranceTrainingUnitGroupOptions = (item: string[] | string) => {
        if (typeof item === 'string') {
            setMuscleStrengthAndMuscleEnduranceTrainingUnitGroup(item);
        }
    };

    /**
     * 處理運動處方-肌力與肌耐力訓練單位(次/組)
     */
    const handleMuscleStrengthAndMuscleEnduranceTrainingUnitNumberOptions = (item: string[] | string) => {
        if (typeof item === 'string') {
            setMuscleStrengthAndMuscleEnduranceTrainingUnitNumber(item);
        }
    };

    /**
     * 處理運動處方訓練部位圖片
     */
    const handleTrainingAreaImage = (trainingAreaData?: string[]) => {
        if (trainingAreaData?.length === 0) {
            return muscleDistribution;
        }
        else {
            if (trainingAreaData?.indexOf('上肢') !== -1 && trainingAreaData?.indexOf('核心') !== -1 && trainingAreaData?.indexOf('下肢') !== -1) {
                return muscleDistributionAll;
            }
            else if (trainingAreaData?.indexOf('上肢') !== -1 && trainingAreaData?.indexOf('下肢') !== -1) {
                return muscleDistributionUpperBodyAndLowerBody;
            }
            else if (trainingAreaData?.indexOf('上肢') !== -1 && trainingAreaData?.indexOf('核心') !== -1) {
                return muscleDistributionUpperBodyAndCore;
            }
            else if (trainingAreaData?.indexOf('下肢') !== -1 && trainingAreaData?.indexOf('核心') !== -1) {
                return muscleDistributionLowerBodyAndCore;
            }
            else if (trainingAreaData?.indexOf('下肢') !== -1) {
                return muscleDistributionLowerBody;
            }
            else if (trainingAreaData?.indexOf('上肢') !== -1) {
                return muscleDistributionUpperBody;
            }
            else if (trainingAreaData?.indexOf('核心') !== -1) {
                return muscleDistributionCore;
            }
        }
    };

    /**
     * 處理運動處方-肌力與肌耐力備註
     */
    const handleMuscleStrengthAndMuscleEnduranceMark = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setMuscleStrengthAndMuscleEnduranceMark(e.target.value);
    };

    useEffect(() => {
        inspectMuscleStrengthAndMuscleEndurance();
    }, [muscleStrengthAndMuscleEnduranceExerciseIntensity]);

    useEffect(() => {
        inspectMuscleStrengthAndMuscleEndurance();
    }, [muscleStrengthAndMuscleEnduranceExerciseFrequency]);

    useEffect(() => {
        inspectMuscleStrengthAndMuscleEndurance();
    }, [muscleStrengthAndMuscleEnduranceTrainingUnitGroup]);

    useEffect(() => {
        inspectMuscleStrengthAndMuscleEndurance();
    }, [muscleStrengthAndMuscleEnduranceTrainingUnitNumber]);

    useEffect(() => {
        inspectMuscleStrengthAndMuscleEndurance();
    }, [muscleStrengthAndMuscleEnduranceTrainingArea]);

    useEffect(() => {
        inspectMuscleStrengthAndMuscleEndurance();
    }, [muscleStrengthAndMuscleEnduranceMark]);

    const handleTrainingArea = (trainingArea: TrainingAreaType) => {
        const trainingAreaIndex = muscleStrengthAndMuscleEnduranceTrainingArea.indexOf(trainingArea);

        if (trainingAreaIndex === -1) {
            setMuscleStrengthAndMuscleEnduranceTrainingArea(state => [...state, trainingArea]);
        }
        else {
            muscleStrengthAndMuscleEnduranceTrainingArea.splice(trainingAreaIndex, 1);
            setMuscleStrengthAndMuscleEnduranceTrainingArea([...muscleStrengthAndMuscleEnduranceTrainingArea]);
        }
    };

    useEffect(() => {
        handleTrainingAreaImage(muscleStrengthAndMuscleEnduranceTrainingArea);
    }, [muscleStrengthAndMuscleEnduranceTrainingArea]);

    /**
     * 顯示運動處方-柔軟度彈窗
     */
    const showVisibleSoftnessDialog = () => {
        setVisibleSoftness(true);
    };

    /**
     * 關閉運動處方-柔軟度彈窗
     */
    const closeVisibleSoftnessDialog = () => {
        setVisibleSoftness(false);

        setSoftnessSports([]);
        setSoftnessExerciseIntensity('');
        setSoftnessExerciseFrequency('');
        setSoftnessTrainingUnitMinute('');
        setSoftnessMark('');
    };

    /**
     * 新增/更新運動處方-柔軟度
     */
    const handleExercisePrescriptionSoftness = () => {
        const isValid = inspectSoftness();

        if (isValid) {
            const data: ExercisePrescriptionSoftnessRequestType = {
                sports: softnessSports,
                exerciseIntensity: softnessExerciseIntensity,
                exerciseFrequency: softnessExerciseFrequency,
                trainingUnitMinute: softnessTrainingUnitMinute,
                mark: softnessMark
            };

            exercisePrescriptionSoftness(storeGetters.studentId, data).then(() => {
                closeVisibleSoftnessDialog();

                setVisibleInfoDialog(true);
                setVisibleInfoDialogTitle('資訊');
                setVisibleInfoDialogMessage('更新資料成功');

                handleExercisePrescriptionList();
            }).catch((error) => {
                const errorCode = axiosErrorCode<ExercisePrescriptionSoftnessErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('格式輸入錯誤');
                        break;
                    case 'IllegalSports':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('非法加強部位');
                        break;
                    case 'StudentDoesNotExist':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('查無學員');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    // 檢查運動處方-柔軟度輸入欄位
    const inspectSoftness = () => {
        const isValid = validate(exercisePrescriptionSoftnessFormRef.current, exercisePrescriptionSoftnessForm);

        if (isValid) {
            if (isValid.exerciseFrequency) {
                setSoftnessExerciseFrequencyMessage(isValid.exerciseFrequency[0].replace('Exercise frequency ', ''));
            }
            else {
                setSoftnessExerciseFrequencyMessage('');
            }

            if (isValid.exerciseIntensity) {
                setSoftnessExerciseIntensityMessage(isValid.exerciseIntensity[0].replace('Exercise intensity ', ''));
            }
            else {
                setSoftnessExerciseIntensityMessage('');
            }

            if (isValid.trainingUnitMinute) {
                setSoftnessTrainingUnitMinuteMessage(isValid.trainingUnitMinute[0].replace('Training unit minute ', ''));
            }
            else {
                setSoftnessTrainingUnitMinuteMessage('');
            }

            if (isValid.mark) {
                setSoftnessMarkMessage(isValid.mark[0].replace('Mark ', ''));
            }
            else {
                setSoftnessMarkMessage('');
            }

            return false;
        }
        else {
            setSoftnessSportsMessage('');
            setSoftnessExerciseFrequencyMessage('');
            setSoftnessExerciseIntensityMessage('');
            setSoftnessTrainingUnitMinuteMessage('');
            setSoftnessMarkMessage('');

            return true;
        }
    };

    /**
     * 處理運動處方-柔軟度訓練項目
     */
    const handleSoftnessSportsOptions = (item: string[] | string) => {
        if (typeof item === 'object') {
            setSoftnessSports(item);
        }
    };

    /**
     * 處理運動處方-柔軟度訓練強度
     */
    const handleSoftnessExerciseIntensityOptions = (item: string[] | string) => {
        if (typeof item === 'string') {
            setSoftnessExerciseIntensity(item);
        }
    };

    /**
     * 處理運動處方-柔軟度訓練頻率(天/週)
     */
    const handleSoftnessExerciseFrequencyOptions = (item: string[] | string) => {
        if (typeof item === 'string') {
            setSoftnessExerciseFrequency(item);
        }
    };

    /**
     * 處理運動處方-柔軟度訓練頻率(分鐘/次)
     */
    const handleSoftnessTrainingUnitMinuteOptions = (item: string[] | string) => {
        if (typeof item === 'string') {
            setSoftnessTrainingUnitMinute(item);
        }
    };

    /**
     * 處理運動處方-柔軟度備註
     */
    const handleSoftnessMark = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setSoftnessMark(e.target.value);
    };

    useEffect(() => {
        inspectSoftness();
    }, [softnessSports]);

    useEffect(() => {
        inspectSoftness();
    }, [softnessExerciseIntensity]);

    useEffect(() => {
        inspectSoftness();
    }, [softnessExerciseFrequency]);

    useEffect(() => {
        inspectSoftness();
    }, [softnessTrainingUnitMinute]);

    useEffect(() => {
        inspectSoftness();
    }, [softnessMark]);




    /** 
     * 顯示處理運動處方-平衡彈窗
     */
    const showVisibleBalanceDialog = () => {
        setVisibleBalanceDialog(true);
    };

    /**
     * 關閉處理運動處方-平衡彈窗
     */
    const closeVisibleBalanceDialog = () => {
        setVisibleBalanceDialog(false);

        setBalanceSports([]);
        setBalanceExerciseIntensity('');
        setBalanceExerciseFrequency('');
        setBalanceTrainingUnitMinute('');
        setBalanceMark('');
    };

    /**
     * 新增/更新運動處方-平衡
     */
    const handleExercisePrescriptionBalance = () => {
        const isValid = inspectBalance();

        if (isValid) {
            const data: ExercisePrescriptionBalanceRequestType = {
                exerciseIntensity: balanceExerciseIntensity,
                exerciseFrequency: balanceExerciseFrequency,
                trainingUnitMinute: balanceTrainingUnitMinute,
                sports: balanceSports,
                mark: balanceMark
            };

            exercisePrescriptionBalance(storeGetters.studentId, data).then(() => {
                closeVisibleBalanceDialog();

                setVisibleInfoDialog(true);
                setVisibleInfoDialogTitle('資訊');
                setVisibleInfoDialogMessage('更新資料成功');

                handleExercisePrescriptionList();
            }).catch((error) => {
                const errorCode = axiosErrorCode<ExercisePrescriptionBalanceErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('格式輸入錯誤');
                        break;
                    case 'IllegalSports':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('非法加強部位');
                        break;
                    case 'StudentDoesNotExist':
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('查無學員');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisibleInfoDialog(true);
                        setVisibleInfoDialogTitle('錯誤');
                        setVisibleInfoDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    const inspectBalance = () => {
        const isValid = validate(exercisePrescriptionBalanceFormRef.current, exercisePrescriptionBalanceForm);

        if (isValid) {
            if (isValid.exerciseFrequency) {
                setBalanceExerciseFrequencyMessage(isValid.exerciseFrequency[0].replace('Exercise frequency ', ''));
            }
            else {
                setBalanceExerciseFrequencyMessage('');
            }

            if (isValid.exerciseIntensity) {
                setBalanceExerciseIntensityMessage(isValid.exerciseIntensity[0].replace('Exercise intensity ', ''));
            }
            else {
                setBalanceExerciseIntensityMessage('');
            }

            if (isValid.trainingUnitMinute) {
                setBalanceTrainingUnitMinuteMessage(isValid.trainingUnitMinute[0].replace('Training unit minute ', ''));
            }
            else {
                setBalanceTrainingUnitMinuteMessage('');
            }

            if (isValid.mark) {
                setBalanceMarkMessage(isValid.mark[0].replace('Mark ', ''));
            }
            else {
                setBalanceMarkMessage('');
            }

            return false;
        }
        else {
            setBalanceSportsMessage('');
            setBalanceExerciseFrequencyMessage('');
            setBalanceExerciseIntensityMessage('');
            setBalanceTrainingUnitMinuteMessage('');
            setBalanceMarkMessage('');

            return true;
        }
    };

    /**
     * 處理運動處方-平衡運動項目
     */
    const handleBalanceSportsOptions = (item: string[] | string) => {
        if (typeof item === 'object') {
            setBalanceSports(item);
        }
    };

    /**
     * 處理運動處方-平衡運動強度
     */
    const handleBalanceExerciseIntensityOptions = (item: string[] | string) => {
        if (typeof item === 'string') {
            setBalanceExerciseIntensity(item);
        }
    };

    /**
     * 處理運動處方-平衡訓練頻率(單位: 天/周)
     */
    const handleBalanceExerciseFrequencyOptions = (item: string[] | string) => {
        if (typeof item === 'string') {
            setBalanceExerciseFrequency(item);
        }
    };

    /**
     * 處理運動處方-平衡訓練頻率(分鐘/次)
     */
    const handleBalanceTrainingUnitMinute = (item: string[] | string) => {
        if (typeof item === 'string') {
            setBalanceTrainingUnitMinute(item);
        }
    };

    /**
     * 處理運動處方-平衡備註
     */
    const handleBalanceMark = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setBalanceMark(e.target.value);
    };

    useEffect(() => {
        inspectBalance();
    }, [balanceSports]);

    useEffect(() => {
        inspectBalance();
    }, [balanceExerciseIntensity]);

    useEffect(() => {
        inspectBalance();
    }, [balanceExerciseFrequency]);

    useEffect(() => {
        inspectBalance();
    }, [balanceTrainingUnitMinute]);

    useEffect(() => {
        inspectBalance();
    }, [balanceMark]);



    useEffect(() => {
        handleExercisePrescriptionList();
        handleLatestExercisePrescriptionTrainingStage();
    }, []);

    return (
        <div className='exercisePrescription'>
            <StudentInfoBar></StudentInfoBar>
            <PageTitleBar title='運動處方'></PageTitleBar>

            {/* 訓練階段 */}
            <div className='training_stage_info'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-11/12 md:w-11/12 lg:w-11/12 xl:w-11/12 xxl:w-11/12'>
                        <div className='card'>
                            <div className='training_stage_anchor'>
                            </div>

                            <div className='training_stage_progress'>
                                <h5 className='training_stage_anchor_text'>
                                    目前階段(第&nbsp;
                                    <CountUp end={trainingStageResponse?.week !== undefined && trainingStageResponse?.week !== 0 ? trainingStageResponse.week : 1} duration={10} />
                                    &nbsp;週)
                                </h5>
                                <img className='training_stage_anchor' src={training_stage_anchor} />
                            </div>

                            <div className='training_stage'>
                                <div className='progress1' style={{ flex: `1 1 ${trainingFirstProgress}%` }}>

                                </div>

                                <div className='progress2' style={{ flex: `1 1 ${trainingMiddleProgress}%` }}>

                                </div>

                                <div className='progress3' style={{ flex: `1 1 ${trainingLastProgress}%` }}>

                                </div>
                            </div>

                            <div className='training_stage_text'>
                                <div className='progress1' style={{ flex: `1 1 ${trainingFirstProgress}%` }}>
                                    開始階段
                                </div>

                                <div className='progress2' style={{ flex: `1 1 ${trainingMiddleProgress}%` }}>
                                    改善階段
                                </div>

                                <div className='progress3' style={{ flex: `1 1 ${trainingLastProgress}%` }}>
                                    維持階段
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='w-full sm:w-1/12 md:w-1/12 lg:w-1/12 xl:w-1/12 xxl:w-1/12'>
                        <div className='card'>
                            <img className='btn_edit_training_stage' src={btn_edit} onClick={showTrainingStageDialog} />
                        </div>
                    </div>
                </div>
            </div>

            {/* 運動處方資訊 */}
            <div className='exercise_prescription_info'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                        <div className='card'>
                            <table>
                                <thead>
                                    <tr>
                                        <th>
                                            <p>項目</p>
                                        </th>

                                        <th>
                                            <p>訓練建議</p>
                                        </th>

                                        <th>
                                            <p>訓練強度</p>
                                        </th>

                                        <th>
                                            <p>訓練頻率</p>
                                        </th>

                                        <th>
                                            <p>備註</p>
                                        </th>

                                        <th>
                                            <p>歷史紀錄</p>
                                        </th>

                                        <th>
                                            <p>編輯</p>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>心肺適能</td>
                                        <td>
                                            {
                                                exercisePrescriptionCardiorespiratoryFitnessGroup?.sports.map((item, index) => {
                                                    return (
                                                        <p key={index}>{item}</p>
                                                    );
                                                })
                                            }
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionCardiorespiratoryFitnessGroup?.exerciseIntensity}</p>
                                            <p>{exercisePrescriptionCardiorespiratoryFitnessGroup?.heartRate}</p>
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionCardiorespiratoryFitnessGroup?.trainingFrequency}</p>
                                        </td>

                                        <td>{exercisePrescriptionCardiorespiratoryFitnessGroup?.mark}</td>

                                        <TdArrowDown dropDownClassName='exercisePrescriptionCardiorespiratoryFitnessChildren' data={exercisePrescriptionCardiorespiratoryFitnessGroup?.children}></TdArrowDown>

                                        <td className='icon_btn_edit' onClick={showVisibleCardiorespiratoryFitnessDialog}>
                                            <img src={btn_edit} />
                                        </td>
                                    </tr>

                                    {
                                        exercisePrescriptionCardiorespiratoryFitnessGroup?.children.map((item, index) => {
                                            return (
                                                <tr className='exercisePrescriptionCardiorespiratoryFitnessChildren' key={index}>
                                                    <td>{item.updateRecordDate}</td>
                                                    <td>
                                                        {
                                                            item?.sports.map((item, index) => {
                                                                return (
                                                                    <p key={index}>{item}</p>
                                                                );
                                                            })
                                                        }
                                                    </td>

                                                    <td>
                                                        <p>{item?.exerciseIntensity}</p>
                                                        <p>{item?.heartRate}</p>
                                                    </td>

                                                    <td>
                                                        <p>{item?.trainingFrequency}</p>
                                                    </td>

                                                    <td>{item?.mark}</td>
                                                </tr>
                                            );
                                        })
                                    }

                                    <tr>
                                        <td>肌力與肌耐力</td>

                                        <td>
                                            肌力訓練
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup?.exerciseIntensity}</p>
                                            <p>{exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup?.heartRate}</p>
                                        </td>

                                        <td>
                                            {exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup?.trainingFrequency}
                                        </td>

                                        <td>
                                            {exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup?.mark}
                                        </td>

                                        <TdArrowDown dropDownClassName='exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupChildren' data={exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup?.children}></TdArrowDown>

                                        <td className='icon_btn_edit' onClick={showVisibleMuscleStrengthAndMuscleEnduranceDialog}>
                                            <img src={btn_edit} />
                                        </td>
                                    </tr>

                                    {
                                        exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup?.children.map((item, index) => {
                                            return (
                                                <tr className='exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupChildren' key={index}>
                                                    <td>
                                                        {item.updateRecordDate}
                                                    </td>

                                                    <td>
                                                        <img src={handleTrainingAreaImage(item.trainingArea)} />
                                                    </td>

                                                    <td>
                                                        <p>{item?.exerciseIntensity}</p>
                                                        <p>{item?.heartRate}</p>
                                                    </td>

                                                    <td>
                                                        {item.trainingFrequency}
                                                    </td>

                                                    <td>
                                                        {item.mark}
                                                    </td>
                                                </tr>
                                            );
                                        })
                                    }

                                    <tr>
                                        <td>柔軟度</td>

                                        <td>
                                            {
                                                exercisePrescriptionSoftnessGroup?.sports.map((item, index) => {
                                                    return (
                                                        <p key={index}>{item}</p>
                                                    );
                                                })
                                            }
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionSoftnessGroup?.exerciseIntensity}</p>
                                            <p>{exercisePrescriptionSoftnessGroup?.heartRate}</p>
                                        </td>

                                        <td>
                                            {exercisePrescriptionSoftnessGroup?.trainingFrequency}
                                        </td>

                                        <td>
                                            {exercisePrescriptionSoftnessGroup?.mark}
                                        </td>

                                        <TdArrowDown dropDownClassName='exercisePrescriptionSoftnessGroupChildren' data={exercisePrescriptionCardiorespiratoryFitnessGroup?.children}></TdArrowDown>

                                        <td className='icon_btn_edit' onClick={showVisibleSoftnessDialog}>
                                            <img src={btn_edit} />
                                        </td>
                                    </tr>

                                    {
                                        exercisePrescriptionSoftnessGroup?.children.map((item, index) => {
                                            return (
                                                <tr className='exercisePrescriptionSoftnessGroupChildren' key={index}>
                                                    <td>{item.updateRecordDate}</td>

                                                    <td>
                                                        {
                                                            item.sports.map((item, index) => {
                                                                return (
                                                                    <p key={index}>{item}</p>
                                                                );
                                                            })
                                                        }
                                                    </td>

                                                    <td>
                                                        <p>{item.exerciseIntensity}</p>
                                                        <p>{item.heartRate}</p>
                                                    </td>

                                                    <td>
                                                        {item.trainingFrequency}
                                                    </td>

                                                    <td>
                                                        {item.mark}
                                                    </td>
                                                </tr>
                                            );
                                        })
                                    }

                                    <tr>
                                        <td>平衡</td>
                                        <td>
                                            {
                                                exercisePrescriptionBalanceGroup?.sports.map((item, index) => {
                                                    return (
                                                        <p key={index}>{item}</p>
                                                    );
                                                })
                                            }
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionBalanceGroup?.exerciseIntensity}</p>
                                            <p>{exercisePrescriptionBalanceGroup?.heartRate}</p>
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionBalanceGroup?.trainingFrequency}</p>
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionBalanceGroup?.mark}</p>
                                        </td>

                                        <TdArrowDown dropDownClassName='exercisePrescriptionBalanceChildren' data={exercisePrescriptionCardiorespiratoryFitnessGroup?.children}></TdArrowDown>

                                        <td className='icon_btn_edit' onClick={showVisibleBalanceDialog}>
                                            <img src={btn_edit} />
                                        </td>
                                    </tr>

                                    {
                                        exercisePrescriptionBalanceGroup?.children.map((item, index) => {
                                            return (
                                                <tr className='exercisePrescriptionBalanceChildren' key={index}>
                                                    <td>
                                                        {item.updateRecordDate}
                                                    </td>

                                                    <td>
                                                        {
                                                            item?.sports.map((item, index) => {
                                                                return (
                                                                    <p key={index}>{item}</p>
                                                                );
                                                            })
                                                        }
                                                    </td>

                                                    <td>
                                                        <p>{item?.exerciseIntensity}</p>
                                                        <p>{item?.heartRate}</p>
                                                    </td>

                                                    <td>
                                                        <p>{item?.trainingFrequency}</p>
                                                    </td>

                                                    <td>
                                                        <p>{item?.mark}</p>
                                                    </td>
                                                </tr>
                                            );
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            {
                visibleTrainingStageDialog && (
                    <Dialog className='visibleTrainingStageDialog'>
                        <Dialog.Title title='編輯'></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <form className='training_stage_form' ref={trainingStageRef}>
                                    <div className='group'>
                                        <h6>設定開始階段週數</h6>
                                        <input type='text' placeholder='設定開始階段週數' defaultValue={trainingFirstStage} onChange={handleTrainingFirstStage} name='firstStage' />
                                        <p>{trainingFirstStageMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>設定改善階段週數</h6>
                                        <input type='text' placeholder='請設定改善階段週數' defaultValue={trainingMiddleStage} onChange={handleTrainingMiddleStage} name='middleStage' />
                                        <p>{trainingMiddleStageMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>設定起始日期</h6>
                                        <input type='date' defaultValue={trainingStartDate} onChange={handleTrainingStartDate} name='startDate' />
                                        <p>{trainingStartDateMessage}</p>
                                    </div>
                                </form>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnBtnYes' onClick={handleExercisePrescriptionTrainingStageReplace}>確定</button>
                            <button className='dialogBtnBtnNo' onClick={closeTrainingStageDialog}>取消</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }

            {/* 運動處方-心肺適能 */}
            {
                visibleCardiorespiratoryFitnessDialog && (
                    <Dialog className='visibleCardiorespiratoryFitnessDialog'>
                        <Dialog.Title title='編輯'></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <form ref={exercisePrescriptionCardiorespiratoryFitnessFormRef}>
                                    <div className='group'>
                                        <h6>訓練項目</h6>
                                        <Select
                                            name='sports'
                                            multiple
                                            options={cardiorespiratoryFitnessSportsOptions}
                                            placeholder='請輸入訓練項目'
                                            handleSelectOption={handleCardiorespiratoryFitnessSportsOptions}
                                        />
                                        <p>{cardiorespiratoryFitnessSportsMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>訓練強度</h6>
                                        <Select
                                            name='exerciseIntensity'
                                            options={cardiorespiratoryFitnessExerciseIntensityOptions}
                                            placeholder='請輸入訓練強度'
                                            handleSelectOption={handleCardiorespiratoryFitnessExerciseIntensitySelectOption}
                                        />
                                        <p>{cardiorespiratoryFitnessExerciseIntensityMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>訓練頻率(天/週)</h6>
                                        <Select
                                            name='exerciseFrequency'
                                            options={cardiorespiratoryFitnessExerciseFrequencyOptions}
                                            placeholder='請輸入訓練頻率(天/週)'
                                            handleSelectOption={handleCardiorespiratoryFitnessExerciseFrequencySelectOption}
                                        />
                                        <p>{cardiorespiratoryFitnessExerciseFrequencyMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>訓練頻率(分鐘/次)</h6>
                                        <Select
                                            name='trainingUnitMinute'
                                            options={cardiorespiratoryFitnessTrainingUnitMinuteOptions}
                                            placeholder='請輸入訓練頻率(分鐘/次)'
                                            handleSelectOption={handleCardiorespiratoryFitnessTrainingUnitMinuteSelectOption}
                                        />
                                        <p>{cardiorespiratoryFitnessTrainingUnitMinuteMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>備註</h6>
                                        <textarea rows={5} cols={30} placeholder='請輸入備註' onChange={handleCardiorespiratoryFitnessMark} name='mark'></textarea>
                                        <p>{cardiorespiratoryFitnessMarkMessage}</p>
                                    </div>
                                </form>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnBtnYes' onClick={handleCardiorespiratoryFitness}>確定</button>
                            <button className='dialogBtnBtnNo' onClick={closeVisibleCardiorespiratoryFitnessDialog}>取消</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }

            {/* 運動處方-肌力與肌耐力 */}
            {
                visibleMuscleStrengthAndMuscleEnduranceDialog && (
                    <Dialog className='visibleMuscleStrengthAndMuscleEnduranceDialog'>
                        <Dialog.Title title='編輯'></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <form ref={exercisePrescriptionMuscleStrengthAndMuscleEnduranceFormRef}>
                                    <div className='group'>
                                        <h6>訓練強度</h6>
                                        <Select
                                            options={muscleStrengthAndMuscleEnduranceExerciseIntensityOptions}
                                            placeholder='請輸入訓練強度'
                                            handleSelectOption={handleMuscleStrengthAndMuscleEnduranceExerciseIntensityOptions}
                                            name='exerciseIntensity'
                                        />
                                        <p>{muscleStrengthAndMuscleEnduranceExerciseIntensityMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>訓練頻率(天/週)</h6>
                                        <Select
                                            options={muscleStrengthAndMuscleEnduranceExerciseFrequencyOptions}
                                            placeholder='請輸入訓練頻率(天/週)'
                                            handleSelectOption={handleMuscleStrengthAndMuscleEnduranceExerciseFrequencyOptions}
                                            name='exerciseFrequency'
                                        />
                                        <p>{muscleStrengthAndMuscleEnduranceExerciseFrequencyMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>訓練頻率(台/組)</h6>
                                        <Select
                                            options={muscleStrengthAndMuscleEnduranceTrainingUnitGroupOptions}
                                            placeholder='請輸入訓練頻率(組/台)'
                                            handleSelectOption={handleMuscleStrengthAndMuscleEnduranceTrainingUnitGroupOptions}
                                            name='trainingUnitGroup'
                                        />
                                        <p>{muscleStrengthAndMuscleEnduranceTrainingUnitGroupMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>訓練頻率(分鐘/次)</h6>
                                        <Select
                                            options={muscleStrengthAndMuscleEnduranceTrainingUnitNumberOptions}
                                            placeholder='請輸入訓練頻率(分鐘/次)'
                                            handleSelectOption={handleMuscleStrengthAndMuscleEnduranceTrainingUnitNumberOptions}
                                            name='trainingUnitNumber'
                                        />
                                        <p>{muscleStrengthAndMuscleEnduranceTrainingUnitNumberMessage}</p>
                                    </div>

                                    <div className='group reinforcement'>
                                        <h6>加強部位</h6>

                                        <div className='leftPictureUpperBodyLeft' onClick={() => handleTrainingArea('上肢')}>

                                        </div>

                                        <div className='leftPictureUpperBodyRight' onClick={() => handleTrainingArea('上肢')}>

                                        </div>

                                        <div className='leftPictureUpperBodyHeart' onClick={() => handleTrainingArea('核心')}>

                                        </div>

                                        <div className='leftPictureUpperBodyBelly' onClick={() => handleTrainingArea('核心')}>

                                        </div>

                                        <div className='leftPictureLowerBodyLeft' onClick={() => handleTrainingArea('下肢')}>

                                        </div>

                                        <div className='leftPictureLowerBodyRight' onClick={() => handleTrainingArea('下肢')}>

                                        </div>


                                        <div className='rightPictureUpperBodyLeft' onClick={() => handleTrainingArea('上肢')}>

                                        </div>

                                        <div className='rightPictureUpperBodyRight' onClick={() => handleTrainingArea('上肢')}>

                                        </div>

                                        <div className='rightPictureUpperBack' onClick={() => handleTrainingArea('核心')}>

                                        </div>

                                        <div className='rightPictureUpperBodyHeart' onClick={() => handleTrainingArea('核心')}>

                                        </div>

                                        <div className='rightPictureUpperBodyBelly' onClick={() => handleTrainingArea('核心')}>

                                        </div>

                                        <div className='rightPictureLowerBodyLeft' onClick={() => handleTrainingArea('下肢')}>

                                        </div>

                                        <div className='rightPictureLowerBodyRight' onClick={() => handleTrainingArea('下肢')}>

                                        </div>

                                        <img className='muscleDistribution' src={handleTrainingAreaImage(muscleStrengthAndMuscleEnduranceTrainingArea)} />

                                        <p>{muscleStrengthAndMuscleEnduranceTrainingAreaMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>備註</h6>
                                        <textarea rows={5} cols={30} placeholder='請輸入備註' onChange={handleMuscleStrengthAndMuscleEnduranceMark} name='mark'></textarea>
                                        <p>{muscleStrengthAndMuscleEnduranceMarkMessage}</p>
                                    </div>
                                </form>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnBtnYes' onClick={handleMuscleStrengthAndMuscleEndurance}>確定</button>
                            <button className='dialogBtnBtnNo' onClick={closeVisibleMuscleStrengthAndMuscleEnduranceDialog}>取消</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }

            {/* 運動處方-柔軟度 */}
            {
                visibleSoftness && (
                    <Dialog className='visibleSoftness'>
                        <Dialog.Title title='編輯'></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <form ref={exercisePrescriptionSoftnessFormRef}>
                                    <div className='group'>
                                        <h6>訓練項目</h6>
                                        <Select
                                            multiple
                                            options={softnessSportsOptions}
                                            placeholder='請輸入訓練項目'
                                            handleSelectOption={handleSoftnessSportsOptions}
                                            name='sports'
                                        />
                                        <p>{softnessSportsMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>訓練強度</h6>
                                        <Select
                                            options={softnessExerciseIntensityOptions}
                                            placeholder='請輸入訓練強度'
                                            handleSelectOption={handleSoftnessExerciseIntensityOptions}
                                            name='exerciseIntensity'
                                        />
                                        <p>{softnessExerciseIntensityMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>訓練頻率(天/週)</h6>
                                        <Select
                                            options={softnessExerciseFrequencyOptions}
                                            placeholder='請輸入訓練頻率(天/週)'
                                            handleSelectOption={handleSoftnessExerciseFrequencyOptions}
                                            name='exerciseFrequency'
                                        />
                                        <p>{softnessExerciseFrequencyMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>訓練頻率(分鐘/次)</h6>
                                        <Select
                                            options={softnessTrainingUnitMinuteOptions}
                                            placeholder='請輸入訓練頻率(分鐘/次)'
                                            handleSelectOption={handleSoftnessTrainingUnitMinuteOptions}
                                            name='trainingUnitMinute'
                                        />
                                        <p>{softnessTrainingUnitMinuteMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>備註</h6>
                                        <textarea defaultValue={balanceMark} rows={5} cols={30} placeholder='請輸入備註' onChange={handleSoftnessMark} name='mark'></textarea>
                                        <p>{softnessMarkMessage}</p>
                                    </div>
                                </form>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnBtnYes' onClick={handleExercisePrescriptionSoftness}>確定</button>
                            <button className='dialogBtnBtnNo' onClick={closeVisibleSoftnessDialog}>取消</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }

            {/* 運動處方-平衡 */}
            {
                visibleBalanceDialog && (
                    <Dialog className='visibleBalanceDialog'>
                        <Dialog.Title title='編輯'></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <form ref={exercisePrescriptionBalanceFormRef}>
                                    <div className='group'>
                                        <h6>訓練項目</h6>
                                        <Select
                                            multiple
                                            options={balanceSportsOptions}
                                            placeholder='請輸入訓練項目'
                                            handleSelectOption={handleBalanceSportsOptions}
                                            name='sports'
                                        />
                                        <p>{balanceSportsMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>訓練強度</h6>
                                        <Select
                                            options={balanceExerciseIntensityOptions}
                                            placeholder='請輸入訓練強度'
                                            handleSelectOption={handleBalanceExerciseIntensityOptions}
                                            name='exerciseIntensity'
                                        />
                                        <p>{balanceExerciseIntensityMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>訓練頻率(天/週)</h6>
                                        <Select
                                            options={balanceExerciseFrequencyOptions}
                                            placeholder='請輸入訓練頻率(天/週)'
                                            handleSelectOption={handleBalanceExerciseFrequencyOptions}
                                            name='exerciseFrequency'
                                        />
                                        <p>{balanceExerciseFrequencyMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>訓練頻率(分鐘/次)</h6>
                                        <Select
                                            options={balanceTrainingUnitMinuteOptions}
                                            placeholder='請輸入訓練頻率(分鐘/次)'
                                            handleSelectOption={handleBalanceTrainingUnitMinute}
                                            name='trainingUnitMinute'
                                        />
                                        <p>{balanceTrainingUnitMinuteMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>備註</h6>
                                        <textarea defaultValue={balanceMark} rows={5} cols={30} placeholder='請輸入備註' onChange={handleBalanceMark} name='mark'></textarea>
                                        <p>{balanceMarkMessage}</p>
                                    </div>
                                </form>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnBtnYes' onClick={handleExercisePrescriptionBalance}>確定</button>
                            <button className='dialogBtnBtnNo' onClick={closeVisibleBalanceDialog}>取消</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }

            {
                visibleInfoDialog && (
                    <Dialog className='visibleInfoDialog'>
                        <Dialog.Title title={visibleInfoDialogTitle}></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <p>{visibleInfoDialogMessage}</p>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnConfirm' onClick={closeDialogInfo}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }
        </div>
    );
};

export default ExercisePrescription;