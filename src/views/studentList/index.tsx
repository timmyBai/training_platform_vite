import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

// css
import './index.sass';

// api
import { getStudentList, disabledStudent } from '@/api/student';

// utils
import { dangerColor } from '@/utils/dangerColor';
import { axiosErrorCode } from '@/utils/request';

// hook
import { useAppDispatch } from '@/hook/store';

// components
import Dialog from '@/components/Dialog';

// images
import user_logo from '@/assets/images/user_photo.svg';
import btn_add from '@/assets/images/btn_add.svg';
import delete_student from '@/assets/images/btn_delete_not_border.svg';
import search_student from '@/assets/images/btn_search.svg';

const StudentList: React.FC = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [responseStudentList, setResponseStudentList] = useState<GetStudentListDateType[]>([]); // 學雸清單回傳
    const [tempStudentList, setTempStudentList] = useState<GetStudentListDateType[]>([]);
    const [activeStudentIndex, setActiveStudentIndex] = useState<number | undefined>(undefined); // 目前學員索引
    const [isShowConfirm, setIsShowConfirm] = useState<boolean>(false); // 確認是否要顯示底下確認按鈕
    const [dialogSearchInput, setDialogSearchInput] = useState<string>(''); // 儲存搜尋文字輸入
    const [isSearch, setIsSearch] = useState<boolean>(false);

    const [visableDialog, setVisableDialog] = useState<boolean>(false); // 資訊彈窗
    const [dialogTitle, setDialogTitle] = useState<string>(''); // 資訊彈窗標題
    const [dialogMessage, setDialogMessage] = useState<string>(''); // 資訊彈窗訊息

    const [visableDeleteDialog, setVisableDeleteDialog] = useState<boolean>(false); // 刪除學員彈窗
    const [visableSearchDialog, setVisableSearchDialog] = useState<boolean>(false); // 查詢學員彈窗

    const [sortItemName, setSortItemName] = useState<string>(''); // 要篩選的名稱
    const [medicalHistorySort, setMedicalHistorySort] = useState<boolean | null>(null);
    const [dangerGradingSort, setDangerGradingSort] = useState<boolean | null>(null);
    const [ageSort, setAgeSort] = useState<boolean | null>(null);
    const [genderSort, setGenderSort] = useState<boolean | null>(null);

    // 取得學員清單
    const handleGetStudentList = () => {
        getStudentList().then((res) => {
            const { data } = res.data;
            setResponseStudentList(data);
        }).catch(() => {
            dispatch({ type: 'user/resetToken' });
        });
    };

    /**
     * 刪除學員(假刪除，只更新帳號狀態)
     */
    const handleDeleteStudent = () => {
        if (activeStudentIndex !== undefined) {
            const userAuthorizedId = responseStudentList[activeStudentIndex].userAuthorizedId;

            disabledStudent(userAuthorizedId).then(() => {
                setVisableDeleteDialog(false);
                setVisableDialog(true);

                setDialogTitle('資訊');
                setDialogMessage('刪除使用者資訊資料成功');

                dispatch({ type: 'studentInfo/clearStudentInfo' });

                setActiveStudentIndex(undefined);

                handleGetStudentList();
            }).catch((error) => {
                const errorCode = axiosErrorCode<ApiDisabledStudentErrorCode>(error);

                setVisableDeleteDialog(false);
                setVisableDialog(true);
                setDialogTitle('錯誤');

                switch (errorCode) {
                    case 'NoSuchStudentFound':
                        setDialogMessage('查無此學員');
                        break;
                    case 'DeleteStudentFail':
                        setDialogMessage('刪除學員失敗');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    // 確認現在點擊哪一個學員
    const handleActiveStudent = (index: number) => {
        setActiveStudentIndex(index);
        setIsShowConfirm(true);
    };

    // 確認選擇學員
    const selectedStudent = () => {
        // 如果沒有點擊任何學員
        if (activeStudentIndex === undefined) {
            setDialogTitle('警告');
            setDialogMessage('未選取任何學員');
            setVisableDialog(true);
        }
        else {
            const targetStudent = responseStudentList[activeStudentIndex];

            // 長出底下清單列，身體評估、運動處方等
            dispatch({ type: 'permission/changeVisableRoutes', payload: true });

            // 儲存目前點擊使用者資訊
            dispatch({ type: 'studentInfo/changeStudentInfo', payload: targetStudent });
            navigate('/bodyCompositionAndPhysicalFitness/');
        }
    };

    // 關閉資料彈窗
    const closeDialog = () => {
        dispatch({ type: 'studentInfo/clearStudentInfo' });
        setVisableDialog(false);
        setDialogTitle('');
        setDialogMessage('');

        setVisableDeleteDialog(false);

        setIsShowConfirm(true);
    };

    // 顯示刪除警告視窗
    const showDeleteStudentDialog = (index: number, e: any) => {
        e.stopPropagation();

        setActiveStudentIndex(index);
        setVisableDeleteDialog(true);
        setIsShowConfirm(false);
    };

    const addStudent = () => {
        navigate('/studentList/add/');
    };

    const showSearchDialog = () => {
        setIsSearch(false);
        setVisableSearchDialog(true);
    };

    // 雙向綁定查詢輸入框
    const handleSearchInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        setDialogSearchInput(e.target.value);
    };

    const filterStudent = () => {
        setIsSearch(true);
        setSortItemName('search_filter_student');
        setVisableSearchDialog(false);
    };

    const closeSearchDialog = () => {
        setIsSearch(false);
        setSortItemName('');
        setVisableSearchDialog(false);
    };

    // 重製排序介面
    const resetSort = () => {
        setMedicalHistorySort(null);
        setDangerGradingSort(null);
        setAgeSort(null);
        setGenderSort(null);
    };

    // 切換疾病史排序
    const handleSortMedicalHistory = () => {
        setSortItemName('medicalHistory');
        resetSort();

        if (medicalHistorySort) {
            setMedicalHistorySort(false);
        }
        else {
            setMedicalHistorySort(true);
        }
    };

    // 切換危險分級排序
    const handleSortDangerGrading = () => {
        setSortItemName('dangerGrading');
        resetSort();

        if (dangerGradingSort) {
            setDangerGradingSort(false);
        }
        else {
            setDangerGradingSort(true);
        }
    };

    // 切換年齡排序
    const handleSortAge = () => {
        resetSort();
        setSortItemName('age');

        if (ageSort) {
            setAgeSort(false);
        }
        else {
            setAgeSort(true);
        }
    };

    // 切換性別排序
    const handleSortGender = () => {
        setSortItemName('gender');
        resetSort();

        if (genderSort) {
            setGenderSort(false);
        }
        else {
            setGenderSort(true);
        }
    };

    // 半自動排序與篩選
    useEffect(() => {
        if (sortItemName === 'medicalHistory') {
            if (medicalHistorySort) {
                const temp = responseStudentList.sort((a: any, b: any) => a.medicalHistoryMequence - b.medicalHistoryMequence).reverse();
                setTempStudentList(temp);
            }
            else {
                const temp = responseStudentList.sort((a: any, b: any) => a.medicalHistoryMequence - b.medicalHistoryMequence);
                setTempStudentList(temp);
            }
        }
        else if (sortItemName === 'dangerGrading') {
            if (dangerGradingSort) {
                const temp = responseStudentList.sort((a: any, b: any) => a.dangerGradingMequence - b.dangerGradingMequence).reverse();
                setTempStudentList(temp);
            }
            else {
                const temp = responseStudentList.sort((a: any, b: any) => a.dangerGradingMequence - b.dangerGradingMequence);
                setTempStudentList(temp);
            }
        }
        else if (sortItemName === 'age') {
            if (ageSort) {
                const temp = responseStudentList.sort((a: any, b: any) => a.age - b.age).reverse();
                setTempStudentList(temp);
            }
            else {
                const temp = responseStudentList.sort((a: any, b: any) => a.age - b.age);
                setTempStudentList(temp);
            }
        }
        else if (sortItemName === 'gender') {
            if (genderSort) {
                const temp = responseStudentList.sort((a: any, b: any) => a.genderMequence - b.genderMequence).reverse();
                setTempStudentList(temp);
            }
            else {
                const temp = responseStudentList.sort((a: any, b: any) => a.genderMequence - b.genderMequence);
                setTempStudentList(temp);
            }
        }
        else if (sortItemName === 'search_filter_student') {
            if (isSearch) {
                if (dialogSearchInput !== '') {
                    const result = responseStudentList.filter((item) => {
                        if (item.name.indexOf(dialogSearchInput.trim()) !== -1 || item.medicalHistory.indexOf(dialogSearchInput.trim()) !== -1) {
                            return item;
                        }
                    });

                    setDialogSearchInput('');
                    setTempStudentList(result);
                    setIsSearch(false);
                }
                else {
                    setTempStudentList(responseStudentList);
                    setDialogSearchInput('');
                    setIsSearch(false);
                }
            }
        }
        else {
            setTempStudentList(responseStudentList);
        }
    }, [sortItemName, medicalHistorySort, dangerGradingSort, isSearch, handleGetStudentList]);

    useEffect(() => {
        dispatch({ type: 'permission/changeVisableRoutes', payload: false });
        handleGetStudentList();
    }, []);

    return (
        <div className='studentList'>
            <div className='page_title'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                        <div className='card'>
                            <div className='headline'>
                                <div className='group'>

                                </div>

                                <div className='group'>
                                    <h2 className='title'>學員清單</h2>
                                </div>

                                <div className='group'>
                                    <img className='add_student' src={btn_add} onClick={addStudent} />
                                    <img className='search_student' src={search_student} onClick={showSearchDialog} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className='student_list_info'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                        <div className='card'>
                            <table className='studentTable'>
                                <thead>
                                    <tr>
                                        <th>
                                            <p>姓名</p>
                                        </th>

                                        <th>
                                            <p onClick={() => handleSortGender()}>性別</p>
                                        </th>

                                        <th>
                                            <p onClick={() => handleSortAge()}>年齡</p>
                                        </th>

                                        <th>
                                            <p>身高/體重</p>
                                        </th>

                                        <th>
                                            <p onClick={() => handleSortMedicalHistory()}>疾病史</p>
                                        </th>

                                        <th>
                                            <p onClick={() => handleSortDangerGrading()}>危險分級</p>
                                        </th>

                                        <th>
                                            <p>操作</p>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {
                                        tempStudentList.map((item, index) => {
                                            return (
                                                <tr className={`item ${activeStudentIndex === index ? 'active' : ''}`} key={item.userAuthorizedId} onClick={() => handleActiveStudent(index)}>
                                                    <td>
                                                        <img className='user_logo' src={item.imageUrl || user_logo} />
                                                        {item.name}
                                                    </td>

                                                    <td>
                                                        {item.gender}
                                                    </td>

                                                    <td>
                                                        {item.age}歲
                                                    </td>

                                                    <td>{item.posture}</td>

                                                    <td>{item.medicalHistory}</td>

                                                    <td>
                                                        <p className={`danger ${dangerColor(item.dangerGrading)}`}>{item.dangerGrading}</p>
                                                    </td>

                                                    <td>
                                                        <img className='delete_student' src={delete_student} onClick={(e) => showDeleteStudentDialog(index, e)} />
                                                    </td>
                                                </tr>
                                            );
                                        })
                                    }
                                </tbody>
                            </table>

                            {
                                (isShowConfirm && activeStudentIndex !== undefined) && (
                                    <div className='confirm'>
                                        <button className='btnComfirm' onClick={selectedStudent}>確定</button>
                                    </div>
                                )
                            }
                        </div>
                    </div>
                </div>
            </div>

            {
                visableDialog && (
                    <Dialog>
                        <Dialog.Title title={dialogTitle}></Dialog.Title>

                        <Dialog.Body>
                            <p>{dialogMessage}</p>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnConfirm' onClick={closeDialog}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }

            {
                visableDeleteDialog && (
                    <Dialog className='visiableDeleteDialog'>
                        <Dialog.Title title={'警告'}></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <p>您確定要刪除此學員</p>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnBtnYes' onClick={() => handleDeleteStudent()}>確定</button>
                            <button className='dialogBtnBtnNo' onClick={() => closeDialog()}>取消</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }

            {
                visableSearchDialog && (
                    <Dialog className='visibleSearchDialog'>
                        <Dialog.Title title={'搜尋'}></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <input className='dialogInput' type='text' placeholder='姓名、疾病史、備註' onChange={handleSearchInput} />
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnBtnYes' onClick={filterStudent}>確定</button>
                            <button className='dialogBtnBtnNo' onClick={closeSearchDialog}>取消</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }
        </div>
    );
};

export default StudentList;