import React from 'react';

// css
import './index.sass';

import StudentBasicInfoContextProvider from './context/StudentBasicInfoContext';
import StudentContainer from './components/StudentContainer';
import QuestionnaireFirstPartContextProvider from './context/QuestionnaireFirstPartContext';
import QuestionnaireTwoPartContextProvider from './context/QuestionnaireTwoPartContext';
import Questionnaire_AHA_And_ACSM_Context_Provider from './context/Questionnaire_AHA_And_ACSM_Context';
import PreTestInfoContextProvider from './context/PreTestInfoContext';

const AddStudentInfo: React.FC = () => {
    return (
        <StudentBasicInfoContextProvider>
            <QuestionnaireFirstPartContextProvider>
                <QuestionnaireTwoPartContextProvider>
                    <Questionnaire_AHA_And_ACSM_Context_Provider>
                        <PreTestInfoContextProvider>
                            <StudentContainer></StudentContainer>
                        </PreTestInfoContextProvider>
                    </Questionnaire_AHA_And_ACSM_Context_Provider>
                </QuestionnaireTwoPartContextProvider>
            </QuestionnaireFirstPartContextProvider>
        </StudentBasicInfoContextProvider>
    );
};

export default AddStudentInfo;