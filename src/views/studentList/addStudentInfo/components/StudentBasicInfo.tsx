import React, { useContext, useRef, useState } from 'react';
import validate from 'validate.js';

// css
import './StudentBasicInfo.sass';

// context
import { StudentBasicInfoContext } from '../context/StudentBasicInfoContext';

const StudentBasicInfo: React.FC = () => {
    const context = useContext(StudentBasicInfoContext);
    const studentBasicInfoFormRef = useRef<HTMLFormElement>(null);
    const [accountErrorMessage, setAccountErrorMessage] = useState<string>('');
    const [nameErrorMessage, setNameErrorMessage] = useState<string>('');
    const [genderErrorMessage, setGenderErrorMessage] = useState<string>('');
    const [birthdayErrorMessage, setBirthdayErrorMessage] = useState<string>('');
    const [heightErrorMessage, setHeightErrorMessage] = useState<string>('');
    const [weightErrorMessage, setWeightErrorMessage] = useState<string>('');
    const [phoneErrorMessage, setPhoneErrorMessage] = useState<string>('');
    const [emailErrorMessage, setEmailErrorMessage] = useState<string>('');
    const [addressErrorMessage, setAddressErrorMessage] = useState<string>('');
    const [passwordErrorMessage, setPasswordErrorMessage] = useState<string>('');
    const [confirmPasswordErrorMessage, setConfirmPasswordErrorMessage] = useState<string>('');

    const studentBasicInfoFormValidate = {
        account: {
            presence: {
                message: '必須輸入帳號'
            },
            format: {
                pattern: '^[A-Za-z0-9]+',
                flags: 'g',
                message: '只能輸入大小寫英文及數字'
            },
            length: {
                minimum: 8,
                maximum: 50,
                message: '帳號最短長度大於 8 個字，最長 50 個字'
            }
        },
        name: {
            presence: {
                message: '必須輸入姓名'
            },
            length: {
                maximum: 10,
                message: '姓名最長 10 個字'
            }
        },
        gender: {
            presence: {
                message: '必須填選性別'
            },
            format: {
                pattern: '(男|女)',
                flags: 'g',
                message: '只能輸入男或女'
            },
            length: {
                minimum: 1,
                message: '只能輸入一個字'
            }
        },
        birthday: {
            presence: {
                message: '必須輸入生日'
            }
        },
        height: {
            presence: {
                message: '必須輸入身高'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數字'
            }
        },
        weight: {
            presence: {
                message: '必須輸入體重'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數字'
            }
        },
        phone: {
            presence: {
                message: '必須輸入電話'
            },
            format: {
                pattern: '^[0-9]+',
                flags: 'g',
                message: '請輸入數字'
            },
            length: {
                minimum: 8,
                maximum: 10,
                message: '電話最短 8 個字，最長 10 個字'
            }
        },
        email: {
            email: {
                message: 'e-mail 格式錯誤'
            },
            format: {
                pattern: '^((?!example).)*$',
                flags: 'g',
                message: '請輸入正確信箱'
            },
            length: {
                maximum: 50,
                message: 'e-mail 最長 50 個字'
            }
        },
        address: {
            length: {
                minimum: 3,
                maximum: 50,
                message: '地址最短長度 3 個字，最長 50 個字'
            }
        },
        password: {
            presence: {
                message: '必輸輸入密碼'
            },
            length: {
                minimum: 8,
                maximum: 50,
                message: '密碼最短長度大於 8 個字，最長 50 個字'
            },
            format: {
                pattern: '^[A-Za-z0-9]+',
                flags: 'g',
                message: '只能輸入大小寫英文及數字'
            }
        },
        confirmPassword: {
            presence: {
                message: '必輸輸入確認密碼'
            },
            length: {
                minimum: 8,
                maximum: 50,
                message: '確認密碼最短長度大於 8 個字，最長 50 個字'
            },
            format: {
                pattern: '^[A-Za-z0-9]+',
                flags: 'g',
                message: '只能輸入大小寫英文及數字'
            }
        }
    };

    /**
     * 處理帳號輸入
     */
    const handleAccountInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        context.handleAccountInput(e.target.value);

        const isValid = validate(studentBasicInfoFormRef.current, studentBasicInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.account) {
                setAccountErrorMessage(isValid.account[0].replace('Account ', ''));
            }
            else {
                setAccountErrorMessage('');
            }
        }
        else {
            setAccountErrorMessage('');
        }
    };

    /**
     * 處理姓名輸入
     */
    const handleNameInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        context.handleNameInput(e.target.value);

        const isValid = validate(studentBasicInfoFormRef.current, studentBasicInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.name) {
                setNameErrorMessage(isValid.name[0].replace('Name ', ''));
            }
            else {
                setNameErrorMessage('');
            }
        }
        else {
            setNameErrorMessage('');
        }
    };

    /**
     * 處理性別輸入
     */
    const handleGenderInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        context.handleGenderInput(e.target.value);

        const isValid = validate(studentBasicInfoFormRef.current, studentBasicInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.gender) {
                setGenderErrorMessage(isValid.gender[0].replace('Gender ', ''));
            }
            else {
                setGenderErrorMessage('');
            }
        }
        else {
            setGenderErrorMessage('');
        }
    };

    /**
     * 處理生日輸入
     */
    const handleBirthdayInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        context.handleBirthdayInput(e.target.value);
        
        const isValid = validate(studentBasicInfoFormRef.current, studentBasicInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.birthday) {
                setBirthdayErrorMessage(isValid.birthday[0].replace('Birthday ', ''));
            }
            else {
                setBirthdayErrorMessage('');
            }
        }
        else {
            setBirthdayErrorMessage('');
        }
    };

    /**
     * 處理身高輸入
     */
    const handleHeightInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const isValid = validate(studentBasicInfoFormRef.current, studentBasicInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.height) {
                setHeightErrorMessage(isValid.height[0].replace('Height ', ''));
            }
            else {
                setHeightErrorMessage('');
                context.handleHeightInput(parseInt(e.target.value));
            }
        }
        else {
            setHeightErrorMessage('');
        }
    };

    /**
     * 處理體重輸入
     */
    const handleWeightInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const isValid = validate(studentBasicInfoFormRef.current, studentBasicInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.weight) {
                setWeightErrorMessage(isValid.weight[0].replace('Weight ', ''));
            }
            else {
                setWeightErrorMessage('');
                context.handleWeightInput(parseInt(e.target.value));
            }
        }
        else {
            setWeightErrorMessage('');
        }
    };

    /**
     * 處理電話輸入
     */
    const handlePhoneInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        context.handlePhoneInput(e.target.value);

        const isValid = validate(studentBasicInfoFormRef.current, studentBasicInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.phone) {
                setPhoneErrorMessage(isValid.phone[0].replace('Phone ', ''));
            }
            else {
                setPhoneErrorMessage('');
            }
        }
        else {
            setPhoneErrorMessage('');
        }
    };

    const handleEmailInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        context.handleEmailInput(e.target.value);

        const isValid = validate(studentBasicInfoFormRef.current, studentBasicInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.email) {
                setEmailErrorMessage(isValid.email[0].replace('Email ', ''));
            }
            else {
                setEmailErrorMessage('');
            }
        }
        else {
            setEmailErrorMessage('');
        }
    };

    /**
     * 處理地址輸入
     */
    const handleAddressInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        context.handleAddressInput(e.target.value);

        const isValid = validate(studentBasicInfoFormRef.current, studentBasicInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.address) {
                setAddressErrorMessage(isValid.address[0].replace('Address ', ''));
            }
            else {
                setAddressErrorMessage('');
            }
        }
        else {
            setAddressErrorMessage('');
        }
    };

    /**
     * 處理密碼輸入
     */
    const handlePasswordInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        context.handlePasswordInput(e.target.value);

        const isValid = validate(studentBasicInfoFormRef.current, studentBasicInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.password) {
                setPasswordErrorMessage(isValid.password[0].replace('Password ', ''));
            }
            else {
                setPasswordErrorMessage('');
            }
        }
        else {
            setPasswordErrorMessage('');
        }
    };

    /**
     * 處理確認密碼輸入
     */
    const handleConfirmPasswordInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        inspectStudentBasicField();
        context.handleConfirmPasswordInput(e.target.value);

        const isValid = validate(studentBasicInfoFormRef.current, studentBasicInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.confirmPassword) {
                setConfirmPasswordErrorMessage(isValid.confirmPassword[0].replace('Confirm password ', ''));
            }
            else {
                setConfirmPasswordErrorMessage('');
            }
        }
        else {
            setConfirmPasswordErrorMessage('');
        }
    };

    const inspectStudentBasicField = (): boolean => {
        const isValid = validate(studentBasicInfoFormRef.current, studentBasicInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.account) {
                setAccountErrorMessage(isValid.account[0].replace('Account ', ''));
            }
            else {
                setAccountErrorMessage('');
            }

            if (isValid.name) {
                setNameErrorMessage(isValid.name[0].replace('Name ', ''));
            }
            else {
                setNameErrorMessage('');
            }

            if (isValid.gender) {
                setGenderErrorMessage(isValid.gender[0].replace('Gender ', ''));
            }
            else {
                setGenderErrorMessage('');
            }

            if (isValid.birthday) {
                setBirthdayErrorMessage(isValid.birthday[0].replace('Birthday ', ''));
            }
            else {
                setBirthdayErrorMessage('');
            }

            if (isValid.height) {
                setHeightErrorMessage(isValid.height[0].replace('Height ', ''));
            }
            else {
                setHeightErrorMessage('');
            }

            if (isValid.weight) {
                setWeightErrorMessage(isValid.weight[0].replace('Weight ', ''));
            }
            else {
                setWeightErrorMessage('');
            }

            if (isValid.phone) {
                setPhoneErrorMessage(isValid.phone[0].replace('Phone ', ''));
            }
            else {
                setPhoneErrorMessage('');
            }

            if (isValid.email) {
                setEmailErrorMessage(isValid.email[0].replace('Email ', ''));
            }
            else {
                setEmailErrorMessage('');
            }

            if (isValid.address) {
                setAddressErrorMessage(isValid.address[0].replace('Address ', ''));
            }
            else {
                setAddressErrorMessage('');
            }

            if (isValid.password) {
                setPasswordErrorMessage(isValid.password[0].replace('Password ', ''));
            }
            else {
                setPasswordErrorMessage('');
            }

            if (isValid.confirmPassword) {
                setConfirmPasswordErrorMessage(isValid.confirmPassword[0].replace('Confirm password ', ''));
            }
            else {
                setConfirmPasswordErrorMessage('');
            }

            return false;
        }
        else {
            setAccountErrorMessage('');
            setNameErrorMessage('');
            setGenderErrorMessage('');
            setBirthdayErrorMessage('');
            setHeightErrorMessage('');
            setWeightErrorMessage('');
            setPhoneErrorMessage('');
            setEmailErrorMessage('');
            setAddressErrorMessage('');
            setPasswordErrorMessage('');
            setConfirmPasswordErrorMessage('');

            return true;
        }
    };

    const confirm = () => {
        const isValid = inspectStudentBasicField();

        if (!isValid) {
            return;
        }
        else {
            const isPassAccount = (/[A-Z]/g.test(context.account) || /[a-z]/g.test(context.account)) && /[0-9]/g.test(context.account);
            const isPassPassword = (/[A-Z]/g.test(context.password) || /[a-z]/g.test(context.password)) && /[0-9]/g.test(context.password);
            const isPassConfirmPassword = (/[A-Z]/g.test(context.confirmPassword) || /[a-z]/g.test(context.confirmPassword)) && /[0-9]/g.test(context.confirmPassword);
            const isPasswordAndConfirmPasswordEqual = context.password === context.confirmPassword;
            const nowYear = new Date().getFullYear();
            const isAgeSuitable = nowYear - parseInt(context.birthday.substring(0, 4));
            
            if (isPassAccount && isPassPassword && isPassConfirmPassword) {
                if (isAgeSuitable <= 20) {
                    setBirthdayErrorMessage('學員年齡必須在 21 歲以上才可適訓練');
                }
                else if (!isPasswordAndConfirmPasswordEqual) {
                    context.handlePasswordInput('');
                    context.handleConfirmPasswordInput('');
                    setPasswordErrorMessage('密碼與確認密碼不相同');
                    setConfirmPasswordErrorMessage('密碼與確認密碼不相同');
                }
                else {
                    context.addPageId();
                }
            }
            else {
                if (!isPassAccount) {
                    setAccountErrorMessage('帳號必須要有英文大寫或小寫及數字組合');
                }

                if (!isPassPassword) {
                    setPasswordErrorMessage('密碼必須要有英文大寫或小寫及數字組合');
                }

                if (!isPassConfirmPassword) {
                    setConfirmPasswordErrorMessage('確認密碼要必須有英文大寫或小寫及數字組合');
                }
            }
        }
    };

    return (
        <div className='studentBasicInfo'>
            <form ref={studentBasicInfoFormRef}>
                <div className='flex flex-wrap'>
                    {/* 帳號 */}
                    <div className='w-full sm:w-3/12 md:w-3/12 lg:w-3/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card left'>
                            <h5>帳號</h5>
                        </div>
                    </div>

                    <div className='w-full sm:w-9/12 md:w-9/12 lg:w-9/12 xl:w-9/12 xxl:w-9/12'>
                        <div className='card right'>
                            <input type='text' placeholder='請輸入帳號' defaultValue={context.account} name='account' onChange={handleAccountInput} />
                            {accountErrorMessage && <p className='error_message'>{accountErrorMessage}</p>}
                        </div>
                    </div>

                    {/* 姓名 */}
                    <div className='w-full sm:w-3/12 md:w-3/12 lg:w-3/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card left'>
                            <h5>姓名</h5>
                        </div>
                    </div>

                    <div className='w-full sm:w-9/12 md:w-9/12 lg:w-9/12 xl:w-9/12 xxl:w-9/12'>
                        <div className='card right'>
                            <input type='text' placeholder='請輸入姓名' defaultValue={context.name} name='name' onChange={handleNameInput} />
                            {nameErrorMessage && <p className='error_message'>{nameErrorMessage}</p>}
                        </div>
                    </div>

                    {/* 生理性別 */}
                    <div className='w-full sm:w-3/12 md:w-3/12 lg:w-3/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card left'>
                            <h5>生理性別</h5>
                        </div>
                    </div>

                    <div className='w-full sm:w-9/12 md:w-9/12 lg:w-9/12 xl:w-9/12 xxl:w-9/12'>
                        <div className='card right'>
                            <label>
                                <input type='radio' value='男' checked={context.gender === '男'} name='gender' onChange={handleGenderInput} />
                                男
                            </label>

                            <label>
                                <input type='radio' value='女' checked={context.gender === '女'} name='gender' onChange={handleGenderInput} />
                                女
                            </label>

                            {genderErrorMessage !== '' && <p className='error_message'>{genderErrorMessage}</p>}
                        </div>
                    </div>

                    {/* 出生日期 */}
                    <div className='w-full sm:w-3/12 md:w-3/12 lg:w-3/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card left'>
                            <h5>出生日期</h5>
                        </div>
                    </div>

                    <div className='w-full sm:w-9/12 md:w-9/12 lg:w-9/12 xl:w-9/12 xxl:w-9/12'>
                        <div className='card right'>
                            <input type='date' defaultValue={context.birthday} name='birthday' onChange={handleBirthdayInput} />
                            {birthdayErrorMessage && <p className='error_message'>{birthdayErrorMessage}</p>}
                        </div>
                    </div>

                    {/* 身高 */}
                    <div className='w-full sm:w-3/12 md:w-3/12 lg:w-3/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card left'>
                            <h5>身高(公分)</h5>
                        </div>
                    </div>

                    <div className='w-full sm:w-9/12 md:w-9/12 lg:w-9/12 xl:w-9/12 xxl:w-9/12'>
                        <div className='card right'>
                            <input type='text' placeholder='請輸入身高(公分)' defaultValue={context.height?.toString()} name='height' onChange={handleHeightInput} />
                            {heightErrorMessage !== '' && <p className='error_message'>{heightErrorMessage}</p>}
                        </div>
                    </div>

                    {/* 體重 */}
                    <div className='w-full sm:w-3/12 md:w-3/12 lg:w-3/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card left'>
                            <h5>體重(公斤)</h5>
                        </div>
                    </div>

                    <div className='w-full sm:w-9/12 md:w-9/12 lg:w-9/12 xl:w-9/12 xxl:w-9/12'>
                        <div className='card right'>
                            <input type='text' placeholder='請輸入體重(公斤)' defaultValue={context.weight?.toString()} name='weight' onChange={handleWeightInput} />
                            {weightErrorMessage !== '' && <p className='error_message'>{weightErrorMessage}</p>}
                        </div>
                    </div>

                    {/* 電話 */}
                    <div className='w-full sm:w-3/12 md:w-3/12 lg:w-3/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card left'>
                            <h5>電話</h5>
                        </div>
                    </div>

                    <div className='w-full sm:w-9/12 md:w-9/12 lg:w-9/12 xl:w-9/12 xxl:w-9/12'>
                        <div className='card right'>
                            <input type='text' placeholder='請輸入電話' defaultValue={context.phone} name='phone' onChange={handlePhoneInput} />
                            {phoneErrorMessage !== '' && <p className='error_message'>{phoneErrorMessage}</p>}
                        </div>
                    </div>

                    {/* 信箱 */}
                    <div className='w-full sm:w-3/12 md:w-3/12 lg:w-3/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card left'>
                            <h5>e-mail 信箱</h5>
                        </div>
                    </div>

                    <div className='w-full sm:w-9/12 md:w-9/12 lg:w-9/12 xl:w-9/12 xxl:w-9/12'>
                        <div className='card right'>
                            <input type='text' placeholder='請輸入 e-mail 信箱' defaultValue={context.email} name='email' onChange={handleEmailInput} />
                            {emailErrorMessage !== '' && <p className='error_message'>{emailErrorMessage}</p>}
                        </div>
                    </div>

                    {/* 地址 */}
                    <div className='w-full sm:w-3/12 md:w-3/12 lg:w-3/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card left'>
                            <h5>地址</h5>
                        </div>
                    </div>

                    <div className='w-full sm:w-9/12 md:w-9/12 lg:w-9/12 xl:w-9/12 xxl:w-9/12'>
                        <div className='card right'>
                            <input type='text' placeholder='請輸入地址' defaultValue={context.address} name='address' onChange={handleAddressInput} />
                            {addressErrorMessage !== '' && <p className='error_message'>{addressErrorMessage}</p>}
                        </div>
                    </div>

                    {/* 設定密碼 */}
                    <div className='w-full sm:w-3/12 md:w-3/12 lg:w-3/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card left'>
                            <h5>設定密碼</h5>
                        </div>
                    </div>

                    <div className='w-full sm:w-3/12 md:w-3/12 lg:w-3/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card left right'>
                            <input type='password' placeholder='請輸入密碼' value={context.password} name='password' onChange={handlePasswordInput} />
                            {passwordErrorMessage !== '' && <p className='error_message'>{passwordErrorMessage}</p>}
                        </div>
                    </div>

                    <div className='w-full sm:w-3/12 md:w-3/12 lg:w-3/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card left right'>
                            <h5>再次輸入密碼</h5>
                        </div>
                    </div>

                    <div className='w-full sm:w-3/12 md:w-3/12 lg:w-3/12 xl:w-3/12 xxl:w-3/12'>
                        <div className='card right'>
                            <input type='password' placeholder='請再次輸入密碼' value={context.confirmPassword} name='confirmPassword' onChange={handleConfirmPasswordInput} />
                            {confirmPasswordErrorMessage !== '' && <p className='error_message'>{confirmPasswordErrorMessage}</p>}
                        </div>
                    </div>
                </div>
            </form>

            {/* 確定 */}
            <div className='flex flex-wrap'>
                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                    <div className='card center'>
                        <button className='confirm' onClick={confirm}>確定</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default StudentBasicInfo;