import React, { Fragment, useContext, useEffect, useRef, useState } from 'react';
import { validate } from 'validate.js';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';

// css
import './PreTestInfo.sass';

// context
import { StudentBasicInfoContext } from '../context/StudentBasicInfoContext';
import { QuestionnaireFirstPartContext } from '../context/QuestionnaireFirstPartContext';
import { QuestionnaireTwoPartContext } from '../context/QuestionnaireTwoPartContext';
import { Questionnaire_AHA_And_ACSM_Context } from '../context/Questionnaire_AHA_And_ACSM_Context';
import { PreTestInfoContext } from '../context/PreTestInfoContext';

// hook
import { useAppDispatch } from '@/hook/store';

// api
import { authRegister } from '@/api/auth';

// utils
import { isStudentGreaterThan65 } from '@/utils/student';
import { axiosErrorCode } from '@/utils/request';

const PreTestInfo: React.FC = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [age, setAge] = useState<number>(0);
    const studentBasicInfoContext = useContext(StudentBasicInfoContext);
    const questionnaireFirstPartContext = useContext(QuestionnaireFirstPartContext);
    const questionnaireTwoPartContext = useContext(QuestionnaireTwoPartContext);
    const questionnaire_aha_and_acsm_context = useContext(Questionnaire_AHA_And_ACSM_Context);
    const preTestInfoContext = useContext(PreTestInfoContext);

    const preTestInfoFormRef = useRef<HTMLFormElement>(null);

    const [pulseRateOneErrorMessage, setPulseRateOneErrorMessage] = useState<string>(''); // 第一次脈搏錯誤訊息
    const [pulseRateTwoErrorMessage, setPulseRateTwoErrorMessage] = useState<string>(''); // 第二次脈搏錯誤訊息
    const [pulseRateThreeErrorMessage, setPlseRateThreeErrorMessage] = useState<string>(''); // 第三次脈搏錯誤訊息
    const [kneeCrunchesFrequencyErrorMessage, setKneeCrunchesFrequencyErrorMessage] = useState<string>(''); // 屈膝仰臥起坐錯誤訊息
    const [seatedForwardBendOneErrorMessage, setSeatedForwardBendOneErrorMessage] = useState<string>(''); // 第一次坐姿體前彎錯誤訊息
    const [seatedForwardBendTwoErrorMessage, setSeatedForwardBendTwoErrorMessage] = useState<string>(''); // 第二次坐姿體前彎錯誤訊息
    const [seatedForwardBendThreeErrorMessage, setSeatedForwardBendThreeErrorMessage] = useState<string>(''); // 第三次坐姿體前彎錯誤訊息
    const [adultEyeOpeningMonopodSecondErrorMessage, setAdultEyeOpeningMonopodSecondErrorMessage] = useState<string>(''); // 壯年單眼開足立

    const [kneeLiftFrequencyErrorMessage, setKneeLiftFrequencyErrorMessage] = useState<string>('');
    const [armCurlUpperBodyFrequencyErrorMessage, setArmCurlUpperBodyFrequencyErrorMessage] = useState<string>('');
    const [armCurlLowerBodyFrequencyErrorMessage, setArmCurlLowerBodyFrequencyErrorMessage] = useState<string>('');
    const [chairSeatedForwardBendOneErrorMessage, setChairSeatedForwardBendOneErrorMessage] = useState<string>('');
    const [chairSeatedForwardBendTwoErrorMessage, setChairSeatedForwardBendTwoErrorMessage] = useState<string>('');
    const [chairSeatedForwardBendThreeErrorMessage, setChairSeatedForwardBendThreeErrorMessage] = useState<string>('');
    const [pullBackTestOneErrorMessage, setPullBackTestOneErrorMessage] = useState<string>('');
    const [pullBackTestTwoErrorMessage, setPullBackTestTwoErrorMessage] = useState<string>('');
    const [elderlyEyeOpeningMonopodSecondErrorMessage, setElderlyEyeOpeningMonopodSecondErrorMessage] = useState<string>('');
    const [sittingAroundOneSecondErrorMessage, setSittingAroundOneSecondErrorMessage] = useState<string>('');
    const [sittingAroundTwoSecondErrorMessage, setSittingAroundTwoSecondErrorMessage] = useState<string>('');

    // 前測壯年驗證欄位
    const preTestAdultInfoFormValidate = {
        pulseRateOne: {
            presence: {
                message: '必須輸入第一次脈搏'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        pulseRateTwo: {
            presence: {
                message: '必須輸入第二次脈搏'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        pulseRateThree: {
            presence: {
                message: '必須輸入第三次脈搏'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        kneeCrunchesFrequency: {
            presence: {
                message: '必須輸入屈膝仰臥起坐'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        seatedForwardBendOne: {
            presence: {
                message: '必須輸入第一次坐姿體前彎'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        seatedForwardBendTwo: {
            presence: {
                message: '必須輸入第二次坐姿體前彎'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        seatedForwardBendThree: {
            presence: {
                message: '必須輸入第三次坐姿體前彎'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        adultEyeOpeningMonopodSecond: {
            presence: {
                message: '必須輸入壯年開眼單足立'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        }
    };

    // 前測老年驗證欄位
    const preTestElderlyInfoFormValidate = {
        kneeLiftFrequency: {
            presence: {
                message: '必須輸入原地站立抬膝'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        armCurlUpperBodyFrequency: {
            presence: {
                message: '必須輸入手臂彎舉(上肢)'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        armCurlLowerBodyFrequency: {
            presence: {
                message: '必須輸入手臂彎舉(下肢)'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        chairSeatedForwardBendOne: {
            presence: {
                message: '必須輸入第一次椅子坐姿體前彎'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        chairSeatedForwardBendTwo: {
            presence: {
                message: '必須輸入第二次椅子坐姿體前彎'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        chairSeatedForwardBendThree: {
            presence: {
                message: '必須輸入第三次椅子坐姿體前彎'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        pullBackTestOne: {
            presence: {
                message: '必須輸入第一次拉背測驗'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        pullBackTestTwo: {
            presence: {
                message: '必須輸入第二次拉背測驗'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        elderlyEyeOpeningMonopodSecond: {
            presence: {
                message: '必須輸入老年開眼單足立'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        sittingAroundOneSecond: {
            presence: {
                message: '必須輸入第一次坐立繞物'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        sittingAroundTwoSecond: {
            presence: {
                message: '必須輸入第二次坐立繞物'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        }
    };

    // 處理第一次脈搏次數輸入
    const handlePulseOneInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestAdultInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.pulseRateOne) {
                preTestInfoContext.handlePulseOneInput(undefined);
                setPulseRateOneErrorMessage(isValid.pulseRateOne[0].replace('Pulse rate one ', ''));
            }
            else {
                preTestInfoContext.handlePulseOneInput(parseInt(e.target.value));
                setPulseRateOneErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handlePulseOneInput(parseInt(e.target.value));
            setPulseRateOneErrorMessage('');
        }
    };

    // 處理第二次脈搏次數輸入
    const handlePulseTwoInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestAdultInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.pulseRateTwo) {
                preTestInfoContext.handlePulseTwoInput(undefined);
                setPulseRateTwoErrorMessage(isValid.pulseRateTwo[0].replace('Pulse rate two ', ''));
            }
            else {
                preTestInfoContext.handlePulseTwoInput(parseInt(e.target.value));
                setPulseRateTwoErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handlePulseTwoInput(parseInt(e.target.value));
            setPulseRateTwoErrorMessage('');
        }
    };

    // 處理第三次脈搏次數輸入
    const handlePulseThreeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestAdultInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.pulseRateThree) {
                preTestInfoContext.handlePulseThreeInput(undefined);
                setPlseRateThreeErrorMessage(isValid.pulseRateThree[0].replace('Pulse rate three ', ''));
            }
            else {
                preTestInfoContext.handlePulseThreeInput(parseInt(e.target.value));
                setPlseRateThreeErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handlePulseThreeInput(parseInt(e.target.value));
            setPlseRateThreeErrorMessage('');
        }
    };

    // 處理屈膝仰臥起坐輸入
    const handleKneeCrunchesFrequencyInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestAdultInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.kneeCrunchesFrequency) {
                preTestInfoContext.handleKneeCrunchesFrequencyInput(undefined);
                setKneeCrunchesFrequencyErrorMessage(isValid.kneeCrunchesFrequency[0].replace('Knee crunches frequency ', ''));
            }
            else {
                preTestInfoContext.handleKneeCrunchesFrequencyInput(parseInt(e.target.value));
                setKneeCrunchesFrequencyErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleKneeCrunchesFrequencyInput(parseInt(e.target.value));
            setKneeCrunchesFrequencyErrorMessage('');
        }
    };

    // 處理第一次坐姿體前彎輸入
    const handleSeatedForwardBendOneInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestAdultInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.seatedForwardBendOne) {
                preTestInfoContext.handleSeatedForwardBendOneInput(undefined);
                setSeatedForwardBendOneErrorMessage(isValid.seatedForwardBendOne[0].replace('Seated forward bend one ', ''));
            }
            else {
                preTestInfoContext.handleSeatedForwardBendOneInput(parseFloat(e.target.value));
                setSeatedForwardBendOneErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleSeatedForwardBendOneInput(parseFloat(e.target.value));
            setSeatedForwardBendOneErrorMessage('');
        }
    };

    // 處理第二次坐姿體前彎輸入
    const handleSeatedForwardBendTwoInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestAdultInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.seatedForwardBendTwo) {
                preTestInfoContext.handleSeatedForwardBendTwoInput(undefined);
                setSeatedForwardBendTwoErrorMessage(isValid.seatedForwardBendTwo[0].replace('Seated forward bend two ', ''));
            }
            else {
                preTestInfoContext.handleSeatedForwardBendTwoInput(parseFloat(e.target.value));
                setSeatedForwardBendTwoErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleSeatedForwardBendTwoInput(parseFloat(e.target.value));
            setSeatedForwardBendTwoErrorMessage('');
        }
    };

    // 處理第三次坐姿體前彎輸入
    const handleSeatedForwardBendThreeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestAdultInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.seatedForwardBendThree) {
                preTestInfoContext.handleSeatedForwardBendThreeInput(undefined);
                setSeatedForwardBendThreeErrorMessage(isValid.seatedForwardBendThree[0].replace('Seated forward bend three ', ''));
            }
            else {
                preTestInfoContext.handleSeatedForwardBendThreeInput(parseFloat(e.target.value));
                setSeatedForwardBendThreeErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleSeatedForwardBendThreeInput(parseFloat(e.target.value));
            setSeatedForwardBendThreeErrorMessage('');
        }
    };

    // 處理壯年開眼單足立輸入
    const handleAdultEyeOpeningMonopodSecond = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestAdultInfoFormValidate);
        
        if (isValid !== undefined) {
            if (isValid.adultEyeOpeningMonopodSecond) {
                preTestInfoContext.handleAdultEyeOpeningMonopodSecondInput(undefined);
                setAdultEyeOpeningMonopodSecondErrorMessage(isValid.adultEyeOpeningMonopodSecond[0].replace('Adult eye opening monopod second ', ''));
            }
            else {
                preTestInfoContext.handleAdultEyeOpeningMonopodSecondInput(parseFloat(e.target.value));
                setAdultEyeOpeningMonopodSecondErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleAdultEyeOpeningMonopodSecondInput(parseFloat(e.target.value));
            setAdultEyeOpeningMonopodSecondErrorMessage('');
        }
    };



    // 處理原地站立抬膝輸入
    const handleKneeLiftFrequencyInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestElderlyInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.kneeLiftFrequency) {
                preTestInfoContext.handleKneeLiftFrequencyInput(undefined);
                setKneeLiftFrequencyErrorMessage(isValid.kneeLiftFrequency[0].replace('Knee lift frequency ', ''));
            }
            else {
                preTestInfoContext.handleKneeLiftFrequencyInput(parseInt(e.target.value));
                setKneeLiftFrequencyErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleKneeLiftFrequencyInput(parseInt(e.target.value));
            setKneeLiftFrequencyErrorMessage('');
        }
    };

    // 處理手臂彎舉(上肢)輸入
    const handleArmCurlUpperBodyFrequencyInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestElderlyInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.armCurlUpperBodyFrequency) {
                preTestInfoContext.handleArmCurlUpperBodyFrequencyInput(undefined);
                setArmCurlUpperBodyFrequencyErrorMessage(isValid.armCurlUpperBodyFrequency[0].replace('Arm curl upper body frequency ', ''));
            }
            else {
                preTestInfoContext.handleArmCurlUpperBodyFrequencyInput(parseInt(e.target.value));
                setArmCurlUpperBodyFrequencyErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleArmCurlUpperBodyFrequencyInput(parseInt(e.target.value));
            setArmCurlUpperBodyFrequencyErrorMessage('');
        }
    };

    // 處理起立坐下(下肢)輸入
    const handleArmCurlLowerBodyFrequencyInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestElderlyInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.armCurlLowerBodyFrequency) {
                preTestInfoContext.handleArmCurlLowerBodyFrequencyInput(undefined);
                setArmCurlLowerBodyFrequencyErrorMessage(isValid.armCurlLowerBodyFrequency[0].replace('Arm curl lower body frequency ', ''));
            }
            else {
                preTestInfoContext.handleArmCurlLowerBodyFrequencyInput(parseInt(e.target.value));
                setArmCurlLowerBodyFrequencyErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleArmCurlLowerBodyFrequencyInput(parseInt(e.target.value));
            setArmCurlLowerBodyFrequencyErrorMessage('');
        }
    };

    // 處理第一次椅子坐姿體前彎輸入
    const handleChairSeatedForwardBendOneInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestElderlyInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.chairSeatedForwardBendOne) {
                preTestInfoContext.handleChairSeatedForwardBendOneInput(undefined);
                setChairSeatedForwardBendOneErrorMessage(isValid.chairSeatedForwardBendOne[0].replace('Chair seated forward bend one ', ''));
            }
            else {
                preTestInfoContext.handleChairSeatedForwardBendOneInput(parseFloat(e.target.value));
                setChairSeatedForwardBendOneErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleChairSeatedForwardBendOneInput(parseFloat(e.target.value));
            setChairSeatedForwardBendOneErrorMessage('');
        }
    };

    // 處理第二次椅子坐姿體前彎輸入
    const handleChairSeatedForwardBendTwoInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestElderlyInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.chairSeatedForwardBendTwo) {
                preTestInfoContext.handleChairSeatedForwardBendTwoInput(undefined);
                setChairSeatedForwardBendTwoErrorMessage(isValid.chairSeatedForwardBendTwo[0].replace('Chair seated forward bend two ', ''));
            }
            else {
                preTestInfoContext.handleChairSeatedForwardBendTwoInput(parseFloat(e.target.value));
                setChairSeatedForwardBendTwoErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleChairSeatedForwardBendTwoInput(parseFloat(e.target.value));
            setChairSeatedForwardBendTwoErrorMessage('');
        }
    };

    // 處理第三次椅子坐姿體前彎輸入
    const handleChairSeatedForwardBendThreeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestElderlyInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.chairSeatedForwardBendThree) {
                preTestInfoContext.handleChairSeatedForwardBendThreeInput(undefined);
                setChairSeatedForwardBendThreeErrorMessage(isValid.chairSeatedForwardBendThree[0].replace('Chair seated forward bend three ', ''));
            }
            else {
                preTestInfoContext.handleChairSeatedForwardBendThreeInput(parseFloat(e.target.value));
                setChairSeatedForwardBendThreeErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleChairSeatedForwardBendThreeInput(parseFloat(e.target.value));
            setChairSeatedForwardBendThreeErrorMessage('');
        }
    };

    // 處理第一次拉背測驗輸入
    const handlePullBackTestOneInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestElderlyInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.pullBackTestOne) {
                preTestInfoContext.handlePullBackTestOneInput(undefined);
                setPullBackTestOneErrorMessage(isValid.pullBackTestOne[0].replace('Pull back test one ', ''));
            }
            else {
                preTestInfoContext.handlePullBackTestOneInput(parseFloat(e.target.value));
                setPullBackTestOneErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handlePullBackTestOneInput(parseFloat(e.target.value));
            setPullBackTestOneErrorMessage('');
        }
    };

    // 處理第二次拉背測驗輸入
    const handlePullBackTestTwoInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestElderlyInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.pullBackTestTwo) {
                preTestInfoContext.handlePullBackTestTwoInput(undefined);
                setPullBackTestTwoErrorMessage(isValid.pullBackTestTwo[0].replace('Pull back test two ', ''));
            }
            else {
                preTestInfoContext.handlePullBackTestTwoInput(parseFloat(e.target.value));
                setPullBackTestTwoErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handlePullBackTestTwoInput(parseFloat(e.target.value));
            setPullBackTestTwoErrorMessage('');
        }
    };

    // 處理老年開眼單足立輸入
    const handleElderlyEyeOpeningMonopodSecondInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestElderlyInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.elderlyEyeOpeningMonopodSecond) {
                preTestInfoContext.handleElderlyEyeOpeningMonopodSecondInput(undefined);
                setElderlyEyeOpeningMonopodSecondErrorMessage(isValid.elderlyEyeOpeningMonopodSecond[0].replace('Elderly eye opening monopod second ', ''));
            }
            else {
                preTestInfoContext.handleElderlyEyeOpeningMonopodSecondInput(parseFloat(e.target.value));
                setElderlyEyeOpeningMonopodSecondErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleElderlyEyeOpeningMonopodSecondInput(parseFloat(e.target.value));
            setElderlyEyeOpeningMonopodSecondErrorMessage('');
        }
    };

    // 處理第一次椅子坐立繞物輸入
    const handleSittingAroundOneSecondInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestElderlyInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.sittingAroundOneSecond) {
                preTestInfoContext.handleSittingAroundOneSecondInput(undefined);
                setSittingAroundOneSecondErrorMessage(isValid.sittingAroundOneSecond[0].replace('Sitting around one second ', ''));
            }
            else {
                preTestInfoContext.handleSittingAroundOneSecondInput(parseFloat(e.target.value));
                setSittingAroundOneSecondErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleSittingAroundOneSecondInput(parseFloat(e.target.value));
            setSittingAroundOneSecondErrorMessage('');
        }
    };

    // 處理第二次椅子坐立繞物輸入
    const handleSittingAroundTwoSecondInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(preTestInfoFormRef.current, preTestElderlyInfoFormValidate);

        if (isValid !== undefined) {
            if (isValid.sittingAroundTwoSecond) {
                preTestInfoContext.handleSittingAroundTwoSecondInput(undefined);
                setSittingAroundTwoSecondErrorMessage(isValid.sittingAroundTwoSecond[0].replace('Sitting around two second ', ''));
            }
            else {
                preTestInfoContext.handleSittingAroundTwoSecondInput(parseFloat(e.target.value));
                setSittingAroundTwoSecondErrorMessage('');
            }
        }
        else {
            preTestInfoContext.handleSittingAroundTwoSecondInput(parseFloat(e.target.value));
            setSittingAroundTwoSecondErrorMessage('');
        }
    };

    // 檢查前測壯年或老年輸入欄位
    const inspectPreTestField = (): boolean => {
        const today = new Date().getFullYear();
        const birthdayYear = +studentBasicInfoContext.birthday.split('-')[0];

        if (isStudentGreaterThan65(today - birthdayYear)) {
            const isValid = validate(preTestInfoFormRef.current, preTestAdultInfoFormValidate);

            if (isValid !== undefined) {
                if (isValid.pulseRateOne) {
                    setPulseRateOneErrorMessage(isValid.pulseRateOne[0].replace('Pulse rate one ', ''));
                }
                else {
                    setPulseRateOneErrorMessage('');
                }

                if (isValid.pulseRateTwo) {
                    setPulseRateTwoErrorMessage(isValid.pulseRateTwo[0].replace('Pulse rate two ', ''));
                }
                else {
                    setPulseRateTwoErrorMessage('');
                }

                if (isValid.pulseRateThree) {
                    setPlseRateThreeErrorMessage(isValid.pulseRateThree[0].replace('Pulse rate three ', ''));
                }
                else {
                    setPlseRateThreeErrorMessage('');
                }

                if (isValid.kneeCrunchesFrequency) {
                    setKneeCrunchesFrequencyErrorMessage(isValid.kneeCrunchesFrequency[0].replace('Knee crunches frequency ', ''));
                }
                else {
                    setKneeCrunchesFrequencyErrorMessage('');
                }

                if (isValid.seatedForwardBendOne) {
                    setSeatedForwardBendOneErrorMessage(isValid.seatedForwardBendOne[0].replace('Seated forward bend one ', ''));
                }
                else {
                    setSeatedForwardBendOneErrorMessage('');
                }

                if (isValid.seatedForwardBendTwo) {
                    setSeatedForwardBendTwoErrorMessage(isValid.seatedForwardBendTwo[0].replace('Seated forward bend two ', ''));
                }
                else {
                    setSeatedForwardBendTwoErrorMessage('');
                }

                if (isValid.seatedForwardBendThree) {
                    setSeatedForwardBendThreeErrorMessage(isValid.seatedForwardBendThree[0].replace('Seated forward bend three ', ''));
                }
                else {
                    setSeatedForwardBendThreeErrorMessage('');
                }

                if (isValid.adultEyeOpeningMonopodSecond) {
                    setAdultEyeOpeningMonopodSecondErrorMessage(isValid.adultEyeOpeningMonopodSecond[0].replace('Adult eye opening monopod second ', ''));
                }
                else {
                    setAdultEyeOpeningMonopodSecondErrorMessage('');
                }

                return false;
            }
            else {
                setPulseRateOneErrorMessage('');
                setPulseRateTwoErrorMessage('');
                setPlseRateThreeErrorMessage('');
                setKneeCrunchesFrequencyErrorMessage('');
                setSeatedForwardBendOneErrorMessage('');
                setSeatedForwardBendTwoErrorMessage('');
                setSeatedForwardBendThreeErrorMessage('');
                setAdultEyeOpeningMonopodSecondErrorMessage('');

                return true;
            }
        }
        else {
            const isValid = validate(preTestInfoFormRef.current, preTestElderlyInfoFormValidate);

            if (isValid !== undefined) {
                if (isValid.kneeLiftFrequency) {
                    setKneeLiftFrequencyErrorMessage(isValid.kneeLiftFrequency[0].replace('Knee lift frequency ', ''));
                }
                else {
                    setKneeLiftFrequencyErrorMessage('');
                }

                if (isValid.armCurlUpperBodyFrequency) {
                    setArmCurlUpperBodyFrequencyErrorMessage(isValid.armCurlUpperBodyFrequency[0].replace('Arm curl upper body frequency ', ''));
                }
                else {
                    setArmCurlUpperBodyFrequencyErrorMessage('');
                }

                if (isValid.armCurlLowerBodyFrequency) {
                    setArmCurlLowerBodyFrequencyErrorMessage(isValid.armCurlLowerBodyFrequency[0].replace('Arm curl lower body frequency ', ''));
                }
                else {
                    setArmCurlLowerBodyFrequencyErrorMessage('');
                }

                if (isValid.chairSeatedForwardBendOne) {
                    setChairSeatedForwardBendOneErrorMessage(isValid.chairSeatedForwardBendOne[0].replace('Chair seated forward bend one ', ''));
                }
                else {
                    setChairSeatedForwardBendOneErrorMessage('');
                }

                if (isValid.chairSeatedForwardBendTwo) {
                    setChairSeatedForwardBendTwoErrorMessage(isValid.chairSeatedForwardBendTwo[0].replace('Chair seated forward bend two ', ''));
                }
                else {
                    setChairSeatedForwardBendTwoErrorMessage('');
                }

                if (isValid.chairSeatedForwardBendThree) {
                    setChairSeatedForwardBendThreeErrorMessage(isValid.chairSeatedForwardBendThree[0].replace('Chair seated forward bend three ', ''));
                }
                else {
                    setChairSeatedForwardBendThreeErrorMessage('');
                }

                if (isValid.pullBackTestOne) {
                    setPullBackTestOneErrorMessage(isValid.pullBackTestOne[0].replace('Pull back test one ', ''));
                }
                else {
                    setPullBackTestOneErrorMessage('');
                }

                if (isValid.pullBackTestTwo) {
                    setPullBackTestTwoErrorMessage(isValid.pullBackTestTwo[0].replace('Pull back test two ', ''));
                }
                else {
                    setPullBackTestTwoErrorMessage('');
                }

                if (isValid.elderlyEyeOpeningMonopodSecond) {
                    setElderlyEyeOpeningMonopodSecondErrorMessage(isValid.elderlyEyeOpeningMonopodSecond[0].replace('Elderly eye opening monopod second ', ''));
                }
                else {
                    setElderlyEyeOpeningMonopodSecondErrorMessage('');
                }

                if (isValid.sittingAroundOneSecond) {
                    setSittingAroundOneSecondErrorMessage(isValid.sittingAroundOneSecond[0].replace('Sitting around one second ', ''));
                }
                else {
                    setSittingAroundOneSecondErrorMessage('');
                }

                if (isValid.sittingAroundTwoSecond) {
                    setSittingAroundTwoSecondErrorMessage(isValid.sittingAroundTwoSecond[0].replace('Sitting around one second ', ''));
                }
                else {
                    setSittingAroundTwoSecondErrorMessage('');
                }

                return false;
            }
            else {
                setKneeLiftFrequencyErrorMessage('');
                setArmCurlUpperBodyFrequencyErrorMessage('');
                setArmCurlLowerBodyFrequencyErrorMessage('');
                setChairSeatedForwardBendOneErrorMessage('');
                setChairSeatedForwardBendTwoErrorMessage('');
                setChairSeatedForwardBendThreeErrorMessage('');
                setPullBackTestOneErrorMessage('');
                setPullBackTestTwoErrorMessage('');
                setElderlyEyeOpeningMonopodSecondErrorMessage('');
                setSittingAroundOneSecondErrorMessage('');
                setSittingAroundOneSecondErrorMessage('');

                return true;
            }
        }
    };

    const handleStudentRelevantInfo = () => {
        const isValid = inspectPreTestField();

        if (!isValid) {
            return false;
        }
        else {
            const temp: { part: number; subject: number; }[] = [];

            questionnaire_aha_and_acsm_context.heart.forEach((item) => {
                temp.push({
                    part: 1,
                    subject: item
                });
            });

            questionnaire_aha_and_acsm_context.symptom.forEach((item) => {
                temp.push({
                    part: 2,
                    subject: item
                });
            });

            questionnaire_aha_and_acsm_context.otherHealthProblems.forEach((item) => {
                temp.push({
                    part: 3,
                    subject: item
                });
            });

            questionnaire_aha_and_acsm_context.riskFactor.forEach((item) => {
                temp.push({
                    part: 4,
                    subject: item
                });
            });

            const adultRegisterRequestData: AuthRegisterRequestType = {
                photo: '',
                account: studentBasicInfoContext.account,
                password: studentBasicInfoContext.password,
                name: studentBasicInfoContext.name,
                gender: studentBasicInfoContext.gender,
                birthday: studentBasicInfoContext.birthday,
                height: studentBasicInfoContext.height,
                weight: studentBasicInfoContext.weight,
                phone: studentBasicInfoContext.phone,
                address: studentBasicInfoContext.address,
                email: studentBasicInfoContext.email,
                questionnaireFirstPart: questionnaireFirstPartContext.questionnaireFirstPart,
                questionnaireTwoPart: questionnaireTwoPartContext.questionnaireTwoPart,
                questionnaire_aha_and_acsm_part: temp,
                pulseRateOne: preTestInfoContext.pulseOne,
                pulseRateTwo: preTestInfoContext.pulseTwo,
                pulseRateThree: preTestInfoContext.pulseThree,
                kneeCrunchesFrequency: preTestInfoContext.kneeCrunchesFrequency,
                seatedForwardBendOne: preTestInfoContext.seatedForwardBendOne,
                seatedForwardBendTwo: preTestInfoContext.seatedForwardBendTwo,
                seatedForwardBendThree: preTestInfoContext.seatedForwardBendThree,
                adultEyeOpeningMonopodSecond: preTestInfoContext.adultEyeOpeningMonopodSecond
            };

            const elderlyRegisterRequestData: AuthRegisterRequestType = {
                photo: '',
                account: studentBasicInfoContext.account,
                password: studentBasicInfoContext.password,
                name: studentBasicInfoContext.name,
                gender: studentBasicInfoContext.gender,
                birthday: studentBasicInfoContext.birthday,
                height: studentBasicInfoContext.height,
                weight: studentBasicInfoContext.weight,
                phone: studentBasicInfoContext.phone,
                address: studentBasicInfoContext.address,
                email: studentBasicInfoContext.email,
                questionnaireFirstPart: questionnaireFirstPartContext.questionnaireFirstPart,
                questionnaireTwoPart: questionnaireTwoPartContext.questionnaireTwoPart,
                questionnaire_aha_and_acsm_part: temp,
                kneeLiftFrequency: preTestInfoContext.kneeLiftFrequency,
                armCurlUpperBodyFrequency: preTestInfoContext.armCurlUpperBodyFrequency,
                armCurlLowerBodyFrequency: preTestInfoContext.armCurlLowerBodyFrequency,
                chairSeatedForwardBendOne: preTestInfoContext.chairSeatedForwardBendOne,
                chairSeatedForwardBendTwo: preTestInfoContext.chairSeatedForwardBendTwo,
                chairSeatedForwardBendThree: preTestInfoContext.chairSeatedForwardBendThree,
                pullBackTestOne: preTestInfoContext.pullBackTestOne,
                pullBackTestTwo: preTestInfoContext.pullBackTestTwo,
                elderlyEyeOpeningMonopodSecond: preTestInfoContext.elderlyEyeOpeningMonopodSecond,
                sittingAroundOneSecond: preTestInfoContext.sittingAroundOneSecond,
                sittingAroundTwoSecond: preTestInfoContext.sittingAroundTwoSecond
            };

            const registerRequestData = isStudentGreaterThan65(age) ? adultRegisterRequestData : elderlyRegisterRequestData;

            authRegister(registerRequestData).then(() => {
                studentBasicInfoContext.initial();
                questionnaireFirstPartContext.initial();
                questionnaireTwoPartContext.initial();
                questionnaire_aha_and_acsm_context.initial();
                preTestInfoContext.initial();
                
                toast.success('註冊成功', {
                    type: 'success',
                    closeButton: false
                });
                
                navigate('/studentList/');
            }).catch((error) => {
                const errorCode = axiosErrorCode<AuthRegisterErrorCodeType>(error);

                switch(errorCode) {
                    case 400:
                    case 'BadRequest':
                        toast.error('輸入格式錯誤', {
                            type: 'error',
                            closeButton: false
                        });
                        break;
                    case 'AgeNotSuitable':
                        toast.error('此年齡適合 21 歲以上操作', {
                            type: 'error',
                            closeButton: false
                        });
                        break;
                    case 'UserAccountAlreadyRegistered':
                        studentBasicInfoContext.targetPageId(1);
                        toast.error('此帳號已註冊', {
                            type: 'error',
                            closeButton: false
                        });
                        break;
                    case 'UserPhoneAlreadyRegistered':
                        studentBasicInfoContext.targetPageId(1);
                        toast.error('此電話已註冊', {
                            type: 'error',
                            closeButton: false
                        });
                        break;
                    case 'InvalidFileExtension':
                        toast.error('圖片檔案格式錯誤', {
                            type: 'error',
                            closeButton: false
                        });
                        break;
                    case 'FileTooLarge':
                        toast.error('圖片大小不可超過 2MB', {
                            type: 'error',
                            closeButton: false
                        });
                        break;
                    case 'UnfilledQuestionnaireTwoPart':
                        toast.error('尚未填寫第二部分問卷', {
                            type: 'error',
                            closeButton: false
                        });
                        studentBasicInfoContext.targetPageId(3);
                        break;
                    case 'ImageSaveFail':
                        toast.error('圖片保存失敗', {
                            type: 'error',
                            closeButton: false
                        });
                        break;
                    case 'ApplyRegisterFail':
                        studentBasicInfoContext.initial();
                        questionnaireFirstPartContext.initial();
                        questionnaireTwoPartContext.initial();
                        questionnaire_aha_and_acsm_context.initial();
                        preTestInfoContext.initial();
                        toast.error('註冊失敗', {
                            type: 'error',
                            closeButton: false
                        });
                        studentBasicInfoContext.targetPageId(1);
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        toast.error('伺服器錯誤', {
                            type: 'error',
                            closeButton: false
                        });
                        break;
                }
            });
        }
    };

    useEffect(() => {
        const year = +studentBasicInfoContext.birthday.split('-')[0];
        const today = new Date().getFullYear();
        setAge(today - year);
    }, []);

    return (
        <div className='preTestInfo'>
            <form className='preTestInfoForm' ref={preTestInfoFormRef}>
                <div className='cardiorespiratory_fitness'>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                            <div className='card'>
                                <span>心肺適能</span>
                            </div>
                        </div>

                        <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                            <div className='card'>
                                <span>{isStudentGreaterThan65(age) ? '登階測驗' : '原地站立抬膝'}</span>
                            </div>
                        </div>

                        <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                            <div className='card'>
                                {
                                    isStudentGreaterThan65(age) ? (
                                        <Fragment>
                                            <div className='pulseOne'>
                                                <label>
                                                    第一次脈搏數
                                                    <input type='text' placeholder='請輸入第一次脈搏次數' defaultValue={preTestInfoContext.pulseOne} name='pulseRateOne' onChange={handlePulseOneInput} />
                                                    次
                                                </label>

                                                <p className='error_message'>{pulseRateOneErrorMessage}</p>
                                            </div>

                                            <div className='pulseTwo'>
                                                <label>
                                                    第二次脈搏數
                                                    <input type='text' placeholder='請輸入第二次脈搏次數' defaultValue={preTestInfoContext.pulseTwo} name='pulseRateTwo' onChange={handlePulseTwoInput} />
                                                    次
                                                </label>

                                                <p className='error_message'>{pulseRateTwoErrorMessage}</p>
                                            </div>

                                            <div className='pulseThree'>
                                                <label>
                                                    第三次脈搏數
                                                    <input type='text' placeholder='請輸入第三次脈搏次數' defaultValue={preTestInfoContext.pulseThree} name='pulseRateThree' onChange={handlePulseThreeInput} />
                                                    次
                                                </label>

                                                <p className='error_message'>{pulseRateThreeErrorMessage}</p>
                                            </div>
                                        </Fragment>
                                    ) : (
                                        <div className='kneeLiftFrequency'>
                                            <label>
                                                <input type='text' placeholder='請輸入原地站立抬膝次數' defaultValue={preTestInfoContext.kneeLiftFrequency} name='kneeLiftFrequency' onChange={handleKneeLiftFrequencyInput} />
                                                次
                                            </label>

                                            <p className='error_message'>{kneeLiftFrequencyErrorMessage}</p>
                                        </div>
                                    )
                                }
                            </div>
                        </div>
                    </div>
                </div>

                <div className='muscle_strength_and_muscle_endurance'>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                            <div className='card'>
                                <span>肌力與肌耐力</span>
                            </div>
                        </div>

                        <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                            {
                                isStudentGreaterThan65(age) ? (
                                    <div className='card'>
                                        <span>屈膝仰臥起坐</span>
                                    </div>
                                ) : (
                                    <Fragment>
                                        <div className="card top">
                                            <span>手臂彎舉</span>
                                        </div>

                                        <div className="card bottom">
                                            <span>起立坐下</span>
                                        </div>
                                    </Fragment>
                                )
                            }
                        </div>

                        <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                            {
                                isStudentGreaterThan65(age) ? (
                                    <div className='card'>
                                        <label>
                                            <input placeholder='請輸入屈膝仰臥起坐次數' defaultValue={preTestInfoContext.kneeCrunchesFrequency} name='kneeCrunchesFrequency' onChange={handleKneeCrunchesFrequencyInput} />
                                            次
                                        </label>

                                        <p className='error_message'>{kneeCrunchesFrequencyErrorMessage}</p>
                                    </div>
                                ) : (
                                    <Fragment>
                                        <div className="card top">
                                            <div className='armCurlUpperBodyFrequency'>
                                                <label>
                                                    <input placeholder='請輸入手臂彎舉次數' defaultValue={preTestInfoContext.armCurlUpperBodyFrequency} name='armCurlUpperBodyFrequency' onChange={handleArmCurlUpperBodyFrequencyInput} />
                                                    次
                                                </label>

                                                <p className='error_message'>{armCurlUpperBodyFrequencyErrorMessage}</p>
                                            </div>
                                        </div>

                                        <div className="card bottom">
                                            <div className='armCurlLowerBodyFrequency'>
                                                <label>
                                                    <input placeholder='請輸入起立坐下次數' defaultValue={preTestInfoContext.armCurlLowerBodyFrequency} name='armCurlLowerBodyFrequency' onChange={handleArmCurlLowerBodyFrequencyInput} />
                                                    次
                                                </label>

                                                <p className='error_message'>{armCurlLowerBodyFrequencyErrorMessage}</p>
                                            </div>
                                        </div>
                                    </Fragment>
                                )
                            }
                        </div>
                    </div>
                </div>

                <div className='softness'>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                            <div className='card'>
                                <span>柔軟度</span>
                            </div>
                        </div>

                        <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                            {
                                isStudentGreaterThan65(age) ? (
                                    <div className='card'>
                                        <span>坐姿體前彎</span>
                                    </div>
                                ) : (
                                    <Fragment>
                                        <div className="card top">
                                            <span>椅子坐姿體前彎</span>
                                        </div>

                                        <div className='card bottom'>
                                            <span>拉背測驗</span>
                                        </div>
                                    </Fragment>
                                )
                            }
                        </div>

                        <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                            {
                                isStudentGreaterThan65(age) ? (
                                    <div className='card'>
                                        <div className='seatedForwardBendOne'>
                                            <label>
                                                第一次坐姿體前彎
                                                <input type='text' placeholder='請輸入第一次坐姿體前彎' defaultValue={preTestInfoContext.seatedForwardBendOne} name='seatedForwardBendOne' onChange={handleSeatedForwardBendOneInput} />
                                                公分
                                            </label>

                                            <p className='error_message'>{seatedForwardBendOneErrorMessage}</p>
                                        </div>

                                        <div className='seatedForwardBendTwo'>
                                            <label>
                                                第二次坐姿體前彎
                                                <input type='text' placeholder='請輸入第二次坐姿體前彎' defaultValue={preTestInfoContext.seatedForwardBendTwo} name='seatedForwardBendTwo' onChange={handleSeatedForwardBendTwoInput} />
                                                公分
                                            </label>

                                            <p className='error_message'>{seatedForwardBendTwoErrorMessage}</p>
                                        </div>

                                        <div className='seatedForwardBendThree'>
                                            <label>
                                                第三次坐姿體前彎
                                                <input type='text' placeholder='請輸入第三次坐姿體前彎' defaultValue={preTestInfoContext.seatedForwardBendThree} name='seatedForwardBendThree' onChange={handleSeatedForwardBendThreeInput} />
                                                公分
                                            </label>

                                            <p className='error_message'>{seatedForwardBendThreeErrorMessage}</p>
                                        </div>
                                    </div>
                                ) : (
                                    <Fragment>
                                        <div className="card top">
                                            <div className='chairSeatedForwardBendOne'>
                                                <label>
                                                    <input type='text' placeholder='請輸入第一次坐姿體前彎' defaultValue={preTestInfoContext.chairSeatedForwardBendOne} name='chairSeatedForwardBendOne' onChange={handleChairSeatedForwardBendOneInput} />
                                                    公分
                                                </label>

                                                <p className='error_message'>{chairSeatedForwardBendOneErrorMessage}</p>
                                            </div>

                                            <div className='chairSeatedForwardBendTwo'>
                                                <label>
                                                    <input type='text' placeholder='請輸入第二次坐姿體前彎' defaultValue={preTestInfoContext.chairSeatedForwardBendTwo} name='chairSeatedForwardBendTwo' onChange={handleChairSeatedForwardBendTwoInput} />
                                                    公分
                                                </label>

                                                <p className='error_message'>{chairSeatedForwardBendTwoErrorMessage}</p>
                                            </div>

                                            <div className='chairSeatedForwardBendThree'>
                                                <label>
                                                    <input type='text' placeholder='請輸入第三次坐姿體前彎' defaultValue={preTestInfoContext.chairSeatedForwardBendThree} name='chairSeatedForwardBendThree' onChange={handleChairSeatedForwardBendThreeInput} />
                                                    公分
                                                </label>

                                                <p className='error_message'>{chairSeatedForwardBendThreeErrorMessage}</p>
                                            </div>
                                        </div>

                                        <div className='card bottom'>
                                            <div className='pullBackTestOne'>
                                                <label>
                                                    <input type='text' placeholder='請輸入第一次拉背測驗' defaultValue={preTestInfoContext.pullBackTestOne} name='pullBackTestOne' onChange={handlePullBackTestOneInput} />
                                                    公分
                                                </label>

                                                <p className='error_message'>{pullBackTestOneErrorMessage}</p>
                                            </div>

                                            <div className='pullBackTestTwo'>
                                                <label>
                                                    <input type='text' placeholder='請輸入第二次拉背測驗' defaultValue={preTestInfoContext.pullBackTestTwo} name='pullBackTestTwo' onChange={handlePullBackTestTwoInput} />
                                                    公分
                                                </label>
                                                
                                                <p className='error_message'>{pullBackTestTwoErrorMessage}</p>
                                            </div>
                                        </div>
                                    </Fragment>
                                )
                            }
                        </div>
                    </div>
                </div>

                <div className='balance'>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                            <div className='card'>
                                <span>平衡</span>
                            </div>
                        </div>

                        <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                            {
                                isStudentGreaterThan65(age) ? (
                                    <div className='card'>
                                        <span>開眼單足立</span>
                                    </div>
                                ) : (
                                    <Fragment>
                                        <div className="card top">
                                            <span>開眼單足立</span>
                                        </div>

                                        <div className="card bottom">
                                            <span>椅子坐立繞物</span>
                                        </div>
                                    </Fragment>
                                )
                            }
                        </div>

                        <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                            {
                                isStudentGreaterThan65(age) ? (
                                    <div className='card'>
                                        <div className='adultEyeOpeningMonopodSecond'>
                                            <label>
                                                <input type="text" placeholder='請輸入單眼開足立秒數' defaultValue={preTestInfoContext.adultEyeOpeningMonopodSecond} name='adultEyeOpeningMonopodSecond' onChange={handleAdultEyeOpeningMonopodSecond} />
                                                秒數
                                            </label>

                                            <p className='error_message'>{adultEyeOpeningMonopodSecondErrorMessage}</p>
                                        </div>
                                    </div>
                                ) : (
                                    <Fragment>
                                        <div className='card top'>
                                            <div className='elderlyEyeOpeningMonopodSecond'>
                                                <label>
                                                    <input type="text" placeholder='請輸入單眼開足立秒數' defaultValue={preTestInfoContext.elderlyEyeOpeningMonopodSecond} name='elderlyEyeOpeningMonopodSecond' onChange={handleElderlyEyeOpeningMonopodSecondInput} />
                                                    秒數
                                                </label>

                                                <p className='error_message'>{elderlyEyeOpeningMonopodSecondErrorMessage}</p>
                                            </div>
                                        </div>

                                        <div className="card bottom">
                                            <div className='sittingAroundOneSecond'>
                                                <label>
                                                    第一次椅子坐立繞物
                                                    <input type="text" placeholder='請輸入第一次椅子坐立繞物秒數' defaultValue={preTestInfoContext.sittingAroundOneSecond} name='sittingAroundOneSecond' onChange={handleSittingAroundOneSecondInput} />
                                                    秒數
                                                </label>

                                                <p className='error_message'>{sittingAroundOneSecondErrorMessage}</p>
                                            </div>

                                            <div className='sittingAroundTwoSecond'>
                                                <label>
                                                    第二次椅子坐立繞物
                                                    <input type="text" placeholder='請輸入第二次椅子坐立繞物秒數' defaultValue={preTestInfoContext.sittingAroundTwoSecond} name='sittingAroundTwoSecond' onChange={handleSittingAroundTwoSecondInput} />
                                                    秒數
                                                </label>

                                                <p className='error_message'>{sittingAroundTwoSecondErrorMessage}</p>
                                            </div>
                                        </div>
                                    </Fragment>
                                )
                            }
                        </div>
                    </div>
                </div>

                <div className='confirm'>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                            <div className="card">
                                <button type='button' onClick={handleStudentRelevantInfo}>確定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default PreTestInfo;