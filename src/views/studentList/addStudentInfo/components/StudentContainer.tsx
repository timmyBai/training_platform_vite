import React, { Fragment, useContext, useMemo } from 'react';

// css
import './StudentContainer.sass';

// context
import { StudentBasicInfoContext } from '../context/StudentBasicInfoContext';

// components
import StudentBasicInfo from './StudentBasicInfo';
import QuestionnaireFirstPart from './QuestionnaireFirstPart';
import QuestionnaireTwoPart from './QuestionnaireTwoPart';
import Questionnaire_AHA_And_ACSM from './Questionnaire_AHA_And_ACSM';
import PreTestInfo from './PreTestInfo';

const StudentContainer: React.FC = () => {
    const context = useContext(StudentBasicInfoContext);

    const pageTitle = useMemo(() => {
        switch(context.pageId) {
            case 1:
                return '新增學員';
            case 2:
                return '身體活動準備問卷 - 第一部分';
            case 3:
                return '慢性疾病狀況問卷 - 第二部分';
            case 4:
                return '健康狀態史與心血管危險因子問卷 - 第三部分';
            case 5:
                return '前測數據';
            default:
                return '';
        }

    }, [context.pageId]);

    const prev = () => {
        // 如果上一步減去 id 不為第二部分問卷
        if (context.pageId - 1 !== 3) {
            context.subPageId();
        }
        else {
            context.targetPageId(2);
        }
    };

    const goBackQuestionnaireFirstPart = () => {
        context.targetPageId(2);
    };

    const goBackPreTest = () => {
        context.targetPageId(5);
    };

    const isQuestionnaireActive = useMemo(() => {
        return context.pageId === 2 || context.pageId === 3 || context.pageId === 4 ? 'active': '';
    }, [context.pageId]);

    const isPreTestActive = useMemo(() => {
        return context.pageId === 5 ? 'active': '';
    }, [context.pageId]);

    return (
        <div className='student_container'>
            <div className='page_title'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                        <div className='card'>
                            <div className='headline'>
                                <div className='group'>
                                    {
                                        context.pageId > 1 && (
                                            <button className='prev' onClick={prev}>上一步</button>
                                        )
                                    }
                                </div>

                                <div className='group'>
                                    <h2 className='title'>{pageTitle}</h2>
                                </div>

                                <div className='group'>
                                    {
                                        context.pageId > 1 && (
                                            <Fragment>
                                                <button className={`questionnaire ${isQuestionnaireActive}`} onClick={goBackQuestionnaireFirstPart}>填寫問卷</button>
                                                <button className={`preTest ${isPreTestActive}`} onClick={goBackPreTest}>填寫前測</button>
                                            </Fragment>
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {
                context.pageId === 1 && (
                    <StudentBasicInfo></StudentBasicInfo>
                )
            }

            {/* 問卷第一部分 */}
            {
                context.pageId === 2 && (
                    <QuestionnaireFirstPart></QuestionnaireFirstPart>
                )
            }

            {/* 問卷第二部分 */}
            {
                context.pageId === 3 && (
                    <QuestionnaireTwoPart></QuestionnaireTwoPart>
                )
            }

            {/* 問卷 AHA 和 ACSM 問卷 */}
            {
                context.pageId === 4 && (
                    <Questionnaire_AHA_And_ACSM></Questionnaire_AHA_And_ACSM>
                )
            }

            {/* 前測數據 */}
            {
                context.pageId === 5 && (
                    <PreTestInfo></PreTestInfo>
                )
            }
        </div>
    );
};

export default StudentContainer;