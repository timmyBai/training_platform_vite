import React, { useContext } from 'react';

// css
import './Questionnaire_AHA_And_ACSM.sass';

// context
import { StudentBasicInfoContext } from '../context/StudentBasicInfoContext';
import { Questionnaire_AHA_And_ACSM_Context } from '../context/Questionnaire_AHA_And_ACSM_Context';

const Questionnaire_AHA_And_ACSM: React.FC = () => {
    const context = useContext(StudentBasicInfoContext);
    const questionnaire_aha_and_acsm_context = useContext(Questionnaire_AHA_And_ACSM_Context);

    const heartSubjects = [
        {
            subject: '心臟病發作'
        },
        {
            subject: '心臟手術'
        },
        {
            subject: '心導管手術'
        },
        {
            subject: '經皮冠狀動派氣球擴張術(PTCA)'
        },
        {
            subject: '心律調節器植入性心臟除顫器/心律異常'
        },
        {
            subject: '心臟瓣膜疾病'
        },
        {
            subject: '心臟衰竭'
        },
        {
            subject: '心臟移植'
        },
        {
            subject: '先天性心臟病'
        }
    ];

    const symptomSubjects = [
        {
            subject: '你曾做身體活動時胸部不適'
        },
        {
            subject: '你曾有原因不明的呼吸困難'
        },
        {
            subject: '你曾頭暈眼花、暈倒或眩暈'
        },
        {
            subject: '你曾踝部水腫'
        },
        {
            subject: '你曾因強且快心跳而感到不適'
        },
        {
            subject: '你服用治療心臟病的藥物'
        }
    ];

    const otherHealthProblemsSubjects = [
        {
            subject: '你有糖尿病'
        },
        {
            subject: '你有氣喘或其他肺部疾病'
        },
        {
            subject: '當短距離行走時，你的小腿有發熱或抽筋感'
        },
        {
            subject: '你有限制你身體活動的肌肉骨骼問題'
        },
        {
            subject: '你關心運動的安全性'
        },
        {
            subject: '你服用處方藥'
        },
        {
            subject: '你懷孕了'
        }
    ];

    const riskFactorSubjects = [
        {
            subject: '你是 45 歲以上的男性'
        },
        {
            subject: '你是 55 歲以上的女性'
        },
        {
            subject: '你吸菸或是 6 個月內戒菸者'
        },
        {
            subject: '你的血壓超過 140/90 mmHg'
        },
        {
            subject: '你不知道你的血壓狀況'
        },
        {
            subject: '你服用降血壓藥物'
        },
        {
            subject: '你的血清膽固醇檢測結果高於 200 mg/dL'
        },
        {
            subject: '你不知道你的膽固醇檢測結果'
        },
        {
            subject: '你有一個近親，他在 55 歲 (父親或兄弟) 或 65 歲 (媽媽或姊妹) 以前發作果心臟病或做過心臟手術'
        },
        {
            subject: '你不大運動 (即身體活動水準少於每週至少三次、每次 30 分鐘)'
        },
        {
            subject: '你的身體質量指數 (BMI) 超過 30'
        },
        {
            subject: '你是糖尿病前期患者'
        },
        {
            subject: '你不知道你是否為糖尿病前期'
        }
    ];

    // 處理病史輸入
    const handleHeartInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.checked) {
            questionnaire_aha_and_acsm_context.handleHeartInput([...questionnaire_aha_and_acsm_context.heart, parseInt(e.target.value)].sort((a, b) => a - b));
        }
        else {
            const index = questionnaire_aha_and_acsm_context.heart.indexOf(parseInt(e.target.value));

            if (index > -1) {
                questionnaire_aha_and_acsm_context.heart.splice(index, 1);
                questionnaire_aha_and_acsm_context.handleHeartInput([...questionnaire_aha_and_acsm_context.heart]);
            }
        }
    };

    // 處理症狀輸入
    const handleSymptomInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.checked) {
            questionnaire_aha_and_acsm_context.handleSymptomInput([...questionnaire_aha_and_acsm_context.symptom, parseInt(e.target.value)].sort((a, b) => a - b));
        }
        else {
            const index = questionnaire_aha_and_acsm_context.symptom.indexOf(parseInt(e.target.value));

            if (index > -1) {
                questionnaire_aha_and_acsm_context.symptom.splice(index, 1);
                questionnaire_aha_and_acsm_context.handleSymptomInput([...questionnaire_aha_and_acsm_context.symptom]);
            }
        }
    };

    // 處理其他健康問題輸入
    const handleOtherHealthProblemsInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.checked) {
            questionnaire_aha_and_acsm_context.handleOtherHealthProblemsInput([...questionnaire_aha_and_acsm_context.otherHealthProblems, parseInt(e.target.value)].sort((a, b) => a - b));
        }
        else {
            const index = questionnaire_aha_and_acsm_context.otherHealthProblems.indexOf(parseInt(e.target.value));

            if (index > -1) {
                questionnaire_aha_and_acsm_context.otherHealthProblems.splice(index, 1);
                questionnaire_aha_and_acsm_context.handleOtherHealthProblemsInput([...questionnaire_aha_and_acsm_context.otherHealthProblems]);
            }
        }
    };

    // 處理危險因子輸入
    const handleRiskFactorInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.checked) {
            questionnaire_aha_and_acsm_context.handleRiskFactorInput([...questionnaire_aha_and_acsm_context.riskFactor, parseInt(e.target.value)].sort((a, b) => a - b));
        }
        else {
            const index = questionnaire_aha_and_acsm_context.riskFactor.indexOf(parseInt(e.target.value));

            if (index > -1) {
                questionnaire_aha_and_acsm_context.riskFactor.splice(index, 1);
                questionnaire_aha_and_acsm_context.handleRiskFactorInput([...questionnaire_aha_and_acsm_context.riskFactor]);
            }
        }
    };

    const next = () => {
        context.addPageId();
    };

    return (
        <div className='questionnaire_aha_and_acsm'>
            <div className='flex flex-wrap'>
                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                    <div className='card'>
                        {/* 病史 */}
                        <h5>你曾經有過下列病史</h5>

                        {
                            heartSubjects.map((heartSubject, index) => {
                                return (
                                    <div className='info' key={index}>
                                        <label>
                                            <input type='checkbox' value={index + 1} name='heart' checked={questionnaire_aha_and_acsm_context.heart.indexOf(index + 1) !== -1} onChange={handleHeartInput} />
                                            {heartSubject.subject}
                                        </label>
                                    </div>
                                );
                            })
                        }

                        {/* 症狀 */}
                        <h5>你曾經有過下列症狀</h5>

                        {
                            symptomSubjects.map((symptomSubject, index) => {
                                return (
                                    <div className='info' key={index}>
                                        <label>
                                            <input type='checkbox' value={index + 1} name='symptom' checked={questionnaire_aha_and_acsm_context.symptom.indexOf(index + 1) !== -1} onChange={handleSymptomInput} />
                                            {symptomSubject.subject}
                                        </label>
                                    </div>
                                );
                            })
                        }

                        {/* 其他健康問題 */}
                        <h5>其他健康問題</h5>

                        {
                            otherHealthProblemsSubjects.map((otherHealthProblemsSubject, index) => {
                                return (
                                    <div className='info' key={index}>
                                        <label>
                                            <input type='checkbox' value={index + 1} name='otherHealthProblems' checked={questionnaire_aha_and_acsm_context.otherHealthProblems.indexOf(index + 1) !== -1} onChange={handleOtherHealthProblemsInput} />
                                            {otherHealthProblemsSubject.subject}
                                        </label>
                                    </div>
                                );
                            })
                        }

                        <hr />

                        {/* 危險因子 */}
                        <h5>你是否有過下列心血管危險因子</h5>

                        {
                            riskFactorSubjects.map((riskFactorSubject, index) => {
                                return (
                                    <div className='info' key={index}>
                                        <label>
                                            <input type='checkbox' value={index + 1} checked={questionnaire_aha_and_acsm_context.riskFactor.indexOf(index + 1) !== -1} name='riskFactor' onChange={handleRiskFactorInput} />
                                            {riskFactorSubject.subject}
                                        </label>
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>
            </div>

            <div className='flex flex-wrap'>
                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                    <div className='card center'>
                        <button onClick={next}>下一步</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Questionnaire_AHA_And_ACSM;