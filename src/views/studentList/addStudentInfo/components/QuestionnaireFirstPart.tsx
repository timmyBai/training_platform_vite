import React, { Fragment, useContext } from 'react';

// css
import './QuestionnaireFirstPart.sass';

// context
import { StudentBasicInfoContext } from '../context/StudentBasicInfoContext';
import { QuestionnaireFirstPartContext } from '../context/QuestionnaireFirstPartContext';

const QuestionnaireFirstPart: React.FC = () => {
    const questionnaireFirstPartContext = useContext(QuestionnaireFirstPartContext);
    const studentBasicInfoContext = useContext(StudentBasicInfoContext);

    const questionnaireFirstPartList = [
        {
            id: 1,
            topic: '醫師曾說你有心臟疾病或高血壓嗎?'
        },
        {
            id: 2,
            topic: '當你休息時、日常活動時或從事運動時，你會感覺到胸痛嗎?'
        },
        {
            id: 3,
            topic: '你曾因為暈眩而失去平衡或是在過去的 12 個月曾經失去意識嗎? (若是因過度換氣，包含費力運動所導致的暈眩，請答\'否\''
        },
        {
            id: 4,
            topic: '你是否曾被診斷被診斷罹患其他慢性疾病? (除了心臟病與高血壓以外)'
        },
        {
            id: 5,
            topic: '你最近有在服用慢性疾病的處方藥?'
        },
        {
            id: 6,
            topic: '你有骨頭或關節的問題 (如膝蓋、腳踝、肩膀或其他部位)，可能因為更多的身體活動而使情況惡化嗎? 如果你有關節的過往病史，但並不影響你最近的身體活動，請答\'否\''
        },
        {
            id: 7,
            topic: '醫師曾說你只能在醫務監督下進行身體活動嗎?'
        }
    ];

    const next = () => {
        let isTransformTwo = false;

        for(const i in questionnaireFirstPartContext.questionnaireFirstPart) {
            if (questionnaireFirstPartContext.questionnaireFirstPart[i].answer) {
                isTransformTwo = true;
                break;
            }
        }

        // 如果身體活動準備問卷任一題為是
        if (isTransformTwo) {
            // 轉跳下一頁 慢性疾病狀況問卷
            studentBasicInfoContext.addPageId();
        }
        else {
            // 轉跳 AHA & ACSM 問卷
            studentBasicInfoContext.targetPageId(4);
        }
    };

    return (
        <div className='questionnaireFirstPart'>
            <div className='flex flex-wrap'>
                {
                    questionnaireFirstPartList.map((item, index) => {
                        return (
                            <Fragment key={item.id}>
                                <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                                    <div className='card left'>
                                        <h5>{index + 1}</h5>
                                    </div>
                                </div>

                                <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                                    <div className='card right'>
                                        <label>
                                            <input type='radio' name={`questionnaire${index + 1}`} checked={questionnaireFirstPartContext.questionnaireFirstPart[index].answer === true} onChange={() => questionnaireFirstPartContext.handleQuestionnaireFirstPart(index)} />
                                            是
                                        </label>

                                        <label>
                                            <input type='radio' name={`questionnaire${index + 1}`} checked={questionnaireFirstPartContext.questionnaireFirstPart[index].answer === false} onChange={() => questionnaireFirstPartContext.handleQuestionnaireFirstPart(index)} />
                                            否
                                        </label>
                                    </div>
                                </div>
                            </Fragment>
                        );
                    })
                }
            </div>

            {/* 確定 */}
            <div className='flex flex-wrap'>
                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                    <div className='card center'>
                        <button className='next' onClick={next}>下一步</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default QuestionnaireFirstPart;