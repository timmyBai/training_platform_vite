import React, { Fragment, useContext } from 'react';

// css
import './QuestionnaireTwoPart.sass';

// context
import { StudentBasicInfoContext } from '../context/StudentBasicInfoContext';
import { QuestionnaireTwoPartContext } from '../context/QuestionnaireTwoPartContext';

const QuestionnaireTwoPart: React.FC = () => {
    const context = useContext(StudentBasicInfoContext);
    const questionnaireTwoPartContext = useContext(QuestionnaireTwoPartContext);

    /**
     * 處理第二部份問題選項，當父題按回去否時，子題同時回去否選項
     */
    const handleQuestionnaireTwoPart = (index: number): void => {
        const temp = questionnaireTwoPartContext.questionnaireTwoPart;
        questionnaireTwoPartContext.handleQuestionnaireTwoPart(index);

        if (Number.isInteger(temp[index].questionNumber) && temp[index].subject === '') {
            if (temp[index].answer === false) {
                for (let i = 0; i < temp.length; i++) {
                    if (temp[i].questionNumber === temp[index].questionNumber && temp[i].subject !== '') {
                        temp[i].answer = false;
                    }
                }
            }
        }
    };

    const next = () => {
        context.addPageId();
    };

    return (
        <div className='questionnaireTwoPart'>
            <div className='flex flex-wrap'>
                {
                    questionnaireTwoPartContext.questionnaireTwoPart.map((item, index) => {
                        return (
                            <Fragment key={index}>
                                {/* 第一大題 */}
                                {
                                    item.questionNumber === 1 && item.subject === '' && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card'>
                                                <h5>{item.questionNumber}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} value={'true'} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} value={'false'} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }
                                {
                                    item.questionNumber === 1 && item.subject !== '' && questionnaireTwoPartContext.showSubjectQuestionnaireOne && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card subject'>
                                                <h5>{item.subject}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }

                                {/* 第二大題 */}
                                {
                                    item.questionNumber === 2 && item.subject === '' && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card'>
                                                <h5>{item.questionNumber}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }
                                {
                                    item.questionNumber === 2 && item.subject !== '' && questionnaireTwoPartContext.showSubjectQuestionnaireTwo && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card subject'>
                                                <h5>{item.subject}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }

                                {/* 第三大題 */}
                                {
                                    item.questionNumber === 3 && item.subject === '' && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card'>
                                                <h5>{item.questionNumber}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }
                                {
                                    item.questionNumber === 3 && item.subject !== '' && questionnaireTwoPartContext.showSubjectQuestionnaireThree && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card subject'>
                                                <h5>{item.subject}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }

                                {/* 第四大題 */}
                                {
                                    item.questionNumber === 4 && item.subject === '' && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card'>
                                                <h5>{item.questionNumber}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }
                                {
                                    item.questionNumber === 4 && item.subject !== '' && questionnaireTwoPartContext.showSubjectQuestionnaireFour && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card subject'>
                                                <h5>{item.subject}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }

                                {/* 第五大題 */}
                                {
                                    item.questionNumber === 5 && item.subject === '' && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card'>
                                                <h5>{item.questionNumber}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }
                                {
                                    item.questionNumber === 5 && item.subject !== '' && questionnaireTwoPartContext.showSubjectQuestionnaireFive && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card subject'>
                                                <h5>{item.subject}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }


                                {/* 第六大題 */}
                                {
                                    item.questionNumber === 6 && item.subject === '' && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card'>
                                                <h5>{item.questionNumber}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }
                                {
                                    item.questionNumber === 6 && item.subject !== '' && questionnaireTwoPartContext.showSubjectQuestionnaireSix && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card subject'>
                                                <h5>{item.subject}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }

                                {/* 第七大題 */}
                                {
                                    item.questionNumber === 7 && item.subject === '' && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card'>
                                                <h5>{item.questionNumber}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }
                                {
                                    item.questionNumber === 7 && item.subject !== '' && questionnaireTwoPartContext.showSubjectQuestionnaireSeven && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card subject'>
                                                <h5>{item.subject}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }

                                {/* 第八大題 */}
                                {
                                    item.questionNumber === 8 && item.subject === '' && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card'>
                                                <h5>{item.questionNumber}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }
                                {
                                    item.questionNumber === 8 && item.subject !== '' && questionnaireTwoPartContext.showSubjectQuestionnaireEight && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card subject'>
                                                <h5>{item.subject}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }

                                {/* 第九大題 */}
                                {
                                    item.questionNumber === 9 && item.subject === '' && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card'>
                                                <h5>{item.questionNumber}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.questionNumber}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }
                                {
                                    item.questionNumber === 9 && item.subject !== '' && questionnaireTwoPartContext.showSubjectQuestionnaireNine && (
                                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                            <div className='card subject'>
                                                <h5>{item.subject}</h5>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === true} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    是
                                                </label>

                                                <label>
                                                    <input type='radio' name={`questionnaire${item.subject}`} checked={item.answer === false} onChange={() => handleQuestionnaireTwoPart(index)} />
                                                    否
                                                </label>
                                            </div>
                                        </div>
                                    )
                                }
                            </Fragment>
                        );
                    })
                }
            </div>

            <div className='flex flex-wrap'>
                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                    <div className='card center'>
                        <button onClick={next}>下一步</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default QuestionnaireTwoPart;