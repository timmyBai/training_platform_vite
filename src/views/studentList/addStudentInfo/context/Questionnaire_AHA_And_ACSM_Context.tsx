import React, { useState, createContext } from 'react';

type Questionnaire_AHA_And_ACSM_Context_Type = {
    heart: number[],
    symptom: number[],
    otherHealthProblems: number[],
    riskFactor: number[],
    initial: () => void,
    handleHeartInput: (heart: number[]) => void,
    handleSymptomInput: (symptom: number[]) => void,
    handleOtherHealthProblemsInput: (otherHealthProblems: number[]) => void,
    handleRiskFactorInput: (riskFactor: number[]) => void
};

export const Questionnaire_AHA_And_ACSM_Context = createContext<Questionnaire_AHA_And_ACSM_Context_Type>({
    heart: [],
    symptom: [],
    otherHealthProblems: [],
    riskFactor: [],
    initial: () => {},
    handleHeartInput: () => { },
    handleSymptomInput: () => { },
    handleOtherHealthProblemsInput: () => { },
    handleRiskFactorInput: () => { }
});

type Questionnaire_AHA_And_ACSM_Context_Props_Type = {
    children: JSX.Element | JSX.Element[]
};

const Questionnaire_AHA_And_ACSM_Context_Provider: React.FC<Questionnaire_AHA_And_ACSM_Context_Props_Type> = ({ children }) => {
    const [heart, setHeart] = useState<number[]>([]);
    const [symptom, setSymptom] = useState<number[]>([]);
    const [otherHealthProblems, setOtherHealthProblems] = useState<number[]>([]);
    const [riskFactor, setRiskFactor] = useState<number[]>([]);

    const initial = () => {
        setHeart([]);
        setSymptom([]);
        setOtherHealthProblems([]);
        setRiskFactor([]);
    };

    const handleHeartInput = (heart: number[]) => {
        setHeart(heart);
    };

    const handleSymptomInput = (symptom: number[]) => {
        setSymptom(symptom);
    };

    const handleOtherHealthProblemsInput = (otherHealthProblems: number[]) => {
        setOtherHealthProblems(otherHealthProblems);
    };

    const handleRiskFactorInput = (riskFactor: number[]) => {
        setRiskFactor(riskFactor);
    };

    return (
        <Questionnaire_AHA_And_ACSM_Context.Provider value={{
            heart: heart,
            symptom: symptom,
            otherHealthProblems: otherHealthProblems,
            riskFactor: riskFactor,
            initial: initial,
            handleHeartInput: handleHeartInput,
            handleSymptomInput: handleSymptomInput,
            handleOtherHealthProblemsInput: handleOtherHealthProblemsInput,
            handleRiskFactorInput: handleRiskFactorInput
        }}>
            {children}
        </Questionnaire_AHA_And_ACSM_Context.Provider>
    );
};

export default Questionnaire_AHA_And_ACSM_Context_Provider;