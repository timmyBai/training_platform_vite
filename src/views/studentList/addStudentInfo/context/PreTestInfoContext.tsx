import React, { createContext, useState } from 'react';

type PreTestInfoContextType = {
    pulseOne: number | undefined,
    pulseTwo: number | undefined,
    pulseThree: number | undefined,
    kneeCrunchesFrequency: number | undefined,
    seatedForwardBendOne: number | undefined,
    seatedForwardBendTwo: number | undefined
    seatedForwardBendThree: number | undefined,
    adultEyeOpeningMonopodSecond: number | undefined,

    kneeLiftFrequency: number | undefined,
    armCurlUpperBodyFrequency: number | undefined,
    armCurlLowerBodyFrequency: number | undefined,
    chairSeatedForwardBendOne: number | undefined,
    chairSeatedForwardBendTwo: number | undefined
    chairSeatedForwardBendThree: number | undefined,
    pullBackTestOne: number | undefined,
    pullBackTestTwo: number | undefined,
    elderlyEyeOpeningMonopodSecond: number | undefined,
    sittingAroundOneSecond: number | undefined,
    sittingAroundTwoSecond: number | undefined,
    initial: () => void,
    handlePulseOneInput: (pulseOne: number | undefined) => void,
    handlePulseTwoInput: (pulseTwo: number | undefined) => void,
    handlePulseThreeInput: (pulseThree: number | undefined) => void,
    handleKneeCrunchesFrequencyInput: (kneeCrunchesFrequency: number | undefined) => void,
    handleSeatedForwardBendOneInput: (seatedForwardBendOne: number | undefined) => void,
    handleSeatedForwardBendTwoInput: (seatedForwardBendTwo: number | undefined) => void,
    handleSeatedForwardBendThreeInput: (seatedForwardBendThree: number | undefined) => void,
    handleAdultEyeOpeningMonopodSecondInput: (adultEyeOpeningMonopodSecond: number | undefined) => void

    handleKneeLiftFrequencyInput: (kneeLiftFrequency: number | undefined) => void,
    handleArmCurlUpperBodyFrequencyInput: (armCurlUpperBodyFrequency: number | undefined) => void,
    handleArmCurlLowerBodyFrequencyInput: (armCurlLowerBodyFrequency: number | undefined) => void,
    handleChairSeatedForwardBendOneInput: (chairSeatedForwardBendOne: number | undefined) => void,
    handleChairSeatedForwardBendTwoInput: (chairSeatedForwardBendTwo: number | undefined) => void,
    handleChairSeatedForwardBendThreeInput: (chairSeatedForwardBendThree: number | undefined) => void,
    handlePullBackTestOneInput: (pullBackTestOne: number | undefined) => void,
    handlePullBackTestTwoInput: (pullBackTestTwo: number | undefined) => void,
    handleElderlyEyeOpeningMonopodSecondInput: (elderlyEyeOpeningMonopodSecond: number | undefined) => void,
    handleSittingAroundOneSecondInput: (sittingAroundOneSecond: number | undefined) => void,
    handleSittingAroundTwoSecondInput: (sittingAroundTwoSecond: number | undefined) => void
};

export const PreTestInfoContext = createContext<PreTestInfoContextType>({
    pulseOne: undefined,
    pulseTwo: undefined,
    pulseThree: undefined,
    kneeCrunchesFrequency: undefined,
    seatedForwardBendOne: undefined,
    seatedForwardBendTwo: undefined,
    seatedForwardBendThree: undefined,
    adultEyeOpeningMonopodSecond: undefined,

    kneeLiftFrequency: undefined,
    armCurlUpperBodyFrequency: undefined,
    armCurlLowerBodyFrequency: undefined,
    chairSeatedForwardBendOne: undefined,
    chairSeatedForwardBendTwo: undefined,
    chairSeatedForwardBendThree: undefined,
    pullBackTestOne: undefined,
    pullBackTestTwo: undefined,
    elderlyEyeOpeningMonopodSecond: undefined,
    sittingAroundOneSecond: undefined,
    sittingAroundTwoSecond: undefined,
    initial: () => {},
    handlePulseOneInput: () => { },
    handlePulseTwoInput: () => { },
    handlePulseThreeInput: () => { },
    handleKneeCrunchesFrequencyInput: () => { },
    handleSeatedForwardBendOneInput: () => { },
    handleSeatedForwardBendTwoInput: () => { },
    handleSeatedForwardBendThreeInput: () => { },
    handleAdultEyeOpeningMonopodSecondInput: () => { },

    handleKneeLiftFrequencyInput: () => { },
    handleArmCurlUpperBodyFrequencyInput: () => { },
    handleArmCurlLowerBodyFrequencyInput: () => { },
    handleChairSeatedForwardBendOneInput: () => { },
    handleChairSeatedForwardBendTwoInput: () => { },
    handleChairSeatedForwardBendThreeInput: () => { },
    handlePullBackTestOneInput: () => { },
    handlePullBackTestTwoInput: () => { },
    handleElderlyEyeOpeningMonopodSecondInput: () => { },
    handleSittingAroundOneSecondInput: () => { },
    handleSittingAroundTwoSecondInput: () => { }
});

type PreTestInfoContextProviderPropsType = {
    children: JSX.Element | JSX.Element[]
};

const PreTestInfoContextProvider: React.FC<PreTestInfoContextProviderPropsType> = ({ children }) => {
    const [pulseOne, setPulseOne] = useState<number | undefined>(undefined); // 第一次脈搏次數
    const [pulseTwo, setPulseTwo] = useState<number | undefined>(undefined); // 第二次脈搏次數
    const [pulseThree, setPulseThree] = useState<number | undefined>(undefined); // 第三次脈搏次數
    const [kneeCrunchesFrequency, setKneeCrunchesFrequency] = useState<number | undefined>(undefined); // 屈膝仰臥起坐次數
    const [seatedForwardBendOne, setSeatedForwardBendOne] = useState<number | undefined>(undefined); // 第一次坐姿體前彎公分
    const [seatedForwardBendTwo, setSeatedForwardBendTwo] = useState<number | undefined>(undefined); // 第二次坐姿體前彎公分
    const [seatedForwardBendThree, setSeatedForwardBendThree] = useState<number | undefined>(undefined); // 第三次坐姿體前彎公分
    const [adultEyeOpeningMonopodSecond, setAdultEyeOpeningMonopodSecond] = useState<number | undefined>(undefined); // 壯年開眼單足立

    const [kneeLiftFrequency, setKneeLiftFrequency] = useState<number | undefined>(undefined);
    const [armCurlUpperBodyFrequency, setArmCurlUpperBodyFrequency] = useState<number | undefined>(undefined);
    const [armCurlLowerBodyFrequency, setArmCurlLowerBodyFrequency] = useState<number | undefined>(undefined);
    const [chairSeatedForwardBendOne, setChairSeatedForwardBendOne] = useState<number | undefined>(undefined);
    const [chairSeatedForwardBendTwo, setChairSeatedForwardBendTwo] = useState<number | undefined>(undefined);
    const [chairSeatedForwardBendThree, setChairSeatedForwardBendThree] = useState<number | undefined>(undefined);
    const [pullBackTestOne, setPullBackTestOne] = useState<number | undefined>(undefined);
    const [pullBackTestTwo, setPullBackTestTwo] = useState<number | undefined>(undefined);
    const [elderlyEyeOpeningMonopodSecond, setElderlyEyeOpeningMonopodSecond] = useState<number | undefined>(undefined);
    const [sittingAroundOneSecond, setSittingAroundOneSecond] = useState<number | undefined>(undefined);
    const [sittingAroundTwoSecond, setSittingAroundTwoSecond] = useState<number | undefined>(undefined);

    const initial = () => {
        setPulseOne(undefined);
        setPulseTwo(undefined);
        setPulseThree(undefined);
        setKneeCrunchesFrequency(undefined);
        setSeatedForwardBendOne(undefined);
        setSeatedForwardBendTwo(undefined);
        setSeatedForwardBendThree(undefined);
        setAdultEyeOpeningMonopodSecond(undefined);
        setKneeLiftFrequency(undefined);
        setArmCurlUpperBodyFrequency(undefined);
        setArmCurlLowerBodyFrequency(undefined);
        setChairSeatedForwardBendOne(undefined);
        setChairSeatedForwardBendTwo(undefined);
        setChairSeatedForwardBendThree(undefined);
        setPullBackTestOne(undefined);
        setPullBackTestTwo(undefined);
        setElderlyEyeOpeningMonopodSecond(undefined);
        setSittingAroundOneSecond(undefined);
        setSittingAroundTwoSecond(undefined);
    };

    // 處理第一次脈搏次數
    const handlePulseOneInput = (pulseOne: number | undefined): void => {
        setPulseOne(pulseOne);
    };

    // 處理第二次脈搏次數
    const handlePulseTwoInput = (pulseTwo: number | undefined): void => {
        setPulseTwo(pulseTwo);
    };

    // 處理第三次脈搏次數
    const handlePulseThreeInput = (pulseThree: number | undefined): void => {
        setPulseThree(pulseThree);
    };

    // 處理屈膝仰臥起坐次數
    const handleKneeCrunchesFrequencyInput = (kneeCrunchesFrequency: number | undefined): void => {
        setKneeCrunchesFrequency(kneeCrunchesFrequency);
    };

    // 處理第一次坐姿體前彎
    const handleSeatedForwardBendOneInput = (seatedForwardBendOne: number | undefined) => {
        setSeatedForwardBendOne(seatedForwardBendOne);
    };

    // 處理第二次坐姿體前彎
    const handleSeatedForwardBendTwoInput = (seatedForwardBendTwo: number | undefined) => {
        setSeatedForwardBendTwo(seatedForwardBendTwo);
    };

    // 處理第三次坐姿體前彎
    const handleSeatedForwardBendThreeInput = (seatedForwardBendThree: number | undefined) => {
        setSeatedForwardBendThree(seatedForwardBendThree);
    };

    // 處理壯年開眼單足立
    const handleAdultEyeOpeningMonopodSecondInput = (adultEyeOpeningMonopodSecond: number | undefined) => {
        setAdultEyeOpeningMonopodSecond(adultEyeOpeningMonopodSecond);
    };

    // 處理原地站立抬膝
    const handleKneeLiftFrequencyInput = (kneeLiftFrequency: number | undefined) => {
        setKneeLiftFrequency(kneeLiftFrequency);
    };

    // 處理手臂彎舉
    const handleArmCurlUpperBodyFrequencyInput = (armCurlUpperBodyFrequency: number | undefined) => {
        setArmCurlUpperBodyFrequency(armCurlUpperBodyFrequency);
    };

    // 處理起立坐下
    const handleArmCurlLowerBodyFrequencyInput = (armCurlLowerBodyFrequency: number | undefined) => {
        setArmCurlLowerBodyFrequency(armCurlLowerBodyFrequency);
    };

    // 第一次椅子坐姿體前彎
    const handleChairSeatedForwardBendOneInput = (chairSeatedForwardBendOne: number | undefined) => {
        setChairSeatedForwardBendOne(chairSeatedForwardBendOne);
    };

    // 第二次椅子坐姿體前彎
    const handleChairSeatedForwardBendTwoInput = (chairSeatedForwardBendTwo: number | undefined) => {
        setChairSeatedForwardBendTwo(chairSeatedForwardBendTwo);
    };

    // 第三次椅子坐姿體前彎
    const handleChairSeatedForwardBendThreeInput = (chairSeatedForwardBendThree: number | undefined) => {
        setChairSeatedForwardBendThree(chairSeatedForwardBendThree);
    };

    // 處理第一次拉背測驗輸入
    const handlePullBackTestOneInput = (pullBackTestOne: number | undefined) => {
        setPullBackTestOne(pullBackTestOne);
    };

    // 處理第二次拉背測驗輸入
    const handlePullBackTestTwoInput = (pullBackTestTwo: number | undefined) => {
        setPullBackTestTwo(pullBackTestTwo);
    };

    // 老年開眼單足立
    const handleElderlyEyeOpeningMonopodSecondInput = (elderlyEyeOpeningMonopodSecond: number | undefined) => {
        setElderlyEyeOpeningMonopodSecond(elderlyEyeOpeningMonopodSecond);
    };

    const handleSittingAroundOneSecondInput = (sittingAroundOneSecond: number | undefined) => {
        setSittingAroundOneSecond(sittingAroundOneSecond);
    };

    const handleSittingAroundTwoSecondInput = (sittingAroundTwoSecond: number | undefined) => {
        setSittingAroundTwoSecond(sittingAroundTwoSecond);
    };

    return (
        <PreTestInfoContext.Provider value={{
            pulseOne: pulseOne,
            pulseTwo: pulseTwo,
            pulseThree: pulseThree,
            kneeCrunchesFrequency: kneeCrunchesFrequency,
            seatedForwardBendOne: seatedForwardBendOne,
            seatedForwardBendTwo: seatedForwardBendTwo,
            seatedForwardBendThree: seatedForwardBendThree,
            adultEyeOpeningMonopodSecond: adultEyeOpeningMonopodSecond,

            kneeLiftFrequency: kneeLiftFrequency,
            armCurlUpperBodyFrequency: armCurlUpperBodyFrequency,
            armCurlLowerBodyFrequency: armCurlLowerBodyFrequency,
            chairSeatedForwardBendOne: chairSeatedForwardBendOne,
            chairSeatedForwardBendTwo: chairSeatedForwardBendTwo,
            chairSeatedForwardBendThree: chairSeatedForwardBendThree,
            pullBackTestOne: pullBackTestOne,
            pullBackTestTwo: pullBackTestTwo,
            elderlyEyeOpeningMonopodSecond: elderlyEyeOpeningMonopodSecond,
            sittingAroundOneSecond: sittingAroundOneSecond,
            sittingAroundTwoSecond: sittingAroundTwoSecond,
            initial: initial,

            handlePulseOneInput: handlePulseOneInput,
            handlePulseTwoInput: handlePulseTwoInput,
            handlePulseThreeInput: handlePulseThreeInput,
            handleKneeCrunchesFrequencyInput: handleKneeCrunchesFrequencyInput,
            handleSeatedForwardBendOneInput: handleSeatedForwardBendOneInput,
            handleSeatedForwardBendTwoInput: handleSeatedForwardBendTwoInput,
            handleSeatedForwardBendThreeInput: handleSeatedForwardBendThreeInput,
            handleAdultEyeOpeningMonopodSecondInput: handleAdultEyeOpeningMonopodSecondInput,

            handleKneeLiftFrequencyInput: handleKneeLiftFrequencyInput,
            handleArmCurlUpperBodyFrequencyInput: handleArmCurlUpperBodyFrequencyInput,
            handleArmCurlLowerBodyFrequencyInput: handleArmCurlLowerBodyFrequencyInput,
            handleChairSeatedForwardBendOneInput: handleChairSeatedForwardBendOneInput,
            handleChairSeatedForwardBendTwoInput: handleChairSeatedForwardBendTwoInput,
            handleChairSeatedForwardBendThreeInput: handleChairSeatedForwardBendThreeInput,
            handlePullBackTestOneInput: handlePullBackTestOneInput,
            handlePullBackTestTwoInput: handlePullBackTestTwoInput,
            handleElderlyEyeOpeningMonopodSecondInput: handleElderlyEyeOpeningMonopodSecondInput,
            handleSittingAroundOneSecondInput: handleSittingAroundOneSecondInput,
            handleSittingAroundTwoSecondInput: handleSittingAroundTwoSecondInput
        }}>
            {children}
        </PreTestInfoContext.Provider>
    );
};

export default PreTestInfoContextProvider;