import { createContext, useState } from 'react';

type QuestionnaireFirstPartContextType = {
    questionnaireFirstPart: QuestionnaireFirstPartType[],
    initial: () => void,
    handleQuestionnaireFirstPart: (index: number) => void
};

type QuestionnaireFirstPartType = {
    questionNumber: number,
    answer: boolean
};

export const QuestionnaireFirstPartContext = createContext<QuestionnaireFirstPartContextType>({
    questionnaireFirstPart: [],
    initial: () => {},
    handleQuestionnaireFirstPart: () => {}
});

type QuestionnaireFirstPartContextProviderPropsType = {
    children: JSX.Element | JSX.Element[]
};

const QuestionnaireFirstPartContextProvider: React.FC<QuestionnaireFirstPartContextProviderPropsType> = ({ children }) => {
    const [questionnaireFirstPart, setQuestionnaireFirstPart] = useState<QuestionnaireFirstPartType[]>([
        {
            questionNumber: 1,
            answer: false
        },
        {
            questionNumber: 2,
            answer: false
        },
        {
            questionNumber: 3,
            answer: false
        },
        {
            questionNumber: 4,
            answer: false
        },
        {
            questionNumber: 5,
            answer: false
        },
        {
            questionNumber: 6,
            answer: false
        },
        {
            questionNumber: 7,
            answer: false
        }
    ]);  

    const initial = (): void => {
        questionnaireFirstPart.forEach((item) => {
            item.answer = false;
        });

        setQuestionnaireFirstPart(questionnaireFirstPart);
    };

    const handleQuestionnaireFirstPart = (index: number): void => {
        const temp = questionnaireFirstPart;
        temp[index].answer = !temp[index].answer;
        setQuestionnaireFirstPart([...temp]);
    };

    return (
        <QuestionnaireFirstPartContext.Provider value={{
            questionnaireFirstPart: questionnaireFirstPart,
            initial: initial,
            handleQuestionnaireFirstPart: handleQuestionnaireFirstPart
        }}>
            {children}
        </QuestionnaireFirstPartContext.Provider>
    );
};

export default QuestionnaireFirstPartContextProvider;