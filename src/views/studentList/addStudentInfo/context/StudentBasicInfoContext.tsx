import React, { createContext, useState } from 'react';

type StudentBasicInfoContextType = {
    pageId: number,
    account: string,
    name: string,
    gender: string,
    birthday: string,
    height: number | undefined,
    weight: number | undefined,
    email: string,
    phone: string,
    address: string,
    password: string,
    confirmPassword: string,
    initial: () => void,
    addPageId: () => void,
    subPageId: () => void,
    targetPageId: (pageId: number) => void,
    handleAccountInput: (account: string) => void,
    handleNameInput: (name: string) => void,
    handleGenderInput: (gender: string) => void,
    handleBirthdayInput: (birthday: string) => void,
    handleHeightInput: (height: number | undefined) => void,
    handleWeightInput: (weight: number | undefined) => void,
    handlePhoneInput: (phone: string) => void,
    handleEmailInput: (email: string) => void,
    handleAddressInput: (address: string) => void,
    handlePasswordInput: (password: string) => void,
    handleConfirmPasswordInput: (confirmPassword: string) => void,
};

export const StudentBasicInfoContext = createContext<StudentBasicInfoContextType>({
    pageId: 1,
    account: '',
    name: '',
    gender: '男',
    birthday: '',
    height: undefined,
    weight: undefined,
    email: '',
    phone: '',
    address: '',
    password: '',
    confirmPassword: '',
    initial: () => {},
    addPageId: () => {},
    subPageId: () => {},
    targetPageId: () => {},
    handleAccountInput: () => {},
    handleNameInput: () => {},
    handleGenderInput: () => {},
    handleBirthdayInput: () => {},
    handleHeightInput: () => {},
    handleWeightInput: () => {},
    handlePhoneInput: () => {},
    handleAddressInput: () => {},
    handlePasswordInput: () => {},
    handleConfirmPasswordInput: () => {},
    handleEmailInput: () => {}
});

type StudentBasicInfoContextProviderPropsType = {
    children: JSX.Element | JSX.Element[]
};

const StudentBasicInfoContextProvider: React.FC<StudentBasicInfoContextProviderPropsType> = ({ children }) => {
    const pageTotal = 5;
    const [pageId, setPageId] = useState<number>(1); // 頁面 id
    
    const [account, setAccount] = useState<string>(''); // 帳號
    const [name, setName] = useState<string>(''); // 姓名
    const [gender, setGender] = useState<string>('男'); // 性別
    const [birthday, setBirthday] = useState<string>(''); // 生日
    const [height, setHeight] = useState<number | undefined>(undefined); // 身高
    const [weight, setWeight] = useState<number | undefined>(undefined); // 體重
    const [phone, setPhone] = useState<string>(''); // 手機
    const [email, setEmail] = useState<string>(''); // e-mail 信箱
    const [address, setAddress] = useState<string>(''); // 地址
    const [password, setPassword] = useState<string>(''); // 密碼
    const [confirmPassword, setConfirmPassword] = useState<string>(''); // 確認密碼

    const addPageId = (): void => {
        if (pageId + 1 <= pageTotal) {
            setPageId(pageId + 1);
        }
    };

    const subPageId = (): void => {
        if (pageId - 1 >= 1) {
            setPageId(pageId - 1);
        }
    };
    
    const targetPageId = (pageId: number): void => {
        if (pageId >= 1 && pageId <= 5) {
            setPageId(pageId);
        }
    };

    const initial = (): void => {
        setAccount('');
        setName('');
        setGender('');
        setBirthday('');
        setHeight(undefined);
        setWeight(undefined);
        setPhone('');
        setEmail('');
        setAddress('');
        setPassword('');
        setConfirmPassword('');
    };

    /**
     * 處理帳號輸入
     */
    const handleAccountInput = (account: string): void => {
        setAccount(account);
    };

    /**
     * 處理姓名輸入
     */
    const handleNameInput = (name: string): void => {
        setName(name);
    };

    /**
     * 處理性別輸入
     */
    const handleGenderInput = (gender: string): void => {
        setGender(gender);
    };

    /**
     * 處理生日輸入
     */
    const handleBirthdayInput = (birthday: string): void => {
        setBirthday(birthday);
    };

    /**
     * 處理身高輸入
     */
    const handleHeightInput = (height: number | undefined): void => {
        setHeight(height);
    };

    /**
     * 處理體重輸入
     */
    const handleWeightInput = (weight: number | undefined): void => {
        setWeight(weight);
    };

    /**
     * 處理電話輸入
     */
    const handlePhoneInput = (phone: string): void => {
        setPhone(phone);
    };

    /**
     * 處理 e-mail 輸入
     */
    const handleEmailInput = (email: string): void => {
        setEmail(email);
    };

    /**
     * 處理地址輸入
     */
    const handleAddressInput = (address: string): void => {
        setAddress(address);
    };

    /**
     * 處理密碼輸入
     */
    const handlePasswordInput = (password: string): void => {
        setPassword(password);
    };

    /**
     * 處理確認密碼輸入
     */
    const handleConfirmPasswordInput = (confirmPassword: string): void => {
        setConfirmPassword(confirmPassword);
    };
    
    return (
        <StudentBasicInfoContext.Provider value={{
            pageId: pageId,
            account: account,
            name: name,
            gender: gender,
            birthday: birthday,
            height: height,
            weight: weight,
            phone: phone,
            email: email,
            address: address,
            password: password,
            confirmPassword: confirmPassword,
            initial: initial,
            addPageId: addPageId,
            subPageId: subPageId,
            targetPageId: targetPageId,
            handleAccountInput: handleAccountInput,
            handleNameInput: handleNameInput,
            handleGenderInput: handleGenderInput,
            handleBirthdayInput: handleBirthdayInput,
            handleHeightInput: handleHeightInput,
            handleWeightInput: handleWeightInput,
            handlePhoneInput: handlePhoneInput,
            handleEmailInput: handleEmailInput,
            handleAddressInput: handleAddressInput,
            handlePasswordInput: handlePasswordInput,
            handleConfirmPasswordInput: handleConfirmPasswordInput
        }}>
            {children}
        </StudentBasicInfoContext.Provider>
    );
};

export default StudentBasicInfoContextProvider;