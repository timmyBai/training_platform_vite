import React, { useEffect, useState, createContext } from 'react';

type QuestionnaireTwoPartContextType = {
    questionnaireTwoPart: QuestionnaireTwoPartType[],
    showSubjectQuestionnaireOne: boolean,
    showSubjectQuestionnaireTwo: boolean,
    showSubjectQuestionnaireThree: boolean,
    showSubjectQuestionnaireFour: boolean,
    showSubjectQuestionnaireFive: boolean,
    showSubjectQuestionnaireSix: boolean,
    showSubjectQuestionnaireSeven: boolean,
    showSubjectQuestionnaireEight: boolean
    showSubjectQuestionnaireNine: boolean,
    initial: () => void,
    handleQuestionnaireTwoPart: (index: number) => void
};

type QuestionnaireTwoPartType = {
    questionNumber: number,
    subject: string,
    answer: boolean
};

export const QuestionnaireTwoPartContext = createContext<QuestionnaireTwoPartContextType>({
    questionnaireTwoPart: [],
    showSubjectQuestionnaireOne: false,
    showSubjectQuestionnaireTwo: false,
    showSubjectQuestionnaireThree: false,
    showSubjectQuestionnaireFour: false,
    showSubjectQuestionnaireFive: false,
    showSubjectQuestionnaireSix: false,
    showSubjectQuestionnaireSeven: false,
    showSubjectQuestionnaireEight: false,
    showSubjectQuestionnaireNine: false,
    initial: () => {},
    handleQuestionnaireTwoPart: () => {}
});

type QuestionnaireTwoPartContextProviderPropsType = {
    children: JSX.Element | JSX.Element[]
};

const QuestionnaireTwoPartContextProvider: React.FC<QuestionnaireTwoPartContextProviderPropsType> = ({ children }) => {
    const [questionnaireTwoPart, setQuestionnaireTwoPart] = useState<QuestionnaireTwoPartType[]>([
        {
            questionNumber: 1,
            subject: '',
            answer: false
        },
        {
            questionNumber: 1,
            subject: '1a',
            answer: false
        },
        {
            questionNumber: 1,
            subject: '1b',
            answer: false
        },
        {
            questionNumber: 1,
            subject: '1c',
            answer: false
        },
        {
            questionNumber: 2,
            subject: '',
            answer: false
        },
        {
            questionNumber: 2,
            subject: '2a',
            answer: false
        },
        {
            questionNumber: 2,
            subject: '2c',
            answer: false
        },
        {
            questionNumber: 3,
            subject: '',
            answer: false
        },
        {
            questionNumber: 3,
            subject: '3a',
            answer: false
        },
        {
            questionNumber: 3,
            subject: '3b',
            answer: false
        },
        {
            questionNumber: 3,
            subject: '3c',
            answer: false
        },
        {
            questionNumber: 3,
            subject: '3d',
            answer: false
        },
        {
            questionNumber: 3,
            subject: '3e',
            answer: false
        },
        {
            questionNumber: 4,
            subject: '',
            answer: false
        },
        {
            questionNumber: 4,
            subject: '4a',
            answer: false
        },
        {
            questionNumber: 4,
            subject: '4b',
            answer: false
        },
        {
            questionNumber: 4,
            subject: '4c',
            answer: false
        },
        {
            questionNumber: 5,
            subject: '',
            answer: false
        },
        {
            questionNumber: 5,
            subject: '5a',
            answer: false
        },
        {
            questionNumber: 5,
            subject: '5b',
            answer: false
        },
        {
            questionNumber: 6,
            subject: '',
            answer: false
        },
        {
            questionNumber: 6,
            subject: '6a',
            answer: false
        },
        {
            questionNumber: 6,
            subject: '6b',
            answer: false
        },
        {
            questionNumber: 6,
            subject: '6c',
            answer: false
        },
        {
            questionNumber: 6,
            subject: '6d',
            answer: false
        },
        {
            questionNumber: 7,
            subject: '',
            answer: false
        },
        {
            questionNumber: 7,
            subject: '7a',
            answer: false
        },
        {
            questionNumber: 7,
            subject: '7b',
            answer: false
        },
        {
            questionNumber: 7,
            subject: '7c',
            answer: false
        },
        {
            questionNumber: 8,
            subject: '',
            answer: false
        },
        {
            questionNumber: 8,
            subject: '8a',
            answer: false
        },
        {
            questionNumber: 8,
            subject: '8b',
            answer: false
        },
        {
            questionNumber: 8,
            subject: '8c',
            answer: false
        },
        {
            questionNumber: 9,
            subject: '',
            answer: false
        },
        {
            questionNumber: 9,
            subject: '9a',
            answer: false
        },
        {
            questionNumber: 9,
            subject: '9b',
            answer: false
        },
        {
            questionNumber: 9,
            subject: '9c',
            answer: false
        }
    ]);

    const [questionnaireTwoPartIndex, setQuestionnaireTwoPartIndex] = useState<number>(0);
    const [showSubjectQuestionnaireOne, setShowSubjectQuestionnaireOne] = useState<boolean>(false);
    const [showSubjectQuestionnaireTwo, setShowSubjectQuestionnaireTwo] = useState<boolean>(false);
    const [showSubjectQuestionnaireThree, setShowSubjectQuestionnaireThree] = useState<boolean>(false);
    const [showSubjectQuestionnaireFour, setShowSubjectQuestionnaireFour] = useState<boolean>(false);
    const [showSubjectQuestionnaireFive, setShowSubjectQuestionnaireFive] = useState<boolean>(false);
    const [showSubjectQuestionnaireSix, setShowSubjectQuestionnaireSix] = useState<boolean>(false);
    const [showSubjectQuestionnaireSeven, setShowSubjectQuestionnaireSeven] = useState<boolean>(false);
    const [showSubjectQuestionnaireEight, setShowSubjectQuestionnaireEight] = useState<boolean>(false);
    const [showSubjectQuestionnaireNine, setShowSubjectQuestionnaireNine] = useState<boolean>(false);

    const initial = (): void => {
        questionnaireTwoPart.forEach((item) => {
            item.answer = false;
        });
    };

    const handleQuestionnaireTwoPart = (index: number) => {
        const temp = questionnaireTwoPart;
        temp[index].answer = !temp[index].answer;

        setQuestionnaireTwoPart([...temp]);
        setQuestionnaireTwoPartIndex(index);
    };

    useEffect(() => {
        const temp = questionnaireTwoPart;
        const index = questionnaireTwoPartIndex;

        if (temp[index].questionNumber === 1 && temp[index].subject === '') {
            if (temp[index].answer) {
                setShowSubjectQuestionnaireOne(true);
            }
            else {
                setShowSubjectQuestionnaireOne(false);
            }
        }

        if (temp[index].questionNumber === 2 && temp[index].subject === '') {
            if (temp[index].answer) {
                setShowSubjectQuestionnaireTwo(true);
            }
            else {
                setShowSubjectQuestionnaireTwo(false);
            }
        }

        if (temp[index].questionNumber === 3 && temp[index].subject === '') {
            if (temp[index].answer) {
                setShowSubjectQuestionnaireThree(true);
            }
            else {
                setShowSubjectQuestionnaireThree(false);
            }
        }

        if (temp[index].questionNumber === 4 && temp[index].subject === '') {
            if (temp[index].answer) {
                setShowSubjectQuestionnaireFour(true);
            }
            else {
                setShowSubjectQuestionnaireFour(false);
            }
        }

        if (temp[index].questionNumber === 5 && temp[index].subject === '') {
            if (temp[index].answer) {
                setShowSubjectQuestionnaireFive(true);
            }
            else {
                setShowSubjectQuestionnaireFive(false);
            }
        }

        if (temp[index].questionNumber === 6 && temp[index].subject === '') {
            if (temp[index].answer) {
                setShowSubjectQuestionnaireSix(true);
            }
            else {
                setShowSubjectQuestionnaireSix(false);
            }
        }

        if (temp[index].questionNumber === 7 && temp[index].subject === '') {
            if (temp[index].answer) {
                setShowSubjectQuestionnaireSeven(true);
            }
            else {
                setShowSubjectQuestionnaireSeven(false);
            }
        }

        if (temp[index].questionNumber === 8 && temp[index].subject === '') {
            if (temp[index].answer) {
                setShowSubjectQuestionnaireEight(true);
            }
            else {
                setShowSubjectQuestionnaireEight(false);
            }
        }

        if (temp[index].questionNumber === 9 && temp[index].subject === '') {
            if (temp[index].answer) {
                setShowSubjectQuestionnaireNine(true);
            }
            else {
                setShowSubjectQuestionnaireNine(false);
            }
        }
    }, [questionnaireTwoPart, questionnaireTwoPartIndex]);

    return (
        <QuestionnaireTwoPartContext.Provider value={{
            questionnaireTwoPart: questionnaireTwoPart,
            showSubjectQuestionnaireOne: showSubjectQuestionnaireOne,
            showSubjectQuestionnaireTwo: showSubjectQuestionnaireTwo,
            showSubjectQuestionnaireThree: showSubjectQuestionnaireThree,
            showSubjectQuestionnaireFour: showSubjectQuestionnaireFour,
            showSubjectQuestionnaireFive: showSubjectQuestionnaireFive,
            showSubjectQuestionnaireSix: showSubjectQuestionnaireSix,
            showSubjectQuestionnaireSeven: showSubjectQuestionnaireSeven,
            showSubjectQuestionnaireEight: showSubjectQuestionnaireEight,
            showSubjectQuestionnaireNine: showSubjectQuestionnaireNine,
            initial: initial,
            handleQuestionnaireTwoPart: handleQuestionnaireTwoPart
        }}>
            {children}
        </QuestionnaireTwoPartContext.Provider>
    );
};

export default QuestionnaireTwoPartContextProvider;