export const zoomOut = 2;
export const delay = 30;
export const duration = 1500;
export const tickMinCircleR = 4;
export const tickMaxCircleR = 6;
export const margin = { top: 10, right: 50, bottom: 80, left: 50 };
// 公差比例
export const tolerance = 0.6;
