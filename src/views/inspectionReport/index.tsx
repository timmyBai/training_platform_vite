import React, { useState } from 'react';

// css
import './index.sass';

// images
import cardiorespiratoryFitnessIcon from '@/assets/cardiorespiratory_fitness.svg';
import muscleStrengthAndMuscleEnduranceIcon from '@/assets/muscle_strength_and_muscle_endurance.svg';
import softnessIcon from '@/assets/softness.svg';
import balanceIcon from '@/assets/balance.svg';
import reportAddButton from '@/assets/images/inspectionReport/reportAddButton.svg';
import btn_delete from '@/assets/images/btn_delete.svg';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

// components
import StudentInfoBar from '@/components/StudentInfoBar';
import InspectionReportCardiorespiratoryFitness from './components/CardiorespiratoryFitness/InspectionReportCardiorespiratoryFitness';
import InspectionReportSoftness from './components/Softness/InspectionReportSoftness';
import InspectionReportBalance from './components/Balance/InspectionReportBalance';
import InspectionReportMuscleStrengthAndMuscleEndurance from './components/InspectionReportMuscleStrengthAndMuscleEndurance';

const InspectionReport: React.FC = () => {
    const dispatch = useAppDispatch();
    const [reportTypeIndex, setReportTypeIndex] = useState<number>(2);
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const reportType = [
        { img: cardiorespiratoryFitnessIcon, text: '心肺適能' },
        { img: muscleStrengthAndMuscleEnduranceIcon, text: '肌力與肌耐力' },
        { img: softnessIcon, text: '柔軟度' },
        { img: balanceIcon, text: '平衡' }
    ];

    const storeGetters = useAppSelector((state) => {
        return {
            summaryPage: state.inspectionReport.summaryPage,
            singleDayPage: state.inspectionReport.singleDayPage,
            showBtnSummary: state.inspectionReport.showBtnSummary,
            showBtnSingleDay: state.inspectionReport.showBtnSingleDay,
            btnSummaryText: state.inspectionReport.btnSummaryText,
            btnSingleDayText: state.inspectionReport.btnSingleDayText,
            showBtnDeleteSummarySingle: state.inspectionReport.showBtnDeleteSummarySingle
        };
    });

    const changeReportType = (index: number) => {
        setIsOpen(false);
        setReportTypeIndex(index);

        visibleButton();
    };

    const showIsOpen = () => {
        setIsOpen(true);
        visibleButton();
    };

    const visibleButton = () => {
        dispatch({ type: 'inspectionReport/resetSummaryText' });
        dispatch({ type: 'inspectionReport/resetPage' });
        dispatch({ type: 'inspectionReport/visibleBtnSummary', payload: false });
        dispatch({ type: 'inspectionReport/visibleBtnSingleDay', payload: false });
    };

    const toggleSummary = () => {
        dispatch({ type: 'inspectionReport/toggleBtnSummaryText' });
        dispatch({ type: 'inspectionReport/visibleBtnDeleteSummarySingle', payload: false });

        if (storeGetters.singleDayPage) {
            if (storeGetters.btnSummaryText === '清單') {
                dispatch({ type: 'inspectionReport/resetPage' });
                dispatch({ type: 'inspectionReport/visibleSummaryListPage', payload: true });
            }
            else {
                dispatch({ type: 'inspectionReport/resetPage' });
                dispatch({ type: 'inspectionReport/visibleSummaryPage', payload: true });
            }
        }
        else {
            if (storeGetters.summaryPage) {
                dispatch({ type: 'inspectionReport/resetPage' });
                dispatch({ type: 'inspectionReport/visibleSummaryListPage', payload: true });
            }
            else {
                dispatch({ type: 'inspectionReport/resetPage' });
                dispatch({ type: 'inspectionReport/visibleSummaryPage', payload: true });
            }
        }
    };

    const toggleSingleDayReport = () => {
        dispatch({ type: 'inspectionReport/resetPage' });
        dispatch({ type: 'inspectionReport/visibleSingleDayPage', payload: true });
        dispatch({ type: 'inspectionReport/visibleBtnDeleteSummarySingle', payload: false });
    };

    const handleVisibleDeleteMarkDialog = () => {
        dispatch({ type: 'inspectionReport/visibleDeleteMarkDialog', payload: true });
    };

    return (
        <div className='inspectionReport'>
            <StudentInfoBar></StudentInfoBar>

            <div className='page_title'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                        <div className='card'>
                            <div className='headline'>
                                <div className='group'>

                                </div>

                                <div className='group'>
                                    <h2 className='title'>檢視報表</h2>
                                    <img className='openUp' src={reportAddButton} onClick={showIsOpen} />
                                </div>

                                <div className='group'>
                                    {storeGetters.showBtnSingleDay && <button className='btnSummary' onClick={toggleSummary}>{storeGetters.btnSummaryText}</button>}
                                    {storeGetters.showBtnSingleDay && <button className='btnSingleDay' onClick={toggleSingleDayReport}>{storeGetters.btnSingleDayText}</button>}
                                    {storeGetters.showBtnDeleteSummarySingle && <img className='btnDelete' src={btn_delete} onClick={handleVisibleDeleteMarkDialog} />}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {
                isOpen && (
                    <div className='reportType'>
                        <div className='container mx-auto'>
                            <div className='flex flex-wrap'>
                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    {
                                        reportType.map((item, index) => {
                                            return (
                                                <div className={`reportTypeInfo ${index === reportTypeIndex - 1 ? 'active' : ''}`} key={index} onClick={() => changeReportType(index + 1)}>
                                                    <img src={item.img} />
                                                    <span>{item.text}</span>
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                )
            }

            {
                !isOpen && reportTypeIndex === 1 && (
                    <InspectionReportCardiorespiratoryFitness></InspectionReportCardiorespiratoryFitness>
                )
            }

            {
                !isOpen && reportTypeIndex === 2 && (
                    <InspectionReportMuscleStrengthAndMuscleEndurance></InspectionReportMuscleStrengthAndMuscleEndurance>
                )
            }

            {
                !isOpen && reportTypeIndex === 3 && (
                    <InspectionReportSoftness></InspectionReportSoftness>
                )
            }

            {
                !isOpen && reportTypeIndex === 4 && (
                    <InspectionReportBalance></InspectionReportBalance>
                )
            }
        </div>
    );
};

export default InspectionReport;