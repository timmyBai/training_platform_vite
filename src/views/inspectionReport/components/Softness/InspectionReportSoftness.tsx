import React, { Fragment, useEffect, useRef, useState, useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import * as d3 from 'd3';
import { validate } from 'validate.js';

// css
import colors from '@/styles/colors.module.sass';
import './InspectionReportSoftness.sass';

// api
import { getInspectionReportPhysicalFitness } from '@/api/inspectionReport';
import { updateTrainingSchedule } from '@/api/trainingSchedule';

// images
import softnessIcon from '@/assets/softness.svg';
import reportArrorLeft from '@/assets/images/inspectionReport/reportArrowLeft.svg';
import reportArrowRight from '@/assets/images/inspectionReport/reportArrowRight.svg';
import reportButtonAddBorder from '@/assets/images/inspectionReport/reportButtonAddBorder.svg';
import reportButtonEditBorder from '@/assets/images/inspectionReport/reportButtonEditBorder.svg';
import reportButtonDeleteBorder from '@/assets/images/inspectionReport/reportButtonDeleteBorder.svg';

// utils
import {
    delay,
    duration,
    zoomOut,
    margin,
    tickMinCircleR,
    tickMaxCircleR,
    tolerance
} from '../../utils/attributes';
import { axiosErrorCode } from '@/utils/request';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

// components
import Dialog from '@/components/Dialog';

const InspectionReportSoftness: React.FC = () => {
    const reportRef = useRef<HTMLDivElement>(null);
    const reportAddFormRef = useRef<HTMLFormElement>(null);
    const reportEditFormRef = useRef<HTMLFormElement>(null);
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [reportSoftness, setReportSoftness] = useState<GetInspectionReportPhysicalFitnessDataType[]>([]); // 報表
    const [targetScheduleDate, setTargetScheduleDate] = useState<string>('');
    const [targetTrainingUnitMinute, setTargetTrainingUnitMinute] = useState<number>(0);
    const [targetActualCompletion, setTargetActualCompletion] = useState<number>(0);
    const [reportIndex, setReportIndex] = useState<number>(0);

    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);

    const [actionMode, setActionMode] = useState<'新增' | '編輯' | '刪除'>('新增');

    const [visibleAddTrainingScheduleDialog, setVisibleAddTrainingScheduleDialog] = useState<boolean>(false); // 新增訓練排程實際完成分鐘彈窗顯示/隱藏
    const [addTrainingSchedule, setAddTrainingSchedule] = useState<string>('');
    const [addActualCompletion, setAddActualCompletion] = useState<number>(0);
    const [addTrainingScheduleDateMessage, setAddTrainingScheduleDateMessage] = useState<string>('');
    const [addActualCompletionMessage, setAddActualCompletionMessage] = useState<string>('');
    const addTrainingScheduleForm = {
        date: {
            presence: {
                message: '必須輸入排程日期'
            }
        },
        actualCompletion: {
            presence: {
                message: '必須輸入實際訓練時間'
            },
            format: {
                pattern: '^[0-9]+$',
                message: '請輸入整數'
            }
        }
    };

    const [visibleEditTrainingScheduleDialog, setVisibleEditTrainingScheduleDialog] = useState<boolean>(false); // 編輯訓練排程實際完成分鐘顯示/隱藏
    const [editActualCompletion, setEditActualCompletion] = useState<number>(0);
    const [editActualCompletionMessage, setEditActualCompletionMessage] = useState<string>('');
    const editTrainingScheduleForm = {
        actualCompletion: {
            presence: {
                message: '必須輸入實際訓練時間'
            },
            format: {
                pattern: '^[0-9]+$',
                message: '請輸入整數'
            }
        }
    };

    const [visibleDeleteTrainingScheduleDialog, setVisibleDeleteTrainingScheduleDialog] = useState<boolean>(false); // 刪除訓練排程實際完成分鐘顯示/隱藏
    
    const [visibleInfoDialog, setVisibleInfoDialog] = useState<boolean>(false);
    const [visibleInfoDialogTitle, setVisibleInfoDialogTitle] = useState<string>('');
    const [visibleInfoDialogMessage, setVisibleInfoDialogMessage] = useState<string>('');

    const storeGetters = useAppSelector((state) => {
        return {
            studentId: state.studentInfo.studentId
        };
    });

    const handleGetInspectionReportPhysicalFitness = () => {
        getInspectionReportPhysicalFitness(storeGetters.studentId, 3).then((res) => {
            const { data } = res.data;

            setReportIndex(data.length - 1);
            setReportSoftness(data.reverse());
            setTargetScheduleDate(data[data.length - 1]?.scheduleDate);
            setTargetTrainingUnitMinute(data[data.length - 1]?.trainingUnitMinute);
            setTargetActualCompletion(data[data.length - 1]?.actualCompletion);
        }).catch((error) => {
            const errorCode = axiosErrorCode<GetInspectionReportPhysicalFitnessErrorCodeType>(error);

            setVisibleInfoDialog(true);
            setVisibleInfoDialogTitle('錯誤');

            switch (errorCode) {
                case 'StudentDoesNotExist':
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                default:
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    const handleResize = () => {
        if (reportRef.current?.clientWidth !== undefined) {
            setWidth(reportRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }
    };

    const drawReportSoftness = useCallback(() => {
        const data = reportSoftness;
        const actualCompletionList: any[] = [];
        const scheduleDateList: any = [];
        const trainingUnitMinuteList: any[] = [];

        // 彙整實際次數資料
        data.forEach((item) => {
            actualCompletionList.push(item.actualCompletion);
            scheduleDateList.push(item.scheduleDate);
            trainingUnitMinuteList.push(item.trainingUnitMinute);
        });

        d3.select('.softnessReport').select('svg').remove();

        const svg = d3.select('.softnessReport')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        // 繪製 x 軸
        const x = d3.scaleBand()
            .domain(scheduleDateList)
            .range([0, width]);

        svg.append('g')
            .call(d3.axisBottom(x).tickFormat((d, i) => {
                const scheduleDate = data?.[i]?.scheduleDate || undefined;
                if (scheduleDate !== undefined) {
                    const scheduleDateSplit = scheduleDate.split('-');
                    const month = scheduleDateSplit[1];
                    const date = scheduleDateSplit[2];
                    return `${month}/${date}`;
                }
                else if (typeof (d) === 'string') {
                    return d;
                }

                return '';
            }).tickPadding(35))
            .attr('class', 'softnessX')
            .attr('transform', `translate(0, ${height})`)
            .style('font-size', '14px')
            .style('color', colors.color_white);

        d3.select('.softnessX')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.softnessX')
            .selectAll('.tick')
            .append('circle')
            .attr('r', (date) => {
                if (date === reportSoftness[0].scheduleDate || date === reportSoftness[reportSoftness.length - 1].scheduleDate) {
                    return tickMaxCircleR;
                }

                return tickMinCircleR;
            })
            .attr('fill', (date) => {
                if (date === targetScheduleDate) {
                    return colors.color_yellow;
                }

                return 'rgba(255, 255, 255, 0.5)';
            })
            .attr('transform', 'translate(0, 20)');

        d3.select('.softnessX')
            .selectAll('.tick')
            .selectAll('line')
            .attr('stroke', 'transparent');

        d3.select('.softnessX').selectAll('.tick')
            .selectAll('text')
            .style('color', (date) => {
                if (date === reportSoftness[0].scheduleDate || date === reportSoftness[reportSoftness.length - 1].scheduleDate) {
                    return 'rgba(255,255,255,0.5)';
                }

                return 'transparent';
            })
            .style('font-size', '12px');


        // 繪製 y 軸
        const actualCompletionMax = d3.max(actualCompletionList);
        const trainingUnitMinuteMax = d3.max(trainingUnitMinuteList);
        const YMax = Math.max(actualCompletionMax, trainingUnitMinuteMax);

        const y = d3.scaleLinear()
            .domain([0, YMax])
            .range([height, 0]);

        svg.append('g')
            .call(d3.axisLeft(y))
            .attr('class', 'softnessY')
            .attr('height', height);

        d3.select('.softnessY')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.softnessY')
            .selectAll('.tick')
            .selectAll('line')
            .style('stroke', 'transparent');

        d3.select('.softnessY')
            .selectAll('.tick')
            .selectAll('text')
            .style('font-size', '14px')
            .style('color', colors.color_white);
        
        // 方塊平均寬度
        const boxAvgWidth = width / data.length;
        // 計算每一方格寬度公差百分比
        const tolerancePercentage = boxAvgWidth * tolerance;
        // 計算每一方格寬度公差百分比一半
        const tolerancePercentageHalf = tolerancePercentage / 2;
        // 方格框度
        const boxWidth = boxAvgWidth - zoomOut - tolerancePercentage;

        // 繪製直方圖(訓練目標分鐘)
        svg.append('g')
            .attr('class', 'trainingUnitMinuteBoxGroup')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) - tolerancePercentageHalf}, ${height}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                return height / YMax * d.trainingUnitMinute;
            })
            .attr('fill', () => {
                return 'rgba(255, 255, 255, 0.1)';
            });

        // 繪製直方圖(實際完成分鐘已完成)
        svg.append('g')
            .attr('class', 'actualCompletionOrangeBoxGroup')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) - tolerancePercentageHalf}, ${height}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.actualCompletion >= d.trainingUnitMinute) {
                    return height / YMax * d.actualCompletion;
                }

                return 0;
            })
            .attr('fill', colors.color_orange);

        // 繪製直方圖(實際完成分鐘未完成)
        svg.append('g')
            .attr('class', 'actualCompletionRedBoxGroup')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) - tolerancePercentageHalf}, ${height}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.actualCompletion < d.trainingUnitMinute) {
                    return height / YMax * d.actualCompletion;
                }

                return 0;
            })
            .attr('fill', colors.color_red);

        svg.append('text')
            .text('過去 60 次訓練時間-分鐘')
            .attr('y', height + margin.top + margin.bottom / 1.3)
            .attr('x', width / 2 - margin.left - margin.right)
            .attr('fill', colors.color_white)
            .style('font-size', '20px');

        const mouseover = (event: any, d: any) => {
            d3.select('.softnessTooltip').select('p.targetScheduleDate').text(`訓練排程日期 ${d.scheduleDate}`);
            d3.select('.softnessTooltip').select('p.targetTrainingUnitMinute').text(`訓練目標時間 ${d.trainingUnitMinute} 分鐘`);
            d3.select('.softnessTooltip').select('p.targetActualCompletion').text(`實際訓練時間 ${d.actualCompletion} 分鐘`);
        };

        const mousemove = (event: any) => {
            const x = event.x;
            const y = event.y;

            if (x > width - 50) {
                d3.select('.softnessTooltip')
                    .style('display', 'block')
                    .style('left', `${x - 410}px`)
                    .style('top', `${y - 260}px`);
            }
            else {
                d3.select('.softnessTooltip')
                    .style('display', 'block')
                    .style('left', `${x - 230}px`)
                    .style('top', `${y - 255}px`);
            }
        };

        const mouseleave = () => {
            d3.select('.softnessTooltip')
                .style('display', 'none');
        };

        d3.select('.softnessReport').select('svg').selectAll('rect').on('mouseover', mouseover);
        d3.select('.softnessReport').select('svg').selectAll('rect').on('mousemove', mousemove);
        d3.select('.softnessReport').select('svg').selectAll('rect').on('mouseleave', mouseleave);
    }, [width, height, reportSoftness, setReportSoftness]);

    const prevScheduleDate = () => {
        if (reportIndex - 1 > 0) {
            setReportIndex(reportIndex - 1);
            setTargetScheduleDate(reportSoftness[reportIndex - 1].scheduleDate);
            setTargetTrainingUnitMinute(reportSoftness[reportIndex - 1].trainingUnitMinute);
            setTargetActualCompletion(reportSoftness[reportIndex - 1].actualCompletion);
        }
        else {
            setReportIndex(0);
            setTargetScheduleDate(reportSoftness[0].scheduleDate);
            setTargetTrainingUnitMinute(reportSoftness[0].trainingUnitMinute);
            setTargetActualCompletion(reportSoftness[0].actualCompletion);
        }
    };

    const nextScheduleDate = () => {
        if (reportIndex + 1 < reportSoftness.length) {
            setReportIndex(reportIndex + 1);
            setTargetScheduleDate(reportSoftness[reportIndex + 1].scheduleDate);
            setTargetTrainingUnitMinute(reportSoftness[reportIndex + 1].trainingUnitMinute);
            setTargetActualCompletion(reportSoftness[reportIndex + 1].actualCompletion);
        }
        else {
            setReportIndex(reportSoftness.length - 1);
            setTargetScheduleDate(reportSoftness[reportSoftness.length - 1].scheduleDate);
            setTargetTrainingUnitMinute(reportSoftness[reportSoftness.length - 1].trainingUnitMinute);
            setTargetActualCompletion(reportSoftness[reportSoftness.length - 1].actualCompletion);
        }
    };

    // 關閉資訊彈窗
    const closeInfoDialog = () => {
        setVisibleInfoDialog(false);
    };

    // 顯示新增訓練排程實際完成分鐘彈窗
    const showAddTrainingScheduleDialog = () => {
        setVisibleAddTrainingScheduleDialog(true);
        setActionMode('新增');
    };

    // 關閉新增訓練排程實際完成分鐘彈窗
    const closeAddTrainingScheduleDialog = () => {
        setVisibleAddTrainingScheduleDialog(false);
        setAddTrainingSchedule('');
        setAddActualCompletion(0);
        setAddTrainingScheduleDateMessage('');
        setAddActualCompletionMessage('');
    };

    const handleAddTrainingSchedule = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAddTrainingSchedule(e.target.value);
        inspectAddTrainingScheduleField();
    };

    const handleAddActualCompletion = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAddActualCompletion(parseInt(e.target.value));
        inspectAddTrainingScheduleField();
    };

    const handleEditActualCompletion = (e: React.ChangeEvent<HTMLInputElement>) => {
        setEditActualCompletion(parseInt(e.target.value));
        inspectEditTrainingScheduleField();
    };

    // 顯示編輯訓練排程實際完成分鐘彈窗
    const showEditTrainingScheduleDialog = () => {
        setVisibleEditTrainingScheduleDialog(true);
        setActionMode('編輯');
    };

    // 隱藏編輯訓練排程實際完成分鐘彈窗
    const closeEditTrainingScheduleDialog = () => {
        setVisibleEditTrainingScheduleDialog(false);
        setEditActualCompletion(0);
        setEditActualCompletionMessage('');
    };

    const showDeleteTrainingScheduleDialog = () => {
        setVisibleDeleteTrainingScheduleDialog(true);
        setActionMode('刪除');
    };

    const closeDeleteTrainingScheduleDialog = () => {
        setVisibleDeleteTrainingScheduleDialog(false);
    };

    /**
     * 更新訓練排程實際完成分鐘
     */
    const handleUpdateTrainingSchedule = (trainingSchedule: string, actualCompletion: number) => {
        const data: UpdateTrainingScheduleRequestType = {
            trainingType: 3,
            scheduleDate: trainingSchedule,
            actualCompletion: actualCompletion
        };

        updateTrainingSchedule(storeGetters.studentId, data).then(() => {
            setVisibleInfoDialog(true);
            setVisibleInfoDialogTitle('資訊');
            setVisibleInfoDialogMessage('修改報表成功');

            closeAddTrainingScheduleDialog();
            closeEditTrainingScheduleDialog();
            closeDeleteTrainingScheduleDialog();

            handleGetInspectionReportPhysicalFitness();
        }).catch((error) => {
            const errorCode = axiosErrorCode<UpdateTrainingScheduleErrorCodeType>(error);

            setVisibleInfoDialog(true);
            setVisibleInfoDialogTitle('錯誤');

            switch (errorCode) {
                case 'BadRequest':
                    setVisibleInfoDialogMessage('格式錯誤');
                    break;
                case 'StudentDoesNotExist':
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 'TrainingScheduleDataExist':
                    setVisibleInfoDialogMessage('查無排程資料');
                    break;
                case 'NotModifyBeforeSixtyTrainingSchedule':
                    setVisibleInfoDialogMessage('不可修改超過過去 60 次訓練排程資料');
                    break;
                case 'UpdateTrainingScheduleCardiorespiratoryFitnessFail':
                    setVisibleInfoDialogMessage('更新訓練排程失敗');
                    break;
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                default:
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    const inspectUpdateTrainingScheduleMode = (trainingSchedule: string, actualCompletion: number) => {
        if (actionMode === '新增') {
            const isValid = inspectAddTrainingScheduleField();

            if (isValid) {
                handleUpdateTrainingSchedule(trainingSchedule, actualCompletion);
            }
        }
        else if (actionMode === '編輯') {
            const isValid = inspectEditTrainingScheduleField();

            if (isValid) {
                handleUpdateTrainingSchedule(trainingSchedule, actualCompletion);
            }
        }
        else {
            handleUpdateTrainingSchedule(trainingSchedule, actualCompletion);
        }
    };

    // 檢查新增訓練排程輸入欄位
    const inspectAddTrainingScheduleField = () => {
        const isValid = validate(reportAddFormRef.current, addTrainingScheduleForm);

        if (isValid !== undefined) {
            if (isValid.date) {
                setAddTrainingScheduleDateMessage(isValid.date[0].replace('Date ', ''));
            }
            else {
                setAddTrainingScheduleDateMessage('');
            }

            if (isValid.actualCompletion) {
                setAddActualCompletionMessage(isValid.actualCompletion[0].replace('Actual completion ', ''));
            }
            else {
                setAddActualCompletionMessage('');
            }


            return false;
        }
        else {
            setAddTrainingScheduleDateMessage('');
            setAddActualCompletionMessage('');

            return true;
        }
    };

    // 檢查新增訓練排程輸入欄位
    const inspectEditTrainingScheduleField = () => {
        const isValid = validate(reportEditFormRef.current, editTrainingScheduleForm);

        if (isValid !== undefined) {
            if (isValid.actualCompletion) {
                setEditActualCompletionMessage(isValid.actualCompletion[0].replace('Actual completion ', ''));
            }
            else {
                setEditActualCompletionMessage('');
            }

            return false;
        }
        else {
            setEditActualCompletionMessage('');

            return true;
        }
    };

    const initWindowSize = () => {
        handleResize();
    };

    useEffect(() => {
        initWindowSize();
        handleGetInspectionReportPhysicalFitness();
        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    useEffect(() => {
        drawReportSoftness();
    }, [drawReportSoftness, reportSoftness]);

    // 監控目前排程變更日期重新指向黃色圓形指標
    useEffect(() => {
        d3.select('.softnessX')
            .selectAll('.tick')
            .selectAll('circle')
            .remove();

        d3.select('.softnessX')
            .selectAll('.tick')
            .append('circle')
            .attr('r', (date) => {
                if (date === reportSoftness[0].scheduleDate || date === reportSoftness[reportSoftness.length - 1].scheduleDate) {
                    return tickMaxCircleR;
                }

                return tickMinCircleR;
            })
            .attr('fill', (date) => {
                if (date === targetScheduleDate) {
                    return colors.color_yellow;
                }

                return 'rgba(255, 255, 255, 0.5)';
            })
            .attr('transform', 'translate(0, 20)');
    }, [targetScheduleDate]);

    return (
        <Fragment>
            <div className='inspectionSoftnessReport'>
                <div className='container mx-auto'>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                            <div className='card'>
                                <div className='softnessReportInfo'>
                                    <div className='title'>
                                        <img src={softnessIcon} />
                                        <h2>柔軟度</h2>
                                    </div>
                                    <hr />

                                    <div className='actionDate'>
                                        <div className='prevDate'>
                                            <img className='reportArrowLeft' src={reportArrorLeft} onClick={prevScheduleDate} />
                                        </div>

                                        <div className='scheduleInfo'>
                                            <h3>{targetScheduleDate}</h3>
                                            <p>訓練目標時間 {targetTrainingUnitMinute} 分鐘</p>
                                            <p>實際訓練時間 {targetActualCompletion} 分鐘</p>
                                        </div>

                                        <div className='nextDate'>
                                            <img className='reportArrowRight' src={reportArrowRight} onClick={nextScheduleDate} />
                                        </div>
                                    </div>

                                    <div className='action'>
                                        <div className='gard'>
                                            <img src={reportButtonAddBorder} onClick={showAddTrainingScheduleDialog} />
                                        </div>

                                        <div className='gard'>
                                            <img src={reportButtonEditBorder} onClick={showEditTrainingScheduleDialog} />
                                        </div>

                                        <div className='gard'>
                                            <img src={reportButtonDeleteBorder} onClick={showDeleteTrainingScheduleDialog} />
                                        </div>
                                    </div>
                                </div>

                                <div className='softnessReport' ref={reportRef}>
                                    <div className='softnessTooltip'>
                                        <p className='targetScheduleDate'>訓練排程日期 </p>
                                        <p className='targetTrainingUnitMinute'>訓練目標時間 0 分鐘</p>
                                        <p className='targetActualCompletion'>實際訓練時間 0 分鐘</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {
                visibleAddTrainingScheduleDialog && (
                    <Dialog className='visibleAddTrainingScheduleDialog'>
                        <Dialog.Title title='新增'></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <form ref={reportAddFormRef}>
                                    <div className='group'>
                                        <h6>排程日期</h6>
                                        <input type='date' name='date' onChange={handleAddTrainingSchedule} />
                                        <p>{addTrainingScheduleDateMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>實際完成時間</h6>
                                        <input type='text' placeholder='請輸入實際完成時間' name='actualCompletion' onChange={handleAddActualCompletion} />
                                        <p>{addActualCompletionMessage}</p>
                                    </div>
                                </form>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnBtnYes' onClick={() => inspectUpdateTrainingScheduleMode(addTrainingSchedule, addActualCompletion)}>確定</button>
                            <button className='dialogBtnBtnNo' onClick={closeAddTrainingScheduleDialog}>取消</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }

            {
                visibleEditTrainingScheduleDialog && (
                    <Dialog className='visibleEditTrainingScheduleDialog'>
                        <Dialog.Title title='編輯'></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <form ref={reportEditFormRef}>
                                    <div className='group'>
                                        <h6>實際完成時間</h6>
                                        <input type='text' placeholder='請輸入實際完成時間' onChange={handleEditActualCompletion} name='actualCompletion' />
                                        <p>{editActualCompletionMessage}</p>
                                    </div>
                                </form>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnBtnYes' onClick={() => inspectUpdateTrainingScheduleMode(targetScheduleDate, editActualCompletion)}>確定</button>
                            <button className='dialogBtnBtnNo' onClick={closeEditTrainingScheduleDialog}>取消</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }

            {
                visibleDeleteTrainingScheduleDialog && (
                    <Dialog className='visibleDeleteTrainingScheduleDialog'>
                        <Dialog.Title title='警告'></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <div className='group'>
                                    <p>您確定要刪除此筆訓練時間?</p>
                                </div>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnBtnYes' onClick={() => inspectUpdateTrainingScheduleMode(targetScheduleDate, 0)}>確定</button>
                            <button className='dialogBtnBtnNo' onClick={closeDeleteTrainingScheduleDialog}>取消</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }

            {/* 資訊彈窗 */}
            {
                visibleInfoDialog && (
                    <Dialog className='visibleInfoDialog'>
                        <Dialog.Title title={visibleInfoDialogTitle}></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <p>{visibleInfoDialogMessage}</p>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnConfirm' onClick={closeInfoDialog}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }
        </Fragment>
    );
};

export default InspectionReportSoftness;