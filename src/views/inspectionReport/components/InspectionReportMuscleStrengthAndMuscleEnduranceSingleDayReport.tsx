import React, { useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';
import CountUp from 'react-countup';

// css
import './InspectionReportMuscleStrengthAndMuscleEnduranceSingleDayReport.sass';
import colors from '@/styles/colors.module.sass';

// hook
import { useAppSelector } from '@/hook/store';

// api
import { getStudentSingleDayReport } from '@/api/inspectionReport';

// utils
import { axiosErrorCode } from '@/utils/request';
import { margin, duration, delay, zoomOut, tolerance } from '../utils/attributes';
import { powerInnerCircleStyle, powerOuterCircleStyle } from '@/utils/inspectionReport';

// images
import arrowLeftWhite from '@/assets/images/arrowLeftWhite.svg';
import arrowRightWhite from '@/assets/images/arrowRightWhite.svg';
import reportArrorLeft from '@/assets/images/inspectionReport/reportArrowLeft.svg';
import reportArrowRight from '@/assets/images/inspectionReport/reportArrowRight.svg';

// components
import ConsciousEffortTrainingStatus from './ConsciousEffortTrainingStatus';
import Dialog from '@/components/Dialog';

const InspectionReportMuscleStrengthAndMuscleEnduranceSingleDayReport: React.FC = () => {
    const singleDayReportRef = useRef<HTMLDivElement>(null);
    const scrollRef = useRef<HTMLDivElement>(null);
    const [responseStudentSingleDayReport, setResponseStudentSingleDayReport] = useState<GetStudentSingleDayReportDataType>();
    const [responseSingleDayReport, setResponseSingleDayReport] = useState<SingleDayReportType>();
    const [singleDayIndex, setSingleDayIndex] = useState<number>(0);
    const [singleDayReportWidth, setSingleDayReportWidth] = useState<number>(0);
    const [singleDayReportHeight, setSingleDayReportHeight] = useState<number>(0);
    const [singleDate, setSingleDate] = useState<string>('');
    const [maxYConsciousEffortTick, setMaxYConsciousEffortTick] = useState<number>(0);

    const [visibleInfoDialog, setVisibleInfoDialog] = useState<boolean>(false);
    const [visibleInfoDialogTitle, setVisibleInfoDialogTitle] = useState<string>('');
    const [visibleInfoDialogMessage, setVisibleInfoDialogMessage] = useState<string>('');

    const storeGetters = useAppSelector((state) => {
        return {
            studentId: state.studentInfo.studentId
        };
    });

    const handleStudentSingleDayReport = (trainingDate = '') => {
        const today = new Date();
        const year = today.getFullYear();
        const month = (today.getMonth() + 1).toString().padStart(2, '0');
        const date = today.getDate().toString().padStart(2, '0');
        const data: GetStudentSingleDayReportRequestType = {
            trainingDate: `${year}-${month}-${date}`
        };

        if (trainingDate === '') {
            data.trainingDate = `${year}-${month}-${date}`;
        }
        else {
            data.trainingDate = trainingDate;
        }

        getStudentSingleDayReport(storeGetters.studentId, data).then((res) => {
            const { data } = res.data;
            setResponseStudentSingleDayReport(data);
            setResponseSingleDayReport(data.singleDayReport[data.singleDayReport.length - 1]);
            setSingleDayIndex(data.singleDayReport.length - 1);

            if (data.singleDayReport.length > 0) {
                setSingleDate(data.singleDayReport[0].trainingDate);
            }
        }).catch((error) => {
            const errorCode = axiosErrorCode<GetStudentSingleDayReportErrorType>(error);
            
            setVisibleInfoDialog(true);
            setVisibleInfoDialogTitle('錯誤');
            
            switch (errorCode) {
                case 'BadRequest':
                    setVisibleInfoDialogMessage('格式錯誤');
                    break;
                case 'StudentDoesNotExist':
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 'NotAnyDayTraining':
                    setVisibleInfoDialogMessage('查無任和訓練資料');
                    break;
                default:
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    // 上一次訓練日期
    const prevTrainingDate = () => {
        if (responseStudentSingleDayReport?.prevDay !== undefined && responseStudentSingleDayReport?.prevDay !== '') {
            handleStudentSingleDayReport(responseStudentSingleDayReport.prevDay);
        }
    };

    // 下一次訓練日期
    const nextTrainingDate = () => {
        if (responseStudentSingleDayReport?.nextDay !== undefined && responseStudentSingleDayReport?.nextDay !== '') {
            handleStudentSingleDayReport(responseStudentSingleDayReport.nextDay);
        }
    };

    // 上一次訓練
    const prevSingleDateFrequency = () => {
        if (singleDayIndex - 1 > 0) {
            setSingleDayIndex(singleDayIndex - 1);
            setResponseSingleDayReport(responseStudentSingleDayReport?.singleDayReport[singleDayIndex - 1]);
        }
        else {
            setSingleDayIndex(0);
            setResponseSingleDayReport(responseStudentSingleDayReport?.singleDayReport[0]);
        }
    };

    // 下一次訓練
    const nextSingleDateFrequency = () => {
        if (responseStudentSingleDayReport?.singleDayReport) {
            if (singleDayIndex + 1 < responseStudentSingleDayReport.singleDayReport.length - 1) {
                setSingleDayIndex(singleDayIndex + 1);
                setResponseSingleDayReport(responseStudentSingleDayReport?.singleDayReport[singleDayIndex + 1]);
            }
            else {
                setSingleDayIndex(responseStudentSingleDayReport.singleDayReport.length - 1);
                setResponseSingleDayReport(responseStudentSingleDayReport?.singleDayReport[responseStudentSingleDayReport.singleDayReport.length - 1]);
            }
        }
    };

    // 繪製速度百分比
    const drawPowerPercentage = useCallback(() => {
        const frequency: string[] = [];
        const speedPercentage: number[] = [];

        if (responseSingleDayReport?.speedPercentage) {
            for (let i = 0; i < responseSingleDayReport.speedPercentage.length; i++) {
                frequency.push((i + 1).toString());
                speedPercentage.push(Math.round(responseSingleDayReport.speedPercentage[i]));
            }
        }

        d3.select('.totalPowerPercentageChart').select('svg').remove();

        const svg = d3.select('.totalPowerPercentageChart')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '25vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        // 繪製 x 軸
        const x = d3.scaleBand()
            .domain(frequency)
            .range([0, singleDayReportWidth]);

        svg.append('g')
            .call(d3.axisBottom(x)
                .tickPadding(10)
                .tickSize(12)
            )
            .attr('class', 'speedPercentageX')
            .attr('transform', `translate(0, ${singleDayReportHeight})`)
            .style('font-size', '14px')
            .style('color', 'white');

        d3.select('.speedPercentageX')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.speedPercentageX')
            .selectAll('.tick')
            .selectAll('line')
            .attr('stroke', 'transparent');

        d3.select('.speedPercentageX').selectAll('.tick')
            .selectAll('text')
            .style('color', 'transparent')
            .style('font-size', '12px');

        // 繪製 y 軸
        const y = d3.scaleLinear()
            .domain([0, 100])
            .range([singleDayReportHeight, 0]);

        svg.append('g')
            .call(d3.axisLeft(y)
                .ticks(2)
                .tickFormat((d) => {
                    return d + '%';
                })
                .tickPadding(10)
                .tickSizeInner(-singleDayReportWidth)
            )
            .attr('class', 'speedPercentageY')
            .attr('height', singleDayReportHeight)
            .style('font-size', '16px')
            .style('color', 'white');

        d3.select('.speedPercentageY')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.speedPercentageY')
            .selectAll('.tick')
            .selectAll('line')
            .attr('stroke', 'white');

        // 方塊平均寬度
        const boxAvgWidth = singleDayReportWidth / speedPercentage.length;
        // 計算每一方格寬度公差百分比
        const tolerancePercentage = boxAvgWidth * tolerance;
        // 計算每一方格寬度公差百分比一半
        const tolerancePercentageHalf = tolerancePercentage / 2;
        // 方格框度
        const boxWidth = boxAvgWidth - zoomOut - tolerancePercentage;

        // 繪製功率方塊
        svg.selectAll('rect')
            .data(speedPercentage)
            .enter()
            .append('rect')
            .attr('x', 1)
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) - tolerancePercentageHalf}, ${singleDayReportHeight}) rotate(180)`;
            })
            .attr('width', boxWidth)
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d !== 0) {
                    if (d > 100) {
                        return singleDayReportHeight / 100 * 100;

                    }

                    return singleDayReportHeight / 100 * d;
                }
                else {
                    return singleDayReportHeight / 100 * 25;
                }
            })
            .attr('fill', (d) => {
                const power = responseSingleDayReport?.power || 0;

                if (d === 0) {
                    return colors.color_red;
                }
                else if (power < 250) {
                    return colors.color_yellow;
                }
                else if (power > 250 && power < 500) {
                    return colors.color_orange;
                }
                else if (power > 500 && power < 750) {
                    return colors.color_red;
                }
                else {
                    return colors.color_purple;
                }

            });

        svg.append('text')
            .text('次數')
            .attr('transform', `translate(${singleDayReportWidth + 5}, ${singleDayReportHeight})`)
            .attr('fill', colors.color_white)
            .style('font-size', '18px');

        svg.append('text')
            .text('60 秒內單次訓練速度百分比')
            .attr('transform', `translate(${singleDayReportWidth / 4}, ${singleDayReportHeight + 35})`)
            .attr('fill', colors.color_white)
            .style('font-size', '18px');
    }, [singleDayReportWidth, singleDayReportHeight, responseStudentSingleDayReport, responseSingleDayReport]);

    const changeConsciousEffortIndex = (index: number) => {
        setSingleDayIndex(index);
        setResponseSingleDayReport(responseStudentSingleDayReport?.singleDayReport[index]);
    };

    const handleMaxYConsciousEffortTick = () => {
        const level: number[] = [];
        const finish: number[] = [];
        const mistake: number[] = [];

        if (responseStudentSingleDayReport?.singleDayReport) {
            responseStudentSingleDayReport.singleDayReport.forEach((item) => {
                level.push(item.level);
                finish.push(item.finish);
                mistake.push(item.mistake);
            });
        }

        const maxLevel = d3.max(level) || 0;
        const maxFinish = d3.max(finish) || 0;
        const maxMistake = d3.max(mistake) || 0;

        setMaxYConsciousEffortTick(Math.max(maxLevel, maxFinish, maxMistake));
    };

    const handleScroll = (e: any) => {
        if (e.deltaY < 0) {
            if (scrollRef.current?.scrollLeft) {
                scrollRef.current.scrollLeft -= 50;
            }
        }
        else {
            if (scrollRef.current?.scrollLeft !== undefined) {
                scrollRef.current.scrollLeft += 50;
            }
        }
    };

    const handleResize = () => {
        if (singleDayReportRef.current?.clientWidth) {
            setSingleDayReportWidth(singleDayReportRef.current.clientWidth - margin.left - margin.right);
            setSingleDayReportHeight(window.innerHeight / 4 - margin.top - margin.bottom);
        }
    };

    /**
     * 關閉資訊彈窗
     */
    const closeVisibleDialog = () => {
        setVisibleInfoDialog(false);
    };

    const initWindowSize = () => {
        handleResize();
    };

    useEffect(() => {
        initWindowSize();
        handleStudentSingleDayReport();

        window.addEventListener('resize', handleResize);
    }, []);

    useEffect(() => {
        drawPowerPercentage();
    }, [drawPowerPercentage]);

    useEffect(() => {
        handleMaxYConsciousEffortTick();

        if (scrollRef.current?.scrollLeft !== undefined) {
            scrollRef.current.scrollLeft = scrollRef.current.scrollWidth;
        }
    }, [responseStudentSingleDayReport?.singleDayReport]);

    return (
        <div className='inspectionReportMuscleStrengthAndMuscleEnduranceSingleDayReport'>
            <div className='singleDay'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                        <div className='card'>
                            <img className='arrowRightWhite' src={arrowLeftWhite} onClick={prevTrainingDate} />
                        </div>
                    </div>

                    <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                        <div className='card'>
                            <h2>{singleDate}</h2>
                        </div>
                    </div>

                    <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                        <div className='card'>
                            <img className='arrowRightWhite' src={arrowRightWhite} onClick={nextTrainingDate} />
                        </div>
                    </div>
                </div>
            </div>

            <div className='machinaryTrainingStatus'>
                <div className='flex flex-wrap'>
                    {/* 訓練機器資訊 */}
                    <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                        <div className='trainingStatus'>
                            <div className='machinaryName'>
                                <h3>{responseSingleDayReport?.machinaryCodeName}</h3>
                            </div>

                            <div className='trainingCount'>
                                {
                                    responseStudentSingleDayReport?.singleDayReport.map((item, index) => {
                                        return (
                                            <div className={`circle ${singleDayIndex === index ? 'yellow' : ''}`} key={index}></div>
                                        );
                                    })
                                }
                            </div>

                            <hr />

                            <div className='trainingInfo'>
                                <div className='group'>
                                    <h4>訓練階數</h4>
                                    <p>
                                        <CountUp start={0} end={responseSingleDayReport?.level !== undefined ? responseSingleDayReport?.level : 0} duration={2}></CountUp>
                                    </p>
                                </div>

                                <div className='group'>
                                    <h4>訓練次數</h4>
                                    <p>
                                        <CountUp start={0} end={responseSingleDayReport?.finish !== undefined ? responseSingleDayReport?.finish : 0} duration={2}></CountUp>
                                    </p>
                                </div>

                                <div className='group'>
                                    <h4>失誤次數</h4>
                                    <p>
                                        <CountUp start={0} end={responseSingleDayReport?.mistake !== undefined ? responseSingleDayReport?.mistake : 0} duration={2}></CountUp>
                                    </p>
                                </div>
                            </div>

                            <div className='totalPower'>
                                <div className='group'>
                                    <img className='arrowLeft' src={reportArrorLeft} onClick={prevSingleDateFrequency} />
                                </div>

                                <div className='group'>
                                    <div className='circle' style={{ background: powerOuterCircleStyle(responseSingleDayReport?.power) }}>
                                        <div className='totalPowerCircle' style={{ backgroundColor: powerInnerCircleStyle(responseSingleDayReport?.power) }}>
                                            <h3 className='power'>
                                                <CountUp start={0} end={responseSingleDayReport?.power !== undefined ? responseSingleDayReport?.power : 0} duration={2}></CountUp>
                                            </h3>
                                            <p className='powerText'>總功率(瓦)</p>
                                        </div>
                                    </div>
                                </div>

                                <div className='group'>
                                    <img className='arrowRight' src={reportArrowRight} onClick={nextSingleDateFrequency} />
                                </div>
                            </div>

                            <div className='totalPowerPercentageChart' ref={singleDayReportRef}>

                            </div>
                        </div>
                    </div>

                    {/* 自覺努力量表 */}
                    <div className='w-full sm:w-8/12 md:w-8/12 lg:w-8/12 xl:w-8/12 xxl:w-8/12'>
                        <div className='consciousEffort'>
                            <div className='title'>
                                <h2>自覺努力量表</h2>
                            </div>

                            <hr />

                            <div className='trainingState'>
                                <div className='scroll' ref={scrollRef} onWheel={handleScroll}>
                                    {
                                        responseStudentSingleDayReport?.singleDayReport.map((item, index) => {
                                            return (
                                                <div className='consciousEffortGrid'
                                                    key={index}
                                                    style={{ opacity: singleDayIndex === index ? 1 : 0.1 }}
                                                    onClick={() => changeConsciousEffortIndex(index)}
                                                >
                                                    <ConsciousEffortTrainingStatus
                                                        index={index + 1}
                                                        singleDayReport={item}
                                                        yMaxTick={maxYConsciousEffortTick}
                                                    ></ConsciousEffortTrainingStatus>
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            </div>

                            <div className='chartTitle'>
                                <h2>訓練狀況</h2>
                            </div>

                            <div className='label'>
                                <div className='group'>
                                    <div className='rect'></div>
                                    <span>訓練階數</span>
                                </div>

                                <div className='group'>
                                    <div className='rect'></div>
                                    <span>失誤次數</span>
                                </div>

                                <div className='group'>
                                    <div className='rect'></div>
                                    <span>訓練次數</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {
                visibleInfoDialog && (
                    <Dialog className='visibleInfoDialog'>
                        <Dialog.Title title={visibleInfoDialogTitle}></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <span>{visibleInfoDialogMessage}</span>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnConfirm' onClick={closeVisibleDialog}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }
        </div>
    );
};

export default InspectionReportMuscleStrengthAndMuscleEnduranceSingleDayReport;