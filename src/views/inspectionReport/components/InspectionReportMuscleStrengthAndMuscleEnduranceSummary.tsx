import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import * as d3 from 'd3';
import { useNavigate } from 'react-router-dom';

// css
import './InspectionReportMuscleStrengthAndMuscleEnduranceSummary.sass';
import colors from '@/styles/colors.module.sass';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

// utils
import { axiosErrorCode } from '@/utils/request';
import { duration, tolerance, zoomOut, delay, tickMinCircleR } from '../utils/attributes';
import { totalPercentageStyle, targetMachinaryPicture, targetMuscleDistribution } from '@/utils/inspectionReport';

// api
import {
    getStudentMachinaryTotalPower,
    getStudentFormalTrainingStatusReport,
    getStudentTrainingPowerReport
} from '@/api/inspectionReport';

// components
import Dialog from '@/components/Dialog';

const InspectionReportMuscleStrengthAndMuscleEnduranceSummary: React.FC = () => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const [responseStudentMachinaryTotalPower, setResponseStudentMachinaryTotalPower] = useState<GetStudentMachinaryTotalPowerDataType>();
    const [responseStudentFormalTrainingStatusReport, setResponseStudentFormalTrainingStatusReport] = useState<GetStudentFormalTrainingStatusReportDataType[]>([]);
    const [responseStudentTrainingPowerReport, setResponseStudentTrainingPowerReport] = useState<GetStudentTrainingPowerReportDataType[]>([]);

    const formalTrainingStateRef = useRef<HTMLDivElement>(null); // 繪製過去 20 正式訓練狀況容器
    const [formalTrainingStateWidth, setFormalTrainingStateWidth] = useState<number>(0); // 繪製過去 20 正式訓練狀況容器寬度
    const [formalTrainingStateHeight, setFormalTrainingStateHeight] = useState<number>(0); // 繪製過去 20 正式訓練狀況容器高度
    const formalTrainingStateMargin = { top: 80, right: 50, bottom: 100, left: 50 };

    const formalTrainingPowerRef = useRef<HTMLDivElement>(null); // 繪製過去 20 正式訓練狀況容器
    const [formalTrainingPowerWidth, setFormalTrainingPowerWidth] = useState<number>(0); // 繪製過去 20 正式訓練狀況容器寬度
    const [formalTrainingPowerHeight, setFormalTrainingPowerHeight] = useState<number>(0); // 繪製過去 20 正式訓練狀況容器高度
    const formalTrainingPowerMargin = { top: 80, right: 50, bottom: 100, left: 50 };

    const [visibleInfoDialog, setVisibleInfoDialog] = useState<boolean>(false); // 資訊彈窗
    const [visibleInfoDialogTitle, setVisibleInfoDialogTitle] = useState<string>('');
    const [visibleInfoDialogMessage, setVisibleInfoDialogMessage] = useState<string>('');

    const storeGetters = useAppSelector((state) => {
        return {
            studentId: state.studentInfo.studentId,
            machinaryCode: state.inspectionReport.machinaryCode,
            machinaryName: state.inspectionReport.machinaryName
        };
    });

    const handleStudentMachinaryTotalPower = () => {
        Promise.all([
            getStudentMachinaryTotalPower(storeGetters.studentId, storeGetters.machinaryCode),
            getStudentFormalTrainingStatusReport(storeGetters.studentId, storeGetters.machinaryCode),
            getStudentTrainingPowerReport(storeGetters.studentId, storeGetters.machinaryCode)
        ]).then((res) => {
            const { data } = res[0].data;
            setResponseStudentMachinaryTotalPower(data);

            return res;
        }).then((res) => {
            const { data } = res[1].data;
            setResponseStudentFormalTrainingStatusReport(data.reverse());

            return res;
        }).then((res) => {
            const { data } = res[2].data;
            setResponseStudentTrainingPowerReport(data.reverse());

        }).catch((error) => {
            const errorCode = axiosErrorCode<GetStudentFormalTrainingSummaryErrorCodeType>(error);

            setVisibleInfoDialog(true);
            setVisibleInfoDialogTitle('錯誤');

            switch (errorCode) {
                case 'StudentDoesNotExist':
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                default:
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    // 機器圖片
    const machinaryPicture = useMemo(() => {
        return targetMachinaryPicture(storeGetters.machinaryCode);
    }, [storeGetters.machinaryCode]);

    // 肌肉分佈圖片
    const muscleDistribution = useMemo(() => {
        return targetMuscleDistribution(storeGetters.machinaryCode);
    }, [storeGetters.machinaryCode]);

    const machinaryPictureStyle = useMemo(() => {
        if (responseStudentMachinaryTotalPower?.totalPower) {
            return totalPercentageStyle(responseStudentMachinaryTotalPower.totalPower);
        }
    }, [responseStudentMachinaryTotalPower]);

    // 繪製過去 20 次正式訓練狀況圖表
    const drawFormalTrainingStatusReport = useCallback(() => {
        const data = responseStudentFormalTrainingStatusReport;
        const trainingDateText: string[] = [];
        const finsh: number[] = [];
        const mistake: number[] = [];
        const level: number[] = [];

        // 彙整訓練日期
        data.forEach((item) => {
            trainingDateText.push(item.trainingDateText);
            finsh.push(item.finsh);
            mistake.push(item.mistake);
            level.push(item.level);
        });

        d3.select('.formal_training_status_report').select('svg').remove();

        const svg = d3.select('.formal_training_status_report')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${formalTrainingStateMargin.left}, ${formalTrainingStateMargin.top})`);

        // 繪製 x 軸
        const x = d3.scaleBand()
            .domain(trainingDateText)
            .range([0, formalTrainingStateWidth]);

        svg.append('g')
            .call(d3.axisBottom(x).tickPadding(30))
            .attr('class', 'formalTrainingStatusX')
            .attr('transform', `translate(0, ${formalTrainingStateHeight})`);

        d3.select('.formalTrainingStatusX')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.formalTrainingStatusX')
            .selectAll('.tick')
            .selectAll('line')
            .attr('stroke', 'transparent');

        d3.select('.formalTrainingStatusX')
            .selectAll('.tick')
            .append('circle')
            .attr('r', tickMinCircleR)
            .attr('fill', () => {
                return 'rgba(255, 255, 255, 0.5)';
            })
            .attr('transform', 'translate(0, 20)');

        d3.select('.formalTrainingStatusX').selectAll('.tick')
            .selectAll('text')
            .style('color', colors.color_white)
            .style('font-size', '12px');

        // 繪製 y 軸
        const maxFinsh = d3.max(finsh) || 0;
        const maxMistake = d3.max(mistake) || 0;
        const maxLevel = d3.max(level) || 0;
        const YMax = Math.max(maxFinsh, maxMistake, maxLevel);

        const y = d3.scaleLinear()
            .domain([0, YMax])
            .range([formalTrainingStateHeight, 0]);

        svg.append('g')
            .call(d3.axisLeft(y).ticks(5))
            .attr('class', 'formalTrainingStatusY')
            .attr('height', formalTrainingStateHeight);

        d3.select('.formalTrainingStatusY')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.formalTrainingStatusY')
            .selectAll('.tick')
            .selectAll('line')
            .style('stroke', 'transparent');

        d3.select('.formalTrainingStatusY')
            .selectAll('.tick')
            .selectAll('text')
            .style('font-size', '14px')
            .style('color', colors.color_white);

        // 方塊平均寬度
        const boxAvgWidth = formalTrainingStateWidth / data.length / 3;
        const tolerancePercentage = boxAvgWidth * tolerance;
        const tolerancePercentageHalf = tolerancePercentage / 2;
        const boxWidth = tolerancePercentage - zoomOut;
        const equipartition = 3;

        // 繪製訓練階數
        svg.append('g')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', 'trainingLevel')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) * equipartition - tolerancePercentageHalf * 6}, ${formalTrainingStateHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.level) {
                    return formalTrainingStateHeight / YMax * d.level;
                }

                return 0;
            })
            .attr('fill', 'rgba(255,255,255, 0.1)');

        // 繪製訓練失誤次數
        svg.append('g')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', 'mistake')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) * equipartition - tolerancePercentageHalf * 4}, ${formalTrainingStateHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.mistake) {
                    return formalTrainingStateHeight / YMax * d.mistake;
                }

                return 0;
            })
            .attr('fill', colors.color_red);

        // 繪製訓練次數
        svg.append('g').selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', 'finish')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) * equipartition - tolerancePercentageHalf * 2}, ${formalTrainingStateHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.finsh) {
                    return formalTrainingStateHeight / YMax * d.finsh;
                }

                return 0;
            })
            .attr('fill', colors.color_orange);

        // 繪製圖表標題
        svg.append('text')
            .text('過去 20 次訓練狀況')
            .attr('x', formalTrainingStateWidth / 2 - 86.5)
            .attr('y', formalTrainingStateHeight + formalTrainingPowerMargin.top)
            .style('fill', colors.color_white)
            .style('font-size', '20px');

        // 繪製訓練階數標籤
        svg.append('text')
            .text('訓練階數')
            .attr('x', formalTrainingStateWidth - 250)
            .attr('y', '-10')
            .style('fill', colors.color_white)
            .style('font-size', '12px');

        svg.append('rect')
            .attr('height', '5')
            .attr('width', '20')
            .attr('fill', 'rgba(255, 255, 255, 0.1)')
            .attr('x', formalTrainingStateWidth - 80 * 3 - 40)
            .attr('y', '-16');

        // 繪製失誤次數標籤
        svg.append('text')
            .text('失誤次數')
            .attr('x', formalTrainingStateWidth - 150)
            .attr('y', '-10')
            .style('fill', colors.color_white)
            .style('font-size', '12px');

        svg.append('rect')
            .attr('height', '5')
            .attr('width', '20')
            .attr('fill', colors.color_red)
            .attr('x', formalTrainingStateWidth - 80 * 2 - 20)
            .attr('y', '-16');

        // 繪製失誤次數標籤
        svg.append('text')
            .text('訓練次數')
            .attr('x', formalTrainingStateWidth - 50)
            .attr('y', '-10')
            .style('fill', colors.color_white)
            .style('font-size', '12px');

        svg.append('rect')
            .attr('height', '5')
            .attr('width', '20')
            .attr('fill', colors.color_orange)
            .attr('x', formalTrainingStateWidth - 80)
            .attr('y', '-16');


        const trainingLevelMouseover = (event: any, d: any) => {
            d3.select('.trainingLevelTooltip').select('p.trainingDate').text(`訓練日期: ${d.trainingDate}`);
            d3.select('.trainingLevelTooltip').select('p.trainingLevel').text(`訓練階數: ${d.level}`);
        };

        const trainingLevelMousemove = (event: any) => {
            const x = event.x;
            const y = event.y;

            if (x > formalTrainingStateWidth - 100) {
                d3.select('.trainingLevelTooltip')
                    .style('display', 'block')
                    .style('left', `${x - 690}px`)
                    .style('top', `${y - 260}px`);
            }
            else {
                d3.select('.trainingLevelTooltip')
                    .style('display', 'block')
                    .style('left', `${x - 510}px`)
                    .style('top', `${y - 260}px`);
            }
        };

        const trainingLevelMouseleave = () => {
            d3.select('.trainingLevelTooltip')
                .style('display', 'none');
        };

        const mistakeMouseover = (event: any, d: any) => {
            d3.select('.mistakeTooltip').select('p.trainingDate').text(`訓練日期: ${d.trainingDate}`);
            d3.select('.mistakeTooltip').select('p.mistake').text(`失誤次數: ${d.mistake} 次`);
        };

        const mistakeMousemove = (event: any) => {
            const x = event.x;
            const y = event.y;

            if (x > formalTrainingStateWidth - 50) {
                d3.select('.mistakeTooltip')
                    .style('display', 'block')
                    .style('left', `${x - 690}px`)
                    .style('top', `${y - 260}px`);
            }
            else {
                d3.select('.mistakeTooltip')
                    .style('display', 'block')
                    .style('left', `${x - 510}px`)
                    .style('top', `${y - 260}px`);
            }
        };

        const mistakeMouseleave = () => {
            d3.select('.mistakeTooltip')
                .style('display', 'none');
        };

        const finishMouseover = (event: any, d: any) => {
            d3.select('.finishTooltip').select('p.trainingDate').text(`訓練日期: ${d.trainingDate}`);
            d3.select('.finishTooltip').select('p.finish').text(`訓練次數: ${d.finsh} 次`);
        };

        const finishMousemove = (event: any) => {
            const x = event.x;
            const y = event.y;

            if (x > formalTrainingStateWidth - 50) {
                d3.select('.finishTooltip')
                    .style('display', 'block')
                    .style('left', `${x - 690}px`)
                    .style('top', `${y - 260}px`);
            }
            else {
                d3.select('.finishTooltip')
                    .style('display', 'block')
                    .style('left', `${x - 510}px`)
                    .style('top', `${y - 255}px`);
            }
        };

        const finishMouseleave = () => {
            d3.select('.finishTooltip')
                .style('display', 'none');
        };

        d3.select('.formal_training_status_report').select('svg').selectAll('.trainingLevel').on('mouseover', trainingLevelMouseover);
        d3.select('.formal_training_status_report').select('svg').selectAll('.trainingLevel').on('mousemove', trainingLevelMousemove);
        d3.select('.formal_training_status_report').select('svg').selectAll('.trainingLevel').on('mouseleave', trainingLevelMouseleave);
        d3.select('.formal_training_status_report').select('svg').selectAll('.mistake').on('mouseover', mistakeMouseover);
        d3.select('.formal_training_status_report').select('svg').selectAll('.mistake').on('mousemove', mistakeMousemove);
        d3.select('.formal_training_status_report').select('svg').selectAll('.mistake').on('mouseleave', mistakeMouseleave);
        d3.select('.formal_training_status_report').select('svg').selectAll('.finish').on('mouseover', finishMouseover);
        d3.select('.formal_training_status_report').select('svg').selectAll('.finish').on('mousemove', finishMousemove);
        d3.select('.formal_training_status_report').select('svg').selectAll('.finish').on('mouseleave', finishMouseleave);
    }, [formalTrainingStateWidth, formalTrainingStateHeight, responseStudentFormalTrainingStatusReport]);


    // 繪製過去 20 次正式訓練狀況功率圖表
    const drawFormalTrainingTotalPowerReport = useCallback(() => {
        const data = responseStudentTrainingPowerReport;

        const trainingDateText: string[] = [];
        const dayTotalPower: number[] = [];

        data.forEach((item) => {
            trainingDateText.push(item.trainingDateText);
            dayTotalPower.push(item.dayTotalPower);
        });

        d3.select('.formal_training_power_report').select('svg').remove();

        const svg = d3.select('.formal_training_power_report')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .append('g')
            .attr('transform', `translate(${formalTrainingPowerMargin.left}, ${formalTrainingPowerMargin.top})`);

        // 繪製 x 軸
        const xScale = d3.scaleBand()
            .domain(trainingDateText)
            .range([0, formalTrainingPowerWidth]);

        svg.append('g')
            .call(d3.axisBottom(xScale).tickPadding(35))
            .attr('transform', `translate(0, ${formalTrainingPowerHeight})`)
            .attr('class', 'formalTrainingPowerX')
            .attr('color', 'white');

        d3.select('.formalTrainingPowerX')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.formalTrainingPowerX')
            .selectAll('.tick')
            .selectAll('line')
            .attr('stroke', 'transparent');

        d3.select('.formalTrainingPowerX')
            .selectAll('.tick')
            .append('circle')
            .attr('r', tickMinCircleR)
            .attr('fill', 'rgba(255, 255, 255, 0.5)')
            .attr('transform', 'translate(0, 20)');

        d3.select('.formalTrainingPowerX').selectAll('.tick')
            .selectAll('text')
            .style('color', colors.color_white)
            .style('font-size', '12px');

        // 繪製 y 軸
        const yScale = d3.scaleLinear()
            .domain([0, d3.max(dayTotalPower) || 0])
            .range([formalTrainingPowerHeight, 0]);

        svg.append('g')
            .call(d3.axisLeft(yScale).ticks(5))
            .attr('class', 'formalTrainingPowerY')
            .attr('color', 'white');

        d3.select('.formalTrainingPowerY')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.formalTrainingPowerY')
            .selectAll('.tick')
            .selectAll('line')
            .style('stroke', 'transparent');

        d3.select('.formalTrainingPowerY')
            .selectAll('.tick')
            .selectAll('text')
            .style('font-size', '14px')
            .style('color', colors.color_white);


        // 繪製折現圖線條
        svg.append('g')
            .append('path')
            .datum(data)
            .attr('transform', 'translate(0, 0)')
            .attr('d', d3.line<GetStudentTrainingPowerReportDataType>()
                .x((d) => {
                    const s = xScale(d.trainingDateText) || undefined;

                    if (s !== undefined) {
                        return s + xScale.bandwidth() / 2;
                    }

                    return xScale.bandwidth() / 2;
                })
                .y((d) => {
                    return yScale(d.dayTotalPower);
                }).curve(d3.curveMonotoneX))
            .attr('fill', 'none')
            .attr('stroke', colors.color_yellow)
            .attr('stroke-width', 2);

        // 繪製折線圖原點
        svg.append('g').selectAll('circle')
            .data(data)
            .enter()
            .append('circle')
            .attr('r', '5')
            .attr('cx', (d) => {
                const s = xScale(d.trainingDateText) || undefined;

                if (s !== undefined) {
                    return s + xScale.bandwidth() / 2;
                }

                return xScale.bandwidth() / 2;
            })
            .attr('cy', d => yScale(d.dayTotalPower))
            .attr('fill', colors.color_yellow);

        // 遮罩
        svg.append('rect')
            .attr('width', formalTrainingStateWidth)
            .attr('height', formalTrainingStateHeight + 20)
            .attr('x', 1)
            .attr('transform', `translate(${formalTrainingStateWidth}, ${formalTrainingStateHeight}) rotate(180)`)
            .attr('fill', '#393939')
            .transition()
            .duration(2000)
            .attr('transform', `translate(${formalTrainingStateWidth * 2}, ${formalTrainingStateHeight}) rotate(180)`);

        // 繪製圖表標題
        svg.append('text')
            .text('過去 20 次訓練狀況 - 功率(千瓦特)')
            .attr('x', formalTrainingPowerWidth / 2 - 150.6)
            .attr('y', formalTrainingPowerHeight + formalTrainingPowerMargin.top)
            .style('fill', colors.color_white)
            .style('font-size', '20px');

        const mouseover = (event: any, d: any) => {
            d3.select('.totalPowerTooltip').select('p.trainingDate').text(`訓練日期: ${d.trainingDate}`);
            d3.select('.totalPowerTooltip').select('p.totalPower').text(`功率: ${d.dayTotalPower} 千瓦特`);
        };

        const mousemove = (event: any) => {
            const x = event.x;
            const y = event.y;

            if (x > formalTrainingStateWidth - 50) {
                d3.select('.totalPowerTooltip')
                    .style('display', 'block')
                    .style('left', `${x - 680}px`)
                    .style('top', `${y - 470}px`);
            }
            else {
                d3.select('.totalPowerTooltip')
                    .style('display', 'block')
                    .style('left', `${x - 510}px`)
                    .style('top', `${y - 470}px`);
            }
        };

        const mouseleave = () => {
            d3.select('.totalPowerTooltip')
                .style('display', 'none');
        };

        d3.select('.formal_training_power_report').select('svg').selectAll('circle').on('mouseover', mouseover);
        d3.select('.formal_training_power_report').select('svg').selectAll('circle').on('mousemove', mousemove);
        d3.select('.formal_training_power_report').select('svg').selectAll('circle').on('mouseleave', mouseleave);
    }, [formalTrainingPowerWidth, formalTrainingPowerHeight, responseStudentTrainingPowerReport]);

    const handleResize = () => {
        if (formalTrainingStateRef.current?.clientWidth !== undefined && formalTrainingStateRef.current?.clientHeight !== undefined && formalTrainingPowerRef.current?.clientWidth !== undefined && formalTrainingPowerRef.current?.clientHeight !== undefined) {
            setFormalTrainingStateWidth(formalTrainingStateRef.current.clientWidth - formalTrainingStateMargin.left - formalTrainingStateMargin.right);
            setFormalTrainingStateHeight(window.innerHeight / 2 - formalTrainingStateMargin.top - formalTrainingStateMargin.bottom);

            setFormalTrainingPowerWidth(formalTrainingPowerRef.current.clientWidth - formalTrainingPowerMargin.left - formalTrainingPowerMargin.right);
            setFormalTrainingPowerHeight(window.innerHeight / 2 - formalTrainingPowerMargin.top - formalTrainingPowerMargin.bottom);
        }
    };

    const initWindowSize = () => {
        handleResize();
    };

    const closeVisibleInfoDialog = () => {
        setVisibleInfoDialog(false);
    };

    useEffect(() => {
        initWindowSize();
        handleStudentMachinaryTotalPower();

        window.addEventListener('resize', handleResize);

        return () => {
            window.addEventListener('resize', handleResize);
        };
    }, []);

    useEffect(() => {
        drawFormalTrainingStatusReport();
    }, [drawFormalTrainingStatusReport]);

    useEffect(() => {
        drawFormalTrainingTotalPowerReport();
    }, [drawFormalTrainingTotalPowerReport]);

    return (
        <div className='inspectionReportMuscleStrengthAndMuscleEnduranceSummary'>
            <div className='flex flex-wrap'>
                {/* 機器部分 */}
                <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                    <div className='machinary_status'>
                        <div className='circle' style={{ background: machinaryPictureStyle }}>
                            <img className='machinary' src={machinaryPicture} />
                        </div>
                        <h2 className='machinaryName'>{storeGetters.machinaryName}</h2>

                        <img className='muscleDistribution' src={muscleDistribution} />
                    </div>
                </div>

                {/* 圖表部分 */}
                <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                    <div className='formal_training_status'>
                        <div className='flex flex-wrap'>
                            <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                <div className='card'>
                                    <div className='formal_training_status_report' ref={formalTrainingStateRef}>
                                        <div className='trainingLevelTooltip'>
                                            <p className='trainingDate'>訓練日期: 0-0-0</p>
                                            <p className='trainingLevel'>訓練階數: 0</p>
                                        </div>

                                        <div className='mistakeTooltip'>
                                            <p className='trainingDate'>訓練日期: 0-0-0</p>
                                            <p className='mistake'>失誤次數: 0 次</p>
                                        </div>

                                        <div className='finishTooltip'>
                                            <p className='trainingDate'>訓練日期: 0-0-0</p>
                                            <p className='finish'>訓練次數: 0 次</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='formal_training_power'>
                        <div className='flex flex-wrap'>
                            <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                <div className='card'>
                                    <div className='formal_training_power_report' ref={formalTrainingPowerRef}>
                                        <div className='totalPowerTooltip'>
                                            <p className='trainingDate'>訓練時間: 0-0-0</p>
                                            <p className='totalPower'>功率: 0</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {
                visibleInfoDialog && (
                    <Dialog className='visibleInfoDialog'>
                        <Dialog.Title title={visibleInfoDialogTitle}></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <p>{visibleInfoDialogMessage}</p>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnConfirm' onClick={closeVisibleInfoDialog}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }
        </div>
    );
};

export default InspectionReportMuscleStrengthAndMuscleEnduranceSummary;