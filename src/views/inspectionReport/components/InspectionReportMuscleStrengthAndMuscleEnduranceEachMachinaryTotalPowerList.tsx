import React, { Fragment, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

// css
import './InspectionReportMuscleStrengthAndMuscleEnduranceEachMachinaryTotalPowerList.sass';

// api
import { getStudentEachMachinaryTotalPowerList } from '@/api/inspectionReport';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

// api
import { axiosErrorCode } from '@/utils/request';

// utils
import { targetMachinaryPicture, totalPercentageStyle } from '@/utils/inspectionReport';

// components
import Dialog from '@/components/Dialog';

const InspectionReportMuscleStrengthAndMuscleEnduranceEachMachinaryTotalPowerList: React.FC = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [responseStudentEachMachinaryTotalPowerList, setResponseStudentEachMachinaryTotalPowerList] = useState<GetStudentEachMachinaryTotalPowerListDataType[]>([]);

    const [visibleInfoDialog, setVisibleInfoDialog] = useState<boolean>(false); // 資訊彈窗
    const [visibleInfoDialogTitle, setVisibleInfoDialogTitle] = useState<string>('');
    const [visibleInfoDialogMessage, setVisibleInfoDialogMessage] = useState<string>('');

    const storeGetters = useAppSelector((state) => {
        return {
            studentId: state.studentInfo.studentId
        };
    });

    const handleGetStudentEachMachinaryTotalPowerList = () => {
        getStudentEachMachinaryTotalPowerList(storeGetters.studentId).then((res) => {
            const { data } = res.data;

            setResponseStudentEachMachinaryTotalPowerList(data);
        }).catch((error) => {
            const errorCode = axiosErrorCode<GetStudentEachMachinaryTotalPowerListErrorCodeType>(error);

            setVisibleInfoDialog(true);
            setVisibleInfoDialogTitle('錯誤');

            switch (errorCode) {
                case 'StudentDoesNotExist':
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                default:
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    const changeMachinaryPicture = (machinaryCode: number) => {
        return targetMachinaryPicture(machinaryCode);
    };

    const changeMachinaryPictureStyle = (power: number) => {
        return totalPercentageStyle(power);
    };

    const handleShowMachinaryTotalPowerPage = (machinaryCode: number) => {
        dispatch({ type: 'inspectionReport/changeMachinaryCode', payload: machinaryCode });
        dispatch({ type: 'inspectionReport/visibleTotalPowerListPage', payload: false });
        dispatch({ type: 'inspectionReport/visibleSummaryPage', payload: true });

        dispatch({ type: 'inspectionReport/visibleBtnSummary', payload: true });
        dispatch({ type: 'inspectionReport/visibleBtnSingleDay', payload: true });
    };

    const closeVisibleInfoDialog = () => {
        setVisibleInfoDialog(false);
    };

    useEffect(() => {
        handleGetStudentEachMachinaryTotalPowerList();

        dispatch({ type: 'inspectionReport/visibleBtnSummary', payload: false });
        dispatch({ type: 'inspectionReport/visibleBtnSingleDay', payload: false });
        dispatch({ type: 'inspectionReport/visibleBtnDeleteSummarySingle', payload: false });
    }, []);

    return (
        <Fragment>
            <div className='inspectionReportMuscleStrengthAndMuscleEnduranceTotalPowerList'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                        <table>
                            <thead>
                                <tr>
                                    <th colSpan={2}>
                                        <p>項目</p>
                                    </th>

                                    <th>
                                        <p>累績功率(千瓦特)</p>
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                                {
                                    responseStudentEachMachinaryTotalPowerList.map((item, index) => {
                                        return (
                                            <tr key={index} onClick={() => handleShowMachinaryTotalPowerPage(item.machinaryCode)}>
                                                <td>
                                                    <div className='circle' style={{ background: changeMachinaryPictureStyle(item.power) }}>
                                                        <img src={changeMachinaryPicture(item.machinaryCode)} />
                                                    </div>
                                                </td>
                                                <td>
                                                    <span>{item.machinaryName}</span>
                                                </td>
                                                <td>{item.power}</td>
                                            </tr>
                                        );
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {
                visibleInfoDialog && (
                    <Dialog className='visibleInfoDialog'>
                        <Dialog.Title title={visibleInfoDialogTitle}></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <p>{visibleInfoDialogMessage}</p>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnConfirm' onClick={closeVisibleInfoDialog}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }
        </Fragment>
    );
};

export default InspectionReportMuscleStrengthAndMuscleEnduranceEachMachinaryTotalPowerList;