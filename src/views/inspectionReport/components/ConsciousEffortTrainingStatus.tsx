import React, { useCallback, useEffect, useRef, useState } from 'react';
import * as d3 from 'd3';

// css
import './ConsciousEffortTrainingStatus.sass';
import colors from '@/styles/colors.module.sass';

// utils
import { targetConsciousEffort, targetMachinaryPicture, powerOuterCircleStyle } from '@/utils/inspectionReport';
import { duration, delay, zoomOut } from '../utils/attributes';

type ConsciousEffortTrainingStatusProps = {
    index: number;
    singleDayReport: SingleDayReportType,
    yMaxTick: number;
};

const ConsciousEffortTrainingStatus: React.FC<ConsciousEffortTrainingStatusProps> = ({ index, singleDayReport, yMaxTick }) => {
    const trainingStateChartRef = useRef<HTMLDivElement>(null);
    const [width, setWidth] = useState<number>(0);
    const [height, setHeight] = useState<number>(0);
    const margin = { top: 10, right: 50, bottom: 80, left: 50 };

    // 繪製訓練狀況
    const drawTrainingState = useCallback(() => {
        const className = `.trainingStateChart${index}`;

        d3.select(className).select('svg').remove();

        const svg = d3.select(className)
            .append('svg')
            .attr('max-width', '100%')
            .attr('height', '20vh')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);


        const x = d3.scaleLinear()
            .domain([0, yMaxTick])
            .range([height, 0]);

        svg.append('g')
            .call(d3.axisLeft(x).ticks(5)
            )
            .attr('class', 'consciousEffort')
            .attr('color', 'white');


        d3.select('.consciousEffort')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.consciousEffort')
            .selectAll('.tick')
            .selectAll('line')
            .attr('stroke', 'transparent');

        d3.select('.consciousEffort').selectAll('.tick')
            .selectAll('text')
            .style('color', '#7c7c7c')
            .style('font-size', '14px');


        // 方塊平均寬度
        let boxAvgWidth = 0;
        let tolerancePercentage = 0;
        let tolerancePercentageHalf = 0;
        let boxWidth = 0;
        let equipartition = 0;

        if (width !== 0) {
            boxAvgWidth = width / 3 / 3;
            tolerancePercentage = boxAvgWidth * 1.5;
            tolerancePercentageHalf = tolerancePercentage / 2;
            boxWidth = tolerancePercentage - zoomOut;
            equipartition = 3;
        }

        // 繪製訓練階數長條圖
        svg.append('rect')
            .attr('class', 'trainingLevel')
            .attr('width', boxWidth)
            .attr('transform', `translate(${boxAvgWidth * (0.2 + 1) * equipartition - tolerancePercentageHalf * 0.1}, ${height}) rotate(180)`)
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('fill', 'rgba(255, 255, 255, 0.1)')
            .attr('height', () => {
                if (height === 0) {
                    return 0;
                }

                return height / yMaxTick * singleDayReport.level;
            });


        // 繪製失誤次數條圖
        svg.append('rect')
            .attr('width', boxWidth)
            .attr('transform', `translate(${boxAvgWidth * (1 + 1) * equipartition - tolerancePercentageHalf * 1.2}, ${height}) rotate(180)`)
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('fill', colors.color_red)
            .attr('height', () => {
                if (height === 0) {
                    return 0;
                }

                return height / yMaxTick * singleDayReport.mistake;
            });


        // 繪製訓練次數條圖
        svg.append('rect')
            .attr('width', boxWidth)
            .attr('transform', `translate(${boxAvgWidth * (2 + 1) * equipartition - tolerancePercentageHalf * 3.2}, ${height}) rotate(180)`)
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('fill', colors.color_orange)
            .attr('height', () => {
                if (height === 0) {
                    return 0;
                }

                return height / yMaxTick * singleDayReport.finish;
            });
    }, [width, height, singleDayReport, yMaxTick]);

    const handleResize = () => {
        if (trainingStateChartRef.current?.clientWidth) {
            setWidth(trainingStateChartRef.current.clientWidth - margin.left - margin.right);
            setHeight(window.innerHeight / 3.6 - margin.top - margin.bottom);
        }
    };

    const initWindowSize = () => {
        handleResize();
    };

    useEffect(() => {
        initWindowSize();
        drawTrainingState();
    }, [drawTrainingState]);

    return (
        <div className='consciousEffortTrainingStatus'>
            <div className='consciousEffortLevel'>
                <img className='consciousEffortPicture' src={targetConsciousEffort(singleDayReport.consciousEffort)} />
            </div>

            <div className={`trainingStateChart trainingStateChart${index}`} ref={trainingStateChartRef}>
                
            </div>

            <div className='machinary'>
                <div className='circle' style={{ background: powerOuterCircleStyle(singleDayReport?.power) }}>
                    <img className='machinaryPicture' src={targetMachinaryPicture(singleDayReport.machinaryCode)} />
                </div>
            </div>
        </div>
    );
};

export default ConsciousEffortTrainingStatus;