import React, { Fragment, useEffect } from 'react';

// css
import './InspectionReportMuscleStrengthAndMuscleEndurance.sass';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

// images
import muscle_strength_and_muscle_endurance from '@/assets/muscle_strength_and_muscle_endurance.svg';

// components
import InspectionReportMuscleStrengthAndMuscleEnduranceEachMachinaryTotalPowerList from './InspectionReportMuscleStrengthAndMuscleEnduranceEachMachinaryTotalPowerList';
import InspectionReportMuscleStrengthAndMuscleEnduranceSummary from './InspectionReportMuscleStrengthAndMuscleEnduranceSummary';
import InspectionReportMuscleStrengthAndMuscleEnduranceSummaryList from './InspectionReportMuscleStrengthAndMuscleEnduranceSummaryList';
import InspectionReportMuscleStrengthAndMuscleEnduranceSingleDayReport from './InspectionReportMuscleStrengthAndMuscleEnduranceSingleDayReport';

const InspectionReportMuscleStrengthAndMuscleEndurance: React.FC = () => {
    const dispatch = useAppDispatch();
    const storeGetters = useAppSelector((state) => {
        return {
            totalPowerListPage: state.inspectionReport.totalPowerListPage,
            summaryPage: state.inspectionReport.summaryPage,
            summaryPageListPage: state.inspectionReport.summaryListPage,
            singleDayPage: state.inspectionReport.singleDayPage
        };
    });

    useEffect(() => {
        dispatch({ type: 'inspectionReport/resetPage' });
        dispatch({ type: 'inspectionReport/visibleTotalPowerListPage', payload: true });
    }, []);

    return (
        <div className='inspectionReportMuscleStrengthAndMuscleEndurance'>
            {
                storeGetters.totalPowerListPage && (
                    <Fragment>
                        <div className='training_title'>
                            <div className='flex flex-wrap'>
                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='card'>
                                        <img src={muscle_strength_and_muscle_endurance} />
                                        <h2>肌力與肌耐力</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
            
                        <InspectionReportMuscleStrengthAndMuscleEnduranceEachMachinaryTotalPowerList></InspectionReportMuscleStrengthAndMuscleEnduranceEachMachinaryTotalPowerList>
                    </Fragment>
                )
            }

            {
                storeGetters.summaryPage && (
                    <InspectionReportMuscleStrengthAndMuscleEnduranceSummary></InspectionReportMuscleStrengthAndMuscleEnduranceSummary>
                )
            }

            {
                storeGetters.summaryPageListPage && (
                    <InspectionReportMuscleStrengthAndMuscleEnduranceSummaryList></InspectionReportMuscleStrengthAndMuscleEnduranceSummaryList>
                )
            }

            {
                storeGetters.singleDayPage && (
                    <InspectionReportMuscleStrengthAndMuscleEnduranceSingleDayReport></InspectionReportMuscleStrengthAndMuscleEnduranceSingleDayReport>
                )
            }
        </div>
    );
};

export default InspectionReportMuscleStrengthAndMuscleEndurance;