import React, { useEffect, useMemo, useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import validate from 'validate.js';

// css
import './InspectionReportMuscleStrengthAndMuscleEnduranceSummaryList.sass';

// api
import {
    getStudentMachinaryTotalPower,
    getStudentTrainingMachinarySummaryList,
    replaceStudentFormalTrainingMark,
    deleteStudentFormalTrainingMark
} from '@/api/inspectionReport';

// utils
import { totalPercentageStyle, targetMachinaryPicture, targetMuscleDistribution } from '@/utils/inspectionReport';
import { axiosErrorCode } from '@/utils/request';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

// images
import btn_search_not_border from '@/assets/images/btn_search_not_border.svg';
import btn_add_not_border from '@/assets/images/btn_add_not_border.svg';
import caution from '@/assets/images/caution.svg';

// components
import Dialog from '@/components/Dialog';

const InspectionReportMuscleStrengthAndMuscleEnduranceSummaryList: React.FC = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const addMarkFormRef = useRef<HTMLFormElement>(null);
    const [responseStudentMachinaryTotalPower, setResponseStudentMachinaryTotalPower] = useState<GetStudentMachinaryTotalPowerDataType>();
    const [responseStudentTrainingMachinarySummaryList, setResponseStudentTrainingMachinarySummaryList] = useState<GetStudentTrainingMachinarySummaryListDataType[]>([]);
    const [searchStudentTrainingMachinarySummaryList, setSearchStudentTrainingMachinarySummaryList] = useState<GetStudentTrainingMachinarySummaryListDataType[]>([]);

    const [summaryIndex, setSummaryIndex] = useState<number | undefined>(undefined);

    const [visibleAddMarkDialog, setVisibleAddMarkDialog] = useState<boolean>(false);
    const [visibleAddMark, setVisibleAddMark] = useState<boolean>(false);
    const [textAreaMark, setTextAreaMark] = useState<string>('');
    const [textAreaErrorMessage, setTextAreaErrorMessage] = useState<string>('');
    const addMarkForm = {
        mark: {
            length: {
                maximum: 150,
                message: '備註最大 150 字'
            }
        }
    };

    const [visibleSearchDialog, setVisibleSearchDialog] = useState<boolean>(false);
    const [searchMark, setSearchMark] = useState<string>('');

    const [visibleInfoDialog, setVisibleInfoDialog] = useState<boolean>(false); // 資訊彈窗
    const [visibleInfoDialogTitle, setVisibleInfoDialogTitle] = useState<string>('');
    const [visibleInfoDialogMessage, setVisibleInfoDialogMessage] = useState<string>('');

    const storeGetters = useAppSelector((state) => {
        return {
            studentId: state.studentInfo.studentId,
            machinaryCode: state.inspectionReport.machinaryCode,
            machinaryName: state.inspectionReport.machinaryName,
            visibleDeleteMarkDialog: state.inspectionReport.visibleDeleteMarkDialog
        };
    });

    const handleStudentMachinaryTotalPower = () => {
        Promise.all([
            getStudentMachinaryTotalPower(storeGetters.studentId, storeGetters.machinaryCode),
            getStudentTrainingMachinarySummaryList(storeGetters.studentId, storeGetters.machinaryCode)
        ]).then((res) => {
            const { data } = res[0].data;
            setResponseStudentMachinaryTotalPower(data);

            return res;
        }).then((res) => {
            const { data } = res[1].data;

            setResponseStudentTrainingMachinarySummaryList(data);
            setSearchStudentTrainingMachinarySummaryList(data);
        }).catch((error) => {
            const errorCode = axiosErrorCode<GetStudentFormalTrainingSummaryErrorCodeType>(error);

            setVisibleInfoDialog(true);
            setVisibleInfoDialogTitle('錯誤');

            switch (errorCode) {
                case 'StudentDoesNotExist':
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                default:
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    const handleGetStudentTrainingMachinarySummaryList = () => {
        getStudentTrainingMachinarySummaryList(storeGetters.studentId, storeGetters.machinaryCode).then((res) => {
            const { data } = res.data;

            setResponseStudentTrainingMachinarySummaryList(data);
            setSearchStudentTrainingMachinarySummaryList(data);
        }).catch((error) => {
            const errorCode = axiosErrorCode<GetStudentFormalTrainingSummaryErrorCodeType>(error);

            setVisibleInfoDialog(true);
            setVisibleInfoDialogTitle('錯誤');

            switch (errorCode) {
                case 'StudentDoesNotExist':
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                default:
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    const closeVisibleInfoDialog = () => {
        setVisibleInfoDialog(false);
    };

    const handleSummaryIndex = (index: number) => {
        setSummaryIndex(index);
        setVisibleAddMark(true);
        dispatch({ type: 'inspectionReport/visibleBtnDeleteSummarySingle', payload: true });
    };

    const showAddMarkDialog = () => {
        setVisibleAddMarkDialog(true);
    };

    const closeAddMarkDialog = () => {
        dispatch({ type: 'inspectionReport/visibleBtnDeleteSummarySingle', payload: false });
        setVisibleAddMarkDialog(false);
        setTextAreaMark('');
        setSummaryIndex(undefined);
        setVisibleAddMark(false);
    };

    const handleTextAreaMark = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setTextAreaMark(e.target.value);

        inspectAddMarkField();
    };

    const inspectAddMarkField = () => {
        const isValid = validate(addMarkFormRef.current, addMarkForm);

        if (isValid !== undefined) {
            if (isValid.mark) {
                setTextAreaErrorMessage(isValid.mark[0].replace('Mark', ''));
            }
            else {
                setTextAreaErrorMessage('');
            }

            return false;
        }
        else {
            setTextAreaErrorMessage('');

            return true;
        }
    };

    const handleReplaceStudentFormalTrainingMark = (e: React.ChangeEvent<HTMLFormElement>) => {
        e.preventDefault();

        const isValid = inspectAddMarkField();
        
        if (isValid) {
            const data: ReplaceStudentTrainingMachinarySummaryRequestType = {
                trainingDate: responseStudentTrainingMachinarySummaryList[summaryIndex || 0].trainingDate,
                mark: textAreaMark
            };
    
            replaceStudentFormalTrainingMark(storeGetters.studentId, storeGetters.machinaryCode, data).then(() => { 
                closeAddMarkDialog();
                setVisibleInfoDialog(true);
                setVisibleInfoDialogTitle('資訊');
                setVisibleInfoDialogMessage('新增備註成功');
                handleGetStudentTrainingMachinarySummaryList();
            }).catch((error) => {
                const errorCode = axiosErrorCode<ReplaceStudentTrainingMachinarySummaryErrorCodeType>(error);
    
                setVisibleInfoDialog(true);
                setVisibleInfoDialogTitle('錯誤');
    
                switch(errorCode) {
                    case 'BadRequest':
                        setVisibleInfoDialogMessage('格式錯誤');
                        break;
                    case 'StudentDoesNotExist':
                        setVisibleInfoDialogMessage('查無學員');
                        break;
                    case 'NotAnyFormalTrainingInfo':
                        setVisibleInfoDialogMessage('查無任何訓練資料');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisibleInfoDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    const closeDeleteMarkDialog = () => {
        dispatch({ type: 'inspectionReport/visibleDeleteMarkDialog', payload: false });
        dispatch({ type: 'inspectionReport/visibleBtnDeleteSummarySingle', payload: false });
        setSummaryIndex(undefined);
        setVisibleAddMark(false);
    };

    const handleDeleteStudentFormalTrainingMark = () => {
        const params: DeleteStudentTrainingMachinarySummaryRequestType = {
            trainingDate: responseStudentTrainingMachinarySummaryList[summaryIndex || 0].trainingDate
        };

        deleteStudentFormalTrainingMark(storeGetters.studentId, storeGetters.machinaryCode, params).then(() => {
            closeDeleteMarkDialog();
            setVisibleInfoDialog(true);
            setVisibleInfoDialogTitle('資訊');
            setVisibleInfoDialogMessage('刪除備註成功');
            handleGetStudentTrainingMachinarySummaryList();
        }).catch((error) => {
            const errorCode = axiosErrorCode<DeleteStudentTrainingMachinarySummaryErrorCodeType>(error);

            setVisibleInfoDialog(true);
            setVisibleInfoDialogTitle('錯誤');

            switch(errorCode) {
                case 'BadRequest':
                    setVisibleInfoDialogMessage('格式錯誤');
                    break;
                case 'StudentDoesNotExist':
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                default:
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    const showSearchMarkDialog = () => {
        setVisibleSearchDialog(true);
        setSearchMark('');
    };

    const confirmSearchMarkDialog = () => {
        if (searchMark !== '') {
            const result = responseStudentTrainingMachinarySummaryList.filter((item) => {
                const key = searchMark.toLocaleLowerCase().trim();
                
                return item.mark.indexOf(key) !== -1;
            });

            setSearchStudentTrainingMachinarySummaryList(result);
        }
        else {
            setSearchStudentTrainingMachinarySummaryList(responseStudentTrainingMachinarySummaryList);
        }

        setVisibleSearchDialog(false);
    };

    const handleSearchMark = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setSearchMark(e.target.value);
    };

    const machinaryPictureStyle = useMemo(() => {
        if (responseStudentMachinaryTotalPower?.totalPower) {
            return totalPercentageStyle(responseStudentMachinaryTotalPower.totalPower);
        }
    }, [responseStudentMachinaryTotalPower]);

    // 機器圖片
    const machinaryPicture = useMemo(() => {
        return targetMachinaryPicture(storeGetters.machinaryCode);
    }, [storeGetters.machinaryCode]);

    // 肌肉分佈圖片
    const muscleDistribution = useMemo(() => {
        return targetMuscleDistribution(storeGetters.machinaryCode);
    }, [storeGetters.machinaryCode]);

    useEffect(() => {
        handleStudentMachinaryTotalPower();
    }, []);

    return (
        <div className='inspectionReportMuscleStrengthAndMuscleEnduranceSummaryList'>
            <div className='flex flex-wrap'>
                <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                    <div className='machinary_status'>
                        <div className='circle' style={{ background: machinaryPictureStyle }}>
                            <img className='machinary' src={machinaryPicture} />
                        </div>
                        <h2 className='machinaryName'>{storeGetters.machinaryName}</h2>

                        <img className='muscleDistribution' src={muscleDistribution} />
                    </div>
                </div>

                <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                    <div className='summaryList'>
                        <table>
                            <thead>
                                <tr>
                                    <th>
                                        <p>訓練日期</p>
                                    </th>

                                    <th>
                                        <p>訓練階數</p>
                                    </th>

                                    <th>
                                        <p>失誤次數</p>
                                    </th>

                                    <th>
                                        <p>訓練次數</p>
                                    </th>

                                    <th>
                                        <p>功率(千瓦特)</p>
                                    </th>

                                    <th>
                                        <p>
                                            <span>備註</span>
                                            
                                            <span className='action'>
                                                <img src={btn_search_not_border} onClick={showSearchMarkDialog} />
                                                {visibleAddMark && <img className='addMark' src={btn_add_not_border} onClick={showAddMarkDialog} />}
                                            </span>
                                        </p>
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                                {
                                    searchStudentTrainingMachinarySummaryList.map((item, index) => {
                                        return (
                                            <tr className={`summary ${summaryIndex === index ? 'active': ''}`} key={index} onClick={() => handleSummaryIndex(index)}>
                                                <td>{item.trainingDate}</td>
                                                <td>{item.level}</td>
                                                <td>{item.mistake}</td>
                                                <td>{item.finsh}</td>
                                                <td>{item.power}</td>
                                                <td className='mark' colSpan={3}>{item.mark}</td>
                                            </tr>
                                        );
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {
                visibleAddMarkDialog && (
                    <Dialog className='visibleAddMarkDialog'>
                        <Dialog.Title title='新增備註'></Dialog.Title>

                        <form onSubmit={handleReplaceStudentFormalTrainingMark} ref={addMarkFormRef}>
                            <Dialog.Body>
                                <div className='content'>
                                    <div className='group'>
                                        <h6>備註</h6>
                                        <textarea rows={5} defaultValue={textAreaMark} placeholder='請輸入備註' name='mark' onChange={handleTextAreaMark}></textarea>
                                        <p className='textAreaErrorMessage'>{textAreaErrorMessage}</p>
                                    </div>
                                </div>
                            </Dialog.Body>

                            <Dialog.Footer>
                                <button type='submit' className='dialogBtnBtnYes'>確定</button>
                                <button type='button' className='dialogBtnBtnNo' onClick={closeAddMarkDialog}>取消</button>
                            </Dialog.Footer>
                        </form>
                    </Dialog>
                )
            }

            {
                storeGetters.visibleDeleteMarkDialog && (
                    <Dialog className='visibleDeleteMarkDialog'>
                        <Dialog.Title title='刪除'></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <div className='group'>
                                    <img className='caution' src={caution} />
                                    <p>您確定要刪除該筆備註資料嗎?</p>
                                </div>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button type='button' className='dialogBtnBtnYes' onClick={handleDeleteStudentFormalTrainingMark}>是</button>
                            <button type='button' className='dialogBtnBtnNo' onClick={closeDeleteMarkDialog}>否</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }

            {
                visibleSearchDialog && (
                    <Dialog className='visibleSearchDialog'>
                        <Dialog.Title title='搜尋備註欄'></Dialog.Title>
                        
                        <Dialog.Body>
                            <div className='content'>
                                <div className='group'>
                                    <h6>請輸入關鍵字</h6>
                                    <textarea rows={5} placeholder='請輸入備註' onChange={handleSearchMark}></textarea>
                                </div>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button type='button' className='dialogBtnConfirm' onClick={confirmSearchMarkDialog}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }

            {
                visibleInfoDialog && (
                    <Dialog className='visibleInfoDialog'>
                        <Dialog.Title title={visibleInfoDialogTitle}></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <p>{visibleInfoDialogMessage}</p>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button type='button' className='dialogBtnConfirm' onClick={closeVisibleInfoDialog}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }
        </div>
    );
};

export default InspectionReportMuscleStrengthAndMuscleEnduranceSummaryList;