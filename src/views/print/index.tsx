import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useNavigate } from 'react-router';
import * as d3 from 'd3';
import CountUp from 'react-countup';
import gsap from 'gsap';
import domtoimage from 'dom-to-image';

// css
import './index.sass';
import colors from '@/styles/colors.module.sass';

// api
import { bodyMassAndPhysicalFitnessList } from '@/api/bodyCompositionAndPhysicalFitness';
import { exercisePrescriptionTrainingStage, exercisePrescriptionList } from '@/api/exercisePrescription';
import { instrumentSettingsSportsPerformanceList } from '@/api/instrumentSettings';
import {
    getInspectionReportPhysicalFitness,
    getStudentEachMachinaryTotalPowerList,
    getStudentMachinaryTotalPower,
    getStudentFormalTrainingStatusReport,
    getStudentTrainingPowerReport
} from '@/api/inspectionReport';
import { printTrainingReport } from '@/api/print';

// utils
import {
    bodyCompositionBmiCommentConvertFraction,
    bodyCompositionBodyFatCommentConvertFraction,
    bodyCompositionVisceralFatCommentConvertFraction,
    bodyCompositioSkeletalMuscleRateCommentConvertFraction
} from '@/utils/bodyComposition';
import {
    bmiColor,
    bodyFatColor,
    skeletalMuscleRateColor,
    visceralFatColor
} from '@/utils/bodyCompositionColor';
import {
    physicalFitnessCardiorespiratoryFitnessColor,
    physicalFitnessMuscleStrengthAndMuscleEnduranceColor,
    physicalFitnessSoftnessColor,
    physicalFitnessBalanceColor
} from '@/utils/physicalFitnessColor';
import { axiosErrorCode } from '@/utils/request';
import { dangerColor } from '@/utils/dangerColor';
import {
    duration,
    delay,
    zoomOut,
    margin,
    tickMinCircleR,
    tickMaxCircleR,
    tolerance
} from '@/views/inspectionReport/utils/attributes';
import { targetMachinaryPicture, totalPercentageStyle, targetMuscleDistribution, getMachinaryName } from '@/utils/inspectionReport';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

// images
import user_logo from '@/assets/images/user_photo.svg';
import btn_delete from '@/assets/images/btn_delete.svg';
import btn_add from '@/assets/images/inspectionReport/reportButtonAddBorder.svg';
import training_stage_anchor from '@/assets/images/training_stage_anchor.svg';
import machinaryCodeOne from '@/assets/images/machinaryCode/machinaryCodeOne.png';
import machinaryCodeTwo from '@/assets/images/machinaryCode/machinaryCodeTwo.png';
import machinaryCodeThree from '@/assets/images/machinaryCode/machinaryCodeThree.png';
import machinaryCodeFour from '@/assets/images/machinaryCode/machinaryCodeFour.png';
import machinaryCodeFive from '@/assets/images/machinaryCode/machinaryCodeFive.png';
import machinaryCodeSix from '@/assets/images/machinaryCode/machinaryCodeSix.png';
import machinaryCodeSeven from '@/assets/images/machinaryCode/machinaryCodeSeven.png';
import machinaryCodeEight from '@/assets/images/machinaryCode/machinaryCodeEight.png';
import machinaryCodeNine from '@/assets/images/machinaryCode/machinaryCodeNine.png';
import machinaryCodeTen from '@/assets/images/machinaryCode/machinaryCodeTen.png';
import cardiorespiratoryFitnessIcon from '@/assets/cardiorespiratory_fitness.svg';
import muscle_strength_and_muscle_endurance from '@/assets/muscle_strength_and_muscle_endurance.svg';
import softnessIcon from '@/assets/softness.svg';
import balanceIcon from '@/assets/balance.svg';

// components
import PageTitleBar from '@/components/PageTitleBar';
import Dialog from '@/components/Dialog';

const Print: React.FC = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [bmiGroup, setBmiGroup] = useState<BmiGroupResponseType>();
    const [bodyFatGroup, setBodyFatGroup] = useState<BodyFatGroupResponseType>();
    const [visceralFatGroup, setVisceralFatGroup] = useState<VisceralFatGroupResponseType>();
    const [skeletalMuscleRateGroup, setSkeletalMuscleRateGroup] = useState<SkeletalMuscleRateGroupResponseType>();
    const [physicalFitnessCardiorespiratoryFitnessGroup, setPhysicalFitnessCardiorespiratoryFitnessGroup] = useState<PhysicalFitnessCardiorespiratoryFitnessGroupResponseType>();
    const [physicalFitnessMuscleStrengthAndMuscleEnduranceGroup, setPhysicalFitnessMuscleStrengthAndMuscleEnduranceGroup] = useState<PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupResponseType>();
    const [physicalFitnessSoftnessGroup, setPhysicalFitnessSoftnessGroup] = useState<PhysicalFitnessSoftnessGroupResponseType>();
    const [physicalFitnessBalanceGroup, setPhysicalFitnessBalanceGroup] = useState<PhysicalFitnessBalanceGroupResponseType>();

    const [trainingStageResponse, setTrainingStageResponse] = useState<ExercisePrescriptionTrainingStageDataType>(); // 運動處處方-訓練階段資料
    const [trainingFirstProgress, setTrainingFirstProgress] = useState<number>(0); // 運動處方-訓練階段(開始階段)
    const [trainingMiddleProgress, setTrainingMiddleProgress] = useState<number>(0); // 運動處方-訓練階段(改善階段)
    const [trainingLastProgress, setTrainingLastProgress] = useState<number>(0); // 運動處方-訓練階段(維持階段)
    const [exercisePrescriptionCardiorespiratoryFitnessGroup, setExercisePrescriptionCardiorespiratoryFitnessGroup] = useState<ExercisePrescriptionCardiorespiratoryFitnessGroupType>(); // 運動處方-心肺適能群組清單
    const [exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup, setExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup] = useState<ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupType>(); // 運動處方-肌力與肌耐力群組清單
    const [exercisePrescriptionSoftnessGroup, setExercisePrescriptionSoftnessGroup] = useState<ExercisePrescriptionSoftnessGroupType>(); // 運動處方-柔軟度群組清單
    const [exercisePrescriptionBalanceGroup, setExercisePrescriptionBalanceGroup] = useState<ExercisePrescriptionBalanceGroupType>(); // 運動處方-平衡群組清單

    const [machinaryCodeOneGroup, setMachinaryCodeOneGroup] = useState<InstrumentSettingsMachinaryCodeOneGroup>();
    const [machinaryCodeTwoGroup, setMachinaryCodeTwoGroup] = useState<InstrumentSettingsMachinaryCodeTwoGroup>();
    const [machinaryCodeThreeGroup, setMachinaryCodeThreeGroup] = useState<InstrumentSettingsMachinaryCodeThreeGroup>();
    const [machinaryCodeFourGroup, setMachinaryCodeFourGroup] = useState<InstrumentSettingsMachinaryCodeFourGroup>();
    const [machinaryCodeFiveGroup, setMachinaryCodeFiveGroup] = useState<InstrumentSettingsMachinaryCodeFiveGroup>();
    const [machinaryCodeSixGroup, setMachinaryCodeSixGroup] = useState<InstrumentSettingsMachinaryCodeSixGroup>();
    const [machinaryCodeSevenGroup, setMachinaryCodeSevenGroup] = useState<InstrumentSettingsMachinaryCodeSevenGroup>();
    const [machinaryCodeEightGroup, setMachinaryCodeEightGroup] = useState<InstrumentSettingsMachinaryCodeEightGroup>();
    const [machinaryCodeNineGroup, setMachinaryCodeNineGroup] = useState<InstrumentSettingsMachinaryCodeNineGroup>();
    const [machinaryCodeTenGroup, setMachinaryCodeTenGroup] = useState<InstrumentSettingsMachinaryCodeTenGroup>();

    const chartRef = useRef<HTMLDivElement>(null);
    const [chartWidth, setChartWidth] = useState<number>(0);
    const [chartHeight, setChartHeight] = useState<number>(0);
    const [reportCardiorespiratoryFitness, setReportCardiorespiratoryFitness] = useState<GetInspectionReportPhysicalFitnessDataType[]>([]); // 心肺適能報表
    const [responseStudentEachMachinaryTotalPowerList, setResponseStudentEachMachinaryTotalPowerList] = useState<GetStudentEachMachinaryTotalPowerListDataType[]>([]);
    const [reportSoftness, setReportSoftness] = useState<GetInspectionReportPhysicalFitnessDataType[]>([]); // 柔軟度報表
    const [reportBalance, setReportBalance] = useState<GetInspectionReportPhysicalFitnessDataType[]>([]); // 平衡報表

    const [isPrintBodyMass, setIsPrintBodyMass] = useState<boolean>(true); // 儲存是否列印身體組成與體適能評價
    const [isPrintExercisePrescription, setIsPrintExercisePrescription] = useState<boolean>(true); // 儲存是否列印運動處方
    const [isPrintInstrumentSettings, setIsPrintInstrumentSettings] = useState<boolean>(true); // 儲存是否列印儀器設定
    const [isPrintInspectionReportCardiorespiratoryFitness, setIsPrintInspectionReportCardiorespiratoryFitness] = useState<boolean>(true); // 儲存是否列印心肺適能報表
    const [isPrintInspectionReportSoftness, setIsPrintInspectionReportSoftness] = useState<boolean>(true); // 儲存是否列印柔軟度報表
    const [isPrintInspectionReportBalance, setIsPrintInspectionReportBalance] = useState<boolean>(true); // 儲存是否列印平衡報表

    const formalTrainingStateRef = useRef<HTMLDivElement>(null);
    const [formalTrainingStateWidth, setFormalTrainingStateWidth] = useState<number>(0);
    const [formalTrainingStateHeight, setFormalTrainingStateHeight] = useState<number>(0);
    const formalTrainingStateMargin = { top: 80, right: 50, bottom: 100, left: 50 };

    const formalTrainingPowerRef = useRef<HTMLDivElement>(null); // 繪製過去 20 正式訓練狀況容器
    const [formalTrainingPowerWidth, setFormalTrainingPowerWidth] = useState<number>(0); // 繪製過去 20 正式訓練狀況容器寬度
    const [formalTrainingPowerHeight, setFormalTrainingPowerHeight] = useState<number>(0); // 繪製過去 20 正式訓練狀況容器高度
    const formalTrainingPowerMargin = { top: 80, right: 50, bottom: 100, left: 50 };

    const [showMachinarySummaryListOne, setShowMachinarySummaryListOne] = useState<boolean>(false);
    const [responseStudentMachinaryTotalPowerOne, setResponseStudentMachinaryTotalPowerOne] = useState<GetStudentMachinaryTotalPowerDataType>();
    const [responseStudentFormalTrainingStatusReportOne, setResponseStudentFormalTrainingStatusReportOne] = useState<GetStudentFormalTrainingStatusReportDataType[]>([]);
    const [responseStudentTrainingPowerReportOne, setResponseStudentTrainingPowerReportOne] = useState<GetStudentTrainingPowerReportDataType[]>([]);

    const [showMachinarySummaryListTwo, setShowMachinarySummaryListTwo] = useState<boolean>(false);
    const [responseStudentMachinaryTotalPowerTwo, setResponseStudentMachinaryTotalPowerTwo] = useState<GetStudentMachinaryTotalPowerDataType>();
    const [responseStudentFormalTrainingStatusReportTwo, setResponseStudentFormalTrainingStatusReportTwo] = useState<GetStudentFormalTrainingStatusReportDataType[]>([]);
    const [responseStudentTrainingPowerReportTwo, setResponseStudentTrainingPowerReportTwo] = useState<GetStudentTrainingPowerReportDataType[]>([]);

    const [showMachinarySummaryListThree, setShowMachinarySummaryListThree] = useState<boolean>(false);
    const [responseStudentMachinaryTotalPowerThree, setResponseStudentMachinaryTotalPowerThree] = useState<GetStudentMachinaryTotalPowerDataType>();
    const [responseStudentFormalTrainingStatusReportThree, setResponseStudentFormalTrainingStatusReportThree] = useState<GetStudentFormalTrainingStatusReportDataType[]>([]);
    const [responseStudentTrainingPowerReportThree, setResponseStudentTrainingPowerReportThree] = useState<GetStudentTrainingPowerReportDataType[]>([]);

    const [showMachinarySummaryListFour, setShowMachinarySummaryListFour] = useState<boolean>(false);
    const [responseStudentMachinaryTotalPowerFour, setResponseStudentMachinaryTotalPowerFour] = useState<GetStudentMachinaryTotalPowerDataType>();
    const [responseStudentFormalTrainingStatusReportFour, setResponseStudentFormalTrainingStatusReportFour] = useState<GetStudentFormalTrainingStatusReportDataType[]>([]);
    const [responseStudentTrainingPowerReportFour, setResponseStudentTrainingPowerReportFour] = useState<GetStudentTrainingPowerReportDataType[]>([]);

    const [showMachinarySummaryListFive, setShowMachinarySummaryListFive] = useState<boolean>(false);
    const [responseStudentMachinaryTotalPowerFive, setResponseStudentMachinaryTotalPowerFive] = useState<GetStudentMachinaryTotalPowerDataType>();
    const [responseStudentFormalTrainingStatusReportFive, setResponseStudentFormalTrainingStatusReportFive] = useState<GetStudentFormalTrainingStatusReportDataType[]>([]);
    const [responseStudentTrainingPowerReportFive, setResponseStudentTrainingPowerReportFive] = useState<GetStudentTrainingPowerReportDataType[]>([]);

    const [showMachinarySummaryListSix, setShowMachinarySummaryListSix] = useState<boolean>(false);
    const [responseStudentMachinaryTotalPowerSix, setResponseStudentMachinaryTotalPowerSix] = useState<GetStudentMachinaryTotalPowerDataType>();
    const [responseStudentFormalTrainingStatusReportSix, setResponseStudentFormalTrainingStatusReportSix] = useState<GetStudentFormalTrainingStatusReportDataType[]>([]);
    const [responseStudentTrainingPowerReportSix, setResponseStudentTrainingPowerReportSix] = useState<GetStudentTrainingPowerReportDataType[]>([]);

    const [showMachinarySummaryListSeven, setShowMachinarySummaryListSeven] = useState<boolean>(false);
    const [responseStudentMachinaryTotalPowerSeven, setResponseStudentMachinaryTotalPowerSeven] = useState<GetStudentMachinaryTotalPowerDataType>();
    const [responseStudentFormalTrainingStatusReportSeven, setResponseStudentFormalTrainingStatusReportSeven] = useState<GetStudentFormalTrainingStatusReportDataType[]>([]);
    const [responseStudentTrainingPowerReportSeven, setResponseStudentTrainingPowerReportSeven] = useState<GetStudentTrainingPowerReportDataType[]>([]);

    const [showMachinarySummaryListEight, setShowMachinarySummaryListEight] = useState<boolean>(false);
    const [responseStudentMachinaryTotalPowerEight, setResponseStudentMachinaryTotalPowerEight] = useState<GetStudentMachinaryTotalPowerDataType>();
    const [responseStudentFormalTrainingStatusReportEight, setResponseStudentFormalTrainingStatusReportEight] = useState<GetStudentFormalTrainingStatusReportDataType[]>([]);
    const [responseStudentTrainingPowerReportEight, setResponseStudentTrainingPowerReportEight] = useState<GetStudentTrainingPowerReportDataType[]>([]);

    const [showMachinarySummaryListNine, setShowMachinarySummaryListNine] = useState<boolean>(false);
    const [responseStudentMachinaryTotalPowerNine, setResponseStudentMachinaryTotalPowerNine] = useState<GetStudentMachinaryTotalPowerDataType>();
    const [responseStudentFormalTrainingStatusReportNine, setResponseStudentFormalTrainingStatusReportNine] = useState<GetStudentFormalTrainingStatusReportDataType[]>([]);
    const [responseStudentTrainingPowerReportNine, setResponseStudentTrainingPowerReportNine] = useState<GetStudentTrainingPowerReportDataType[]>([]);

    const [showMachinarySummaryListTen, setShowMachinarySummaryListTen] = useState<boolean>(false);
    const [responseStudentMachinaryTotalPowerTen, setResponseStudentMachinaryTotalPowerTen] = useState<GetStudentMachinaryTotalPowerDataType>();
    const [responseStudentFormalTrainingStatusReportTen, setResponseStudentFormalTrainingStatusReportTen] = useState<GetStudentFormalTrainingStatusReportDataType[]>([]);
    const [responseStudentTrainingPowerReportTen, setResponseStudentTrainingPowerReportTen] = useState<GetStudentTrainingPowerReportDataType[]>([]);

    const [visibleInfoDialog, setVisibleInfoDialog] = useState<boolean>(false); // 資訊彈窗
    const [visibleInfoDialogTitle, setVisibleInfoDialogTitle] = useState<string>('');
    const [visibleInfoDialogMessage, setVisibleInfoDialogMessage] = useState<string>('');

    const storeGetters = useAppSelector((state) => {
        return {
            student_logo: state.studentInfo.student_logo,
            studentId: state.studentInfo.studentId,
            name: state.studentInfo.name,
            gender: state.studentInfo.gender,
            age: state.studentInfo.age,
            posture: state.studentInfo.posture,
            phone: state.studentInfo.phone,
            address: state.studentInfo.address,
            medicalHistory: state.studentInfo.medicalHistory,
            dangerGrading: state.studentInfo.dangerGrading,
            machinaryName: state.inspectionReport.machinaryName
        };
    });

    // 學員身體組成與體適能清單
    const handlePrint = () => {
        Promise.all([
            bodyMassAndPhysicalFitnessList(storeGetters.studentId),
            exercisePrescriptionTrainingStage(storeGetters.studentId),
            exercisePrescriptionList(storeGetters.studentId),
            instrumentSettingsSportsPerformanceList(storeGetters.studentId),
            getInspectionReportPhysicalFitness(storeGetters.studentId, 1),
            getStudentEachMachinaryTotalPowerList(storeGetters.studentId),
            getInspectionReportPhysicalFitness(storeGetters.studentId, 3),
            getInspectionReportPhysicalFitness(storeGetters.studentId, 4)
        ]).then((res) => {
            const { data } = res[0].data;

            setBmiGroup(data.bmiGroup);
            setBodyFatGroup(data.bodyFatGroup);
            setVisceralFatGroup(data.visceralFatGroup);
            setSkeletalMuscleRateGroup(data.skeletalMuscleRateGroup);
            setPhysicalFitnessCardiorespiratoryFitnessGroup(data.physicalFitnessCardiorespiratoryFitnessGroup);
            setPhysicalFitnessMuscleStrengthAndMuscleEnduranceGroup(data.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup);
            setPhysicalFitnessSoftnessGroup(data.physicalFitnessSoftnessGroup);
            setPhysicalFitnessBalanceGroup(data.physicalFitnessBalanceGroup);

            // bmi 繪製百分等級
            const bmiValue = bodyCompositionBmiCommentConvertFraction(data.bmiGroup.bmiCommentLevel);
            drawProgressBar('.bmiBar', bmiValue);

            // 體脂肪 繪製百分等級
            const bodyFatValue = bodyCompositionBodyFatCommentConvertFraction(data.bodyFatGroup.bodyFatCommentLevel);
            drawProgressBar('.bodyFatBar', bodyFatValue);

            // 內臟脂肪 繪製百分等級
            const visceralFatValue = bodyCompositionVisceralFatCommentConvertFraction(data.visceralFatGroup.visceralFatCommentLevel);
            drawProgressBar('.visceralFatBar', visceralFatValue);

            // 骨骼肌率 繪製百分等級
            const skeletalMuscleRateValue = bodyCompositioSkeletalMuscleRateCommentConvertFraction(data.skeletalMuscleRateGroup.skeletalMuscleRateCommentLevel);
            drawProgressBar('.skeletalMuscleRateBar', skeletalMuscleRateValue);

            // 心肺適能 繪製百分等級
            const physicalFitnessCardiorespiratoryFitnessValue = data.physicalFitnessCardiorespiratoryFitnessGroup.cardiorespiratoryFitnessFraction;
            drawProgressBar('.physicalFitnessCardiorespiratoryFitnessBar', physicalFitnessCardiorespiratoryFitnessValue);

            // 肌力與肌耐力 繪製百分等級
            const physicalFitnessMuscleStrengthAndMuscleEndurance = data.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.muscleStrengthAndMuscleEnduranceFitness;
            drawProgressBar('.physicalFitnessMuscleStrengthAndMuscleEnduranceBar', physicalFitnessMuscleStrengthAndMuscleEndurance);

            // 柔軟度 繪製百分等級
            const physicalFitnessSoftnessValue = data.physicalFitnessSoftnessGroup.softnessFitness;
            drawProgressBar('.physicalFitnessSoftnessBar', physicalFitnessSoftnessValue);

            // 平衡 繪製百分等級
            const physicalFitnessBalanceValue = data.physicalFitnessBalanceGroup.balanceFitness;
            drawProgressBar('.physicalFitnessBalanceBar', physicalFitnessBalanceValue);

            return res;
        }).then((res) => {
            const { data } = res[1].data;

            if (data !== null) {
                setTrainingStageResponse(data);
                drawTrainingStage(data);
            }

            return res;
        }).then((res) => {
            const { data } = res[2].data;

            setExercisePrescriptionCardiorespiratoryFitnessGroup(data.exercisePrescriptionCardiorespiratoryFitnessGroup);
            setExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup(data.exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup);
            setExercisePrescriptionSoftnessGroup(data.exercisePrescriptionSoftnessGroup);
            setExercisePrescriptionBalanceGroup(data.exercisePrescriptionBalanceGroup);

            return res;
        }).then((res) => {
            const { data } = res[3].data;

            setMachinaryCodeOneGroup(data.machinaryCodeOneGroup);
            setMachinaryCodeTwoGroup(data.machinaryCodeTwoGroup);
            setMachinaryCodeThreeGroup(data.machinaryCodeThreeGroup);
            setMachinaryCodeFourGroup(data.machinaryCodeFourGroup);
            setMachinaryCodeFiveGroup(data.machinaryCodeFiveGroup);
            setMachinaryCodeSixGroup(data.machinaryCodeSixGroup);
            setMachinaryCodeSevenGroup(data.machinaryCodeSevenGroup);
            setMachinaryCodeEightGroup(data.machinaryCodeEightGroup);
            setMachinaryCodeNineGroup(data.machinaryCodeNineGroup);
            setMachinaryCodeTenGroup(data.machinaryCodeTenGroup);

            return res;
        }).then((res) => {
            const { data } = res[4].data;

            setReportCardiorespiratoryFitness(data);

            return res;
        }).then((res) => {
            const { data } = res[5].data;

            setResponseStudentEachMachinaryTotalPowerList(data);

            return res;
        }).then((res) => {
            const { data } = res[6].data;

            setReportSoftness(data);

            return res;
        }).then((res) => {
            const { data } = res[7].data;

            setReportBalance(data);
        }).catch((error) => {
            const errorCode = axiosErrorCode<BodyMassAndPhysicalFitnessListErrorCodeType>(error);

            setVisibleInfoDialog(true);
            setVisibleInfoDialogTitle('錯誤');

            switch (errorCode) {
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                case 'StudentDoesNotExist':
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                default:
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    // 繪製身體組成與體適能評價進度條
    const drawProgressBar = (className: string, range: number) => {
        const height = 50;

        d3.select(className).select('svg').remove();

        const svg = d3.select(className)
            .append('svg')
            .attr('width', '100%')
            .attr('height', height)
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(0, ${height / 2 - 5})`);

        svg.append('rect')
            .attr('width', '100%')
            .attr('height', '10px')
            .attr('rx', '5px')
            .attr('ry', '5px')
            .style('fill', colors.color_white);

        svg.append('rect')
            .attr('width', '0')
            .attr('height', '10px')
            .attr('rx', '5px')
            .attr('ry', '5px')
            .style('fill', colors.color_yellow)
            .transition()
            .duration(2000)
            .attr('width', `${range}%`)
            .attr('height', '10px');
    };

    // 繪製目前訓練階段
    const drawTrainingStage = (data: ExercisePrescriptionTrainingStageDataType) => {
        const firstStage = data.firstStage;
        const middleStage = data.middleStage;
        const total = firstStage + middleStage + firstStage;
        const weekPercentage = data.week / total * 100;

        setTrainingFirstProgress(Math.round(firstStage / total * 100));
        setTrainingMiddleProgress(middleStage / total * 100);
        setTrainingLastProgress(firstStage / total * 100);

        if (data.week === 1) {
            gsap.to('.training_stage_anchor', {
                left: '-1%',
                ease: 'elastic.out(1, 0.3)',
                duration: 5
            });

            gsap.to('.training_stage_anchor_text', {
                left: '-4.2%',
                ease: 'elastic.out(1, 0.3)',
                duration: 5
            });
        }
        else if (data.week >= total) {
            gsap.to('.training_stage_anchor', {
                left: 'calc(100% - 18px)',
                ease: 'elastic.out(1, 0.3)',
                duration: 5
            });

            gsap.to('.training_stage_anchor_text', {
                left: 'calc(100% - 60px)',
                ease: 'elastic.out(1, 0.3)',
                duration: 5
            });
        }
        else {
            gsap.to('.training_stage_anchor', {
                left: `calc(${weekPercentage}% - 1.2%)`,
                ease: 'elastic.out(1, 0.3)',
                duration: 5
            });

            gsap.to('.training_stage_anchor_text', {
                left: `calc(${weekPercentage}% - 4.3%)`,
                ease: 'elastic.out(1, 0.3)',
                duration: 5
            });
        }
    };

    // 繪製心肺適能直方圖報表
    const drawReportCardiorespiratoryFitness = useCallback(() => {
        const data = reportCardiorespiratoryFitness;
        const actualCompletionList: any[] = [];
        const scheduleDateList: any = [];
        const trainingUnitMinuteList: any[] = [];

        // 彙整實際次數資料
        data.forEach((item) => {
            actualCompletionList.push(item.actualCompletion);
            scheduleDateList.push(item.scheduleDate);
            trainingUnitMinuteList.push(item.trainingUnitMinute);
        });

        d3.select('.cardiorespiratoryFitnessChart').select('svg').remove();

        const svg = d3.select('.cardiorespiratoryFitnessChart')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        // 繪製 x 軸
        const x = d3.scaleBand()
            .domain(scheduleDateList)
            .range([0, chartWidth]);

        svg.append('g')
            .call(d3.axisBottom(x).tickFormat((d, i) => {
                const scheduleDate = data?.[i]?.scheduleDate || undefined;
                if (scheduleDate !== undefined) {
                    const scheduleDateSplit = scheduleDate.split('-');
                    const month = scheduleDateSplit[1];
                    const date = scheduleDateSplit[2];
                    return `${month}/${date}`;
                }
                else if (typeof (d) === 'string') {
                    return d;
                }

                return '';
            }).tickPadding(35))
            .attr('class', 'cardiorespiratoryFitnessX')
            .attr('transform', `translate(0, ${chartHeight})`)
            .attr('width', '100%')
            .style('font-size', '12px')
            .style('color', colors.color_white);

        d3.select('.cardiorespiratoryFitnessX')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.cardiorespiratoryFitnessX')
            .selectAll('.tick')
            .append('circle')
            .attr('r', (date) => {
                if (date === reportCardiorespiratoryFitness[0].scheduleDate || date === reportCardiorespiratoryFitness[reportCardiorespiratoryFitness.length - 1].scheduleDate) {
                    return tickMaxCircleR;
                }

                return tickMinCircleR;
            })
            .attr('fill', 'rgba(255, 255, 255, 0.5)')
            .attr('transform', 'translate(0, 20)');

        d3.select('.cardiorespiratoryFitnessX')
            .selectAll('.tick')
            .selectAll('line')
            .attr('stroke', 'transparent');

        d3.select('.cardiorespiratoryFitnessX').selectAll('.tick')
            .selectAll('text')
            .style('color', (date) => {
                if (date === reportCardiorespiratoryFitness[0].scheduleDate || date === reportCardiorespiratoryFitness[reportCardiorespiratoryFitness.length - 1].scheduleDate) {
                    return 'rgba(255,255,255,0.5)';
                }

                return 'transparent';
            })
            .style('font-size', '12px');


        // 繪製 y 軸
        const actualCompletionMax = d3.max(actualCompletionList);
        const trainingUnitMinuteMax = d3.max(trainingUnitMinuteList);
        const YMax = Math.max(actualCompletionMax, trainingUnitMinuteMax);

        const y = d3.scaleLinear()
            .domain([0, YMax])
            .range([chartHeight, 0]);

        svg.append('g')
            .call(d3.axisLeft(y))
            .attr('class', 'cardiorespiratoryFitnessY')
            .attr('height', chartHeight);

        d3.select('.cardiorespiratoryFitnessY')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.cardiorespiratoryFitnessY')
            .selectAll('.tick')
            .selectAll('line')
            .style('stroke', 'transparent');

        d3.select('.cardiorespiratoryFitnessY')
            .selectAll('.tick')
            .selectAll('text')
            .style('font-size', '14px')
            .style('color', colors.color_white);

        // 方塊平均寬度
        const boxAvgWidth = chartWidth / data.length;
        // 計算每一方格寬度公差百分比
        const tolerancePercentage = boxAvgWidth * tolerance;
        // 計算每一方格寬度公差百分比一半
        const tolerancePercentageHalf = tolerancePercentage / 2;
        // 方格框度
        const boxWidth = boxAvgWidth - zoomOut - tolerancePercentage;

        // 繪製直方圖(訓練目標分鐘)
        svg.append('g')
            .attr('class', 'trainingUnitMinuteBoxGroup')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) - tolerancePercentageHalf}, ${chartHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                return chartHeight / YMax * d.trainingUnitMinute;
            })
            .attr('fill', () => {
                return 'rgba(255, 255, 255, 0.1)';
            });

        // 繪製直方圖(實際完成分鐘已完成)
        svg.append('g')
            .attr('class', 'actualCompletionOrangeBoxGroup')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) - tolerancePercentageHalf}, ${chartHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.actualCompletion >= d.trainingUnitMinute) {
                    return chartHeight / YMax * d.actualCompletion;
                }

                return 0;
            })
            .attr('fill', colors.color_orange);

        // 繪製直方圖(實際完成分鐘未完成)
        svg.append('g')
            .attr('class', 'actualCompletionRedBoxGroup')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) - tolerancePercentageHalf}, ${chartHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.actualCompletion < d.trainingUnitMinute) {
                    return chartHeight / YMax * d.actualCompletion;
                }

                return 0;
            })
            .attr('fill', colors.color_red);

        svg.append('text')
            .text('過去 60 次訓練時間-分鐘')
            .attr('y', chartHeight + margin.top + margin.bottom / 1.3)
            .attr('x', chartWidth / 2 - margin.left - margin.right)
            .attr('fill', colors.color_white)
            .style('font-size', '20px');
    }, [chartWidth, chartHeight, reportCardiorespiratoryFitness]);

    // 繪製柔軟度直方圖報表
    const drawReportSoftness = useCallback(() => {
        const data = reportSoftness;
        const actualCompletionList: any[] = [];
        const scheduleDateList: any = [];
        const trainingUnitMinuteList: any[] = [];

        // 彙整實際次數資料
        data.forEach((item) => {
            actualCompletionList.push(item.actualCompletion);
            scheduleDateList.push(item.scheduleDate);
            trainingUnitMinuteList.push(item.trainingUnitMinute);
        });

        d3.select('.softnessChart').select('svg').remove();

        const svg = d3.select('.softnessChart')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        // 繪製 x 軸
        const x = d3.scaleBand()
            .domain(scheduleDateList)
            .range([0, chartWidth]);

        svg.append('g')
            .call(d3.axisBottom(x).tickFormat((d, i) => {
                const scheduleDate = data?.[i]?.scheduleDate || undefined;
                if (scheduleDate !== undefined) {
                    const scheduleDateSplit = scheduleDate.split('-');
                    const month = scheduleDateSplit[1];
                    const date = scheduleDateSplit[2];
                    return `${month}/${date}`;
                }
                else if (typeof (d) === 'string') {
                    return d;
                }

                return '';
            }).tickPadding(35))
            .attr('class', 'softnessX')
            .attr('transform', `translate(0, ${chartHeight})`)
            .style('font-size', '14px')
            .style('color', colors.color_white);

        d3.select('.softnessX')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.softnessX')
            .selectAll('.tick')
            .append('circle')
            .attr('r', (date) => {
                if (date === reportSoftness[0].scheduleDate || date === reportSoftness[reportSoftness.length - 1].scheduleDate) {
                    return tickMaxCircleR;
                }

                return tickMinCircleR;
            })
            .attr('fill', 'rgba(255, 255, 255, 0.5)')
            .attr('transform', 'translate(0, 20)');

        d3.select('.softnessX')
            .selectAll('.tick')
            .selectAll('line')
            .attr('stroke', 'transparent');

        d3.select('.softnessX').selectAll('.tick')
            .selectAll('text')
            .style('color', (date) => {
                if (date === reportSoftness[0].scheduleDate || date === reportSoftness[reportSoftness.length - 1].scheduleDate) {
                    return 'rgba(255,255,255,0.5)';
                }

                return 'transparent';
            })
            .style('font-size', '12px');


        // 繪製 y 軸
        const actualCompletionMax = d3.max(actualCompletionList);
        const trainingUnitMinuteMax = d3.max(trainingUnitMinuteList);
        const YMax = Math.max(actualCompletionMax, trainingUnitMinuteMax);

        const y = d3.scaleLinear()
            .domain([0, YMax])
            .range([chartHeight, 0]);

        svg.append('g')
            .call(d3.axisLeft(y))
            .attr('class', 'softnessY')
            .attr('height', chartHeight);

        d3.select('.softnessY')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.softnessY')
            .selectAll('.tick')
            .selectAll('line')
            .style('stroke', 'transparent');

        d3.select('.softnessY')
            .selectAll('.tick')
            .selectAll('text')
            .style('font-size', '14px')
            .style('color', colors.color_white);

        // 方塊平均寬度
        const boxAvgWidth = chartWidth / data.length;
        // 計算每一方格寬度公差百分比
        const tolerancePercentage = boxAvgWidth * tolerance;
        // 計算每一方格寬度公差百分比一半
        const tolerancePercentageHalf = tolerancePercentage / 2;
        // 方格框度
        const boxWidth = boxAvgWidth - zoomOut - tolerancePercentage;

        // 繪製直方圖(訓練目標分鐘)
        svg.append('g')
            .attr('class', 'trainingUnitMinuteBoxGroup')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) - tolerancePercentageHalf}, ${chartHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                return chartHeight / YMax * d.trainingUnitMinute;
            })
            .attr('fill', () => {
                return 'rgba(255, 255, 255, 0.1)';
            });

        // 繪製直方圖(實際完成分鐘已完成)
        svg.append('g')
            .attr('class', 'actualCompletionOrangeBoxGroup')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) - tolerancePercentageHalf}, ${chartHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.actualCompletion >= d.trainingUnitMinute) {
                    return chartHeight / YMax * d.actualCompletion;
                }

                return 0;
            })
            .attr('fill', colors.color_orange);

        // 繪製直方圖(實際完成分鐘未完成)
        svg.append('g')
            .attr('class', 'actualCompletionRedBoxGroup')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) - tolerancePercentageHalf}, ${chartHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.actualCompletion < d.trainingUnitMinute) {
                    return chartHeight / YMax * d.actualCompletion;
                }

                return 0;
            })
            .attr('fill', colors.color_red);

        svg.append('text')
            .text('過去 60 次訓練時間-分鐘')
            .attr('y', chartHeight + margin.top + margin.bottom / 1.3)
            .attr('x', chartWidth / 2 - margin.left - margin.right)
            .attr('fill', colors.color_white)
            .style('font-size', '20px');
    }, [chartWidth, chartHeight, reportSoftness]);

    // 繪製平衡直方圖報表
    const drawReportBalance = useCallback(() => {
        const data = reportBalance;
        const actualCompletionList: any[] = [];
        const scheduleDateList: any = [];
        const trainingUnitMinuteList: any[] = [];

        // 彙整實際次數資料
        data.forEach((item) => {
            actualCompletionList.push(item.actualCompletion);
            scheduleDateList.push(item.scheduleDate);
            trainingUnitMinuteList.push(item.trainingUnitMinute);
        });

        d3.select('.balanceChart').select('svg').remove();

        const svg = d3.select('.balanceChart')
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`);

        // 繪製 x 軸
        const x = d3.scaleBand()
            .domain(scheduleDateList)
            .range([0, chartWidth]);

        svg.append('g')
            .call(d3.axisBottom(x).tickFormat((d, i) => {
                const scheduleDate = data?.[i]?.scheduleDate || undefined;
                if (scheduleDate !== undefined) {
                    const scheduleDateSplit = scheduleDate.split('-');
                    const month = scheduleDateSplit[1];
                    const date = scheduleDateSplit[2];
                    return `${month}/${date}`;
                }
                else if (typeof (d) === 'string') {
                    return d;
                }

                return '';
            }).tickPadding(35))
            .attr('class', 'balanceX')
            .attr('transform', `translate(0, ${chartHeight})`)
            .style('font-size', '14px')
            .style('color', colors.color_white);

        d3.select('.balanceX')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.balanceX')
            .selectAll('.tick')
            .append('circle')
            .attr('r', (date) => {
                if (date === reportBalance[0].scheduleDate || date === reportBalance[reportBalance.length - 1].scheduleDate) {
                    return tickMaxCircleR;
                }

                return tickMinCircleR;
            })
            .attr('fill', 'rgba(255, 255, 255, 0.5)')
            .attr('transform', 'translate(0, 20)');

        d3.select('.balanceX')
            .selectAll('.tick')
            .selectAll('line')
            .attr('stroke', 'transparent');

        d3.select('.balanceX').selectAll('.tick')
            .selectAll('text')
            .style('color', (date) => {
                if (date === reportBalance[0].scheduleDate || date === reportBalance[reportBalance.length - 1].scheduleDate) {
                    return 'rgba(255,255,255,0.5)';
                }

                return 'transparent';
            })
            .style('font-size', '12px');


        // 繪製 y 軸
        const actualCompletionMax = d3.max(actualCompletionList);
        const trainingUnitMinuteMax = d3.max(trainingUnitMinuteList);
        const YMax = Math.max(actualCompletionMax, trainingUnitMinuteMax);

        const y = d3.scaleLinear()
            .domain([0, YMax])
            .range([chartHeight, 0]);

        svg.append('g')
            .call(d3.axisLeft(y))
            .attr('class', 'balanceY')
            .attr('height', chartHeight);

        d3.select('.balanceY')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.select('.balanceY')
            .selectAll('.tick')
            .selectAll('line')
            .style('stroke', 'transparent');

        d3.select('.balanceY')
            .selectAll('.tick')
            .selectAll('text')
            .style('font-size', '14px')
            .style('color', colors.color_white);

        // 方塊平均寬度
        const boxAvgWidth = chartWidth / data.length;
        // 計算每一方格寬度公差百分比
        const tolerancePercentage = boxAvgWidth * tolerance;
        // 計算每一方格寬度公差百分比一半
        const tolerancePercentageHalf = tolerancePercentage / 2;
        // 方格框度
        const boxWidth = boxAvgWidth - zoomOut - tolerancePercentage;

        // 繪製直方圖(訓練目標分鐘)
        svg.append('g')
            .attr('class', 'trainingUnitMinuteBoxGroup')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) - tolerancePercentageHalf}, ${chartHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                return chartHeight / YMax * d.trainingUnitMinute;
            })
            .attr('fill', () => {
                return 'rgba(255, 255, 255, 0.1)';
            });

        // 繪製直方圖(實際完成分鐘已完成)
        svg.append('g')
            .attr('class', 'actualCompletionOrangeBoxGroup')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) - tolerancePercentageHalf}, ${chartHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.actualCompletion >= d.trainingUnitMinute) {
                    return chartHeight / YMax * d.actualCompletion;
                }

                return 0;
            })
            .attr('fill', colors.color_orange);

        // 繪製直方圖(實際完成分鐘未完成)
        svg.append('g')
            .attr('class', 'actualCompletionRedBoxGroup')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) - tolerancePercentageHalf}, ${chartHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.actualCompletion < d.trainingUnitMinute) {
                    return chartHeight / YMax * d.actualCompletion;
                }

                return 0;
            })
            .attr('fill', colors.color_red);

        svg.append('text')
            .text('過去 60 次訓練時間-分鐘')
            .attr('y', chartHeight + margin.top + margin.bottom / 1.3)
            .attr('x', chartWidth / 2 - margin.left - margin.right)
            .attr('fill', colors.color_white)
            .style('font-size', '20px');
    }, [chartWidth, chartHeight, reportBalance]);



    // 繪製過去 20 次正式訓練狀況圖表
    const drawFormalTrainingStatusReport = (className: string, machinaryCode: number, responseStudentFormalTrainingStatusReport: GetStudentFormalTrainingStatusReportDataType[]) => {
        const data = responseStudentFormalTrainingStatusReport;
        const trainingDateText: string[] = [];
        const finsh: number[] = [];
        const mistake: number[] = [];
        const level: number[] = [];

        // 彙整訓練日期
        data.forEach((item) => {
            trainingDateText.push(item.trainingDateText);
            finsh.push(item.finsh);
            mistake.push(item.mistake);
            level.push(item.level);
        });

        const formal_training_state_report = document.getElementsByClassName('formal_training_status_report')[machinaryCode];
        const formal_training_state_report_width = formalTrainingStateWidth <= 0 ? formal_training_state_report.clientWidth - formalTrainingStateMargin.left - formalTrainingStateMargin.right : formalTrainingStateWidth - formalTrainingStateMargin.left - formalTrainingStateMargin.right;

        d3.select(`.${className}`).select('svg').remove();

        const svg = d3.select(`.${className}`)
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(${formalTrainingStateMargin.left}, ${formalTrainingStateMargin.top})`);

        // 繪製 x 軸
        const x = d3.scaleBand()
            .domain(trainingDateText)
            .range([0, formal_training_state_report_width]);

        svg.append('g')
            .call(d3.axisBottom(x).tickPadding(30))
            .attr('class', 'formalTrainingStatusX')
            .attr('transform', `translate(0, ${formalTrainingStateHeight})`);

        d3.selectAll('.formalTrainingStatusX')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.selectAll('.formalTrainingStatusX')
            .selectAll('.tick')
            .selectAll('line')
            .attr('stroke', 'transparent');

        d3.selectAll('.formalTrainingStatusX')
            .selectAll('.tick')
            .append('circle')
            .attr('r', tickMinCircleR)
            .attr('fill', () => {
                return 'rgba(255, 255, 255, 0.5)';
            })
            .attr('transform', 'translate(0, 20)');

        d3.selectAll('.formalTrainingStatusX').selectAll('.tick')
            .selectAll('text')
            .style('color', colors.color_white)
            .style('font-size', '12px');

        // 繪製 y 軸
        const maxFinsh = d3.max(finsh) || 0;
        const maxMistake = d3.max(mistake) || 0;
        const maxLevel = d3.max(level) || 0;
        const YMax = Math.max(maxFinsh, maxMistake, maxLevel);

        const y = d3.scaleLinear()
            .domain([0, YMax])
            .range([formalTrainingStateHeight, 0]);

        svg.append('g')
            .call(d3.axisLeft(y).ticks(5))
            .attr('class', 'formalTrainingStatusY')
            .attr('height', formalTrainingStateHeight);

        d3.selectAll('.formalTrainingStatusY')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.selectAll('.formalTrainingStatusY')
            .selectAll('.tick')
            .selectAll('line')
            .style('stroke', 'transparent');

        d3.selectAll('.formalTrainingStatusY')
            .selectAll('.tick')
            .selectAll('text')
            .style('font-size', '14px')
            .style('color', colors.color_white);

        // 方塊平均寬度
        const boxAvgWidth = formal_training_state_report_width / data.length / 3;
        const tolerancePercentage = boxAvgWidth * tolerance;
        const tolerancePercentageHalf = tolerancePercentage / 2;
        const boxWidth = tolerancePercentage - zoomOut;
        const equipartition = 3;

        // 繪製訓練階數
        svg.append('g')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', 'trainingLevel')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) * equipartition - tolerancePercentageHalf * 6}, ${formalTrainingStateHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.level) {
                    return formalTrainingStateHeight / YMax * d.level;
                }

                return 0;
            })
            .attr('fill', 'rgba(255,255,255, 0.1)');

        // 繪製訓練失誤次數
        svg.append('g')
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', 'mistake')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) * equipartition - tolerancePercentageHalf * 4}, ${formalTrainingStateHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.mistake) {
                    return formalTrainingStateHeight / YMax * d.mistake;
                }

                return 0;
            })
            .attr('fill', colors.color_red);

        // 繪製訓練次數
        svg.append('g').selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', 'finish')
            .attr('x', '1')
            .attr('transform', (d, i) => {
                return `translate(${boxAvgWidth * (i + 1) * equipartition - tolerancePercentageHalf * 2}, ${formalTrainingStateHeight}) rotate(180)`;
            })
            .attr('width', () => {
                return boxWidth;
            })
            .transition()
            .duration(duration)
            .delay((d, i) => {
                return delay * i;
            })
            .ease(d3.easeElastic)
            .attr('height', (d) => {
                if (d.finsh) {
                    return formalTrainingStateHeight / YMax * d.finsh;
                }

                return 0;
            })
            .attr('fill', colors.color_orange);

        // 繪製圖表標題
        svg.append('text')
            .text('過去 20 次訓練狀況')
            .attr('x', formal_training_state_report_width / 2 - 86.5)
            .attr('y', formalTrainingStateHeight + formalTrainingStateMargin.top)
            .style('fill', colors.color_white)
            .style('font-size', '20px');

        // 繪製訓練階數標籤
        svg.append('text')
            .text('訓練階數')
            .attr('x', formal_training_state_report_width - 250)
            .attr('y', '-10')
            .style('fill', colors.color_white)
            .style('font-size', '12px');

        svg.append('rect')
            .attr('height', '5')
            .attr('width', '20')
            .attr('fill', 'rgba(255, 255, 255, 0.1)')
            .attr('x', formal_training_state_report_width - 80 * 3 - 40)
            .attr('y', '-16');

        // 繪製失誤次數標籤
        svg.append('text')
            .text('失誤次數')
            .attr('x', formal_training_state_report_width - 150)
            .attr('y', '-10')
            .style('fill', colors.color_white)
            .style('font-size', '12px');

        svg.append('rect')
            .attr('height', '5')
            .attr('width', '20')
            .attr('fill', colors.color_red)
            .attr('x', formal_training_state_report_width - 80 * 2 - 20)
            .attr('y', '-16');

        // 繪製失誤次數標籤
        svg.append('text')
            .text('訓練次數')
            .attr('x', formal_training_state_report_width - 50)
            .attr('y', '-10')
            .style('fill', colors.color_white)
            .style('font-size', '12px');

        svg.append('rect')
            .attr('height', '5')
            .attr('width', '20')
            .attr('fill', colors.color_orange)
            .attr('x', formal_training_state_report_width - 80)
            .attr('y', '-16');
    };

    // 繪製過去 20 次正式訓練狀況圖表一
    const drawFormalTrainingStatusReportOne = useCallback((className: string, responseStudentFormalTrainingStatusReport: GetStudentFormalTrainingStatusReportDataType[]) => {
        drawFormalTrainingStatusReport(className, 0, responseStudentFormalTrainingStatusReport);
    }, [formalTrainingStateWidth, formalTrainingStateHeight, responseStudentFormalTrainingStatusReportOne]);

    // 繪製過去 20 次正式訓練狀況圖表二
    const drawFormalTrainingStatusReportTwo = useCallback((className: string, responseStudentFormalTrainingStatusReport: GetStudentFormalTrainingStatusReportDataType[]) => {
        drawFormalTrainingStatusReport(className, 1, responseStudentFormalTrainingStatusReport);
    }, [formalTrainingStateWidth, formalTrainingStateHeight, responseStudentFormalTrainingStatusReportTwo]);

    // 繪製過去 20 次正式訓練狀況圖表三
    const drawFormalTrainingStatusReportThree = useCallback((className: string, responseStudentFormalTrainingStatusReport: GetStudentFormalTrainingStatusReportDataType[]) => {
        drawFormalTrainingStatusReport(className, 2, responseStudentFormalTrainingStatusReport);
    }, [formalTrainingStateWidth, formalTrainingStateHeight, responseStudentFormalTrainingStatusReportThree]);

    // 繪製過去 20 次正式訓練狀況圖表四
    const drawFormalTrainingStatusReportFour = useCallback((className: string, responseStudentFormalTrainingStatusReport: GetStudentFormalTrainingStatusReportDataType[]) => {
        drawFormalTrainingStatusReport(className, 3, responseStudentFormalTrainingStatusReport);
    }, [formalTrainingStateWidth, formalTrainingStateHeight, responseStudentFormalTrainingStatusReportFour]);

    // 繪製過去 20 次正式訓練狀況圖表五
    const drawFormalTrainingStatusReportFive = useCallback((className: string, responseStudentFormalTrainingStatusReport: GetStudentFormalTrainingStatusReportDataType[]) => {
        drawFormalTrainingStatusReport(className, 4, responseStudentFormalTrainingStatusReport);
    }, [formalTrainingStateWidth, formalTrainingStateHeight, responseStudentFormalTrainingStatusReportFive]);

    // 繪製過去 20 次正式訓練狀況圖表六
    const drawFormalTrainingStatusReportSix = useCallback((className: string, responseStudentFormalTrainingStatusReport: GetStudentFormalTrainingStatusReportDataType[]) => {
        drawFormalTrainingStatusReport(className, 5, responseStudentFormalTrainingStatusReport);
    }, [formalTrainingStateWidth, formalTrainingStateHeight, responseStudentFormalTrainingStatusReportSix]);

    // 繪製過去 20 次正式訓練狀況圖表七
    const drawFormalTrainingStatusReportSeven = useCallback((className: string, responseStudentFormalTrainingStatusReport: GetStudentFormalTrainingStatusReportDataType[]) => {
        drawFormalTrainingStatusReport(className, 6, responseStudentFormalTrainingStatusReport);
    }, [formalTrainingStateWidth, formalTrainingStateHeight, responseStudentFormalTrainingStatusReportSeven]);

    // 繪製過去 20 次正式訓練狀況圖表八
    const drawFormalTrainingStatusReportEight = useCallback((className: string, responseStudentFormalTrainingStatusReport: GetStudentFormalTrainingStatusReportDataType[]) => {
        drawFormalTrainingStatusReport(className, 7, responseStudentFormalTrainingStatusReport);
    }, [formalTrainingStateWidth, formalTrainingStateHeight, responseStudentFormalTrainingStatusReportEight]);

    // 繪製過去 20 次正式訓練狀況圖表九
    const drawFormalTrainingStatusReportNine = useCallback((className: string, responseStudentFormalTrainingStatusReport: GetStudentFormalTrainingStatusReportDataType[]) => {
        drawFormalTrainingStatusReport(className, 8, responseStudentFormalTrainingStatusReport);
    }, [formalTrainingStateWidth, formalTrainingStateHeight, responseStudentFormalTrainingStatusReportNine]);

    // 繪製過去 20 次正式訓練狀況圖表十
    const drawFormalTrainingStatusReportTen = useCallback((className: string, responseStudentFormalTrainingStatusReport: GetStudentFormalTrainingStatusReportDataType[]) => {
        drawFormalTrainingStatusReport(className, 9, responseStudentFormalTrainingStatusReport);
    }, [formalTrainingStateWidth, formalTrainingStateHeight, responseStudentFormalTrainingStatusReportTen]);



    // 繪製過去 20 次正式訓練狀況功率圖表
    const drawFormalTrainingTotalPowerReport = (className: string, machinaryCode: number, responseFormalTrainingPowerList: GetStudentTrainingPowerReportDataType[]) => {
        const data = responseFormalTrainingPowerList;

        const trainingDateText: string[] = [];
        const dayTotalPower: number[] = [];

        data.forEach((item) => {
            trainingDateText.push(item.trainingDateText);
            dayTotalPower.push(item.dayTotalPower);
        });

        const formal_training_power_report = document.getElementsByClassName('formal_training_power_report')[machinaryCode];
        const formal_training_power_report_width = formalTrainingStateWidth <= 0 ? formal_training_power_report.clientWidth - formalTrainingPowerMargin.left - formalTrainingPowerMargin.right : formalTrainingStateWidth - formalTrainingPowerMargin.left - formalTrainingPowerMargin.right;

        d3.select(`.${className}`).select('svg').remove();

        const svg = d3.select(`.${className}`)
            .append('svg')
            .attr('width', '100%')
            .attr('height', '50vh')
            .append('g')
            .attr('transform', `translate(${formalTrainingPowerMargin.left}, ${formalTrainingPowerMargin.top})`);

        // 繪製 x 軸
        const xScale = d3.scaleBand()
            .domain(trainingDateText)
            .range([0, formal_training_power_report_width]);

        svg.append('g')
            .call(d3.axisBottom(xScale).tickPadding(35))
            .attr('transform', `translate(0, ${formalTrainingPowerHeight})`)
            .attr('class', 'formalTrainingPowerX')
            .attr('color', 'white');

        d3.selectAll('.formalTrainingPowerX')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.selectAll('.formalTrainingPowerX')
            .selectAll('.tick')
            .selectAll('line')
            .attr('stroke', 'transparent');

        d3.selectAll('.formalTrainingPowerX')
            .selectAll('.tick')
            .append('circle')
            .attr('r', tickMinCircleR)
            .attr('fill', 'rgba(255, 255, 255, 0.5)')
            .attr('transform', 'translate(0, 20)');

        d3.selectAll('.formalTrainingPowerX').selectAll('.tick')
            .selectAll('text')
            .style('color', colors.color_white)
            .style('font-size', '12px');

        // 繪製 y 軸
        const yScale = d3.scaleLinear()
            .domain([0, d3.max(dayTotalPower) || 0])
            .range([formalTrainingPowerHeight, 0]);

        svg.append('g')
            .call(d3.axisLeft(yScale).ticks(5))
            .attr('class', 'formalTrainingPowerY')
            .attr('color', 'white');

        d3.selectAll('.formalTrainingPowerY')
            .selectAll('.domain')
            .attr('stroke', 'transparent');

        d3.selectAll('.formalTrainingPowerY')
            .selectAll('.tick')
            .selectAll('line')
            .style('stroke', 'transparent');

        d3.selectAll('.formalTrainingPowerY')
            .selectAll('.tick')
            .selectAll('text')
            .style('font-size', '14px')
            .style('color', colors.color_white);


        // 繪製折現圖線條
        svg.append('g')
            .append('path')
            .datum(data)
            .attr('transform', 'translate(0, 0)')
            .attr('d', d3.line<GetStudentTrainingPowerReportDataType>()
                .x((d) => {
                    const s = xScale(d.trainingDateText) || undefined;

                    if (s !== undefined) {
                        return s + xScale.bandwidth() / 2;
                    }

                    return xScale.bandwidth() / 2;
                })
                .y((d) => {
                    return yScale(d.dayTotalPower);
                }).curve(d3.curveMonotoneX))
            .attr('fill', 'none')
            .attr('stroke', colors.color_yellow)
            .attr('stroke-width', 2);

        // 繪製折線圖原點
        svg.append('g').selectAll('circle')
            .data(data)
            .enter()
            .append('circle')
            .attr('r', '5')
            .attr('cx', (d) => {
                const s = xScale(d.trainingDateText) || undefined;

                if (s !== undefined) {
                    return s + xScale.bandwidth() / 2;
                }

                return xScale.bandwidth() / 2;
            })
            .attr('cy', d => yScale(d.dayTotalPower))
            .attr('fill', colors.color_yellow);

        // 遮罩動畫
        svg.append('rect')
            .attr('width', formalTrainingStateWidth)
            .attr('height', formalTrainingStateHeight + 20)
            .attr('x', 1)
            .attr('transform', `translate(${formal_training_power_report_width}, ${formalTrainingStateHeight}) rotate(180)`)
            .attr('fill', '#393939')
            .transition()
            .duration(2000)
            .attr('transform', `translate(${formal_training_power_report_width * 2}, ${formalTrainingStateHeight}) rotate(180)`);

        // 繪製圖表標題
        svg.append('text')
            .text('過去 20 次訓練狀況 - 功率(千瓦特)')
            .attr('x', formal_training_power_report_width / 2 - 150.6)
            .attr('y', formalTrainingPowerHeight + formalTrainingPowerMargin.top)
            .style('fill', colors.color_white)
            .style('font-size', '20px');
    };

    // 繪製過去 20 次正式訓練狀況功率圖表一
    const drawFormalTrainingTotalPowerReportOne = useCallback((className: string, responseFormalTrainingPowerList: GetStudentTrainingPowerReportDataType[]) => {
        drawFormalTrainingTotalPowerReport(className, 0, responseFormalTrainingPowerList);
    }, [formalTrainingPowerWidth, formalTrainingPowerHeight, responseStudentTrainingPowerReportOne]);

    // 繪製過去 20 次正式訓練狀況功率圖表二
    const drawFormalTrainingTotalPowerReportTwo = useCallback((className: string, responseFormalTrainingPowerList: GetStudentTrainingPowerReportDataType[]) => {
        drawFormalTrainingTotalPowerReport(className, 1, responseFormalTrainingPowerList);
    }, [formalTrainingPowerWidth, formalTrainingPowerHeight, responseStudentTrainingPowerReportTwo]);

    // 繪製過去 20 次正式訓練狀況功率圖表三
    const drawFormalTrainingTotalPowerReportThree = useCallback((className: string, responseFormalTrainingPowerList: GetStudentTrainingPowerReportDataType[]) => {
        drawFormalTrainingTotalPowerReport(className, 2, responseFormalTrainingPowerList);
    }, [formalTrainingPowerWidth, formalTrainingPowerHeight, responseStudentTrainingPowerReportThree]);

    // 繪製過去 20 次正式訓練狀況功率圖表四
    const drawFormalTrainingTotalPowerReportFour = useCallback((className: string, responseFormalTrainingPowerList: GetStudentTrainingPowerReportDataType[]) => {
        drawFormalTrainingTotalPowerReport(className, 3, responseFormalTrainingPowerList);
    }, [formalTrainingPowerWidth, formalTrainingPowerHeight, responseStudentTrainingPowerReportFour]);

    // 繪製過去 20 次正式訓練狀況功率圖表五
    const drawFormalTrainingTotalPowerReportFive = useCallback((className: string, responseFormalTrainingPowerList: GetStudentTrainingPowerReportDataType[]) => {
        drawFormalTrainingTotalPowerReport(className, 4, responseFormalTrainingPowerList);
    }, [formalTrainingPowerWidth, formalTrainingPowerHeight, responseStudentTrainingPowerReportFive]);

    // 繪製過去 20 次正式訓練狀況功率圖表六
    const drawFormalTrainingTotalPowerReportSix = useCallback((className: string, responseFormalTrainingPowerList: GetStudentTrainingPowerReportDataType[]) => {
        drawFormalTrainingTotalPowerReport(className, 5, responseFormalTrainingPowerList);
    }, [formalTrainingPowerWidth, formalTrainingPowerHeight, responseStudentTrainingPowerReportSix]);

    // 繪製過去 20 次正式訓練狀況功率圖表七
    const drawFormalTrainingTotalPowerReportSeven = useCallback((className: string, responseFormalTrainingPowerList: GetStudentTrainingPowerReportDataType[]) => {
        drawFormalTrainingTotalPowerReport(className, 6, responseFormalTrainingPowerList);
    }, [formalTrainingPowerWidth, formalTrainingPowerHeight, responseStudentTrainingPowerReportSeven]);

    // 繪製過去 20 次正式訓練狀況功率圖表八
    const drawFormalTrainingTotalPowerReportEight = useCallback((className: string, responseFormalTrainingPowerList: GetStudentTrainingPowerReportDataType[]) => {
        drawFormalTrainingTotalPowerReport(className, 7, responseFormalTrainingPowerList);
    }, [formalTrainingPowerWidth, formalTrainingPowerHeight, responseStudentTrainingPowerReportEight]);

    // 繪製過去 20 次正式訓練狀況功率圖表九
    const drawFormalTrainingTotalPowerReportNine = useCallback((className: string, responseFormalTrainingPowerList: GetStudentTrainingPowerReportDataType[]) => {
        drawFormalTrainingTotalPowerReport(className, 8, responseFormalTrainingPowerList);
    }, [formalTrainingPowerWidth, formalTrainingPowerHeight, responseStudentTrainingPowerReportNine]);

    // 繪製過去 20 次正式訓練狀況功率圖表十
    const drawFormalTrainingTotalPowerReportTen = useCallback((className: string, responseFormalTrainingPowerList: GetStudentTrainingPowerReportDataType[]) => {
        drawFormalTrainingTotalPowerReport(className, 9, responseFormalTrainingPowerList);
    }, [formalTrainingPowerWidth, formalTrainingPowerHeight, responseStudentTrainingPowerReportTen]);



    // 取得各台機器訓練總功率、過去 20 次訓練狀況、過去 20 次訓練狀況-功率
    const handleStudentMachinaryTotalPower = (machinaryCode: number, index: number) => {
        document.getElementsByClassName('circle')[index].classList.toggle('active');

        const handleMachinaryTotalPower = (totalPower: GetStudentMachinaryTotalPowerDataType, formalTrainingStatusList: GetStudentFormalTrainingStatusReportDataType[], formalTrainingPowerList: GetStudentTrainingPowerReportDataType[]) => {
            if (machinaryCode === 1) {
                setShowMachinarySummaryListOne(true);
                setResponseStudentMachinaryTotalPowerOne(totalPower);

                setResponseStudentFormalTrainingStatusReportOne(formalTrainingStatusList);
                setResponseStudentTrainingPowerReportOne(formalTrainingPowerList);
            }
            else if (machinaryCode === 2) {
                setShowMachinarySummaryListTwo(true);
                setResponseStudentMachinaryTotalPowerTwo(totalPower);

                setResponseStudentFormalTrainingStatusReportTwo(formalTrainingStatusList);
                setResponseStudentTrainingPowerReportTwo(formalTrainingPowerList);
            }
            else if (machinaryCode === 3) {
                setShowMachinarySummaryListThree(true);
                setResponseStudentMachinaryTotalPowerThree(totalPower);

                setResponseStudentFormalTrainingStatusReportThree(formalTrainingStatusList);
                setResponseStudentTrainingPowerReportThree(formalTrainingPowerList);
            }
            else if (machinaryCode === 4) {
                setShowMachinarySummaryListFour(true);
                setResponseStudentMachinaryTotalPowerFour(totalPower);

                setResponseStudentFormalTrainingStatusReportFour(formalTrainingStatusList);
                setResponseStudentTrainingPowerReportFour(formalTrainingPowerList);
            }
            else if (machinaryCode === 5) {
                setShowMachinarySummaryListFive(true);
                setResponseStudentMachinaryTotalPowerFive(totalPower);

                setResponseStudentFormalTrainingStatusReportFive(formalTrainingStatusList);
                setResponseStudentTrainingPowerReportFive(formalTrainingPowerList);
            }
            else if (machinaryCode === 6) {
                setShowMachinarySummaryListSix(true);
                setResponseStudentMachinaryTotalPowerSix(totalPower);

                setResponseStudentFormalTrainingStatusReportSix(formalTrainingStatusList);
                setResponseStudentTrainingPowerReportSix(formalTrainingPowerList);
            }
            else if (machinaryCode === 7) {
                setShowMachinarySummaryListSeven(true);
                setResponseStudentMachinaryTotalPowerSeven(totalPower);

                setResponseStudentFormalTrainingStatusReportSeven(formalTrainingStatusList);
                setResponseStudentTrainingPowerReportSeven(formalTrainingPowerList);
            }
            else if (machinaryCode === 8) {
                setShowMachinarySummaryListEight(true);
                setResponseStudentMachinaryTotalPowerEight(totalPower);

                setResponseStudentFormalTrainingStatusReportEight(formalTrainingStatusList);
                setResponseStudentTrainingPowerReportEight(formalTrainingPowerList);
            }
            else if (machinaryCode === 9) {
                setShowMachinarySummaryListNine(true);
                setResponseStudentMachinaryTotalPowerNine(totalPower);

                setResponseStudentFormalTrainingStatusReportNine(formalTrainingStatusList);
                setResponseStudentTrainingPowerReportNine(formalTrainingPowerList);
            }
            else {
                setShowMachinarySummaryListTen(true);
                setResponseStudentMachinaryTotalPowerTen(totalPower);

                setResponseStudentFormalTrainingStatusReportTen(formalTrainingStatusList);
                setResponseStudentTrainingPowerReportTen(formalTrainingPowerList);
            }
        };


        if (machinaryCode === 1 && responseStudentMachinaryTotalPowerOne !== undefined) {
            if (showMachinarySummaryListOne) {
                setShowMachinarySummaryListOne(false);
            }
            else {
                setShowMachinarySummaryListOne(true);
            }
        }
        else if (machinaryCode === 2 && responseStudentMachinaryTotalPowerTwo !== undefined) {
            if (showMachinarySummaryListTwo) {
                setShowMachinarySummaryListTwo(false);
            }
            else {
                setShowMachinarySummaryListTwo(true);
            }
        }
        else if (machinaryCode === 3 && responseStudentMachinaryTotalPowerThree !== undefined) {
            if (showMachinarySummaryListThree) {
                setShowMachinarySummaryListThree(false);
            }
            else {
                setShowMachinarySummaryListThree(true);
            }
        }
        else if (machinaryCode === 4 && responseStudentMachinaryTotalPowerFour !== undefined) {
            if (showMachinarySummaryListFour) {
                setShowMachinarySummaryListFour(false);
            }
            else {
                setShowMachinarySummaryListFour(true);
            }
        }
        else if (machinaryCode === 5 && responseStudentMachinaryTotalPowerFive !== undefined) {
            if (showMachinarySummaryListFive) {
                setShowMachinarySummaryListFive(false);
            }
            else {
                setShowMachinarySummaryListFive(true);
            }
        }
        else if (machinaryCode === 6 && responseStudentMachinaryTotalPowerSix !== undefined) {
            if (showMachinarySummaryListSix) {
                setShowMachinarySummaryListSix(false);
            }
            else {
                setShowMachinarySummaryListSix(true);
            }
        }
        else if (machinaryCode === 7 && responseStudentMachinaryTotalPowerSeven !== undefined) {
            if (showMachinarySummaryListSeven) {
                setShowMachinarySummaryListSeven(false);
            }
            else {
                setShowMachinarySummaryListSeven(true);
            }
        }
        else if (machinaryCode === 8 && responseStudentMachinaryTotalPowerEight !== undefined) {
            if (showMachinarySummaryListEight) {
                setShowMachinarySummaryListEight(false);
            }
            else {
                setShowMachinarySummaryListEight(true);
            }
        }
        else if (machinaryCode === 9 && responseStudentMachinaryTotalPowerNine !== undefined) {
            if (showMachinarySummaryListNine) {
                setShowMachinarySummaryListNine(false);
            }
            else {
                setShowMachinarySummaryListNine(true);
            }
        }
        else if (machinaryCode === 10 && responseStudentMachinaryTotalPowerTen !== undefined) {
            if (showMachinarySummaryListTen) {
                setShowMachinarySummaryListTen(false);
            }
            else {
                setShowMachinarySummaryListTen(true);
            }
        }
        else {
            Promise.all([
                getStudentMachinaryTotalPower(storeGetters.studentId, machinaryCode),
                getStudentFormalTrainingStatusReport(storeGetters.studentId, machinaryCode),
                getStudentTrainingPowerReport(storeGetters.studentId, machinaryCode)
            ]).then((res) => {
                const { data: totalPower } = res[0].data;
                const { data: formalTrainingStatusList } = res[1].data;
                const { data: formalTrainingPowerList } = res[2].data;

                handleMachinaryTotalPower(totalPower, formalTrainingStatusList, formalTrainingPowerList);

                return res;
            }).catch((error) => {
                const errorCode = axiosErrorCode<GetStudentFormalTrainingSummaryErrorCodeType>(error);

                setVisibleInfoDialog(true);
                setVisibleInfoDialogTitle('錯誤');

                switch (errorCode) {
                    case 'StudentDoesNotExist':
                        setVisibleInfoDialogMessage('查無學員');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisibleInfoDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    // 處理是否列印身體組成與體適能評價
    const handleIsPrintBodyMass = () => {
        if (isPrintBodyMass) {
            setIsPrintBodyMass(false);
        }
        else {
            setIsPrintBodyMass(true);
        }
    };

    // 處理是否列印運動處方
    const handlePrintExercisePrescription = () => {
        if (isPrintExercisePrescription) {
            setIsPrintExercisePrescription(false);
        }
        else {
            setIsPrintExercisePrescription(true);
        }
    };

    // 處理是否列印儀器設定
    const handlePrintInstrumentSettings = () => {
        if (isPrintInstrumentSettings) {
            setIsPrintInstrumentSettings(false);
        }
        else {
            setIsPrintInstrumentSettings(true);
        }
    };

    // 處理是否列印心肺適能報表
    const handlePrintInspectionReportCardiorespiratoryFitness = () => {
        if (isPrintInspectionReportCardiorespiratoryFitness) {
            setIsPrintInspectionReportCardiorespiratoryFitness(false);
        }
        else {
            setIsPrintInspectionReportCardiorespiratoryFitness(true);
        }
    };

    // 處理是否列印柔軟度報表
    const handlePrintInspectionReportSoftness = () => {
        if (isPrintInspectionReportSoftness) {
            setIsPrintInspectionReportSoftness(false);
        }
        else {
            setIsPrintInspectionReportSoftness(true);
        }
    };

    // 處理是否列印平衡報表
    const handlePrintInspectionReportBalance = () => {
        if (isPrintInspectionReportBalance) {
            setIsPrintInspectionReportBalance(false);
        }
        else {
            setIsPrintInspectionReportBalance(true);
        }
    };

    // 關閉資訊彈窗
    const closeInfoDialog = () => {
        setVisibleInfoDialog(false);
        setVisibleInfoDialogTitle('');
        setVisibleInfoDialogMessage('');
    };

    const inspectionReportCardiorespiratoryFitnessRef = useRef<HTMLDivElement>(null);
    const inspectionReportSoftnessRef = useRef<HTMLDivElement>(null);
    const inspectionReportBalanceRef = useRef<HTMLDivElement>(null);

    // window print
    const confirmPrint = async () => {
        let cardiorespiratoryFitnessImage = '';
        let softnessImage = '';
        let balanceImage = '';
        let machinaryTrainingStatusOneImage = '';
        let machinaryTrainingStatusTwoImage = '';
        let machinaryTrainingStatusThreeImage = '';
        let machinaryTrainingStatusFourImage = '';
        let machinaryTrainingStatusFiveImage = '';
        let machinaryTrainingStatusSixImage = '';
        let machinaryTrainingStatusSevenImage = '';
        let machinaryTrainingStatusEightImage = '';
        let machinaryTrainingStatusNineImage = '';
        let machinaryTrainingStatusTenImage = '';

        let isProductCardiorespiratoryFitnessImage = false;
        let isProductSoftnessImage = false;
        let isProductBalanceImage = false;
        let isProductMachinaryTrainingStatusOneImage = false;
        let isProductMachinaryTrainingStatusTwoImage = false;
        let isProductMachinaryTrainingStatusThreeImage = false;
        let isProductMachinaryTrainingStatusFourImage = false;
        let isProductMachinaryTrainingStatusFiveImage = false;
        let isProductMachinaryTrainingStatusSixImage = false;
        let isProductMachinaryTrainingStatusSevenImage = false;
        let isProductMachinaryTrainingStatusEightImage = false;
        let isProductMachinaryTrainingStatusNineImage = false;
        let isProductMachinaryTrainingStatusTenImage = false;

        // 包裝心肺適能圖片
        if (isPrintInspectionReportCardiorespiratoryFitness) {
            domtoimage.toPng(inspectionReportCardiorespiratoryFitnessRef.current as HTMLElement).then((dataUrl) => {
                cardiorespiratoryFitnessImage = dataUrl.replace('data:image/png;base64,', '');
                isProductCardiorespiratoryFitnessImage = true;
            }).catch(() => {
                dispatch({ type: 'loading/changeLoading', payload: false });
            });
        }
        else {
            isProductCardiorespiratoryFitnessImage = true;
        }

        // 包裝柔軟度圖片
        if (isPrintInspectionReportSoftness) {
            domtoimage.toPng(inspectionReportSoftnessRef.current as HTMLElement).then((dataUrl) => {
                softnessImage = dataUrl.replace('data:image/png;base64,', '');
                isProductSoftnessImage = true;
            }).catch(() => {
                dispatch({ type: 'loading/changeLoading', payload: false });
            });
        }
        else {
            isProductSoftnessImage = true;
        }

        // 包裝平衡圖片
        if (isPrintInspectionReportBalance) {
            domtoimage.toPng(inspectionReportBalanceRef.current as HTMLElement).then((dataUrl) => {
                balanceImage = dataUrl.replace('data:image/png;base64,', '');
                isProductBalanceImage = true;
            }).catch(() => {
                dispatch({ type: 'loading/changeLoading', payload: false });
            });
        }
        else {
            isProductBalanceImage = true;
        }

        // 包裝機器正式訓練狀況圖片一
        if (showMachinarySummaryListOne) {
            domtoimage.toPng(document.getElementsByClassName('machinaryCodeOne')[0] as HTMLElement).then((dataUrl) => {
                machinaryTrainingStatusOneImage = dataUrl.replace('data:image/png;base64,', '');
                isProductMachinaryTrainingStatusOneImage = true;
            }).catch(() => {
                dispatch({ type: 'loading/changeLoading', payload: false });
            });
        }
        else {
            isProductMachinaryTrainingStatusOneImage = true;
        }

        // 包裝機器正式訓練狀況圖片二
        if (showMachinarySummaryListTwo) {
            domtoimage.toPng(document.getElementsByClassName('machinaryCodeTwo')[0] as HTMLElement).then((dataUrl) => {
                machinaryTrainingStatusTwoImage = dataUrl.replace('data:image/png;base64,', '');
                isProductMachinaryTrainingStatusTwoImage = true;
            }).catch(() => {
                dispatch({ type: 'loading/changeLoading', payload: false });
            });
        }
        else {
            isProductMachinaryTrainingStatusTwoImage = true;
        }

        // 包裝機器正式訓練狀況圖片三
        if (showMachinarySummaryListThree) {
            domtoimage.toPng(document.getElementsByClassName('machinaryCodeThree')[0] as HTMLElement).then((dataUrl) => {
                machinaryTrainingStatusThreeImage = dataUrl.replace('data:image/png;base64,', '');
                isProductMachinaryTrainingStatusThreeImage = true;
            }).catch(() => {
                dispatch({ type: 'loading/changeLoading', payload: false });
            });
        }
        else {
            isProductMachinaryTrainingStatusThreeImage = true;
        }

        // 包裝機器正式訓練狀況圖片四
        if (showMachinarySummaryListFour) {
            domtoimage.toPng(document.getElementsByClassName('machinaryCodeFour')[0] as HTMLElement).then((dataUrl) => {
                machinaryTrainingStatusFourImage = dataUrl.replace('data:image/png;base64,', '');
                isProductMachinaryTrainingStatusFourImage = true;
            }).catch(() => {
                dispatch({ type: 'loading/changeLoading', payload: false });
            });
        }
        else {
            isProductMachinaryTrainingStatusFourImage = true;
        }

        // 包裝機器正式訓練狀況圖片五
        if (showMachinarySummaryListFive) {
            domtoimage.toPng(document.getElementsByClassName('machinaryCodeFive')[0] as HTMLElement).then((dataUrl) => {
                machinaryTrainingStatusFiveImage = dataUrl.replace('data:image/png;base64,', '');
                isProductMachinaryTrainingStatusFiveImage = true;
            }).catch(() => {
                dispatch({ type: 'loading/changeLoading', payload: false });
            });
        }
        else {
            isProductMachinaryTrainingStatusFiveImage = true;
        }

        // 包裝機器正式訓練狀況圖片六
        if (showMachinarySummaryListSix) {
            domtoimage.toPng(document.getElementsByClassName('machinaryCodeSix')[0] as HTMLElement).then((dataUrl) => {
                machinaryTrainingStatusSixImage = dataUrl.replace('data:image/png;base64,', '');
                isProductMachinaryTrainingStatusSixImage = true;
            }).catch(() => {
                dispatch({ type: 'loading/changeLoading', payload: false });
            });
        }
        else {
            isProductMachinaryTrainingStatusSixImage = true;
        }

        // 包裝機器正式訓練狀況圖片七
        if (showMachinarySummaryListSeven) {
            domtoimage.toPng(document.getElementsByClassName('machinaryCodeSeven')[0] as HTMLElement).then((dataUrl) => {
                machinaryTrainingStatusSevenImage = dataUrl.replace('data:image/png;base64,', '');
                isProductMachinaryTrainingStatusSevenImage = true;
            }).catch(() => {
                dispatch({ type: 'loading/changeLoading', payload: false });
            });
        }
        else {
            isProductMachinaryTrainingStatusSevenImage = true;
        }


        // 包裝機器正式訓練狀況圖片八
        if (showMachinarySummaryListEight) {
            domtoimage.toPng(document.getElementsByClassName('machinaryCodeEight')[0] as HTMLElement).then((dataUrl) => {
                machinaryTrainingStatusEightImage = dataUrl.replace('data:image/png;base64,', '');
                isProductMachinaryTrainingStatusEightImage = true;
            }).catch(() => {
                dispatch({ type: 'loading/changeLoading', payload: false });
            });
        }
        else {
            isProductMachinaryTrainingStatusEightImage = true;
        }

        // 包裝機器正式訓練狀況圖片九
        if (showMachinarySummaryListNine) {
            domtoimage.toPng(document.getElementsByClassName('machinaryCodeNine')[0] as HTMLElement).then((dataUrl) => {
                machinaryTrainingStatusNineImage = dataUrl.replace('data:image/png;base64,', '');
                isProductMachinaryTrainingStatusNineImage = true;
            }).catch(() => {
                dispatch({ type: 'loading/changeLoading', payload: false });
            });
        }
        else {
            isProductMachinaryTrainingStatusNineImage = true;
        }

        // 包裝機器正式訓練狀況圖片十
        if (showMachinarySummaryListTen) {
            domtoimage.toPng(document.getElementsByClassName('machinaryCodeTen')[0] as HTMLElement).then((dataUrl) => {
                machinaryTrainingStatusTenImage = dataUrl.replace('data:image/png;base64,', '');
                isProductMachinaryTrainingStatusTenImage = true;
            }).catch(() => {
                dispatch({ type: 'loading/changeLoading', payload: false });
            });
        }
        else {
            isProductMachinaryTrainingStatusTenImage = true;
        }

        const interval = setInterval(() => {
            dispatch({ type: 'loading/changeLoading', payload: true });

            if (isProductCardiorespiratoryFitnessImage && isProductSoftnessImage && isProductBalanceImage &&
                isProductMachinaryTrainingStatusOneImage && isProductMachinaryTrainingStatusTwoImage && isProductMachinaryTrainingStatusThreeImage &&
                isProductMachinaryTrainingStatusFourImage && isProductMachinaryTrainingStatusFiveImage && isProductMachinaryTrainingStatusSixImage &&
                isProductMachinaryTrainingStatusSevenImage && isProductMachinaryTrainingStatusEightImage && isProductMachinaryTrainingStatusEightImage &&
                isProductMachinaryTrainingStatusNineImage && isProductMachinaryTrainingStatusTenImage) {
                const data: PrintTrainingReportRequestType = {
                    isPrintBodyCompositionAndPhysicalFitness: isPrintBodyMass,
                    isPrintExercisePrescription: isPrintExercisePrescription,
                    isPrintInstrumentSetting: isPrintInstrumentSettings,
                    printInspectionReportCardiorespiratoryFitness: cardiorespiratoryFitnessImage,
                    printInspectionReportMachinaryOne: machinaryTrainingStatusOneImage,
                    printInspectionReportMachinaryTwo: machinaryTrainingStatusTwoImage,
                    printInspectionReportMachinaryThree: machinaryTrainingStatusThreeImage,
                    printInspectionReportMachinaryFour: machinaryTrainingStatusFourImage,
                    printInspectionReportMachinaryFive: machinaryTrainingStatusFiveImage,
                    printInspectionReportMachinarySix: machinaryTrainingStatusSixImage,
                    printInspectionReportMachinarySeven: machinaryTrainingStatusSevenImage,
                    printInspectionReportMachinaryEight: machinaryTrainingStatusEightImage,
                    printInspectionReportMachinaryNine: machinaryTrainingStatusNineImage,
                    printInspectionReportMachinaryTen: machinaryTrainingStatusTenImage,
                    printInspectionReportSoftness: softnessImage,
                    printInspectionReportBalance: balanceImage
                };

                printTrainingReport(storeGetters.studentId, data).then((res) => {
                    const url = window.URL.createObjectURL(new Blob([res.data]));
                    const link = document.createElement('a');
                    link.style.display = 'none';
                    link.href = url;

                    link.download = '等速肌力訓練報表.pdf';
                    document.body.appendChild(link);
                    link.click();
                    link.remove();
                }).catch((error) => {
                    const errorCode = axiosErrorCode<PrintTrainingReportErrorCodeType>(error);

                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');

                    switch (errorCode) {
                        case 'StudentDoesNotExist':
                            setVisibleInfoDialogMessage('查無學員');
                            break;
                        case 401:
                            dispatch({ type: 'user/resetToken' });
                            navigate('/home');
                            break;
                        default:
                            setVisibleInfoDialogMessage('伺服器錯誤');
                            break;
                    }
                });

                clearInterval(interval);
            }
        }, 1000);
    };

    const handleResize = (): void => {
        if (chartRef.current?.clientWidth !== undefined) {
            setChartWidth(chartRef.current.clientWidth - margin.left - margin.right);
            setChartHeight(window.innerHeight / 2 - margin.top - margin.bottom);
        }

        const formal_training_status_report = document.getElementsByClassName('formal_training_status_report')[0];
        if (formalTrainingStateRef.current?.clientWidth !== undefined) {
            // formalTrainingStateRef.current.clientWidth - formalTrainingStateMargin.left - formalTrainingStateMargin.right;
            setFormalTrainingStateWidth(formal_training_status_report.clientWidth);
            setFormalTrainingStateHeight(window.innerHeight / 2 - formalTrainingStateMargin.top - formalTrainingStateMargin.bottom);
        }

        const formal_training_power_report = document.getElementsByClassName('formal_training_power_report')[0];
        if (formalTrainingPowerRef.current?.clientWidth !== undefined) {
            // formalTrainingPowerRef.current.clientWidth - formalTrainingPowerMargin.left - formalTrainingPowerMargin.right;
            setFormalTrainingPowerWidth(formal_training_power_report.clientWidth);
            setFormalTrainingPowerHeight(window.innerHeight / 2 - formalTrainingPowerMargin.top - formalTrainingPowerMargin.bottom);
        }
    };

    const initWindowSize = () => {
        handleResize();
    };

    useEffect(() => {
        handlePrint();
        initWindowSize();

        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    useEffect(() => {
        drawReportCardiorespiratoryFitness();
    }, [drawReportCardiorespiratoryFitness]);

    useEffect(() => {
        drawReportSoftness();
    }, [drawReportSoftness]);

    useEffect(() => {
        drawReportBalance();
    }, [drawReportBalance]);



    useEffect(() => {
        drawFormalTrainingStatusReportOne('formal_training_status_report_one', responseStudentFormalTrainingStatusReportOne);
    }, [drawFormalTrainingStatusReportOne]);

    useEffect(() => {
        drawFormalTrainingStatusReportTwo('formal_training_status_report_two', responseStudentFormalTrainingStatusReportTwo);
    }, [drawFormalTrainingStatusReportTwo]);

    useEffect(() => {
        drawFormalTrainingStatusReportThree('formal_training_status_report_three', responseStudentFormalTrainingStatusReportThree);
    }, [drawFormalTrainingStatusReportThree]);

    useEffect(() => {
        drawFormalTrainingStatusReportFour('formal_training_status_report_four', responseStudentFormalTrainingStatusReportFour);
    }, [drawFormalTrainingStatusReportFour]);

    useEffect(() => {
        drawFormalTrainingStatusReportFive('formal_training_status_report_five', responseStudentFormalTrainingStatusReportFive);
    }, [drawFormalTrainingStatusReportFive]);

    useEffect(() => {
        drawFormalTrainingStatusReportSix('formal_training_status_report_six', responseStudentFormalTrainingStatusReportSix);
    }, [drawFormalTrainingStatusReportSix]);

    useEffect(() => {
        drawFormalTrainingStatusReportSeven('formal_training_status_report_seven', responseStudentFormalTrainingStatusReportSeven);
    }, [drawFormalTrainingStatusReportSeven]);

    useEffect(() => {
        drawFormalTrainingStatusReportEight('formal_training_status_report_eight', responseStudentFormalTrainingStatusReportEight);
    }, [drawFormalTrainingStatusReportEight]);

    useEffect(() => {
        drawFormalTrainingStatusReportNine('formal_training_status_report_nine', responseStudentFormalTrainingStatusReportNine);
    }, [drawFormalTrainingStatusReportNine]);

    useEffect(() => {
        drawFormalTrainingStatusReportTen('formal_training_status_report_ten', responseStudentFormalTrainingStatusReportTen);
    }, [drawFormalTrainingStatusReportTen]);



    useEffect(() => {
        drawFormalTrainingTotalPowerReportOne('formal_training_power_report_one', responseStudentTrainingPowerReportOne);
    }, [drawFormalTrainingTotalPowerReport]);

    useEffect(() => {
        drawFormalTrainingTotalPowerReportTwo('formal_training_power_report_two', responseStudentTrainingPowerReportTwo);
    }, [drawFormalTrainingTotalPowerReportTwo]);

    useEffect(() => {
        drawFormalTrainingTotalPowerReportThree('formal_training_power_report_three', responseStudentTrainingPowerReportThree);
    }, [drawFormalTrainingTotalPowerReportThree]);

    useEffect(() => {
        drawFormalTrainingTotalPowerReportFour('formal_training_power_report_four', responseStudentTrainingPowerReportFour);
    }, [drawFormalTrainingTotalPowerReportFour]);

    useEffect(() => {
        drawFormalTrainingTotalPowerReportFive('formal_training_power_report_five', responseStudentTrainingPowerReportFive);
    }, [drawFormalTrainingTotalPowerReportFive]);

    useEffect(() => {
        drawFormalTrainingTotalPowerReportSix('formal_training_power_report_six', responseStudentTrainingPowerReportSix);
    }, [drawFormalTrainingTotalPowerReportSix]);

    useEffect(() => {
        drawFormalTrainingTotalPowerReportSeven('formal_training_power_report_seven', responseStudentTrainingPowerReportSeven);
    }, [drawFormalTrainingTotalPowerReportSeven]);

    useEffect(() => {
        drawFormalTrainingTotalPowerReportEight('formal_training_power_report_eight', responseStudentTrainingPowerReportEight);
    }, [drawFormalTrainingTotalPowerReportEight]);

    useEffect(() => {
        drawFormalTrainingTotalPowerReportNine('formal_training_power_report_nine', responseStudentTrainingPowerReportNine);
    }, [drawFormalTrainingTotalPowerReportNine]);

    useEffect(() => {
        drawFormalTrainingTotalPowerReportTen('formal_training_power_report_ten', responseStudentTrainingPowerReportTen);
    }, [drawFormalTrainingTotalPowerReportTen]);

    return (
        <div className='print'>
            <div className='print_page'>
                <div className='print_page_content'>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                            <h2>請選擇要列印的項目</h2>
                        </div>
                    </div>
                </div>
            </div>

            {/* 使用者資訊 */}
            <div className='studentInfoBar'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-6/12 md:w-6/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <img className='student_logo' src={storeGetters.student_logo || user_logo} />
                    </div>

                    <div className='w-full sm:w-6/12 md:w-6/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <p>{storeGetters.name}</p>
                    </div>

                    <div className='w-full sm:w-6/12 md:w-6/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <p>{storeGetters.gender}</p>
                    </div>

                    <div className='w-full sm:w-6/12 md:w-6/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <p>{storeGetters.age}歲</p>
                    </div>

                    <div className='w-full sm:w-6/12 md:w-6/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <p>{storeGetters.posture}</p>
                    </div>

                    <div className='w-full sm:w-6/12 md:w-6/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <p>{storeGetters.medicalHistory}</p>
                    </div>

                    <div className='w-full sm:w-6/12 md:w-6/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <p className={`dangerGrading ${dangerColor(storeGetters.dangerGrading)}`}>{storeGetters.dangerGrading}</p>
                    </div>

                    <div className='w-full sm:w-6/12 md:w-6/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <p>{storeGetters.phone}</p>
                    </div>

                    <div className='w-full sm:w-6/12 md:w-6/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <p>{storeGetters.address}</p>
                    </div>
                </div>
            </div>

            {/* 身體組成與體適能評價 */}
            <PageTitleBar className='body_mass_and_physical_fitness' title='身體組成與體適能評價'></PageTitleBar>
            <div className='body_mass_and_physical_fitness_info'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                        <div className='card'>
                            <table>
                                <thead>
                                    <tr>
                                        <th>
                                            <p>項目</p>
                                        </th>

                                        <th colSpan={2}>
                                            <p>目前數值</p>
                                        </th>

                                        <th>
                                            <p>評價等級</p>
                                        </th>

                                        <th>
                                            <p>備註</p>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>BMI</td>
                                        <td className='bmiBar'></td>
                                        <td>{bmiGroup?.bmiCommentLevel !== '-' ? bmiGroup?.bmi : '-'}</td>
                                        <td className={`bmiComment ${bmiColor(bmiGroup?.bmiCommentLevel)}`}>{bmiGroup?.bmiCommentLevel}</td>
                                        <td>{bmiGroup?.mark}</td>
                                    </tr>

                                    <tr>
                                        <td>體脂肪</td>
                                        <td className='bodyFatBar'></td>
                                        <td>{bodyFatGroup?.bodyFatCommentLevel !== '-' ? bodyFatGroup?.bodyFat : '-'}</td>
                                        <td className={`bodyFatComment ${bodyFatColor(bodyFatGroup?.bodyFatCommentLevel)}`}>{bodyFatGroup?.bodyFatCommentLevel}</td>
                                        <td>{bodyFatGroup?.mark}</td>
                                    </tr>

                                    <tr>
                                        <td>內臟脂肪</td>
                                        <td className='visceralFatBar'></td>
                                        <td>{visceralFatGroup?.visceralFatCommentLevel !== '-' ? visceralFatGroup?.visceralFat : '-'}</td>
                                        <td className={`visceralFatComment ${visceralFatColor(visceralFatGroup?.visceralFatCommentLevel)}`}>{visceralFatGroup?.visceralFatCommentLevel}</td>
                                        <td>{visceralFatGroup?.mark}</td>
                                    </tr>

                                    <tr>
                                        <td>骨骼肌率</td>
                                        <td className='skeletalMuscleRateBar'></td>
                                        <td>{skeletalMuscleRateGroup?.skeletalMuscleRateCommentLevel !== '-' ? skeletalMuscleRateGroup?.skeletalMuscleRate : '-'}</td>
                                        <td className={`skeletalMuscleRatComment ${skeletalMuscleRateColor(skeletalMuscleRateGroup?.skeletalMuscleRateCommentLevel)}`}>{skeletalMuscleRateGroup?.skeletalMuscleRateCommentLevel}</td>
                                        <td className='mark'>{skeletalMuscleRateGroup?.mark}</td>
                                    </tr>

                                    <tr>
                                        <td>心肺適能</td>
                                        <td className='physicalFitnessCardiorespiratoryFitnessBar'></td>
                                        <td>-</td>
                                        <td className={`physicalFitnessCardiorespiratoryFitnessComment ${physicalFitnessCardiorespiratoryFitnessColor(physicalFitnessCardiorespiratoryFitnessGroup?.cardiorespiratoryFitnessComment)}`}>{physicalFitnessCardiorespiratoryFitnessGroup?.cardiorespiratoryFitnessComment}</td>
                                        <td>{physicalFitnessCardiorespiratoryFitnessGroup?.mark}</td>
                                    </tr>

                                    <tr>
                                        <td>肌力與肌耐力</td>
                                        <td className='physicalFitnessMuscleStrengthAndMuscleEnduranceBar'></td>
                                        <td>-</td>
                                        <td className={`physicalFitnessMuscleStrengthAndMuscleEnduranceComment ${physicalFitnessMuscleStrengthAndMuscleEnduranceColor(physicalFitnessMuscleStrengthAndMuscleEnduranceGroup?.muscleStrengthAndMuscleEnduranceComment)}`}>{physicalFitnessMuscleStrengthAndMuscleEnduranceGroup?.muscleStrengthAndMuscleEnduranceComment}</td>
                                        <td>{physicalFitnessMuscleStrengthAndMuscleEnduranceGroup?.mark}</td>
                                    </tr>

                                    <tr>
                                        <td>柔軟度</td>
                                        <td className='physicalFitnessSoftnessBar'></td>
                                        <td>-</td>
                                        <td className={`physicalFitnessSoftnessComment ${physicalFitnessSoftnessColor(physicalFitnessSoftnessGroup?.softnessComment)}`}>{physicalFitnessSoftnessGroup?.softnessComment}</td>
                                        <td>{physicalFitnessSoftnessGroup?.mark}</td>
                                    </tr>

                                    <tr>
                                        <td>平衡</td>
                                        <td className='physicalFitnessBalanceBar'></td>
                                        <td>-</td>
                                        <td className={`physicalFitnessBalanceComment ${physicalFitnessBalanceColor(physicalFitnessBalanceGroup?.balanceComment, storeGetters.age)}`}>{physicalFitnessBalanceGroup?.balanceComment}</td>
                                        <td>{physicalFitnessBalanceGroup?.mark}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <div className='card'>
                            {!isPrintBodyMass && <img src={btn_add} onClick={handleIsPrintBodyMass} />}
                            {isPrintBodyMass && <img src={btn_delete} onClick={handleIsPrintBodyMass} />}
                        </div>
                    </div>
                </div>
            </div>

            {/* 運動處方 */}
            <PageTitleBar className='exercise_prescription_title' title='運動處方'></PageTitleBar>
            <div className='exercisePrescription'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                        <div className='training_stage_info'>
                            <div className='training_stage_anchor'>
                            </div>

                            <div className='training_stage_progress'>
                                <h5 className='training_stage_anchor_text'>
                                    目前階段(第&nbsp;
                                    <CountUp end={trainingStageResponse?.week !== undefined && trainingStageResponse.week !== 0 ? trainingStageResponse.week : 1} duration={10} />
                                    &nbsp;週)
                                </h5>
                                <img className='training_stage_anchor' src={training_stage_anchor} />
                            </div>

                            <div className='training_stage'>
                                <div className='progress1' style={{ flex: `1 1 ${trainingFirstProgress}%` }}>

                                </div>

                                <div className='progress2' style={{ flex: `1 1 ${trainingMiddleProgress}%` }}>

                                </div>

                                <div className='progress3' style={{ flex: `1 1 ${trainingLastProgress}%` }}>

                                </div>
                            </div>

                            <div className='training_stage_text'>
                                <div className='progress1' style={{ flex: `1 1 ${trainingFirstProgress}%` }}>
                                    開始階段
                                </div>

                                <div className='progress2' style={{ flex: `1 1 ${trainingMiddleProgress}%` }}>
                                    改善階段
                                </div>

                                <div className='progress3' style={{ flex: `1 1 ${trainingLastProgress}%` }}>
                                    維持階段
                                </div>
                            </div>
                        </div>

                        <div className='exercise_prescription_info'>
                            <table>
                                <thead>
                                    <tr>
                                        <th>
                                            <p>項目</p>
                                        </th>

                                        <th>
                                            <p>訓練建議</p>
                                        </th>

                                        <th>
                                            <p>訓練強度</p>
                                        </th>

                                        <th>
                                            <p>訓練頻率</p>
                                        </th>

                                        <th>
                                            <p>備註</p>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>心肺適能</td>
                                        <td>
                                            {
                                                exercisePrescriptionCardiorespiratoryFitnessGroup?.sports.map((item, index) => {
                                                    return (
                                                        <p key={index}>{item}</p>
                                                    );
                                                })
                                            }
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionCardiorespiratoryFitnessGroup?.exerciseIntensity}</p>
                                            <p>{exercisePrescriptionCardiorespiratoryFitnessGroup?.heartRate}</p>
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionCardiorespiratoryFitnessGroup?.trainingFrequency}</p>
                                        </td>

                                        <td>{exercisePrescriptionCardiorespiratoryFitnessGroup?.mark}</td>
                                    </tr>

                                    <tr>
                                        <td>肌力與肌耐力</td>

                                        <td>
                                            肌力訓練
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup?.exerciseIntensity}</p>
                                            <p>{exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup?.heartRate}</p>
                                        </td>

                                        <td>
                                            {exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup?.trainingFrequency}
                                        </td>

                                        <td>
                                            {exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup?.mark}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>柔軟度</td>

                                        <td>
                                            {
                                                exercisePrescriptionSoftnessGroup?.sports.map((item, index) => {
                                                    return (
                                                        <p key={index}>{item}</p>
                                                    );
                                                })
                                            }
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionSoftnessGroup?.exerciseIntensity}</p>
                                            <p>{exercisePrescriptionSoftnessGroup?.heartRate}</p>
                                        </td>

                                        <td>
                                            {exercisePrescriptionSoftnessGroup?.trainingFrequency}
                                        </td>

                                        <td>
                                            {exercisePrescriptionSoftnessGroup?.mark}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>平衡</td>
                                        <td>
                                            {
                                                exercisePrescriptionBalanceGroup?.sports.map((item, index) => {
                                                    return (
                                                        <p key={index}>{item}</p>
                                                    );
                                                })
                                            }
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionBalanceGroup?.exerciseIntensity}</p>
                                            <p>{exercisePrescriptionBalanceGroup?.heartRate}</p>
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionBalanceGroup?.trainingFrequency}</p>
                                        </td>

                                        <td>
                                            <p>{exercisePrescriptionBalanceGroup?.mark}</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <div className='card'>
                            {!isPrintExercisePrescription && <img src={btn_add} onClick={handlePrintExercisePrescription} />}
                            {isPrintExercisePrescription && <img src={btn_delete} onClick={handlePrintExercisePrescription} />}
                        </div>
                    </div>
                </div>
            </div>

            {/* 儀器設定 */}
            <PageTitleBar className='instrument_settings_title' title='儀器設定'></PageTitleBar>
            <div className='instrument_settings_info'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                        <div className='card'>
                            <table>
                                <thead>
                                    <tr>
                                        <th colSpan={2}>
                                            <p>項目</p>
                                        </th>

                                        <th>
                                            <p>最適阻力等級</p>
                                        </th>

                                        <th>
                                            <p>活動範圍</p>
                                        </th>

                                        <th>
                                            <p>最大運動表現</p>
                                        </th>

                                        <th>
                                            <p>備註</p>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    {/* 機器編號一 */}
                                    {
                                        machinaryCodeOneGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeOne} />
                                                </td>

                                                <td>{machinaryCodeOneGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeOneGroup?.level}</td>
                                                <td>{machinaryCodeOneGroup?.maxRange}</td>
                                                <td>{machinaryCodeOneGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeOneGroup?.mark}</td>
                                            </tr>
                                        )
                                    }


                                    {/* 機器編號二 */}
                                    {
                                        machinaryCodeTwoGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeTwo} />
                                                </td>
                                                <td>{machinaryCodeTwoGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeTwoGroup?.level}</td>
                                                <td>{machinaryCodeTwoGroup?.maxRange}</td>
                                                <td>{machinaryCodeTwoGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeTwoGroup?.mark}</td>
                                            </tr>
                                        )
                                    }


                                    {/* 機器編號三 */}
                                    {
                                        machinaryCodeThreeGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeThree} />
                                                </td>
                                                <td>{machinaryCodeThreeGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeThreeGroup?.level}</td>
                                                <td>{machinaryCodeThreeGroup?.maxRange}</td>
                                                <td>{machinaryCodeThreeGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeThreeGroup?.mark}</td>
                                            </tr>
                                        )
                                    }


                                    {/* 機器編號四 */}
                                    {
                                        machinaryCodeFourGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeFour} />
                                                </td>
                                                <td>{machinaryCodeFourGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeFourGroup?.level}</td>
                                                <td>{machinaryCodeFourGroup?.maxRange}</td>
                                                <td>{machinaryCodeFourGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeFourGroup?.mark}</td>
                                            </tr>
                                        )
                                    }


                                    {/* 機器編號五 */}
                                    {
                                        machinaryCodeFiveGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeFive} />
                                                </td>
                                                <td>{machinaryCodeFiveGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeFiveGroup?.level}</td>
                                                <td>{machinaryCodeFiveGroup?.maxRange}</td>
                                                <td>{machinaryCodeFiveGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeFiveGroup?.mark}</td>
                                            </tr>
                                        )
                                    }

                                    {/* 機器編號六 */}
                                    {
                                        machinaryCodeSixGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeSix} />
                                                </td>
                                                <td>{machinaryCodeSixGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeSixGroup?.level}</td>
                                                <td>{machinaryCodeSixGroup?.maxRange}</td>
                                                <td>{machinaryCodeSixGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeSixGroup?.mark}</td>
                                            </tr>
                                        )
                                    }


                                    {/* 機器編號七 */}
                                    {
                                        machinaryCodeSevenGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeSeven} />
                                                </td>
                                                <td>{machinaryCodeSevenGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeSevenGroup?.level}</td>
                                                <td>{machinaryCodeSevenGroup?.maxRange}</td>
                                                <td>{machinaryCodeSevenGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeSevenGroup?.mark}</td>
                                            </tr>
                                        )
                                    }


                                    {/* 機器編號八 */}
                                    {
                                        machinaryCodeEightGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeEight} />
                                                </td>
                                                <td>{machinaryCodeEightGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeEightGroup?.level}</td>
                                                <td>{machinaryCodeEightGroup?.maxRange}</td>
                                                <td>{machinaryCodeEightGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeEightGroup?.mark}</td>
                                            </tr>
                                        )
                                    }


                                    {/* 機器編號九 */}
                                    {
                                        machinaryCodeNineGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeNine} />
                                                </td>
                                                <td>{machinaryCodeNineGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeNineGroup?.level}</td>
                                                <td>{machinaryCodeNineGroup?.maxRange}</td>
                                                <td>{machinaryCodeNineGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeNineGroup?.mark}</td>
                                            </tr>
                                        )
                                    }

                                    {/* 機器編號十 */}
                                    {
                                        machinaryCodeTenGroup?.userAuthorizedId !== 0 && (
                                            <tr>
                                                <td>
                                                    <img className='machinaryPicture' src={machinaryCodeTen} />
                                                </td>
                                                <td>{machinaryCodeTenGroup?.machinaryCodeName}</td>
                                                <td>{machinaryCodeTenGroup?.level}</td>
                                                <td>{machinaryCodeTenGroup?.maxRange}</td>
                                                <td>{machinaryCodeTenGroup?.power}</td>
                                                <td className='mark'>{machinaryCodeTenGroup?.mark}</td>
                                            </tr>
                                        )
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <div className='card'>
                            {!isPrintInstrumentSettings && <img src={btn_add} onClick={handlePrintInstrumentSettings} />}
                            {isPrintInstrumentSettings && <img src={btn_delete} onClick={handlePrintInstrumentSettings} />}
                        </div>
                    </div>
                </div>
            </div>

            {/* 檢視報表-心肺適能 */}
            <PageTitleBar className='inspection_report_title' title='檢視報表'></PageTitleBar>
            <div className='cardiorespiratoryFitness_chart_info'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12' ref={chartRef}>
                        <div id='cardiorespiratoryFitness_chart' className='card' ref={inspectionReportCardiorespiratoryFitnessRef}>
                            <div className='title'>
                                <img src={cardiorespiratoryFitnessIcon} />
                                <h2>心肺適能</h2>
                            </div>
                            <hr />

                            <div className='cardiorespiratoryFitnessChart'>

                            </div>
                        </div>
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <div className='card'>
                            {!isPrintInspectionReportCardiorespiratoryFitness && <img src={btn_add} onClick={handlePrintInspectionReportCardiorespiratoryFitness} />}
                            {isPrintInspectionReportCardiorespiratoryFitness && <img src={btn_delete} onClick={handlePrintInspectionReportCardiorespiratoryFitness} />}
                        </div>
                    </div>
                </div>
            </div>

            {/* 肌力與肌耐力標題 */}
            <div className='muscle_strength_and_muscle_endurance_info'>
                <div className='container mx-auto'>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                            <div className='card'>
                                <img src={muscle_strength_and_muscle_endurance} />
                                <h5>肌力與肌耐力</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* 機器訓練清單 */}
            <div className='machinaryList'>
                <div className='row'>
                    {
                        responseStudentEachMachinaryTotalPowerList.map((item, index) => {
                            return (
                                <div className='col' key={index}>
                                    <div className='card'>
                                        <div className='circle' onClick={() => handleStudentMachinaryTotalPower(item.machinaryCode, index)}>
                                            <img src={targetMachinaryPicture(item.machinaryCode)} />
                                        </div>
                                    </div>
                                </div>
                            );
                        })
                    }

                    <div className='col-12'>
                        <div className='card'>
                            <p>請選擇要列印的儀器</p>
                        </div>
                    </div>
                </div>
            </div>

            {/* 檢視報表-機器正式訓練一 */}
            {
                <div className='machinaryCodeOne' style={{ display: showMachinarySummaryListOne ? 'block' : 'none' }}>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                            <div className='machinary_status'>
                                <div className='circle' style={{ background: totalPercentageStyle(responseStudentMachinaryTotalPowerOne?.totalPower || 0) }}>
                                    <img className='machinary' src={targetMachinaryPicture(1)} />
                                </div>
                                <h2 className='machinaryName'>{getMachinaryName(1)}</h2>

                                <img className='muscleDistribution' src={targetMuscleDistribution(1)} />
                            </div>
                        </div>

                        <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                            <div className='flex flex-wrap'>
                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_status_report'>
                                        <div className='formal_training_status_report_one' ref={formalTrainingStateRef}>

                                        </div>
                                    </div>
                                </div>

                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_power_report'>
                                        <div className='formal_training_power_report_one' ref={formalTrainingPowerRef}>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }

            {/* 檢視報表-機器正式訓練二 */}
            {
                <div className='machinaryCodeTwo' style={{ display: showMachinarySummaryListTwo ? 'block' : 'none' }}>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                            <div className='machinary_status'>
                                <div className='circle' style={{ background: totalPercentageStyle(responseStudentMachinaryTotalPowerTwo?.totalPower || 0) }}>
                                    <img className='machinary' src={targetMachinaryPicture(2)} />
                                </div>
                                <h2 className='machinaryName'>{getMachinaryName(2)}</h2>

                                <img className='muscleDistribution' src={targetMuscleDistribution(2)} />
                            </div>
                        </div>

                        <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                            <div className='flex flex-wrap'>
                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_status_report'>
                                        <div className='formal_training_status_report_two'>

                                        </div>
                                    </div>
                                </div>

                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_power_report'>
                                        <div className='formal_training_power_report_two'>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }

            {/* 檢視報表-機器正式訓練三 */}
            {
                <div className='machinaryCodeThree' style={{ display: showMachinarySummaryListThree ? 'block' : 'none' }}>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                            <div className='machinary_status'>
                                <div className='circle' style={{ background: totalPercentageStyle(responseStudentMachinaryTotalPowerThree?.totalPower || 0) }}>
                                    <img className='machinary' src={targetMachinaryPicture(3)} />
                                </div>
                                <h2 className='machinaryName'>{getMachinaryName(3)}</h2>

                                <img className='muscleDistribution' src={targetMuscleDistribution(3)} />
                            </div>
                        </div>

                        <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                            <div className='flex flex-wrap'>
                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_status_report'>
                                        <div className='formal_training_status_report_three'>

                                        </div>
                                    </div>
                                </div>

                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_power_report'>
                                        <div className='formal_training_power_report_three'>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }

            {/* 檢視報表-機器正式訓練四 */}
            {
                <div className='machinaryCodeFour' style={{ display: showMachinarySummaryListFour ? 'block' : 'none' }}>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                            <div className='machinary_status'>
                                <div className='circle' style={{ background: totalPercentageStyle(responseStudentMachinaryTotalPowerFour?.totalPower || 0) }}>
                                    <img className='machinary' src={targetMachinaryPicture(4)} />
                                </div>
                                <h2 className='machinaryName'>{getMachinaryName(4)}</h2>

                                <img className='muscleDistribution' src={targetMuscleDistribution(4)} />
                            </div>
                        </div>

                        <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                            <div className='flex flex-wrap'>
                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_status_report'>
                                        <div className='formal_training_status_report_four'>

                                        </div>
                                    </div>
                                </div>

                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_power_report'>
                                        <div className='formal_training_power_report_four'>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }

            {/* 檢視報表-機器正式訓練五 */}
            {
                <div className='machinaryCodeFive' style={{ display: showMachinarySummaryListFive ? 'block' : 'none' }}>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                            <div className='machinary_status'>
                                <div className='circle' style={{ background: totalPercentageStyle(responseStudentMachinaryTotalPowerFive?.totalPower || 0) }}>
                                    <img className='machinary' src={targetMachinaryPicture(5)} />
                                </div>
                                <h2 className='machinaryName'>{getMachinaryName(5)}</h2>

                                <img className='muscleDistribution' src={targetMuscleDistribution(5)} />
                            </div>
                        </div>

                        <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                            <div className='card'>
                                <div className='flex flex-wrap'>
                                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                        <div className='formal_training_status_report'>
                                            <div className='formal_training_status_report_five'>

                                            </div>
                                        </div>
                                    </div>

                                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                        <div className='formal_training_power_report'>
                                            <div className='formal_training_power_report_five'>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }

            {/* 檢視報表-機器正式訓練六 */}
            {
                <div className='machinaryCodeSix' style={{ display: showMachinarySummaryListSix ? 'block' : 'none' }}>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                            <div className='machinary_status'>
                                <div className='circle' style={{ background: totalPercentageStyle(responseStudentMachinaryTotalPowerSix?.totalPower || 0) }}>
                                    <img className='machinary' src={targetMachinaryPicture(6)} />
                                </div>
                                <h2 className='machinaryName'>{getMachinaryName(6)}</h2>

                                <img className='muscleDistribution' src={targetMuscleDistribution(6)} />
                            </div>
                        </div>

                        <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                            <div className='flex flex-wrap'>
                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_status_report'>
                                        <div className='formal_training_status_report_six'>

                                        </div>
                                    </div>
                                </div>

                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_power_report'>
                                        <div className='formal_training_power_report_six'>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }

            {/* 檢視報表-機器正式訓練七 */}
            {
                <div className='machinaryCodeSeven' style={{ display: showMachinarySummaryListSeven ? 'block' : 'none' }}>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                            <div className='machinary_status'>
                                <div className='circle' style={{ background: totalPercentageStyle(responseStudentMachinaryTotalPowerSeven?.totalPower || 0) }}>
                                    <img className='machinary' src={targetMachinaryPicture(7)} />
                                </div>
                                <h2 className='machinaryName'>{getMachinaryName(7)}</h2>

                                <img className='muscleDistribution' src={targetMuscleDistribution(7)} />
                            </div>
                        </div>

                        <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                            <div className='flex flex-wrap'>
                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_status_report'>
                                        <div className='formal_training_status_report_seven'>

                                        </div>
                                    </div>
                                </div>

                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_power_report'>
                                        <div className='formal_training_power_report_seven'>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }


            {/* 檢視報表-機器正式訓練八 */}
            {
                <div className='machinaryCodeEight' style={{ display: showMachinarySummaryListEight ? 'block' : 'none' }}>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                            <div className='machinary_status'>
                                <div className='circle' style={{ background: totalPercentageStyle(responseStudentMachinaryTotalPowerEight?.totalPower || 0) }}>
                                    <img className='machinary' src={targetMachinaryPicture(8)} />
                                </div>
                                <h2 className='machinaryName'>{getMachinaryName(8)}</h2>

                                <img className='muscleDistribution' src={targetMuscleDistribution(8)} />
                            </div>
                        </div>

                        <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                            <div className='flex flex-wrap'>
                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_status_report'>
                                        <div className='formal_training_status_report_eight'>

                                        </div>
                                    </div>
                                </div>

                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_power_report'>
                                        <div className='formal_training_power_report_eight'>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }

            {/* 檢視報表-機器正式訓練九 */}
            {
                <div className='machinaryCodeNine' style={{ display: showMachinarySummaryListNine ? 'block' : 'none' }}>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                            <div className='machinary_status'>
                                <div className='circle' style={{ background: totalPercentageStyle(responseStudentMachinaryTotalPowerNine?.totalPower || 0) }}>
                                    <img className='machinary' src={targetMachinaryPicture(9)} />
                                </div>
                                <h2 className='machinaryName'>{getMachinaryName(9)}</h2>

                                <img className='muscleDistribution' src={targetMuscleDistribution(9)} />
                            </div>
                        </div>

                        <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                            <div className='flex flex-wrap'>
                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_status_report'>
                                        <div className='formal_training_status_report_nine'>

                                        </div>
                                    </div>
                                </div>

                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_power_report'>
                                        <div className='formal_training_power_report_nine'>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }

            {/* 檢視報表-機器正式訓練十 */}
            {
                <div className='machinaryCodeTen' style={{ display: showMachinarySummaryListTen ? 'block' : 'none' }}>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                            <div className='machinary_status'>
                                <div className='circle' style={{ background: totalPercentageStyle(responseStudentMachinaryTotalPowerTen?.totalPower || 0) }}>
                                    <img className='machinary' src={targetMachinaryPicture(10)} />
                                </div>
                                <h2 className='machinaryName'>{getMachinaryName(10)}</h2>

                                <img className='muscleDistribution' src={targetMuscleDistribution(10)} />
                            </div>
                        </div>

                        <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                            <div className='flex flex-wrap'>
                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_status_report'>
                                        <div className='formal_training_status_report_ten'>

                                        </div>
                                    </div>
                                </div>

                                <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                                    <div className='formal_training_power_report'>
                                        <div className='formal_training_power_report_ten'>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }

            {/* 檢視報表-柔軟度 */}
            <div className='softness_chart_info'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                        <div className='card' ref={inspectionReportSoftnessRef}>
                            <div className='title'>
                                <img src={softnessIcon} />
                                <h2>柔軟度</h2>
                            </div>
                            <hr />

                            <div className='softnessChart'>

                            </div>
                        </div>
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <div className='card'>
                            {!isPrintInspectionReportSoftness && <img src={btn_add} onClick={handlePrintInspectionReportSoftness} />}
                            {isPrintInspectionReportSoftness && <img src={btn_delete} onClick={handlePrintInspectionReportSoftness} />}
                        </div>
                    </div>
                </div>
            </div>

            {/* 檢視報表-平衡 */}
            <div className='balance_chart_info'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-10/12 md:w-10/12 lg:w-10/12 xl:w-10/12 xxl:w-10/12'>
                        <div className='card' ref={inspectionReportBalanceRef}>
                            <div className='title'>
                                <img src={balanceIcon} />
                                <h2>平衡</h2>
                            </div>
                            <hr />

                            <div className='balanceChart'>

                            </div>
                        </div>
                    </div>

                    <div className='w-full sm:w-2/12 md:w-2/12 lg:w-2/12 xl:w-2/12 xxl:w-2/12'>
                        <div className='card'>
                            {!isPrintInspectionReportBalance && <img src={btn_add} onClick={handlePrintInspectionReportBalance} />}
                            {isPrintInspectionReportBalance && <img src={btn_delete} onClick={handlePrintInspectionReportBalance} />}
                        </div>
                    </div>
                </div>
            </div>

            <div className='confirm_page'>
                <div className='container mx-auto'>
                    <div className='flex flex-wrap'>
                        <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                            <div className='card'>
                                <button className='btn_confirm' onClick={confirmPrint}>確定</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* 資訊彈窗 */}
            {
                visibleInfoDialog && (
                    <Dialog className='visibleInfoDialog'>
                        <Dialog.Title title={visibleInfoDialogTitle}></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <p>{visibleInfoDialogMessage}</p>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnConfirm' onClick={closeInfoDialog}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }
        </div>
    );
};

export default Print;