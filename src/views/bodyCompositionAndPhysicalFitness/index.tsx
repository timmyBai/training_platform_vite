import React, { FormEvent, Fragment, ReactNode, useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import validate from 'validate.js';
import * as d3 from 'd3';

// css
import './index.sass';
import colors from '@/styles/colors.module.sass';

// hook
import { useAppSelector } from '@/hook/store';

// components
import StudentInfoBar from '@/components/StudentInfoBar';

// api
import { bodyMassAndPhysicalFitnessList } from '@/api/bodyCompositionAndPhysicalFitness';
import { bodyCompositionBmiAndMarkReplace, bodyCompositionBodyFatAndMarkReplace, bodyCompositionVisceralFatAndMarkReplace, bodyCompositionSkeletalMuscleRateAndMarkReplace } from '@/api/bodyComposition';
import {
    replacePhysicalFitnessCardiorespiratoryFitnessStepTestAndMark,
    replacePhysicalFitnessCardiorespiratoryFitnessStandingKneeRaise,
    replacePhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequency,
    replacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequency,
    replacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequency,
    replacePhysicalFitnessSoftnessSeatedForwardBend,
    replacePhysicalFitnessSoftnessChairSeatedForwardBend,
    replacePhysicalFitnessSoftnessPullBackTest,
    replacePhysicalFitnessBalanceAdultEyeOpeningMonopodSecond,
    replacePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecond,
    replacePhysicalFitnessBalanceSittingAroundOneSecond,
    replacePhysicalFitnessBalanceSittingAroundTwoSecond
} from '@/api/physicalFitness';

// utils
import { axiosErrorCode } from '@/utils/request';
import { bodyCompositionBmiCommentConvertFraction, bodyCompositionBodyFatCommentConvertFraction, bodyCompositionVisceralFatCommentConvertFraction, bodyCompositioSkeletalMuscleRateCommentConvertFraction } from '@/utils/bodyComposition';
import { bmiColor, bodyFatColor, skeletalMuscleRateColor, visceralFatColor } from '@/utils/bodyCompositionColor';
import { physicalFitnessCardiorespiratoryFitnessColor, physicalFitnessMuscleStrengthAndMuscleEnduranceColor, physicalFitnessSoftnessColor, physicalFitnessBalanceColor } from '@/utils/physicalFitnessColor';
import { isStudentGreaterThan65 } from '@/utils/student';

// images
import btn_edit from '@/assets/images/btn_edit.svg';

// components
import TdArrowDown from '@/components/Table/TdArrowDown';
import Dialog from '@/components/Dialog';
import PageTitleBar from '@/components/PageTitleBar';

const BodyMassRecord: React.FC = (): ReactNode => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const bodyCompositionFormRef = useRef<HTMLFormElement>(null);
    const physicalFitnessCardiorespiratoryFitnessFormRef = useRef<HTMLFormElement>(null); // 體適能-心肺適能彈窗
    const physicalFitnessMscleStrengthAndMuscleEnduranceFormRef = useRef<HTMLFormElement>(null); // 體適能-肌力與肌耐力彈窗
    const physicalFitnessSoftnessFormRef = useRef<HTMLFormElement>(null); // 體適能-柔軟度彈窗
    const physicalFitnessBalanceFormRef = useRef<HTMLFormElement>(null); // 體適能平衡彈窗

    const [bmiGroup, setBmiGroup] = useState<BmiGroupResponseType>(); // 身體組成-bmi 群組資料
    const [bodyFatGroup, setBodyFatGroup] = useState<BodyFatGroupResponseType>(); // 身體組成-體脂肪群組資料
    const [visceralFatGroup, setVisceralFatGroup] = useState<VisceralFatGroupResponseType>(); // 身體組成-內臟脂肪群組資料
    const [skeletalMuscleRateGroup, setSkeletalMuscleRateGroup] = useState<SkeletalMuscleRateGroupResponseType>(); // 身體組成-骨骼心肌率群組資料
    const [physicalFitnessCardiorespiratoryFitnessGroup, setPhysicalFitnessCardiorespiratoryFitnessGroup] = useState<PhysicalFitnessCardiorespiratoryFitnessGroupResponseType>(); // 體適能-心肺適能群組資料
    const [physicalFitnessMuscleStrengthAndMuscleEnduranceGroup, setPhysicalFitnessMuscleStrengthAndMuscleEnduranceGroup] = useState<PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupResponseType>(); // 體適能-肌力與肌耐力群組資料
    const [physicalFitnessSoftnessGroup, setPhysicalFitnessSoftnessGroup] = useState<PhysicalFitnessSoftnessGroupResponseType>(); // 體適能-柔軟度群組資料
    const [physicalFitnessBalanceGroup, setPhysicalFitnessBalanceGroup] = useState<PhysicalFitnessBalanceGroupResponseType>(); // 體適能-平衡群組資料

    const [inputBodyCompositionValue, setInputBodyCompositionValue] = useState<number | string>(); // 身體組成 數值 欄位
    const [inputBodyCompositionMark, setInputBodyCompositionMark] = useState<string | undefined>(undefined); // 身體組成 備註 欄位

    const [inputBodyCompositionValueErrorMessage, setInputBodyCompositionValueErrorMessage] = useState<string>(''); // 身體組成 數值 欄位錯誤訊息
    const [inputBodyCompositionMarkErrorMessage, setInputBodyCompositionMarkErrorMessage] = useState<string>(''); // 身體組成 備註 欄位錯誤訊息

    const [visableBodyCompositionDialog, setVisableBodyCompositionDialog] = useState<boolean>(false); // 新增或修改身體組成彈窗
    const [editBodyCompositionKey, setEditBodyCompositionKey] = useState<EditBodyCompositionKeyType>('bmi'); // 儲存要更新哪一項身體組成

    const [visiblePhysicalFitnessCardiorespiratoryFitnessDialog, setVisiblePhysicalFitnessCardiorespiratoryFitnessDialog] = useState<boolean>(false); // 顯示或隱藏體適能心肺適能彈窗
    const [physicalFitnessCardiorespiratoryFitnessPulseRateOne, setPhysicalFitnessCardiorespiratoryFitnessPulseRateOne] = useState<number | undefined>(undefined); // 體適能-心肺適能第一次脈搏次數
    const [physicalFitnessCardiorespiratoryFitnessPulseRateTwo, setPhysicalFitnessCardiorespiratoryFitnessPulseRateTwo] = useState<number | undefined>(undefined); // 體適能-心肺適能第二次脈搏次數
    const [physicalFitnessCardiorespiratoryFitnessPulseRateThree, setPhysicalFitnessCardiorespiratoryFitnessPulseRateThree] = useState<number | undefined>(undefined); // 體適能-心肺適能第三次脈搏次數
    const [physicalFitnessCardiorespiratoryFitnessKneeLiftFrequency, setPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequency] = useState<number | undefined>(undefined); // 體適能-原地抬膝次數
    const [physicalFitnessCardiorespiratoryFitnessMark, setPhysicalFitnessCardiorespiratoryFitnessMark] = useState<string | undefined>(undefined); // 體適能-心肺適能備註

    const [physicalFitnessCardiorespiratoryFitnessPulseRateOneErrorMessage, setPhysicalFitnessCardiorespiratoryFitnessPulseRateOneErrorMessage] = useState<string>(''); // 體適能-心肺適能第一次脈搏次數 錯誤訊息
    const [physicalFitnessCardiorespiratoryFitnessPulseRateTwoErrorMessage, setPhysicalFitnessCardiorespiratoryFitnessPulseRateTwoErrorMessage] = useState<string>(''); // 體適能-心肺適能第二次脈搏次數 錯誤訊息
    const [physicalFitnessCardiorespiratoryFitnessPulseRateThreeErrorMessage, setPhysicalFitnessCardiorespiratoryFitnessPulseRateThreeErrorMessage] = useState<string>(''); // 體適能-心肺適能第三次脈搏次數 錯誤訊息
    const [physicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyErrorMessage, setPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyErrorMessage] = useState<string>(''); // 體適能-心肺適能第三次脈搏次數 錯誤訊息
    const [physicalFitnessCardiorespiratoryFitnessMarkErrorMessage, setPhysicalFitnessCardiorespiratoryFitnessMarkErrorMessage] = useState<string>(''); // 體適能-心肺適能備註 錯誤訊息

    const [visiblePhysicalFitnessMuscleStrengthAndMuscleEnduranceDialog, setVisiblePhysicalFitnessMuscleStrengthAndMuscleEnduranceDialog] = useState<boolean>(false); // 顯示或隱藏體適能-肌力與肌耐力彈窗
    const [physicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequency, setPhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequency] = useState<number | undefined>(undefined); // 體適能-肌力與肌耐力原地站立抬膝
    const [physicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequency, setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequency] = useState<number | undefined>(undefined); // 體適能-肌力與肌耐力椅子坐姿體前彎
    const [physicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequency, setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequency] = useState<number | undefined>(undefined); // 體適能-肌力與肌耐力起立坐下
    const [physicalFitnessMscleStrengthAndMuscleEnduranceMark, setPhysicalFitnessMscleStrengthAndMuscleEnduranceMark] = useState<string | undefined>(undefined); // 體適能-肌力與肌耐力備註

    const [physicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyErrorMessage, setPhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyErrorMessage] = useState<string>(''); // 體適能-肌力與肌耐力原地站立抬膝 錯誤訊息
    const [physicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyErrorMessage, setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyErrorMessage] = useState<string>(''); // 體適能-肌力與肌耐力椅子坐姿體前彎 錯誤訊息
    const [physicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyErrorMessage, setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyErrorMessage] = useState<string>(''); // 體適能-肌力與肌耐力起立坐下 錯誤訊息
    const [physicalFitnessMscleStrengthAndMuscleEnduranceMarkErrorMessage, setPhysicalFitnessMscleStrengthAndMuscleEnduranceMarkErrorMessage] = useState<string>(''); // 體適能-肌力與肌耐力備註 錯誤訊息

    const [visiblePhysicalFitnessSoftnessDialog, setVisiblePhysicalFitnessSoftnessDialog] = useState<boolean>(false); // 顯示或隱藏體適能-柔軟度彈窗
    const [physicalFitnessSoftnessSeatedForwardBendOne, setPhysicalFitnessSoftnessSeatedForwardBendOne] = useState<number | undefined>(undefined); // 體適能-柔軟度第一次坐姿體前彎
    const [physicalFitnessSoftnessSeatedForwardBendTwo, setPhysicalFitnessSoftnessSeatedForwardBendTwo] = useState<number | undefined>(undefined); // 體適能-柔軟度第二次坐姿體前彎
    const [physicalFitnessSoftnessSeatedForwardBendThree, setPhysicalFitnessSoftnessSeatedForwardBendThree] = useState<number | undefined>(undefined); // 體適能-柔軟度第三次坐姿體前彎
    const [physicalFitnessSoftnessChairSeatedForwardBendOne, setPhysicalFitnessSoftnessChairSeatedForwardBendOne] = useState<number | undefined>(undefined); // 體適能-柔軟度第一次椅子坐姿體前彎
    const [physicalFitnessSoftnessChairSeatedForwardBendTwo, setPhysicalFitnessSoftnessChairSeatedForwardBendTwo] = useState<number | undefined>(undefined); // 體適能-柔軟度第二次椅子坐姿體前彎
    const [physicalFitnessSoftnessChairSeatedForwardBendThree, setPhysicalFitnessSoftnessChairSeatedForwardBendThree] = useState<number | undefined>(undefined); // 體適能-柔軟度第三次椅子坐姿體前彎
    const [physicalFitnessSoftnessPullBackTestOne, setPhysicalFitnessSoftnessPullBackTestOne] = useState<number | undefined>(undefined); // 體適能-柔軟度第一次拉背測驗
    const [physicalFitnessSoftnessPullBackTestTwo, setPhysicalFitnessSoftnessPullBackTestTwo] = useState<number | undefined>(undefined); // 體適能-柔軟度第二次拉背測驗
    const [physicalFitnessSoftnessMark, setPhysicalFitnessSoftnessMark] = useState<string | undefined>(undefined); // 體適能-柔軟度備註

    const [physicalFitnessSoftnessSeatedForwardBendOneErrorMessage, setPhysicalFitnessSoftnessSeatedForwardBendOneErrorMessage] = useState<string>(''); // 體適能-柔軟度第一次坐姿體前彎 錯誤訊息
    const [physicalFitnessSoftnessSeatedForwardBendTwoErrorMessage, setPhysicalFitnessSoftnessSeatedForwardBendTwoErrorMessage] = useState<string>(''); // 體適能-柔軟度第二次坐姿體前彎 錯誤訊息
    const [physicalFitnessSoftnessSeatedForwardBendThreeErrorMessage, setPhysicalFitnessSoftnessSeatedForwardBendThreeErrorMessage] = useState<string>(''); // 體適能-柔軟度第三次坐姿體前彎 錯誤訊息
    const [physicalFitnessSoftnessChairSeatedForwardBendOneErrorMessage, setPhysicalFitnessSoftnessChairSeatedForwardBendOneErrorMessage] = useState<string>(''); // 體適能-柔軟度第一次椅子坐姿體前彎 錯誤訊息
    const [physicalFitnessSoftnessChairSeatedForwardBendTwoErrorMessage, setPhysicalFitnessSoftnessChairSeatedForwardBendTwoErrorMessage] = useState<string>(''); // 體適能-柔軟度第二次椅子坐姿體前彎 錯誤訊息
    const [physicalFitnessSoftnessChairSeatedForwardBendThreeErrorMessage, setPhysicalFitnessSoftnessChairSeatedForwardBendThreeErrorMessage] = useState<string>(''); // 體適能-柔軟度第三次椅子坐姿體前彎 錯誤訊息
    const [physicalFitnessSoftnessPullBackTestOneErrorMessage, setPhysicalFitnessSoftnessPullBackTestOneErrorMessage] = useState<string>(''); // 體適能-柔軟度第一次拉背測驗 錯誤訊息
    const [physicalFitnessSoftnessPullBackTestTwoErrorMessage, setPhysicalFitnessSoftnessPullBackTestTwoErrorMessage] = useState<string>(''); // 體適能-柔軟度第二次拉背測驗 錯誤訊息
    const [physicalFitnessSoftnessMarkErrorMessage, setPhysicalFitnessSoftnessMarkErrorMessage] = useState<string>(''); // 體適能-柔軟度備註 錯誤訊息

    const [visiblePhysicalFitnessBalanceDialog, setVisiblePhysicalFitnessBalanceDialog] = useState<boolean>(false); // 顯示或隱藏體適能-柔軟度彈窗
    const [physicalFitnessBalanceAdultEyeOpeningMonopodSecond, setPhysicalFitnessBalanceAdultEyeOpeningMonopodSecond] = useState<number | undefined>(undefined); // 體適能-平衡壯年開眼單足立
    const [physicalFitnessBalanceElderlyEyeOpeningMonopodSecond, setPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecond] = useState<number | undefined>(undefined); // 體適能-平衡老年開眼單足立
    const [physicalFitnessBalanceSittingAroundOneSecond, setPhysicalFitnessBalanceSittingAroundOneSecond] = useState<number | undefined>(undefined); // 體適能-平衡第一次坐立繞物
    const [physicalFitnessBalanceSittingAroundTwoSecond, setPhysicalFitnessBalanceSittingAroundTwoSecond] = useState<number | undefined>(undefined); // 體適能-平衡第二次坐立繞物
    const [physicalFitnessBalanceMark, setPhysicalFitnessBalanceMark] = useState<string | undefined>(undefined); // 體適能-平衡備注

    const [physicalFitnessBalanceAdultEyeOpeningMonopodSecondErrorMessage, setPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondErrorMessage] = useState<string>(''); // 體適能-平衡壯年開眼單足立 錯誤訊息
    const [physicalFitnessBalanceElderlyEyeOpeningMonopodSecondErrorMessage, setPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondErrorMessage] = useState<string>(''); // 體適能-平衡老年開眼單足立 錯誤訊息
    const [physicalFitnessBalanceSittingAroundOneSecondErrorMessage, setPhysicalFitnessBalanceSittingAroundOneSecondErrorMessage] = useState<string>(''); // 體適能-平衡第一次坐立繞物 錯誤訊息
    const [physicalFitnessBalanceSittingAroundTwoSecondErrorMessage, setPhysicalFitnessBalanceSittingAroundTwoSecondErrorMessage] = useState<string>(''); // 體適能-平衡第二次坐立繞物 錯誤訊息
    const [physicalFitnessBalanceMarkErrorMessage, setPhysicalFitnessBalanceMarkErrorMessage] = useState<string>(''); // 體適能-平衡備注 錯誤訊息


    const [visableInfoDialog, setVisableInfoDialog] = useState<boolean>(false); // 資訊彈窗
    const [visableInfoDialogTitle, setVisableInfoDialogTitle] = useState<string>('');
    const [visableInfoDialogMessage, setVisableInfoDialogMessage] = useState<string>('');

    const formBodyCompositionMark = {
        'value': {
            presence: {
                message: '請輸入數值'
            },
            format: {
                pattern: '^[0-9]+$',
                message: '請輸入整數'
            }
        },
        'mark': {
            length: {
                maximum: 150,
                message: '備註最大 150 字'
            }
        }
    };

    // 壯年體適能心肺適能-驗證表
    const physicalFitnessCardiorespiratoryFitnessFormAdultValidate = {
        pulseRateOne: {
            presence: {
                message: '必須輸入第一次脈搏'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        pulseRateTwo: {
            presence: {
                message: '必須輸入第二次脈搏'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        pulseRateThree: {
            presence: {
                message: '必須輸入第三次脈搏'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        mark: {
            length: {
                maximum: 150,
                message: '備註最長 150 字'
            }
        }
    };

    // 老年體適能心肺適能-驗證表
    const physicalFitnessCardiorespiratoryFitnessFormElderlyValidate = {
        kneeLiftFrequency: {
            presence: {
                message: '必須輸入原地站立抬膝'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        mark: {
            length: {
                maximum: 150,
                message: '備註最長 150 字'
            }
        }
    };

    // 壯年體適能肌力與肌耐力-驗證表
    const physicalFitnessMuscleStrengthAndMuscleEnduranceFormAdultValidate = {
        kneeCrunchesFrequency: {
            presence: {
                message: '必須輸入屈膝仰臥起坐'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        mark: {
            length: {
                maximum: 150,
                message: '備註最長 150 字'
            }
        }
    };

    // 老年體適能肌力與肌耐力-驗證表
    const physicalFitnessMuscleStrengthAndMuscleEnduranceFormElderlyValidate = {
        armCurlUpperBodyFrequency: {
            presence: {
                message: '必須輸入手臂彎舉(上肢)'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        armCurlLowerBodyFrequency: {
            presence: {
                message: '必須輸入手臂彎舉(下肢)'
            },
            format: {
                pattern: '[0-9]+',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        mark: {
            length: {
                maximum: 150,
                message: '備註最長 150 字'
            }
        }
    };

    // 壯年體適能柔軟度-驗證表
    const physicalFitnessSoftnessAdultValidate = {
        seatedForwardBendOne: {
            presence: {
                message: '必須輸入第一次坐姿體前彎'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        seatedForwardBendTwo: {
            presence: {
                message: '必須輸入第二次坐姿體前彎'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        seatedForwardBendThree: {
            presence: {
                message: '必須輸入第三次坐姿體前彎'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        mark: {
            length: {
                maximum: 150,
                message: '備註最長 150 字'
            }
        }
    };

    // 老年體適能柔軟度-驗證表
    const physicalFitnessSoftnessElderlyValidate = {
        chairSeatedForwardBendOne: {
            presence: {
                message: '必須輸入第一次椅子坐姿體前彎'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        chairSeatedForwardBendTwo: {
            presence: {
                message: '必須輸入第二次椅子坐姿體前彎'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        chairSeatedForwardBendThree: {
            presence: {
                message: '必須輸入第三次椅子坐姿體前彎'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        pullBackTestOne: {
            presence: {
                message: '必須輸入第一次拉背測驗'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        pullBackTestTwo: {
            presence: {
                message: '必須輸入第二次拉背測驗'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        mark: {
            length: {
                maximum: 150,
                message: '備註最長 150 字'
            }
        }
    };

    // 壯年體適能平衡-驗證表
    const physicalFitnessBalanceFormAdultValidate = {
        adultEyeOpeningMonopodSecond: {
            presence: {
                message: '必須輸入壯年開眼單足立'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        mark: {
            length: {
                maximum: 150,
                message: '備註最長 150 字'
            }
        }
    };

    // 老年體適能平衡-驗證表
    const physicalFitnessBalanceFormElderlyValidate = {
        elderlyEyeOpeningMonopodSecond: {
            presence: {
                message: '必須輸入老年開眼單足立'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        sittingAroundOneSecond: {
            presence: {
                message: '必須輸入第一次坐立繞物'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        sittingAroundTwoSecond: {
            presence: {
                message: '必須輸入第二次坐立繞物'
            },
            format: {
                pattern: '^([0-9]+|[0-9]+.[0-9]+)$',
                flags: 'g',
                message: '請輸入數值'
            }
        },
        mark: {
            length: {
                maximum: 150,
                message: '備註最長 150 字'
            }
        }
    };

    const storeGetters = useAppSelector((state) => {
        return {
            studentId: state.studentInfo.studentId,
            age: state.studentInfo.age
        };
    });

    // 學員身體組成與體適能清單
    const handleBodyMassAndPhysicalFitnessList = (): void => {
        bodyMassAndPhysicalFitnessList(storeGetters.studentId).then((res) => {
            const { data } = res.data;

            setBmiGroup(data.bmiGroup);
            setBodyFatGroup(data.bodyFatGroup);
            setVisceralFatGroup(data.visceralFatGroup);
            setSkeletalMuscleRateGroup(data.skeletalMuscleRateGroup);
            setPhysicalFitnessCardiorespiratoryFitnessGroup(data.physicalFitnessCardiorespiratoryFitnessGroup);
            setPhysicalFitnessMuscleStrengthAndMuscleEnduranceGroup(data.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup);
            setPhysicalFitnessSoftnessGroup(data.physicalFitnessSoftnessGroup);
            setPhysicalFitnessBalanceGroup(data.physicalFitnessBalanceGroup);

            // bmi 繪製百分等級
            const bmiValue = bodyCompositionBmiCommentConvertFraction(data.bmiGroup.bmiCommentLevel);
            drawProgressBar('.bmiBar', bmiValue);

            // 體脂肪 繪製百分等級
            const bodyFatValue = bodyCompositionBodyFatCommentConvertFraction(data.bodyFatGroup.bodyFatCommentLevel);
            drawProgressBar('.bodyFatBar', bodyFatValue);

            // 內臟脂肪 繪製百分等級
            const visceralFatValue = bodyCompositionVisceralFatCommentConvertFraction(data.visceralFatGroup.visceralFatCommentLevel);
            drawProgressBar('.visceralFatBar', visceralFatValue);

            // 骨骼肌率 繪製百分等級
            const skeletalMuscleRateValue = bodyCompositioSkeletalMuscleRateCommentConvertFraction(data.skeletalMuscleRateGroup.skeletalMuscleRateCommentLevel);
            drawProgressBar('.skeletalMuscleRateBar', skeletalMuscleRateValue);

            // 心肺適能 繪製百分等級
            const physicalFitnessCardiorespiratoryFitnessValue = data.physicalFitnessCardiorespiratoryFitnessGroup.cardiorespiratoryFitnessFraction;
            drawProgressBar('.physicalFitnessCardiorespiratoryFitnessBar', physicalFitnessCardiorespiratoryFitnessValue);

            // 肌力與肌耐力 繪製百分等級
            const physicalFitnessMuscleStrengthAndMuscleEndurance = data.physicalFitnessMuscleStrengthAndMuscleEnduranceGroup.muscleStrengthAndMuscleEnduranceFitness;
            drawProgressBar('.physicalFitnessMuscleStrengthAndMuscleEnduranceBar', physicalFitnessMuscleStrengthAndMuscleEndurance);

            // 柔軟度 繪製百分等級
            const physicalFitnessSoftnessValue = data.physicalFitnessSoftnessGroup.softnessFitness;
            drawProgressBar('.physicalFitnessSoftnessBar', physicalFitnessSoftnessValue);

            // 平衡 繪製百分等級
            const physicalFitnessBalanceValue = data.physicalFitnessBalanceGroup.balanceFitness;
            drawProgressBar('.physicalFitnessBalanceBar', physicalFitnessBalanceValue);
        }).catch((error) => {
            const errorCode = axiosErrorCode<BodyMassAndPhysicalFitnessListErrorCodeType>(error);

            switch (errorCode) {
                case 'StudentDoesNotExist':
                    setVisableInfoDialog(true);
                    setVisableInfoDialogTitle('錯誤');
                    setVisableInfoDialogMessage('查無學員');
                    break;
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                default:
                    setVisableInfoDialog(true);
                    setVisableInfoDialogTitle('錯誤');
                    setVisableInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    /**
     * 新增或更新身體組成 bmi
     */
    const handleBodyCompositionBmiAndMarkReplace = (): void => {
        const isValid = inspectBodyCompositionField();

        if (isValid) {
            const data: BodyCompositionBmiAndMarkRequestType = {
                bmi: inputBodyCompositionValue,
                mark: inputBodyCompositionMark
            };

            bodyCompositionBmiAndMarkReplace(storeGetters.studentId, data).then(() => {
                closeBodyCompositionDialog();
                setVisableInfoDialog(true);
                setVisableInfoDialogTitle('資訊');
                setVisableInfoDialogMessage('更新 bmi 資料成功');
                handleBodyMassAndPhysicalFitnessList();
            }).catch((error) => {
                const errorCode = axiosErrorCode<BodyCompositionBmiAndMarkErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('格式輸入錯誤');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    /**
     * 新增或更新身體組成 體脂肪
     */
    const handleBodyCompositionBodyFatAndMarkReplace = (): void => {
        const isValid = inspectBodyCompositionField();

        if (isValid) {
            const data: BodyCompositionBodyFatAndMarkRequestType = {
                bodyFat: inputBodyCompositionValue,
                mark: inputBodyCompositionMark
            };

            bodyCompositionBodyFatAndMarkReplace(storeGetters.studentId, data).then(() => {
                closeBodyCompositionDialog();
                setVisableInfoDialog(true);
                setVisableInfoDialogTitle('資訊');
                setVisableInfoDialogMessage('更新 體脂肪 資料成功');
                handleBodyMassAndPhysicalFitnessList();
            }).catch((error) => {
                const errorCode = axiosErrorCode<BodyCompositionBodyFatAndMarkErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('格式輸入錯誤');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    /**
     * 新增或更新身體組成 內臟脂肪
     */
    const handleBodyCompositionVisceralFatAndMarkReplace = (): void => {
        const isValid = inspectBodyCompositionField();

        if (isValid) {
            const data: BodyCompositionVisceralFatAndMarkRequestType = {
                visceralFat: inputBodyCompositionValue,
                mark: inputBodyCompositionMark
            };

            bodyCompositionVisceralFatAndMarkReplace(storeGetters.studentId, data).then(() => {
                closeBodyCompositionDialog();
                setVisableInfoDialog(true);
                setVisableInfoDialogTitle('資訊');
                setVisableInfoDialogMessage('更新 內臟脂肪 資料成功');
                handleBodyMassAndPhysicalFitnessList();
            }).catch((error) => {
                const errorCode = axiosErrorCode<BodyCompositionVisceralFatAndMarkErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('格式輸入錯誤');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    /**
     * 新增或更新身體組成 骨骼肌率
     */
    const handleBodyCompositionSkeletalMuscleRateAndMarkReplace = (): void => {
        const isValid = inspectBodyCompositionField();

        if (isValid) {
            const data: BodyCompositionSkeletalMuscleRateAndMarkRequestType = {
                skeletalMuscleRate: inputBodyCompositionValue,
                mark: inputBodyCompositionMark
            };

            bodyCompositionSkeletalMuscleRateAndMarkReplace(storeGetters.studentId, data).then(() => {
                closeBodyCompositionDialog();
                setVisableInfoDialog(true);
                setVisableInfoDialogTitle('資訊');
                setVisableInfoDialogMessage('更新 骨骼肌率 資料成功');
                handleBodyMassAndPhysicalFitnessList();
            }).catch((error) => {
                const errorCode = axiosErrorCode<BodyCompositionSkeletalMuscleRateAndMarkErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('格式輸入錯誤');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    const handleBodyCompositionReplace = (e: React.ChangeEvent<HTMLFormElement>): void => {
        e.preventDefault();

        if (editBodyCompositionKey === 'bmi') {
            handleBodyCompositionBmiAndMarkReplace();
        }
        else if (editBodyCompositionKey === 'bodyFat') {
            handleBodyCompositionBodyFatAndMarkReplace();
        }
        else if (editBodyCompositionKey === 'visceralFat') {
            handleBodyCompositionVisceralFatAndMarkReplace();
        }
        else {
            handleBodyCompositionSkeletalMuscleRateAndMarkReplace();
        }
    };

    // 處理身體組成彈窗輸入
    const handleChangeBodyCompositionValue = (e: React.ChangeEvent<HTMLInputElement>): void => {
        setInputBodyCompositionValue(e.target.value);
        inspectBodyCompositionField();
    };

    // 處理身體組成彈窗備註
    const handleChangeBodyCompositionMark = (e: React.ChangeEvent<HTMLTextAreaElement>): void => {
        setInputBodyCompositionMark(e.target.value);
        inspectBodyCompositionField();
    };

    const inspectBodyCompositionField = (): boolean => {
        const isValid = validate(bodyCompositionFormRef.current, formBodyCompositionMark);

        if (isValid !== undefined) {
            if (isValid.value) {
                setInputBodyCompositionValueErrorMessage(isValid.value[0].replace('Value ', ''));
            }

            if (isValid.mark) {
                setInputBodyCompositionMarkErrorMessage(isValid.mark[0].replace('Mark ', ''));
            }

            return false;
        }
        else {
            setInputBodyCompositionValueErrorMessage('');
            setInputBodyCompositionMarkErrorMessage('');

            return true;
        }
    };

    /**
     * 身體組成與體適能百分比等級繪圖
     */
    const drawProgressBar = (className: string, range: number): void => {
        const height = 50;

        d3.select(className).select('svg').remove();

        const svg = d3.select(className)
            .append('svg')
            .attr('width', '100%')
            .attr('height', height)
            .attr('viewbox', '-50 -50 100 100')
            .append('g')
            .attr('transform', `translate(0, ${height / 2 - 5})`);

        svg.append('rect')
            .attr('width', '100%')
            .attr('height', '10px')
            .attr('rx', '5px')
            .attr('ry', '5px')
            .style('fill', colors.color_white);

        svg.append('rect')
            .attr('width', '0')
            .attr('height', '10px')
            .attr('rx', '5px')
            .attr('ry', '5px')
            .style('fill', colors.color_yellow)
            .transition()
            .duration(2000)
            .attr('width', `${range}%`)
            .attr('height', '10px');
    };

    const showBodyCompositionDialog = (editBodyCompositionKey: EditBodyCompositionKeyType, value: number | undefined, mark: string | undefined): void => {
        setVisableBodyCompositionDialog(true);
        setEditBodyCompositionKey(editBodyCompositionKey);

        setInputBodyCompositionValue(value);
        setInputBodyCompositionMark(mark);
    };

    // 關閉身體組成對話彈窗
    const closeBodyCompositionDialog = (): void => {
        setVisableBodyCompositionDialog(false);
        setInputBodyCompositionValue(undefined);
        setInputBodyCompositionMark('');
    };

    /**
     * -----------------------------------------------------------
     * -------------------- 心肺適能 ------------------------------
     * ------------------------------------------------------------
     */

    // 顯示體適能-心肺適能彈窗
    const showPhysicalFitnessCardiorespiratoryFitnessDialog = (): void => {
        setVisiblePhysicalFitnessCardiorespiratoryFitnessDialog(true);
        resetPhysicalFitnessCardiorespiratoryFitnessField();
    };

    /**
     * 關閉體適能-心肺適能彈窗
     */
    const closePhysicalFitnessCardiorespiratoryFitnessDialog = (): void => {
        setVisiblePhysicalFitnessCardiorespiratoryFitnessDialog(false);
        resetPhysicalFitnessCardiorespiratoryFitnessField();
    };

    /**
     * 重洗體適能心肺適能欄位
     */
    const resetPhysicalFitnessCardiorespiratoryFitnessField = (): void => {
        setPhysicalFitnessCardiorespiratoryFitnessPulseRateOne(undefined);
        setPhysicalFitnessCardiorespiratoryFitnessPulseRateTwo(undefined);
        setPhysicalFitnessCardiorespiratoryFitnessPulseRateThree(undefined);
        setPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequency(undefined);
        setPhysicalFitnessCardiorespiratoryFitnessMark(undefined);

        setPhysicalFitnessCardiorespiratoryFitnessPulseRateOneErrorMessage('');
        setPhysicalFitnessCardiorespiratoryFitnessPulseRateTwoErrorMessage('');
        setPhysicalFitnessCardiorespiratoryFitnessPulseRateThreeErrorMessage('');
        setPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyErrorMessage('');
        setPhysicalFitnessCardiorespiratoryFitnessMarkErrorMessage('');
    };

    // 處理體適能心肺適能-第一次脈搏次數
    const handlePhysicalFitnessCardiorespiratoryFitnessPulseRateOneInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const isValid = validate(physicalFitnessCardiorespiratoryFitnessFormRef.current, physicalFitnessCardiorespiratoryFitnessFormAdultValidate);

        if (isValid !== undefined) {
            if (isValid.pulseRateOne) {
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateOne(undefined);
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateOneErrorMessage(isValid.pulseRateOne[0].replace('Pulse rate one ', ''));
            }
            else {
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateOne(parseInt(e.target.value));
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateOneErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessCardiorespiratoryFitnessPulseRateOne(parseInt(e.target.value));
            setPhysicalFitnessCardiorespiratoryFitnessPulseRateOneErrorMessage('');
        }
    };

    // 處理體適能心肺適能-第二次脈搏次數
    const handlePhysicalFitnessCardiorespiratoryFitnessPulseRateTwoInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const isValid = validate(physicalFitnessCardiorespiratoryFitnessFormRef.current, physicalFitnessCardiorespiratoryFitnessFormAdultValidate);

        if (isValid !== undefined) {
            if (isValid.pulseRateTwo) {
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateTwo(undefined);
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateTwoErrorMessage(isValid.pulseRateTwo[0].replace('Pulse rate two ', ''));
            }
            else {
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateTwo(parseInt(e.target.value));
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateTwoErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessCardiorespiratoryFitnessPulseRateTwo(parseInt(e.target.value));
            setPhysicalFitnessCardiorespiratoryFitnessPulseRateTwoErrorMessage('');
        }
    };

    // 處理體適能心肺適能-第三次脈搏次數
    const handlePhysicalFitnessCardiorespiratoryFitnessPulseRateThreeInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const isValid = validate(physicalFitnessCardiorespiratoryFitnessFormRef.current, physicalFitnessCardiorespiratoryFitnessFormAdultValidate);

        if (isValid !== undefined) {
            if (isValid.pulseRateThree) {
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateThree(undefined);
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateThreeErrorMessage(isValid.pulseRateThree[0].replace('Pulse rate three ', ''));
            }
            else {
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateThree(parseInt(e.target.value));
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateThreeErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessCardiorespiratoryFitnessPulseRateThree(parseInt(e.target.value));
            setPhysicalFitnessCardiorespiratoryFitnessPulseRateThreeErrorMessage('');
        }
    };

    // 處理體適能心肺適能-原地抬膝次數
    const handlePhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequency = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const isValid = validate(physicalFitnessCardiorespiratoryFitnessFormRef.current, physicalFitnessCardiorespiratoryFitnessFormElderlyValidate);

        if (isValid !== undefined) {
            if (isValid.kneeLiftFrequency) {
                setPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequency(undefined);
                setPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyErrorMessage(isValid.kneeLiftFrequency[0].replace('Knee lift frequency ', ''));
            }
            else {
                setPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequency(parseInt(e.target.value));
                setPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequency(parseInt(e.target.value));
            setPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyErrorMessage('');
        }
    };

    // 處理體適能心肺適能-備註
    const handlePhysicalFitnessCardiorespiratoryFitnessMarkInput = (e: React.ChangeEvent<HTMLTextAreaElement>): void => {
        const targetValid = isStudentGreaterThan65(storeGetters.age) ? physicalFitnessCardiorespiratoryFitnessFormAdultValidate : physicalFitnessCardiorespiratoryFitnessFormElderlyValidate;
        const isValid = validate(physicalFitnessCardiorespiratoryFitnessFormRef.current, targetValid);

        if (isValid !== undefined) {
            if (isValid.mark) {
                setPhysicalFitnessCardiorespiratoryFitnessMark(undefined);
                setPhysicalFitnessCardiorespiratoryFitnessMarkErrorMessage(isValid.mark[0].replace('Mark ', ''));
            }
            else {
                setPhysicalFitnessCardiorespiratoryFitnessMark(e.target.value);
                setPhysicalFitnessCardiorespiratoryFitnessMarkErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessCardiorespiratoryFitnessMark(e.target.value);
            setPhysicalFitnessCardiorespiratoryFitnessMarkErrorMessage('');
        }
    };

    /**
     * 檢查心肺適能相關欄位
     */
    const inspectPhysicalFitnessCardiorespiratoryFitnessField = (): boolean => {
        if (isStudentGreaterThan65(storeGetters.age)) {
            const isValid = validate(physicalFitnessCardiorespiratoryFitnessFormRef.current, physicalFitnessCardiorespiratoryFitnessFormAdultValidate);

            if (isValid !== undefined) {
                if (isValid.pulseRateOne) {
                    setPhysicalFitnessCardiorespiratoryFitnessPulseRateOneErrorMessage(isValid.pulseRateOne[0].replace('Pulse rate one ', ''));
                }
                else {
                    setPhysicalFitnessCardiorespiratoryFitnessPulseRateOneErrorMessage('');
                }

                if (isValid.pulseRateTwo) {
                    setPhysicalFitnessCardiorespiratoryFitnessPulseRateTwoErrorMessage(isValid.pulseRateTwo[0].replace('Pulse rate two ', ''));
                }
                else {
                    setPhysicalFitnessCardiorespiratoryFitnessPulseRateTwoErrorMessage('');
                }

                if (isValid.pulseRateThree) {
                    setPhysicalFitnessCardiorespiratoryFitnessPulseRateThreeErrorMessage(isValid.pulseRateThree[0].replace('Pulse rate three ', ''));
                }
                else {
                    setPhysicalFitnessCardiorespiratoryFitnessPulseRateThreeErrorMessage('');
                }

                if (isValid.mark) {
                    setPhysicalFitnessCardiorespiratoryFitnessMarkErrorMessage(isValid.mark[0].replace('Mark ', ''));
                }
                else {
                    setPhysicalFitnessCardiorespiratoryFitnessMarkErrorMessage('');
                }

                return false;
            }
            else {
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateOneErrorMessage('');
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateTwoErrorMessage('');
                setPhysicalFitnessCardiorespiratoryFitnessPulseRateThreeErrorMessage('');
                setPhysicalFitnessCardiorespiratoryFitnessMarkErrorMessage('');

                return true;
            }
        }
        else {
            const isValid = validate(physicalFitnessCardiorespiratoryFitnessFormRef.current, physicalFitnessCardiorespiratoryFitnessFormElderlyValidate);

            if (isValid !== undefined) {
                if (isValid.kneeLiftFrequency) {
                    setPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyErrorMessage(isValid.kneeLiftFrequency[0].replace('Knee lift frequency ', ''));
                }
                else {
                    setPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyErrorMessage('');
                }

                if (isValid.mark) {
                    setPhysicalFitnessCardiorespiratoryFitnessMarkErrorMessage(isValid.mark[0].replace('Mark ', ''));
                }
                else {
                    setPhysicalFitnessCardiorespiratoryFitnessMarkErrorMessage('');
                }

                return false;
            }
            else {
                setPhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyErrorMessage('');
                setPhysicalFitnessCardiorespiratoryFitnessMarkErrorMessage('');

                return true;
            }
        }
    };

    /**
     * 新增或更新體適能-心肺適能
     */
    const handleReplacePhysicalFitnessCardiorespiratoryFitnessInfo = (e: React.FormEvent<HTMLFormElement>): void | boolean => {
        e.preventDefault();

        const isValid = inspectPhysicalFitnessCardiorespiratoryFitnessField();

        if (!isValid) {
            return false;
        }
        else {
            const stepTestData: ReplacePhysicalFitnessCardiorespiratoryFitnessStepTestAndMarkRequestType = {
                pulseRateOne: physicalFitnessCardiorespiratoryFitnessPulseRateOne,
                pulseRateTwo: physicalFitnessCardiorespiratoryFitnessPulseRateTwo,
                pulseRateThree: physicalFitnessCardiorespiratoryFitnessPulseRateThree,
                mark: physicalFitnessCardiorespiratoryFitnessMark
            };

            const kneeLiftFrequencyData: ReplacePhysicalFitnessCardiorespiratoryFitnessStandingKneeRaiseRequestType = {
                kneeLiftFrequency: physicalFitnessCardiorespiratoryFitnessKneeLiftFrequency,
                mark: physicalFitnessCardiorespiratoryFitnessMark
            };

            const promises = isStudentGreaterThan65(storeGetters.age) ? [replacePhysicalFitnessCardiorespiratoryFitnessStepTestAndMark(storeGetters.studentId, stepTestData)] : [replacePhysicalFitnessCardiorespiratoryFitnessStandingKneeRaise(storeGetters.studentId, kneeLiftFrequencyData)];

            Promise.all(promises).then(() => {
                closePhysicalFitnessCardiorespiratoryFitnessDialog();
                setVisableInfoDialog(true);
                setVisableInfoDialogTitle('資訊');
                setVisableInfoDialogMessage('更新心肺適能資料成功');
                handleBodyMassAndPhysicalFitnessList();
            }).catch((error) => {
                const errorCode = axiosErrorCode<PhysicalFitnessCardiorespiratoryFitnessMarkErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('格式輸入錯誤');
                        break;
                    case 'StudentDoesNotExist':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('查無學員');
                        break;
                    case 'UnderTheAgeOf21To65':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('此體適能項目適合 21 到 65 歲');
                        break;
                    case 'UnderTheAgeOf65':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('此體適能項目適合 21 到 65 歲');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('伺服器錯誤');
                        break;
                }
            });
        }
    };

    /**
     * ------------------------------------------------------------
     * -------------------- 肌力與肌耐力 ---------------------------
     * ------------------------------------------------------------
     */

    // 顯示體適能-肌力與肌耐力彈窗
    const showPhysicalFitnessMuscleStrengthAndMuscleEnduranceDialog = (): void => {
        setVisiblePhysicalFitnessMuscleStrengthAndMuscleEnduranceDialog(true);

        resetPhysicalFitnessMuscleStrengthAndMuscleEnduranceField();
    };

    // 關閉體適能-肌力與肌耐力彈窗
    const closePhysicalFitnessMuscleStrengthAndMuscleEnduranceDialog = (): void => {
        setVisiblePhysicalFitnessMuscleStrengthAndMuscleEnduranceDialog(false);

        resetPhysicalFitnessMuscleStrengthAndMuscleEnduranceField();
    };

    /**
     * 重洗體適能肌力與肌耐力欄位
     */
    const resetPhysicalFitnessMuscleStrengthAndMuscleEnduranceField = (): void => {
        setPhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequency(undefined);
        setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequency(undefined);
        setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequency(undefined);
        setPhysicalFitnessMscleStrengthAndMuscleEnduranceMark(undefined);

        setPhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyErrorMessage('');
        setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyErrorMessage('');
        setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyErrorMessage('');
        setPhysicalFitnessMscleStrengthAndMuscleEnduranceMarkErrorMessage('');
    };

    // 處理體適能-肌力與肌耐力原地站立抬膝輸入
    const handlePhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const isValid = validate(physicalFitnessMscleStrengthAndMuscleEnduranceFormRef.current, physicalFitnessMuscleStrengthAndMuscleEnduranceFormAdultValidate);

        if (isValid !== undefined) {
            if (isValid.kneeCrunchesFrequency) {
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequency(undefined);
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyErrorMessage(isValid.kneeCrunchesFrequency[0].replace('Knee crunches frequency ', ''));
            }
            else {
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequency(parseInt(e.target.value));
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequency(parseInt(e.target.value));
            setPhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyErrorMessage('');
        }
    };

    // 處理體適能-肌力與肌耐力椅子坐姿體前彎輸入
    const handlePhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const isValid = validate(physicalFitnessMscleStrengthAndMuscleEnduranceFormRef.current, physicalFitnessMuscleStrengthAndMuscleEnduranceFormElderlyValidate);

        if (isValid !== undefined) {
            if (isValid.armCurlUpperBodyFrequency) {
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequency(undefined);
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyErrorMessage(isValid.armCurlUpperBodyFrequency[0].replace('Arm curl upper body frequency ', ''));
            }
            else {
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequency(parseInt(e.target.value));
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequency(parseInt(e.target.value));
            setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyErrorMessage('');
        }
    };

    // 處理體適能-肌力與肌耐力起立坐下輸入
    const handlePhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyInput = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const isValid = validate(physicalFitnessMscleStrengthAndMuscleEnduranceFormRef.current, physicalFitnessMuscleStrengthAndMuscleEnduranceFormElderlyValidate);

        if (isValid !== undefined) {
            if (isValid.armCurlLowerBodyFrequency) {
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequency(undefined);
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyErrorMessage(isValid.armCurlLowerBodyFrequency[0].replace('Arm curl lower body frequency ', ''));
            }
            else {
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequency(parseInt(e.target.value));
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequency(parseInt(e.target.value));
            setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyErrorMessage('');
        }
    };

    // 處理體適能-肌力與肌耐力起立備註
    const handlePhysicalFitnessMscleStrengthAndMuscleEnduranceMarkInput = (e: React.ChangeEvent<HTMLTextAreaElement>): void => {
        const targetValid = isStudentGreaterThan65(storeGetters.age) ? physicalFitnessMuscleStrengthAndMuscleEnduranceFormAdultValidate : physicalFitnessMuscleStrengthAndMuscleEnduranceFormElderlyValidate;
        const isValid = validate(physicalFitnessMscleStrengthAndMuscleEnduranceFormRef.current, targetValid);

        if (isValid !== undefined) {
            if (isValid.mark) {
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceMark(undefined);
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceMarkErrorMessage(isValid.mark[0].replace('Mark ', ''));
            }
            else {
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceMark(e.target.value);
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceMarkErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessMscleStrengthAndMuscleEnduranceMark(e.target.value);
            setPhysicalFitnessMscleStrengthAndMuscleEnduranceMarkErrorMessage('');
        }
    };

    /**
     * 檢查肌力與肌耐力相關欄位
     */
    const inspectPhysicalFitnessMuscleStrengthAndMuscleEnduranceField = (): boolean => {
        if (isStudentGreaterThan65(storeGetters.age)) {
            const isValid = validate(physicalFitnessMscleStrengthAndMuscleEnduranceFormRef.current, physicalFitnessMuscleStrengthAndMuscleEnduranceFormAdultValidate);

            if (isValid !== undefined) {
                if (isValid.kneeCrunchesFrequency) {
                    setPhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyErrorMessage(isValid.kneeCrunchesFrequency[0].replace('Knee crunches frequency ', ''));
                }
                else {
                    setPhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyErrorMessage('');
                }

                if (isValid.mark) {
                    setPhysicalFitnessMscleStrengthAndMuscleEnduranceMarkErrorMessage(isValid.mark[0].replace('Mark ', ''));
                }
                else {
                    setPhysicalFitnessMscleStrengthAndMuscleEnduranceMarkErrorMessage('');
                }

                return false;
            }
            else {
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyErrorMessage('');
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceMarkErrorMessage('');

                return true;
            }
        }
        else {
            const isValid = validate(physicalFitnessMscleStrengthAndMuscleEnduranceFormRef.current, physicalFitnessMuscleStrengthAndMuscleEnduranceFormElderlyValidate);

            if (isValid !== undefined) {
                if (isValid.armCurlUpperBodyFrequency) {
                    setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyErrorMessage(isValid.armCurlUpperBodyFrequency[0].replace('Arm curl upper body frequency ', ''));
                }
                else {
                    setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyErrorMessage('');
                }

                if (isValid.armCurlLowerBodyFrequency) {
                    setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyErrorMessage(isValid.armCurlLowerBodyFrequency[0].replace('Arm curl lower body frequency ', ''));
                }
                else {
                    setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyErrorMessage('');
                }

                if (isValid.mark) {
                    setPhysicalFitnessMscleStrengthAndMuscleEnduranceMarkErrorMessage(isValid.mark[0].replace('Mark ', ''));
                }
                else {
                    setPhysicalFitnessMscleStrengthAndMuscleEnduranceMarkErrorMessage('');
                }

                return false;
            }
            else {
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyErrorMessage('');
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyErrorMessage('');
                setPhysicalFitnessMscleStrengthAndMuscleEnduranceMarkErrorMessage('');

                return true;
            }
        }
    };

    /**
     * 新增或更新體適能-肌力與肌耐力
     */
    const handleReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceInfo = (e: React.FormEvent<HTMLFormElement>): void => {
        e.preventDefault();

        const isValid = inspectPhysicalFitnessMuscleStrengthAndMuscleEnduranceField();

        if (!isValid) {
            return;
        }
        else {
            const kneeCrunchesFrequencyData: ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyRequestType = {
                kneeCrunchesFrequency: physicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequency,
                mark: physicalFitnessMscleStrengthAndMuscleEnduranceMark
            };

            const armCurlUpperBodyFrequencyData: ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyRequestType = {
                armCurlUpperBodyFrequency: physicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequency,
                mark: physicalFitnessMscleStrengthAndMuscleEnduranceMark
            };

            const armCurlLowerBodyFrequencyData: ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyRequestType = {
                armCurlLowerBodyFrequency: physicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequency,
                mark: physicalFitnessMscleStrengthAndMuscleEnduranceMark
            };

            const handleErrorCode = (error: any) => {
                const errorCode = axiosErrorCode<ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('格式輸入錯誤');
                        break;
                    case 'StudentDoesNotExist':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('查無學員');
                        break;
                    case 'UnderTheAgeOf21To65':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('此體適能項目適合 21 到 65 歲');
                        break;
                    case 'UnderTheAgeOf65':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('此體適能項目適合 65 歲以上');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('伺服器錯誤');
                        break;
                }
            };

            if (isStudentGreaterThan65(storeGetters.age)) {
                replacePhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequency(storeGetters.studentId, kneeCrunchesFrequencyData).then(() => {
                    closePhysicalFitnessMuscleStrengthAndMuscleEnduranceDialog();
                    setVisableInfoDialog(true);
                    setVisableInfoDialogTitle('資訊');
                    setVisableInfoDialogMessage('更新肌力與肌耐力資料成功');
                    handleBodyMassAndPhysicalFitnessList();
                }).catch((error) => {
                    handleErrorCode(error);
                });
            }
            else {
                replacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequency(storeGetters.studentId, armCurlUpperBodyFrequencyData).then(() => {
                    replacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequency(storeGetters.studentId, armCurlLowerBodyFrequencyData).then(() => {
                        closePhysicalFitnessMuscleStrengthAndMuscleEnduranceDialog();
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('資訊');
                        setVisableInfoDialogMessage('更新心肺適能資料成功');
                        handleBodyMassAndPhysicalFitnessList();
                    }).catch((error) => {
                        handleErrorCode(error);
                    });
                }).catch((error) => {
                    handleErrorCode(error);
                });
            }
        }
    };

    /**
     * ------------------------------------------------------------
     * ---------------------- 柔軟度 ------------------------------
     * ------------------------------------------------------------
     */

    // 顯示體適能-柔軟度彈窗
    const showPhysicalFitnessSoftnessDialog = (): void => {
        setVisiblePhysicalFitnessSoftnessDialog(true);

        resetPhysicalFitnessSoftnessField();
    };

    // 關閉體適能-柔軟度彈窗
    const closePhysicalFitnessSoftnessDialog = (): void => {
        setVisiblePhysicalFitnessSoftnessDialog(false);

        resetPhysicalFitnessSoftnessField();
    };

    /**
     * 重洗體適能柔軟度相關欄位
     */
    const resetPhysicalFitnessSoftnessField = (): void => {
        setPhysicalFitnessSoftnessSeatedForwardBendOne(undefined);
        setPhysicalFitnessSoftnessSeatedForwardBendTwo(undefined);
        setPhysicalFitnessSoftnessSeatedForwardBendThree(undefined);
        setPhysicalFitnessSoftnessChairSeatedForwardBendOne(undefined);
        setPhysicalFitnessSoftnessChairSeatedForwardBendTwo(undefined);
        setPhysicalFitnessSoftnessChairSeatedForwardBendThree(undefined);
        setPhysicalFitnessSoftnessPullBackTestOne(undefined);
        setPhysicalFitnessSoftnessPullBackTestTwo(undefined);
        setPhysicalFitnessSoftnessMark(undefined);

        setPhysicalFitnessSoftnessSeatedForwardBendOneErrorMessage('');
        setPhysicalFitnessSoftnessSeatedForwardBendTwoErrorMessage('');
        setPhysicalFitnessSoftnessSeatedForwardBendThreeErrorMessage('');
        setPhysicalFitnessSoftnessChairSeatedForwardBendOneErrorMessage('');
        setPhysicalFitnessSoftnessChairSeatedForwardBendTwoErrorMessage('');
        setPhysicalFitnessSoftnessChairSeatedForwardBendThreeErrorMessage('');
        setPhysicalFitnessSoftnessPullBackTestOneErrorMessage('');
        setPhysicalFitnessSoftnessPullBackTestTwoErrorMessage('');
        setPhysicalFitnessSoftnessMarkErrorMessage('');
    };

    // 處理體適能-柔軟度第一次坐姿體前彎輸入
    const handlePhysicalFitnessSoftnessSeatedForwardBendOneInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(physicalFitnessSoftnessFormRef.current, physicalFitnessSoftnessAdultValidate);

        if (isValid !== undefined) {
            if (isValid.seatedForwardBendOne) {
                setPhysicalFitnessSoftnessSeatedForwardBendOne(undefined);
                setPhysicalFitnessSoftnessSeatedForwardBendOneErrorMessage(isValid.seatedForwardBendOne[0].replace('Seated forward bend one ', ''));
            }
            else {
                setPhysicalFitnessSoftnessSeatedForwardBendOne(parseFloat(e.target.value));
                setPhysicalFitnessSoftnessSeatedForwardBendOneErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessSoftnessSeatedForwardBendOne(parseFloat(e.target.value));
            setPhysicalFitnessSoftnessSeatedForwardBendOneErrorMessage('');
        }
    };

    // 處理體適能-柔軟度第二次坐姿體前彎輸入
    const handlePhysicalFitnessSoftnessSeatedForwardBendTwoInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(physicalFitnessSoftnessFormRef.current, physicalFitnessSoftnessAdultValidate);

        if (isValid !== undefined) {
            if (isValid.seatedForwardBendTwo) {
                setPhysicalFitnessSoftnessSeatedForwardBendTwo(undefined);
                setPhysicalFitnessSoftnessSeatedForwardBendTwoErrorMessage(isValid.seatedForwardBendTwo[0].replace('Seated forward bend two ', ''));
            }
            else {
                setPhysicalFitnessSoftnessSeatedForwardBendTwo(parseFloat(e.target.value));
                setPhysicalFitnessSoftnessSeatedForwardBendTwoErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessSoftnessSeatedForwardBendTwo(parseFloat(e.target.value));
            setPhysicalFitnessSoftnessSeatedForwardBendTwoErrorMessage('');
        }
    };

    // 處理體適能-柔軟度第三次坐姿體前彎輸入
    const handlePhysicalFitnessSoftnessSeatedForwardBendThreeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(physicalFitnessSoftnessFormRef.current, physicalFitnessSoftnessAdultValidate);

        if (isValid !== undefined) {
            if (isValid.seatedForwardBendThree) {
                setPhysicalFitnessSoftnessSeatedForwardBendThree(undefined);
                setPhysicalFitnessSoftnessSeatedForwardBendThreeErrorMessage(isValid.seatedForwardBendThree[0].replace('Seated forward bend three ', ''));
            }
            else {
                setPhysicalFitnessSoftnessSeatedForwardBendThree(parseFloat(e.target.value));
                setPhysicalFitnessSoftnessSeatedForwardBendThreeErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessSoftnessSeatedForwardBendThree(parseFloat(e.target.value));
            setPhysicalFitnessSoftnessSeatedForwardBendThreeErrorMessage('');
        }
    };

    // 處理體適能-柔軟度第一次椅子坐姿體前彎輸入
    const handlePhysicalFitnessSoftnessChairSeatedForwardBendOneInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(physicalFitnessSoftnessFormRef.current, physicalFitnessSoftnessElderlyValidate);

        if (isValid !== undefined) {
            if (isValid.chairSeatedForwardBendOne) {
                setPhysicalFitnessSoftnessChairSeatedForwardBendOne(undefined);
                setPhysicalFitnessSoftnessChairSeatedForwardBendOneErrorMessage(isValid.chairSeatedForwardBendOne[0].replace('Chair seated forward bend one ', ''));
            }
            else {
                setPhysicalFitnessSoftnessChairSeatedForwardBendOne(parseFloat(e.target.value));
                setPhysicalFitnessSoftnessChairSeatedForwardBendOneErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessSoftnessChairSeatedForwardBendOne(parseFloat(e.target.value));
            setPhysicalFitnessSoftnessChairSeatedForwardBendOneErrorMessage('');
        }
    };

    // 處理體適能-柔軟度第二次椅子坐姿體前彎輸入
    const handlePhysicalFitnessSoftnessChairSeatedForwardBendTwoInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(physicalFitnessSoftnessFormRef.current, physicalFitnessSoftnessElderlyValidate);

        if (isValid !== undefined) {
            if (isValid.chairSeatedForwardBendTwo) {
                setPhysicalFitnessSoftnessChairSeatedForwardBendTwo(undefined);
                setPhysicalFitnessSoftnessChairSeatedForwardBendTwoErrorMessage(isValid.chairSeatedForwardBendTwo[0].replace('Chair seated forward bend two ', ''));
            }
            else {
                setPhysicalFitnessSoftnessChairSeatedForwardBendTwo(parseFloat(e.target.value));
                setPhysicalFitnessSoftnessChairSeatedForwardBendTwoErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessSoftnessChairSeatedForwardBendTwo(parseFloat(e.target.value));
            setPhysicalFitnessSoftnessChairSeatedForwardBendTwoErrorMessage('');
        }
    };

    // 處理體適能-柔軟度第三次椅子坐姿體前彎輸入
    const handlePhysicalFitnessSoftnessChairSeatedForwardBendThreeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(physicalFitnessSoftnessFormRef.current, physicalFitnessSoftnessElderlyValidate);

        if (isValid !== undefined) {
            if (isValid.chairSeatedForwardBendThree) {
                setPhysicalFitnessSoftnessChairSeatedForwardBendThree(undefined);
                setPhysicalFitnessSoftnessChairSeatedForwardBendThreeErrorMessage(isValid.chairSeatedForwardBendThree[0].replace('Chair seated forward bend three ', ''));
            }
            else {
                setPhysicalFitnessSoftnessChairSeatedForwardBendThree(parseFloat(e.target.value));
                setPhysicalFitnessSoftnessChairSeatedForwardBendThreeErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessSoftnessChairSeatedForwardBendThree(parseFloat(e.target.value));
            setPhysicalFitnessSoftnessChairSeatedForwardBendThreeErrorMessage('');
        }
    };

    // 處理體適能-柔軟度第一次拉背測驗輸入
    const handlePhysicalFitnessSoftnessPullBackTestOneInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(physicalFitnessSoftnessFormRef.current, physicalFitnessSoftnessElderlyValidate);

        if (isValid !== undefined) {
            if (isValid.pullBackTestOne) {
                setPhysicalFitnessSoftnessPullBackTestOne(undefined);
                setPhysicalFitnessSoftnessPullBackTestOneErrorMessage(isValid.pullBackTestOne[0].replace('Pull back test one ', ''));
            }
            else {
                setPhysicalFitnessSoftnessPullBackTestOne(parseFloat(e.target.value));
                setPhysicalFitnessSoftnessPullBackTestOneErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessSoftnessPullBackTestOne(parseFloat(e.target.value));
            setPhysicalFitnessSoftnessPullBackTestOneErrorMessage('');
        }
    };

    // 處理體適能-柔軟度第二次拉背測驗輸入
    const handlePhysicalFitnessSoftnessPullBackTestTwoInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(physicalFitnessSoftnessFormRef.current, physicalFitnessSoftnessElderlyValidate);

        if (isValid !== undefined) {
            if (isValid.pullBackTestTwo) {
                setPhysicalFitnessSoftnessPullBackTestTwo(undefined);
                setPhysicalFitnessSoftnessPullBackTestTwoErrorMessage(isValid.pullBackTestTwo[0].replace('Pull back test two ', ''));
            }
            else {
                setPhysicalFitnessSoftnessPullBackTestTwo(parseFloat(e.target.value));
                setPhysicalFitnessSoftnessPullBackTestTwoErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessSoftnessPullBackTestTwo(parseFloat(e.target.value));
            setPhysicalFitnessSoftnessPullBackTestTwoErrorMessage('');
        }
    };

    // 處理體適能-柔軟度備註
    const handlePhysicalFitnessSoftnessMarkInput = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        const targetValid = isStudentGreaterThan65(storeGetters.age) ? physicalFitnessSoftnessAdultValidate : physicalFitnessSoftnessElderlyValidate;
        const isValid = validate(physicalFitnessSoftnessFormRef.current, targetValid);

        if (isValid !== undefined) {
            if (isValid.mark) {
                setPhysicalFitnessSoftnessMark(undefined);
                setPhysicalFitnessSoftnessMarkErrorMessage(isValid.mark[0].replace('Mark ', ''));
            }
            else {
                setPhysicalFitnessSoftnessMark(e.target.value);
                setPhysicalFitnessSoftnessMarkErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessSoftnessMark(e.target.value);
            setPhysicalFitnessSoftnessMarkErrorMessage('');
        }
    };

    // 處理體適能-柔軟度相關欄位
    const inspectPhysicalFitnessSoftnessField = () => {
        if (isStudentGreaterThan65(storeGetters.age)) {
            const isValid = validate(physicalFitnessSoftnessFormRef.current, physicalFitnessSoftnessAdultValidate);

            if (isValid !== undefined) {
                if (isValid.seatedForwardBendOne) {
                    setPhysicalFitnessSoftnessSeatedForwardBendOneErrorMessage(isValid.seatedForwardBendOne[0].replace('Seated forward bend one ', ''));
                }
                else {
                    setPhysicalFitnessSoftnessSeatedForwardBendOneErrorMessage('');
                }

                if (isValid.seatedForwardBendTwo) {
                    setPhysicalFitnessSoftnessSeatedForwardBendTwoErrorMessage(isValid.seatedForwardBendTwo[0].replace('Seated forward bend two ', ''));
                }
                else {
                    setPhysicalFitnessSoftnessSeatedForwardBendTwoErrorMessage('');
                }

                if (isValid.seatedForwardBendThree) {
                    setPhysicalFitnessSoftnessSeatedForwardBendThreeErrorMessage(isValid.seatedForwardBendThree[0].replace('Seated forward bend three ', ''));
                }
                else {
                    setPhysicalFitnessSoftnessSeatedForwardBendThreeErrorMessage('');
                }

                if (isValid.mark) {
                    setPhysicalFitnessSoftnessMarkErrorMessage(isValid.mark[0].replace('Mark ', ''));
                }
                else {
                    setPhysicalFitnessSoftnessMarkErrorMessage('');
                }

                return false;
            }
            else {
                setPhysicalFitnessSoftnessSeatedForwardBendOneErrorMessage('');
                setPhysicalFitnessSoftnessSeatedForwardBendTwoErrorMessage('');
                setPhysicalFitnessSoftnessSeatedForwardBendThreeErrorMessage('');
                setPhysicalFitnessSoftnessMarkErrorMessage('');

                return true;
            }
        }
        else {
            const isValid = validate(physicalFitnessSoftnessFormRef.current, physicalFitnessSoftnessElderlyValidate);

            if (isValid !== undefined) {
                if (isValid.chairSeatedForwardBendOne) {
                    setPhysicalFitnessSoftnessChairSeatedForwardBendOneErrorMessage(isValid.chairSeatedForwardBendOne[0].replace('Chair seated forward bend one ', ''));
                }
                else {
                    setPhysicalFitnessSoftnessChairSeatedForwardBendOneErrorMessage('');
                }

                if (isValid.chairSeatedForwardBendTwo) {
                    setPhysicalFitnessSoftnessChairSeatedForwardBendTwoErrorMessage(isValid.chairSeatedForwardBendTwo[0].replace('Chair seated forward bend two ', ''));
                }
                else {
                    setPhysicalFitnessSoftnessChairSeatedForwardBendTwoErrorMessage('');
                }

                if (isValid.chairSeatedForwardBendThree) {
                    setPhysicalFitnessSoftnessChairSeatedForwardBendThreeErrorMessage(isValid.chairSeatedForwardBendThree[0].replace('Chair seated forward bend three ', ''));
                }
                else {
                    setPhysicalFitnessSoftnessChairSeatedForwardBendThreeErrorMessage('');
                }

                if (isValid.pullBackTestOne) {
                    setPhysicalFitnessSoftnessPullBackTestOneErrorMessage(isValid.pullBackTestOne[0].replace('Pull back test one ', ''));
                }
                else {
                    setPhysicalFitnessSoftnessPullBackTestOneErrorMessage('');
                }

                if (isValid.pullBackTestTwo) {
                    setPhysicalFitnessSoftnessPullBackTestTwoErrorMessage(isValid.pullBackTestTwo[0].replace('Pull back test two ', ''));
                }
                else {
                    setPhysicalFitnessSoftnessPullBackTestTwoErrorMessage('');
                }

                if (isValid.mark) {
                    setPhysicalFitnessSoftnessMarkErrorMessage(isValid.mark[0].replace('Mark ', ''));
                }
                else {
                    setPhysicalFitnessSoftnessMarkErrorMessage('');
                }

                return false;
            }
            else {
                setPhysicalFitnessSoftnessChairSeatedForwardBendOneErrorMessage('');
                setPhysicalFitnessSoftnessChairSeatedForwardBendTwoErrorMessage('');
                setPhysicalFitnessSoftnessChairSeatedForwardBendThreeErrorMessage('');
                setPhysicalFitnessSoftnessPullBackTestOneErrorMessage('');
                setPhysicalFitnessSoftnessPullBackTestTwoErrorMessage('');
                setPhysicalFitnessSoftnessMarkErrorMessage('');

                return true;
            }
        }
    };

    /**
     * 新增或更新體適能-柔軟度
     */
    const handlePhysicalFitnessSoftnessMarkReplace = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        const isValid = inspectPhysicalFitnessSoftnessField();

        if (!isValid) {
            return true;
        }
        else {
            const seatedForwardBendData: ReplacePhysicalFitnessSoftnessSeatedForwardBendRequestType = {
                seatedForwardBendOne: physicalFitnessSoftnessSeatedForwardBendOne,
                seatedForwardBendTwo: physicalFitnessSoftnessSeatedForwardBendTwo,
                seatedForwardBendThree: physicalFitnessSoftnessSeatedForwardBendThree,
                mark: physicalFitnessSoftnessMark
            };

            const chairSeatedForwardBendData: ReplacePhysicalFitnessSoftnessChairSeatedForwardBendRequestType = {
                chairSeatedForwardBendOne: physicalFitnessSoftnessChairSeatedForwardBendOne,
                chairSeatedForwardBendTwo: physicalFitnessSoftnessChairSeatedForwardBendTwo,
                chairSeatedForwardBendThree: physicalFitnessSoftnessChairSeatedForwardBendThree,
                mark: physicalFitnessSoftnessMark
            };

            const pullBackTestData: ReplacePhysicalFitnessSoftnessPullBackTestRequestType = {
                pullBackTestOne: physicalFitnessSoftnessPullBackTestOne,
                pullBackTestTwo: physicalFitnessSoftnessPullBackTestTwo,
                mark: physicalFitnessSoftnessMark
            };

            const handleErrorCode = (error: any) => {
                const errorCode = axiosErrorCode<ReplacePhysicalFitnessSoftnessErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('格式輸入錯誤');
                        break;
                    case 'StudentDoesNotExist':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('查無學員');
                        break;
                    case 'UnderTheAgeOf21To65':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('此體適能項目適合 21 到 65 歲');
                        break;
                    case 'UnderTheAgeOf65':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('此體適能項目適合 65 歲以上');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('伺服器錯誤');
                        break;
                }
            };

            if (isStudentGreaterThan65(storeGetters.age)) {
                replacePhysicalFitnessSoftnessSeatedForwardBend(storeGetters.studentId, seatedForwardBendData).then(() => {
                    closePhysicalFitnessSoftnessDialog();
                    setVisableInfoDialog(true);
                    setVisableInfoDialogTitle('資訊');
                    setVisableInfoDialogMessage('更新柔軟度資料成功');
                    handleBodyMassAndPhysicalFitnessList();
                }).catch((error) => {
                    handleErrorCode(error);
                });
            }
            else {
                replacePhysicalFitnessSoftnessChairSeatedForwardBend(storeGetters.studentId, chairSeatedForwardBendData).then(() => {
                    replacePhysicalFitnessSoftnessPullBackTest(storeGetters.studentId, pullBackTestData).then(() => {
                        closePhysicalFitnessSoftnessDialog();
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('資訊');
                        setVisableInfoDialogMessage('更新柔軟度資料成功');
                        handleBodyMassAndPhysicalFitnessList();
                    }).catch((error) => {
                        handleErrorCode(error);
                    });
                }).catch((error) => {
                    handleErrorCode(error);
                });
            }
        }
    };


    /**
     * ------------------------------------------------------------
     * ---------------------- 平衡 --------------------------------
     * ------------------------------------------------------------
     */    

    // 顯示體適能-平衡彈窗
    const showPhysicalFitnessBalanceDialog = () => {
        setVisiblePhysicalFitnessBalanceDialog(true);

        resetPhysicalFitnessBalanceField();
    };

    // 關閉體適能-平衡彈窗
    const closePhysicalFitnessBalanceDialog = () => {
        setVisiblePhysicalFitnessBalanceDialog(false);
        
        resetPhysicalFitnessBalanceField();
    };

    const resetPhysicalFitnessBalanceField = () => {
        setPhysicalFitnessBalanceAdultEyeOpeningMonopodSecond(undefined);
        setPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecond(undefined);
        setPhysicalFitnessBalanceSittingAroundOneSecond(undefined);
        setPhysicalFitnessBalanceSittingAroundTwoSecond(undefined);
        setPhysicalFitnessBalanceMark(undefined);
        
        setPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondErrorMessage('');
        setPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondErrorMessage('');
        setPhysicalFitnessBalanceSittingAroundOneSecondErrorMessage('');
        setPhysicalFitnessBalanceSittingAroundTwoSecondErrorMessage('');
        setPhysicalFitnessBalanceMarkErrorMessage('');
    };

    /**
     * 處理體適能-壯年開眼單足立
     */
    const handlePhysicalFitnessBalanceAdultEyeOpeningMonopodSecondInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(physicalFitnessBalanceFormRef.current, physicalFitnessBalanceFormAdultValidate);

        if (isValid !== undefined) {
            if (isValid.adultEyeOpeningMonopodSecond) {
                setPhysicalFitnessBalanceAdultEyeOpeningMonopodSecond(undefined);
                setPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondErrorMessage(isValid.adultEyeOpeningMonopodSecond[0].replace('Adult eye opening monopod second ', ''));
            }
            else {
                setPhysicalFitnessBalanceAdultEyeOpeningMonopodSecond(parseFloat(e.target.value));
                setPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessBalanceAdultEyeOpeningMonopodSecond(parseFloat(e.target.value));
            setPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondErrorMessage('');
        }
    };

    /**
     * 處理體適能-老年開眼單足立
     */
    const handlePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(physicalFitnessBalanceFormRef.current, physicalFitnessBalanceFormElderlyValidate);

        if (isValid !== undefined) {
            if (isValid.elderlyEyeOpeningMonopodSecond) {
                setPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecond(undefined);
                setPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondErrorMessage(isValid.elderlyEyeOpeningMonopodSecond[0].replace('Elderly eye opening monopod second ', ''));
            }
            else {
                setPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecond(parseFloat(e.target.value));
                setPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecond(parseFloat(e.target.value));
            setPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondErrorMessage('');
        }
    };

    /**
     * 處理體適能-第一次坐立繞物輸入
     */
    const handlePhysicalFitnessBalanceSittingAroundOneSecondInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(physicalFitnessBalanceFormRef.current, physicalFitnessBalanceFormElderlyValidate);

        if (isValid !== undefined) {
            if (isValid.sittingAroundOneSecond) {
                setPhysicalFitnessBalanceSittingAroundOneSecond(undefined);
                setPhysicalFitnessBalanceSittingAroundOneSecondErrorMessage(isValid.sittingAroundOneSecond[0].replace('Sitting around one second ', ''));
            }
            else {
                setPhysicalFitnessBalanceSittingAroundOneSecond(parseFloat(e.target.value));
                setPhysicalFitnessBalanceSittingAroundOneSecondErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessBalanceSittingAroundOneSecond(parseFloat(e.target.value));
            setPhysicalFitnessBalanceSittingAroundOneSecondErrorMessage('');
        }
    };

    /**
     * 處理體適能-第二次坐立繞物輸入
     */
    const handlePhysicalFitnessBalanceSittingAroundTwoSecondInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const isValid = validate(physicalFitnessBalanceFormRef.current, physicalFitnessBalanceFormElderlyValidate);

        if (isValid !== undefined) {
            if (isValid.sittingAroundTwoSecond) {
                setPhysicalFitnessBalanceSittingAroundTwoSecond(undefined);
                setPhysicalFitnessBalanceSittingAroundTwoSecondErrorMessage(isValid.sittingAroundTwoSecond[0].replace('Sitting around two second ', ''));
            }
            else {
                setPhysicalFitnessBalanceSittingAroundTwoSecond(parseFloat(e.target.value));
                setPhysicalFitnessBalanceSittingAroundTwoSecondErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessBalanceSittingAroundTwoSecond(parseFloat(e.target.value));
            setPhysicalFitnessBalanceSittingAroundTwoSecondErrorMessage('');
        }
    };

    /**
     * 處理體適能-備註輸入
     */
    const handlePhysicalFitnessBalanceMarkInput = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        const targetValid = isStudentGreaterThan65(storeGetters.age) ? physicalFitnessBalanceFormAdultValidate : physicalFitnessBalanceFormElderlyValidate;
        const isValid = validate(physicalFitnessBalanceFormRef.current, targetValid);

        if (isValid !== undefined) {
            if (isValid.mark) {
                setPhysicalFitnessBalanceMark(undefined);
                setPhysicalFitnessBalanceMarkErrorMessage(isValid.mark[0].replace('Mark ', ''));
            }
            else {
                setPhysicalFitnessBalanceMark(e.target.value);
                setPhysicalFitnessBalanceMarkErrorMessage('');
            }
        }
        else {
            setPhysicalFitnessBalanceMark(e.target.value);
            setPhysicalFitnessBalanceMarkErrorMessage('');
        }
    };

    /**
     * 檢查體適能-平衡相關欄位
     */
    const inspectPhysicalFitnessBalanceField = (): boolean => {
        if (isStudentGreaterThan65(storeGetters.age)) {
            const isValid = validate(physicalFitnessBalanceFormRef.current, physicalFitnessBalanceFormAdultValidate);

            if (isValid !== undefined) {
                if (isValid.adultEyeOpeningMonopodSecond) {
                    setPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondErrorMessage(isValid.adultEyeOpeningMonopodSecond[0].replace('Adult eye opening monopod second ', ''));
                }
                else {
                    setPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondErrorMessage('');
                }

                if (isValid.mark) {
                    setPhysicalFitnessBalanceMarkErrorMessage(isValid.mark[0].replace('Mark ', ''));
                }
                else {
                    setPhysicalFitnessBalanceMarkErrorMessage('');
                }

                return false;
            }
            else {
                setPhysicalFitnessBalanceAdultEyeOpeningMonopodSecondErrorMessage('');
                setPhysicalFitnessBalanceMarkErrorMessage('');

                return true;
            }
        }
        else {
            const isValid = validate(physicalFitnessBalanceFormRef.current, physicalFitnessBalanceFormElderlyValidate);

            if (isValid !== undefined) {
                if (isValid.elderlyEyeOpeningMonopodSecond) {
                    setPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondErrorMessage(isValid.elderlyEyeOpeningMonopodSecond[0].replace('Elderly eye opening monopod second ', ''));
                }
                else {
                    setPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondErrorMessage('');
                }

                if (isValid.sittingAroundOneSecond) {
                    setPhysicalFitnessBalanceSittingAroundOneSecondErrorMessage(isValid.sittingAroundOneSecond[0].replace('Sitting around one second ', ''));
                }
                else {
                    setPhysicalFitnessBalanceSittingAroundOneSecondErrorMessage('');
                }

                if (isValid.sittingAroundTwoSecond) {
                    setPhysicalFitnessBalanceSittingAroundTwoSecondErrorMessage(isValid.sittingAroundTwoSecond[0].replace('Sitting around two second ', ''));
                }
                else {
                    setPhysicalFitnessBalanceSittingAroundTwoSecondErrorMessage('');
                }

                if (isValid.mark) {
                    setPhysicalFitnessBalanceMarkErrorMessage(isValid.mark[0].replace('Mark ', ''));
                }
                else {
                    setPhysicalFitnessBalanceMarkErrorMessage('');
                }

                return false;
            }
            else {
                setPhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondErrorMessage('');
                setPhysicalFitnessBalanceSittingAroundOneSecondErrorMessage('');
                setPhysicalFitnessBalanceSittingAroundTwoSecondErrorMessage('');
                setPhysicalFitnessBalanceMarkErrorMessage('');

                return true;
            }
        }
    };

    /**
     * 新增或更新體適能-平衡
     */
    const handlePhysicalFitnessBalanceMark = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        const isValid = inspectPhysicalFitnessBalanceField();

        if (!isValid) {
            return;
        }
        else {
            const adultEyeOpeningMonopodSecondData: ReplacePhysicalFitnessBalanceAdultEyeOpeningMonopodSecondRequestType = {
                adultEyeOpeningMonopodSecond: physicalFitnessBalanceAdultEyeOpeningMonopodSecond,
                mark: physicalFitnessBalanceMark
            };

            const elderlyEyeOpeningMonopodSecondData: ReplacePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondRequestType = {
                elderlyEyeOpeningMonopodSecond: physicalFitnessBalanceElderlyEyeOpeningMonopodSecond,
                mark: physicalFitnessBalanceMark
            };

            const sittingAroundOneSecondData: ReplacePhysicalFitnessBalanceSittingAroundOneSecondRequestType = {
                sittingAroundOneSecond: physicalFitnessBalanceSittingAroundOneSecond,
                mark: physicalFitnessBalanceMark
            };

            const sittingAroundTwoSecondData: ReplacePhysicalFitnessBalanceSittingAroundTwoSecondRequestType = {
                sittingAroundTwoSecond: physicalFitnessBalanceSittingAroundTwoSecond,
                mark: physicalFitnessBalanceMark
            };

            const handleErrorCode = (error: any) => {
                const errorCode = axiosErrorCode<ReplacePhysicalFitnessBalanceErrorCodeType>(error);

                switch (errorCode) {
                    case 400:
                    case 'BadRequest':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('格式輸入錯誤');
                        break;
                    case 'StudentDoesNotExist':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('查無學員');
                        break;
                    case 'UnderTheAgeOf21To65':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('此體適能項目適合 21 到 65 歲');
                        break;
                    case 'UnderTheAgeOf65':
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('此體適能項目適合 65 歲以上');
                        break;
                    case 401:
                        dispatch({ type: 'user/resetToken' });
                        navigate('/home');
                        break;
                    default:
                        setVisableInfoDialog(true);
                        setVisableInfoDialogTitle('錯誤');
                        setVisableInfoDialogMessage('伺服器錯誤');
                        break;
                }
            };

            if (isStudentGreaterThan65(storeGetters.age)) {
                replacePhysicalFitnessBalanceAdultEyeOpeningMonopodSecond(storeGetters.studentId, adultEyeOpeningMonopodSecondData).then(() => {
                    closePhysicalFitnessBalanceDialog();
                    setVisableInfoDialog(true);
                    setVisableInfoDialogTitle('資訊');
                    setVisableInfoDialogMessage('更新平衡資料成功');
                    handleBodyMassAndPhysicalFitnessList();
                }).catch((error) => {
                    handleErrorCode(error);
                });
            }
            else {
                replacePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecond(storeGetters.studentId, elderlyEyeOpeningMonopodSecondData).then(() => {
                    replacePhysicalFitnessBalanceSittingAroundOneSecond(storeGetters.studentId, sittingAroundOneSecondData).then(() => {
                        replacePhysicalFitnessBalanceSittingAroundTwoSecond(storeGetters.studentId, sittingAroundTwoSecondData).then(() => {
                            closePhysicalFitnessBalanceDialog();
                            setVisableInfoDialog(true);
                            setVisableInfoDialogTitle('資訊');
                            setVisableInfoDialogMessage('更新平衡資料成功');
                            handleBodyMassAndPhysicalFitnessList();
                        }).catch((error) => {
                            handleErrorCode(error);
                        });
                    }).catch((error) => {
                        handleErrorCode(error);
                    });
                }).catch((error) => {
                    handleErrorCode(error);
                });
            }
        }
    };

    // 關閉資訊彈窗
    const closeInfoDialog = () => {
        setVisableInfoDialog(false);
        setVisableInfoDialogTitle('');
        setVisableInfoDialogMessage('');
    };

    useEffect(() => {
        handleBodyMassAndPhysicalFitnessList();
    }, []);

    useEffect(() => {
        bmiGroup?.children.forEach((item, index) => {
            const value = bodyCompositionBmiCommentConvertFraction(item.bmiCommentLevel);
            drawProgressBar(`.bmiBar${index + 1}`, value);
        });
    }, [bmiGroup]);

    useEffect(() => {
        bodyFatGroup?.children.forEach((item, index) => {
            const value = bodyCompositionBodyFatCommentConvertFraction(item.bodyFatCommentLevel);
            drawProgressBar(`.bodyFatBar${index + 1}`, value);
        });
    }, [bodyFatGroup]);

    useEffect(() => {
        visceralFatGroup?.children.forEach((item, index) => {
            const value = bodyCompositionVisceralFatCommentConvertFraction(item.visceralFatCommentLevel);
            drawProgressBar(`.visceralFat${index + 1}`, value);
        });
    }, [visceralFatGroup]);

    useEffect(() => {
        skeletalMuscleRateGroup?.children.forEach((item, index) => {
            const value = bodyCompositioSkeletalMuscleRateCommentConvertFraction(item.skeletalMuscleRateCommentLevel);
            drawProgressBar(`.skeletalMuscleRateBar${index + 1}`, value);
        });
    }, [skeletalMuscleRateGroup]);

    useEffect(() => {
        physicalFitnessCardiorespiratoryFitnessGroup?.children.forEach((item, index) => {
            const value = item.cardiorespiratoryFitnessFraction;
            drawProgressBar(`.physicalFitnessCardiorespiratoryFitnessBar${index + 1}`, value);
        });
    }, [physicalFitnessCardiorespiratoryFitnessGroup]);

    useEffect(() => {
        physicalFitnessMuscleStrengthAndMuscleEnduranceGroup?.children.forEach((item, index) => {
            const value = item.muscleStrengthAndMuscleEnduranceFitness;
            drawProgressBar(`.physicalFitnessMuscleStrengthAndMuscleEnduranceBar${index + 1}`, value);
        });
    }, [physicalFitnessMuscleStrengthAndMuscleEnduranceGroup]);

    useEffect(() => {
        physicalFitnessSoftnessGroup?.children.forEach((item, index) => {
            const value = item.softnessFitness;
            drawProgressBar(`.physicalFitnessSoftnessBar${index + 1}`, value);
        });
    }, [physicalFitnessSoftnessGroup]);

    useEffect(() => {
        physicalFitnessBalanceGroup?.children.forEach((item, index) => {
            const value = item.balanceFitness;
            drawProgressBar(`.physicalFitnessBalanceBar${index + 1}`, value);
        });
    }, [physicalFitnessBalanceGroup]);

    return (
        <div className='bodyMassRecord'>
            <StudentInfoBar></StudentInfoBar>
            <PageTitleBar title='身體組成與體適能評價'></PageTitleBar>

            <div className='body_mass_and_physical_fitness_info'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                        <div className='card'>
                            <table>
                                <thead>
                                    <tr>
                                        <th>
                                            <p>項目</p>
                                        </th>

                                        <th colSpan={2}>
                                            <p>目前數值</p>
                                        </th>

                                        <th>
                                            <p>評價等級</p>
                                        </th>

                                        <th>
                                            <p>備註</p>
                                        </th>

                                        <th>
                                            <p>歷史紀錄</p>
                                        </th>

                                        <th>
                                            <p>編輯</p>
                                        </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>BMI</td>
                                        <td className='bmiBar'></td>
                                        <td>{bmiGroup?.bmiCommentLevel !== '-' ? bmiGroup?.bmi : '-'}</td>
                                        <td className={`bmiComment ${bmiColor(bmiGroup?.bmiCommentLevel)}`}>{bmiGroup?.bmiCommentLevel}</td>
                                        <td>{bmiGroup?.mark}</td>

                                        <TdArrowDown dropDownClassName='bmiChildren' data={bmiGroup?.children}></TdArrowDown>

                                        <td className='icon_btn_edit' onClick={() => showBodyCompositionDialog('bmi', bmiGroup?.bmi, bmiGroup?.mark)}>
                                            <img src={btn_edit} />
                                        </td>
                                    </tr>

                                    {
                                        bmiGroup?.children.map((item, index) => {
                                            const className = `bmiBar${index + 1}`;

                                            return (
                                                <tr className='bmiChildren' key={index}>
                                                    <td>{item.updateRecordDate}</td>
                                                    <td className={className}></td>
                                                    <td>{item.bmi}</td>
                                                    <td className={`bmiComment ${bmiColor(item?.bmiCommentLevel)}`}>{item.bmiCommentLevel}</td>
                                                    <td>{item.mark}</td>
                                                </tr>
                                            );
                                        })
                                    }

                                    <tr>
                                        <td>體脂肪</td>
                                        <td className='bodyFatBar'></td>
                                        <td>{bodyFatGroup?.bodyFatCommentLevel !== '-' ? bodyFatGroup?.bodyFat : '-'}</td>
                                        <td className={`bodyFatComment ${bodyFatColor(bodyFatGroup?.bodyFatCommentLevel)}`}>{bodyFatGroup?.bodyFatCommentLevel}</td>
                                        <td>{bodyFatGroup?.mark}</td>

                                        <TdArrowDown dropDownClassName='bodyFatChildren' data={bodyFatGroup?.children}></TdArrowDown>

                                        <td className='icon_btn_edit' onClick={() => showBodyCompositionDialog('bodyFat', bodyFatGroup?.bodyFat, bodyFatGroup?.mark)}>
                                            <img src={btn_edit} />
                                        </td>
                                    </tr>

                                    {
                                        bodyFatGroup?.children.map((item, index) => {
                                            const className = `bodyFatBar${index + 1}`;

                                            return (
                                                <tr className='bodyFatChildren' key={index}>
                                                    <td>{item.updateRecordDate}</td>
                                                    <td className={className}></td>
                                                    <td>{item.bodyFat}</td>
                                                    <td className={`bodyFatComment ${bodyFatColor(item?.bodyFatCommentLevel)}`}>{item.bodyFatCommentLevel}</td>
                                                    <td>{item.mark}</td>
                                                </tr>
                                            );
                                        })
                                    }

                                    <tr>
                                        <td>內臟脂肪</td>
                                        <td className='visceralFatBar'></td>
                                        <td>{visceralFatGroup?.visceralFatCommentLevel !== '-' ? visceralFatGroup?.visceralFat : '-'}</td>
                                        <td className={`visceralFatComment ${visceralFatColor(visceralFatGroup?.visceralFatCommentLevel)}`}>{visceralFatGroup?.visceralFatCommentLevel}</td>
                                        <td>{visceralFatGroup?.mark}</td>

                                        <TdArrowDown dropDownClassName='visceralFatChildren' data={visceralFatGroup?.children}></TdArrowDown>

                                        <td className='icon_btn_edit' onClick={() => showBodyCompositionDialog('visceralFat', visceralFatGroup?.visceralFat, visceralFatGroup?.mark)}>
                                            <img src={btn_edit} />
                                        </td>
                                    </tr>

                                    {
                                        visceralFatGroup?.children.map((item, index) => {
                                            const className = `visceralFat${index + 1}`;

                                            return (
                                                <tr className='visceralFatChildren' key={index}>
                                                    <td>{item.updateRecordDate}</td>
                                                    <td className={className}></td>
                                                    <td>{item.visceralFat}</td>
                                                    <td className={`visceralFatComment ${visceralFatColor(item?.visceralFatCommentLevel)}`}>{item.visceralFatCommentLevel}</td>
                                                    <td>{item.mark}</td>
                                                </tr>
                                            );
                                        })
                                    }

                                    <tr>
                                        <td>骨骼肌率</td>
                                        <td className='skeletalMuscleRateBar'></td>
                                        <td>{skeletalMuscleRateGroup?.skeletalMuscleRateCommentLevel !== '-' ? skeletalMuscleRateGroup?.skeletalMuscleRate : '-'}</td>
                                        <td className={`skeletalMuscleRatComment ${skeletalMuscleRateColor(skeletalMuscleRateGroup?.skeletalMuscleRateCommentLevel)}`}>{skeletalMuscleRateGroup?.skeletalMuscleRateCommentLevel}</td>
                                        <td className='mark'>{skeletalMuscleRateGroup?.mark}</td>

                                        <TdArrowDown dropDownClassName='skeletalMuscleRateChildren' data={skeletalMuscleRateGroup?.children}></TdArrowDown>

                                        <td className='icon_btn_edit' onClick={() => showBodyCompositionDialog('skeletalMuscleRate', skeletalMuscleRateGroup?.skeletalMuscleRate, skeletalMuscleRateGroup?.mark)}>
                                            <img src={btn_edit} />
                                        </td>
                                    </tr>

                                    {
                                        skeletalMuscleRateGroup?.children.map((item, index) => {
                                            const className = `skeletalMuscleRateBar${index + 1}`;

                                            return (
                                                <tr className='skeletalMuscleRateChildren' key={index}>
                                                    <td>{item.updateRecordDate}</td>
                                                    <td className={className}></td>
                                                    <td>{item.skeletalMuscleRate}</td>
                                                    <td className={`skeletalMuscleRatComment ${skeletalMuscleRateColor(item?.skeletalMuscleRateCommentLevel)}`}>{item.skeletalMuscleRateCommentLevel}</td>
                                                    <td>{item.mark}</td>
                                                </tr>
                                            );
                                        })
                                    }

                                    <tr>
                                        <td>心肺適能</td>
                                        <td className='physicalFitnessCardiorespiratoryFitnessBar'></td>
                                        <td>-</td>
                                        <td className={`physicalFitnessCardiorespiratoryFitnessComment ${physicalFitnessCardiorespiratoryFitnessColor(physicalFitnessCardiorespiratoryFitnessGroup?.cardiorespiratoryFitnessComment)}`}>{physicalFitnessCardiorespiratoryFitnessGroup?.cardiorespiratoryFitnessComment}</td>
                                        <td>{physicalFitnessCardiorespiratoryFitnessGroup?.mark}</td>

                                        <TdArrowDown dropDownClassName='physicalFitnessCardiorespiratoryFitnessChildren' data={physicalFitnessCardiorespiratoryFitnessGroup?.children}></TdArrowDown>

                                        <td className='icon_btn_edit' onClick={showPhysicalFitnessCardiorespiratoryFitnessDialog}>
                                            <img src={btn_edit} />
                                        </td>
                                    </tr>

                                    {
                                        physicalFitnessCardiorespiratoryFitnessGroup?.children.map((item, index) => {
                                            const className = `physicalFitnessCardiorespiratoryFitnessBar${index + 1}`;

                                            return (
                                                <tr className='physicalFitnessCardiorespiratoryFitnessChildren' key={index}>
                                                    <td>{item.updateRecordDate}</td>
                                                    <td className={className}></td>
                                                    <td>{item.cardiorespiratoryFitnessFraction}</td>
                                                    <td className={`physicalFitnessCardiorespiratoryFitnessComment ${physicalFitnessCardiorespiratoryFitnessColor(item?.cardiorespiratoryFitnessComment)}`}>{item.cardiorespiratoryFitnessComment}</td>
                                                    <td>{item.mark}</td>
                                                </tr>
                                            );
                                        })
                                    }

                                    <tr>
                                        <td>肌力與肌耐力</td>
                                        <td className='physicalFitnessMuscleStrengthAndMuscleEnduranceBar'></td>
                                        <td>-</td>
                                        <td className={`physicalFitnessMuscleStrengthAndMuscleEnduranceComment ${physicalFitnessMuscleStrengthAndMuscleEnduranceColor(physicalFitnessMuscleStrengthAndMuscleEnduranceGroup?.muscleStrengthAndMuscleEnduranceComment)}`}>{physicalFitnessMuscleStrengthAndMuscleEnduranceGroup?.muscleStrengthAndMuscleEnduranceComment}</td>
                                        <td>{physicalFitnessMuscleStrengthAndMuscleEnduranceGroup?.mark}</td>

                                        <TdArrowDown dropDownClassName='physicalFitnessMuscleStrengthAndMuscleEnduranceChildren' data={physicalFitnessMuscleStrengthAndMuscleEnduranceGroup?.children}></TdArrowDown>

                                        <td className='icon_btn_edit'>
                                            <img src={btn_edit} onClick={showPhysicalFitnessMuscleStrengthAndMuscleEnduranceDialog} />
                                        </td>
                                    </tr>

                                    {
                                        physicalFitnessMuscleStrengthAndMuscleEnduranceGroup?.children.map((item, index) => {
                                            const className = `physicalFitnessMuscleStrengthAndMuscleEnduranceBar${index + 1}`;

                                            return (
                                                <tr className='physicalFitnessMuscleStrengthAndMuscleEnduranceChildren' key={index}>
                                                    <td>{item.updateRecordDate}</td>
                                                    <td className={className}></td>
                                                    <td>{item.muscleStrengthAndMuscleEnduranceFitness}</td>
                                                    <td className={`physicalFitnessMuscleStrengthAndMuscleEnduranceComment ${physicalFitnessMuscleStrengthAndMuscleEnduranceColor(item?.muscleStrengthAndMuscleEnduranceComment)}`}>{item.muscleStrengthAndMuscleEnduranceComment}</td>
                                                    <td>{item.mark}</td>
                                                </tr>
                                            );
                                        })
                                    }

                                    <tr>
                                        <td>柔軟度</td>
                                        <td className='physicalFitnessSoftnessBar'></td>
                                        <td>-</td>
                                        <td className={`physicalFitnessSoftnessComment ${physicalFitnessSoftnessColor(physicalFitnessSoftnessGroup?.softnessComment)}`}>{physicalFitnessSoftnessGroup?.softnessComment}</td>
                                        <td>{physicalFitnessSoftnessGroup?.mark}</td>

                                        <TdArrowDown dropDownClassName='physicalFitnessSoftnessChildren' data={physicalFitnessSoftnessGroup?.children}></TdArrowDown>

                                        <td className='icon_btn_edit'>
                                            <img src={btn_edit} onClick={showPhysicalFitnessSoftnessDialog} />
                                        </td>
                                    </tr>

                                    {
                                        physicalFitnessSoftnessGroup?.children.map((item, index) => {
                                            const className = `physicalFitnessSoftnessBar${index + 1}`;

                                            return (
                                                <tr className='physicalFitnessSoftnessChildren' key={index}>
                                                    <td>{item.updateRecordDate}</td>
                                                    <td className={className}></td>
                                                    <td>{item.softnessFitness}</td>
                                                    <td className={`physicalFitnessSoftnessComment ${physicalFitnessSoftnessColor(item?.softnessComment)}`}>{item.softnessComment}</td>
                                                    <td>{item.mark}</td>
                                                </tr>
                                            );
                                        })
                                    }

                                    <tr>
                                        <td>平衡</td>
                                        <td className='physicalFitnessBalanceBar'></td>
                                        <td>-</td>
                                        <td className={`physicalFitnessBalanceComment ${physicalFitnessBalanceColor(physicalFitnessBalanceGroup?.balanceComment, storeGetters.age)}`}>{physicalFitnessBalanceGroup?.balanceComment}</td>
                                        <td>{physicalFitnessBalanceGroup?.mark}</td>

                                        <TdArrowDown dropDownClassName='physicalFitnessBalanceChildren' data={physicalFitnessBalanceGroup?.children}></TdArrowDown>

                                        <td className='icon_btn_edit'>
                                            <img src={btn_edit} onClick={showPhysicalFitnessBalanceDialog} />
                                        </td>
                                    </tr>

                                    {
                                        physicalFitnessBalanceGroup?.children.map((item, index) => {
                                            const className = `physicalFitnessBalanceBar${index + 1}`;

                                            return (
                                                <tr className='physicalFitnessBalanceChildren' key={index}>
                                                    <td>{item.updateRecordDate}</td>
                                                    <td className={className}></td>
                                                    <td>{item.balanceFitness}</td>
                                                    <td className={`physicalFitnessBalanceComment ${physicalFitnessBalanceColor(item?.balanceComment, storeGetters.age)}`}>{item.balanceComment}</td>
                                                    <td>{item.mark}</td>
                                                </tr>
                                            );
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            {/* 身體組成彈窗 */}
            {
                visableBodyCompositionDialog && (
                    <Dialog className='visableBodyCompositionDialog'>
                        <Dialog.Title title='編輯'></Dialog.Title>

                        <form id='bodyCompositionForm' onSubmit={handleBodyCompositionReplace} ref={bodyCompositionFormRef}>
                            <Dialog.Body>
                                <div className='content'>
                                    <div className='group'>
                                        <h6>數值</h6>
                                        <input className='value' type='text' placeholder='請輸入數值' name='value' defaultValue={inputBodyCompositionValue} onChange={handleChangeBodyCompositionValue} />
                                        <p className='error_message'>{inputBodyCompositionValueErrorMessage}</p>
                                    </div>

                                    <div className='group'>
                                        <h6>備註</h6>
                                        <textarea className='mark' rows={5} cols={30} placeholder='請輸入備註' name='mark' defaultValue={inputBodyCompositionMark} onChange={handleChangeBodyCompositionMark}></textarea>
                                        <p className='error_message'>{inputBodyCompositionMarkErrorMessage}</p>
                                    </div>
                                </div>
                            </Dialog.Body>

                            <Dialog.Footer>
                                <button type='submit' className='dialogBtnBtnYes'>確定</button>
                                <button className='dialogBtnBtnNo' onClick={closeBodyCompositionDialog}>取消</button>
                            </Dialog.Footer>
                        </form>
                    </Dialog>
                )
            }

            {/* 心肺適能 */}
            {
                visiblePhysicalFitnessCardiorespiratoryFitnessDialog && (
                    <Dialog className='visiblePhysicalFitnessCardiorespiratoryFitnessDialog'>
                        <Dialog.Title title='編輯'></Dialog.Title>

                        <form className='physicalFitnessForm' ref={physicalFitnessCardiorespiratoryFitnessFormRef} onSubmit={handleReplacePhysicalFitnessCardiorespiratoryFitnessInfo}>
                            <Dialog.Body>
                                <div className='content'>
                                    {
                                        isStudentGreaterThan65(storeGetters.age) ? (
                                            <Fragment>
                                                <div className='group'>
                                                    <h6>登階測驗-第一次脈搏 (單位: 次數)</h6>
                                                    <input className='pulseRateOne' type='text' placeholder='請輸入第一次脈搏次數' name='pulseRateOne' defaultValue={physicalFitnessCardiorespiratoryFitnessPulseRateOne} onChange={handlePhysicalFitnessCardiorespiratoryFitnessPulseRateOneInput} />
                                                    <p className='error_message'>{physicalFitnessCardiorespiratoryFitnessPulseRateOneErrorMessage}</p>
                                                </div>

                                                <div className='group'>
                                                    <h6>登階測驗-第二次脈搏次數 (單位: 次數)</h6>
                                                    <input className='pulseRateTwo' type='text' placeholder='請輸入第二次脈搏次數' name='pulseRateTwo' defaultValue={physicalFitnessCardiorespiratoryFitnessPulseRateTwo} onChange={handlePhysicalFitnessCardiorespiratoryFitnessPulseRateTwoInput} />
                                                    <p className='error_message'>{physicalFitnessCardiorespiratoryFitnessPulseRateTwoErrorMessage}</p>
                                                </div>

                                                <div className='group'>
                                                    <h6>登階測驗-第三次脈搏次數 (單位: 次數)</h6>
                                                    <input className='pulseRateThree' type='text' placeholder='請輸入第三次脈搏次數' name='pulseRateThree' defaultValue={physicalFitnessCardiorespiratoryFitnessPulseRateThree} onChange={handlePhysicalFitnessCardiorespiratoryFitnessPulseRateThreeInput} />
                                                    <p className='error_message'>{physicalFitnessCardiorespiratoryFitnessPulseRateThreeErrorMessage}</p>
                                                </div>
                                            </Fragment>
                                        ) : (
                                            <Fragment>
                                                <div className='group'>
                                                    <h6>原地站立抬膝 (單位: 次數)</h6>
                                                    <input className='kneeLiftFrequency' type='text' placeholder='請輸入原地站立抬膝次數' name='kneeLiftFrequency' defaultValue={physicalFitnessCardiorespiratoryFitnessKneeLiftFrequency} onChange={handlePhysicalFitnessCardiorespiratoryFitnessKneeLiftFrequency} />
                                                    <p className='error_message'>{physicalFitnessCardiorespiratoryFitnessKneeLiftFrequencyErrorMessage}</p>
                                                </div>
                                            </Fragment>
                                        )
                                    }

                                    <div className='group'>
                                        <h6>備註</h6>
                                        <textarea className='mark' rows={5} cols={30} placeholder='請輸入備註' defaultValue={physicalFitnessCardiorespiratoryFitnessMark} name='mark' onChange={handlePhysicalFitnessCardiorespiratoryFitnessMarkInput}></textarea>
                                        <p className='error_message'>{physicalFitnessCardiorespiratoryFitnessMarkErrorMessage}</p>
                                    </div>
                                </div>
                            </Dialog.Body>

                            <Dialog.Footer>
                                <button type='submit' className='dialogBtnBtnYes'>確定</button>
                                <button type='button' className='dialogBtnBtnNo' onClick={closePhysicalFitnessCardiorespiratoryFitnessDialog}>取消</button>
                            </Dialog.Footer>
                        </form>
                    </Dialog>
                )
            }

            {/* 肌力與肌耐力 */}
            {
                visiblePhysicalFitnessMuscleStrengthAndMuscleEnduranceDialog && (
                    <Dialog className='visiblePhysicalFitnessMuscleStrengthAndMuscleEnduranceDialog'>
                        <Dialog.Title title='編輯'></Dialog.Title>

                        <form className='physicalFitnessMscleStrengthAndMuscleEnduranceForm' ref={physicalFitnessMscleStrengthAndMuscleEnduranceFormRef} onSubmit={handleReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceInfo}>
                            <Dialog.Body>
                                <div className='content'>
                                    {
                                        isStudentGreaterThan65(storeGetters.age) ? (
                                            <Fragment>
                                                <div className='group'>
                                                    <h6>屈膝仰臥起坐 (單位: 次數)</h6>
                                                    <input className='kneeCrunchesFrequency' type='text' placeholder='請輸入屈膝仰臥起坐次數' name='kneeCrunchesFrequency' defaultValue={physicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequency} onChange={handlePhysicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyInput} />
                                                    <p className='error_message'>{physicalFitnessMscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyErrorMessage}</p>
                                                </div>
                                            </Fragment>
                                        ) : (
                                            <Fragment>
                                                <div className='group'>
                                                    <h6>手臂彎舉 (單位: 次數)</h6>
                                                    <input className='armCurlUpperBodyFrequency' type='text' placeholder='請輸入手臂彎舉' name='armCurlUpperBodyFrequency' defaultValue={physicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequency} onChange={handlePhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyInput} />
                                                    <p className='error_message'>{physicalFitnessMscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyErrorMessage}</p>
                                                </div>

                                                <div className='group'>
                                                    <h6>起立坐下 (單位: 次數)</h6>
                                                    <input className='armCurlLowerBodyFrequency' type='text' placeholder='請輸入起立坐下次數' name='armCurlLowerBodyFrequency' defaultValue={physicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequency} onChange={handlePhysicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyInput} />
                                                    <p className='error_message'>{physicalFitnessMscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyErrorMessage}</p>
                                                </div>
                                            </Fragment>
                                        )
                                    }

                                    <div className='group'>
                                        <h6>備註</h6>
                                        <textarea className='mark' rows={5} cols={30} placeholder='請輸入備註' name='mark' defaultValue={physicalFitnessMscleStrengthAndMuscleEnduranceMark} onChange={handlePhysicalFitnessMscleStrengthAndMuscleEnduranceMarkInput}></textarea>
                                        <p className='error_message'>{physicalFitnessMscleStrengthAndMuscleEnduranceMarkErrorMessage}</p>
                                    </div>
                                </div>
                            </Dialog.Body>

                            <Dialog.Footer>
                                <button className='dialogBtnBtnYes' type='submit'>確定</button>
                                <button className='dialogBtnBtnNo' type='button' onClick={closePhysicalFitnessMuscleStrengthAndMuscleEnduranceDialog}>取消</button>
                            </Dialog.Footer>
                        </form>
                    </Dialog>
                )
            }

            {/* 柔軟度 */}
            {
                visiblePhysicalFitnessSoftnessDialog && (
                    <Dialog className='visiblePhysicalFitnessSoftnessDialog'>
                        <Dialog.Title title='編輯'></Dialog.Title>

                        <form className='physicalFitnessSoftnessForm' ref={physicalFitnessSoftnessFormRef} onSubmit={handlePhysicalFitnessSoftnessMarkReplace}>
                            <Dialog.Body>
                                <div className='content'>
                                    {
                                        isStudentGreaterThan65(storeGetters.age) ? (
                                            <Fragment>
                                                <div className='group'>
                                                    <h6>第一次坐姿體前彎 (單位: 公分)</h6>
                                                    <input className='seatedForwardBendOne' type="text" placeholder='請輸入第一次坐姿體前彎' name='seatedForwardBendOne' defaultValue={physicalFitnessSoftnessSeatedForwardBendOne} onChange={handlePhysicalFitnessSoftnessSeatedForwardBendOneInput} />
                                                    <p className='error_message'>{physicalFitnessSoftnessSeatedForwardBendOneErrorMessage}</p>
                                                </div>

                                                <div className='group'>
                                                    <h6>第二次坐姿體前彎 (單位: 公分)</h6>
                                                    <input className='seatedForwardBendTwo' type="text" placeholder='請輸入第二次坐姿體前彎' name='seatedForwardBendTwo' defaultValue={physicalFitnessSoftnessSeatedForwardBendTwo} onChange={handlePhysicalFitnessSoftnessSeatedForwardBendTwoInput} />
                                                    <p className='error_message'>{physicalFitnessSoftnessSeatedForwardBendTwoErrorMessage}</p>
                                                </div>

                                                <div className='group'>
                                                    <h6>第三次坐姿體前彎 (單位: 公分)</h6>
                                                    <input className='seatedForwardBendThree' type="text" placeholder='請輸入第三次坐姿體前彎' name='seatedForwardBendThree' defaultValue={physicalFitnessSoftnessSeatedForwardBendThree} onChange={handlePhysicalFitnessSoftnessSeatedForwardBendThreeInput} />
                                                    <p className='error_message'>{physicalFitnessSoftnessSeatedForwardBendThreeErrorMessage}</p>
                                                </div>
                                            </Fragment>
                                        ) : (
                                            <Fragment>
                                                <div className='group'>
                                                    <h6>第一次椅子坐姿體前彎 (單位: 公分)</h6>
                                                    <input className='chairSeatedForwardBendOne' type="text" placeholder='請輸入第一次椅子坐姿體前彎' name='chairSeatedForwardBendOne' defaultValue={physicalFitnessSoftnessChairSeatedForwardBendOne} onChange={handlePhysicalFitnessSoftnessChairSeatedForwardBendOneInput} />
                                                    <p className='error_message'>{physicalFitnessSoftnessChairSeatedForwardBendOneErrorMessage}</p>
                                                </div>

                                                <div className='group'>
                                                    <h6>第二次椅子坐姿體前彎 (單位: 公分)</h6>
                                                    <input className='chairSeatedForwardBendTwo' type="text" placeholder='請輸入第二次椅子坐姿體前彎' name='chairSeatedForwardBendTwo' defaultValue={physicalFitnessSoftnessChairSeatedForwardBendTwo} onChange={handlePhysicalFitnessSoftnessChairSeatedForwardBendTwoInput} />
                                                    <p className='error_message'>{physicalFitnessSoftnessChairSeatedForwardBendTwoErrorMessage}</p>
                                                </div>

                                                <div className='group'>
                                                    <h6>第三次椅子坐姿體前彎 (單位: 公分)</h6>
                                                    <input className='chairSeatedForwardBendThree' type="text" placeholder='請輸入第三次椅子坐姿體前彎' name='chairSeatedForwardBendThree' defaultValue={physicalFitnessSoftnessChairSeatedForwardBendThree} onChange={handlePhysicalFitnessSoftnessChairSeatedForwardBendThreeInput} />
                                                    <p className='error_message'>{physicalFitnessSoftnessChairSeatedForwardBendThreeErrorMessage}</p>
                                                </div>

                                                <div className='group'>
                                                    <h6>第一次拉背測驗 (單位: 公分)</h6>
                                                    <input className='pullBackTestOne' type="text" placeholder='請輸入第一次拉背測驗' name='pullBackTestOne' defaultValue={physicalFitnessSoftnessPullBackTestOne} onChange={handlePhysicalFitnessSoftnessPullBackTestOneInput} />
                                                    <p className='error_message'>{physicalFitnessSoftnessPullBackTestOneErrorMessage}</p>
                                                </div>

                                                <div className='group'>
                                                    <h6>第二次拉背測驗 (單位: 公分)</h6>
                                                    <input className='pullBackTestTwo' type="text" placeholder='請輸入第二次拉背測驗' name='pullBackTestTwo' defaultValue={physicalFitnessSoftnessPullBackTestTwo} onChange={handlePhysicalFitnessSoftnessPullBackTestTwoInput} />
                                                    <p className='error_message'>{physicalFitnessSoftnessPullBackTestTwoErrorMessage}</p>
                                                </div>
                                            </Fragment>
                                        )
                                    }

                                    <div className='group'>
                                        <h6>備註</h6>
                                        <textarea className='mark' rows={5} cols={30} placeholder='請輸入備註' name='mark' defaultValue={physicalFitnessSoftnessMark} onChange={handlePhysicalFitnessSoftnessMarkInput}></textarea>
                                        <p className='error_message'>{physicalFitnessSoftnessMarkErrorMessage}</p>
                                    </div>
                                </div>
                            </Dialog.Body>

                            <Dialog.Footer>
                                <button className='dialogBtnBtnYes' type='submit'>確定</button>
                                <button className='dialogBtnBtnNo' type='button' onClick={closePhysicalFitnessSoftnessDialog}>取消</button>
                            </Dialog.Footer>
                        </form>
                    </Dialog>
                )
            }

            {/* 平衡 */}
            {
                visiblePhysicalFitnessBalanceDialog && (
                    <Dialog className='visiblePhysicalFitnessBalanceDialog'>
                        <Dialog.Title title='編輯'></Dialog.Title>

                        <form className='physicalFitnessBalanceForm' ref={physicalFitnessBalanceFormRef} onSubmit={handlePhysicalFitnessBalanceMark}>
                            <Dialog.Body>
                                <div className='content'>
                                    {
                                        isStudentGreaterThan65(storeGetters.age) ? (
                                            <Fragment>
                                                <div className='group'>
                                                    <h6>開眼單足立 (單位: 秒數)</h6>
                                                    <input className='adultEyeOpeningMonopodSecond' type="text" placeholder='請輸入開眼單足立' name='adultEyeOpeningMonopodSecond' defaultValue={physicalFitnessBalanceAdultEyeOpeningMonopodSecond} onChange={handlePhysicalFitnessBalanceAdultEyeOpeningMonopodSecondInput} />
                                                    <p className='error_message'>{physicalFitnessBalanceAdultEyeOpeningMonopodSecondErrorMessage}</p>
                                                </div>
                                            </Fragment>
                                        ) : (
                                            <Fragment>
                                                <div className='group'>
                                                    <h6>開眼單足立 (單位: 秒數)</h6>
                                                    <input className='elderlyEyeOpeningMonopodSecond' type="text" placeholder='請輸入開眼單足立' name='elderlyEyeOpeningMonopodSecond' defaultValue={physicalFitnessBalanceElderlyEyeOpeningMonopodSecond} onChange={handlePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondInput} />
                                                    <p className='error_message'>{physicalFitnessBalanceElderlyEyeOpeningMonopodSecondErrorMessage}</p>
                                                </div>

                                                <div className='group'>
                                                    <h6>第一次坐立繞物 (單位: 秒數)</h6>
                                                    <input className='sittingAroundOneSecond' type="text" placeholder='請輸入第一次坐立繞物' name='sittingAroundOneSecond' defaultValue={physicalFitnessBalanceSittingAroundOneSecond} onChange={handlePhysicalFitnessBalanceSittingAroundOneSecondInput} />
                                                    <p className='error_message'>{physicalFitnessBalanceSittingAroundOneSecondErrorMessage}</p>
                                                </div>

                                                <div className='group'>
                                                    <h6>第二次坐立繞物 (單位: 秒數)</h6>
                                                    <input className='sittingAroundTwoSecond' type="text" placeholder='請輸入第二次坐立繞物' name='sittingAroundTwoSecond' defaultValue={physicalFitnessBalanceSittingAroundTwoSecond} onChange={handlePhysicalFitnessBalanceSittingAroundTwoSecondInput} />
                                                    <p className='error_message'>{physicalFitnessBalanceSittingAroundTwoSecondErrorMessage}</p>
                                                </div>
                                            </Fragment>
                                        )
                                    }

                                    <div className='group'>
                                        <h6>備註</h6>
                                        <textarea className='mark' rows={5} cols={30} placeholder='請輸入備註' name='mark' defaultValue={physicalFitnessBalanceMark} onChange={handlePhysicalFitnessBalanceMarkInput}></textarea>
                                        <p className='error_message'>{physicalFitnessBalanceMarkErrorMessage}</p>
                                    </div>
                                </div>
                            </Dialog.Body>

                            <Dialog.Footer>
                                <button className='dialogBtnBtnYes' type='submit'>確定</button>
                                <button className='dialogBtnBtnNo' type='button' onClick={closePhysicalFitnessBalanceDialog}>取消</button>
                            </Dialog.Footer>
                        </form>
                    </Dialog>
                )
            }

            {/* 資訊彈窗 */}
            {
                visableInfoDialog && (
                    <Dialog className='visibleInfoDialog'>
                        <Dialog.Title title={visableInfoDialogTitle}></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <p>{visableInfoDialogMessage}</p>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnConfirm' type='button' onClick={closeInfoDialog}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }
        </div>
    );
};

export default BodyMassRecord;