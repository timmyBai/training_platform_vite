import React from 'react';

// css
import './404.sass';

import errorPage404 from '@/assets/images/404.jpg';

const ErrorPage404: React.FC = () => {
    return (
        <div className='errorPage'>
            <img src={errorPage404} />
        </div>
    );
};

export default ErrorPage404;