import React, { useRef, useState, useEffect, ReactNode } from 'react';
import { useNavigate } from 'react-router-dom';
import validate from 'validate.js';

// css
import './index.sass';

// store
import { login } from '@/store/modules/user';

// hook
import { useAppDispatch } from '@/hook/store';

// utils
import { axiosErrorCode } from '@/utils/request';

// components
import Dialog from '@/components/Dialog';

const Login: React.FC = (): ReactNode => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const loginFormRef = useRef<HTMLFormElement>(null);
    const [account, setAccount] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const accountRef = useRef<HTMLInputElement>(null);
    const passwordRef = useRef<HTMLInputElement>(null);
    const [accountMessage, setAccountMessage] = useState<string>('');
    const [passwordMessage, setPasswordMessage] = useState<string>('');
    const [authLoginApiMessage, setAuthLoginApiMessage] = useState<string>('');
    const [dialogVisable, setDialogVisable] = useState<boolean>(false);

    const formValidate = {
        'account': {
            presence: {
                message: '必須輸入帳號'
            },
            length: {
                maximum: 50,
                message: '帳號小於等於 50 字'
            }
        },
        'password': {
            presence: {
                message: '必須輸入密碼'
            },
            length: {
                maximum: 50,
                message: '密碼小於等於 50 字'
            }
        }
    };

    useEffect(() => {
        if (accountRef.current?.value === '') {
            accountRef.current.focus();
        }
        else {
            passwordRef.current?.focus();
        }
    }, []);

    const handleLogin = (e: React.FormEvent): void => {
        e.preventDefault();

        const isErrors = inspactLoginForm();

        if (isErrors) {
            return;
        }
        
        const data = {
            account,
            password
        };

        dispatch(login(data)).unwrap().then((res) => {
            dispatch({ type: 'user/changeToken', payload: res.data });
            navigate('/redirect/');
        }).catch((error) => {
            const errorCode = axiosErrorCode<ApiAuthLoginErrorCode>(error);
            setDialogVisable(true);
            resetInput();

            switch(errorCode) {
                case '400':
                case 'BadRequest':
                    setAuthLoginApiMessage('輸入格式錯誤');
                    break;
                case '403':
                case 'NoSuchUser':
                    setAuthLoginApiMessage('無此使用者');
                    break;
                case '410':
                case 'AccountDisabled':
                    setAuthLoginApiMessage('帳號已停用');
                    break;
                default:
                    setAuthLoginApiMessage('伺服器錯誤');
                    break;
            }
        });
    };

    const handleAccount = (e: React.ChangeEvent<HTMLInputElement>): void => {
        setAccount(e.target.value);
        inspactAccount();
    };

    const handlePassword = (e: React.ChangeEvent<HTMLInputElement>): void => {
        setPassword(e.target.value);
        inspactPassword();
    };

    const inspactAccount = (): void => {
        const errors = validate(loginFormRef.current, formValidate);

        if (errors !== undefined) {
            if (errors.account) {
                setAccountMessage(errors.account[0].replace('Account', ''));
                accountRef.current?.classList.add('error');
            }
            else {
                setAccountMessage('');
                accountRef.current?.classList.remove('error');
            }
        }
        else {
            setAccountMessage('');
            accountRef.current?.classList.remove('error');
        }
    };

    const inspactPassword = (): void => {
        const errors = validate(loginFormRef.current, formValidate);
        
        if (errors !== undefined) {
            if (errors.password) {
                setPasswordMessage(errors.password[0].replace('Password', ''));
                passwordRef.current?.classList.add('error');
            }
            else {
                setPasswordMessage('');
                passwordRef.current?.classList.remove('error');
            }
        }
        else {
            setPasswordMessage('');
            passwordRef.current?.classList.remove('error');
        }
    };

    const inspactLoginForm = (): boolean => {
        const errors = validate(loginFormRef.current, formValidate);
        if (errors !== undefined) {
            if (errors.account) {
                setAccountMessage(errors.account[0].replace('Account', ''));
                accountRef.current?.classList.add('error');
            }

            if (errors.password) {
                setPasswordMessage(errors.password[0].replace('Password', ''));
                passwordRef.current?.classList.add('error');
            }

            return true;
        }

        return false;
    };

    const closeLoginDialog = (): void => {
        setDialogVisable(false);
    };

    const resetInput = (): void => {
        if (accountRef.current?.value && passwordRef.current?.value) {
            accountRef.current.value = '';
            passwordRef.current.value = '';
            setAccount('');
            setPassword('');
            setAccountMessage('');
            setPasswordMessage('');
        }
    };

    return (
        <div className='login'>
            <form className='form' onSubmit={handleLogin} ref={loginFormRef}>
                <div className='form_control'>
                    <h1 className='title'>等速肌力訓練後台管理</h1>
                </div>

                <div className='form_control'>
                    <h3 className='subMenu'>銀髮專用智慧型運動器材</h3>
                </div>

                <div className='form_control'>
                    <span>帳號</span>
                    <input className='account' type='text' placeholder='請輸入帳號' name='account' onChange={handleAccount} ref={accountRef} />
                    <p className='accountMessage'>{accountMessage}</p>
                </div>

                <div className='form_control'>
                    <span>密碼</span>
                    <input className='password' type='password' placeholder='請輸入密碼' name='password' onChange={handlePassword} ref={passwordRef} />
                    <p className='passwordMessage'>{passwordMessage}</p>
                </div>

                <div className='form_control'>
                    <button className='submit' type='submit'>登入</button>
                </div>
            </form>

            {
                dialogVisable && (
                    <Dialog className='visibleInfoDialog'>
                        <Dialog.Title title='錯誤'></Dialog.Title>
                        <Dialog.Body>
                            <div className='content'>
                                <p>{authLoginApiMessage}</p>
                            </div>
                        </Dialog.Body>
                        <Dialog.Footer>
                            <button className='btnDialogConfirm' onClick={closeLoginDialog}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }
        </div>
    );
};

export default Login;