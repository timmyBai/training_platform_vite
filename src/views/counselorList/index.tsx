import React, { ReactNode } from 'react';

// css
import './index.sass';

const CounselorList: React.FC = (): ReactNode => {
    return (
        <div className='counselorList'>
            輔導員清單
        </div>
    );
};

export default CounselorList;