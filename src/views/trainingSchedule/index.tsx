import React, { useEffect, useState, useMemo } from 'react';
import { useNavigate } from 'react-router-dom';

// css
import './index.sass';

// api
import {
    trainingScheduleNotify,
    getThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequency,
    getThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequency,
    getThisWeekTrainingScheduleSoftnessRemainingFrequency,
    getThisWeekTrainingScheduleBalanceRemainingFrequency,
    addTrainingScheduleWeek
} from '@/api/trainingSchedule';

// hook
import { useAppDispatch, useAppSelector } from '@/hook/store';

// utils
import { axiosErrorCode } from '@/utils/request';

// images
import arrowLeftAndRight from '@/assets/images/arrowLeftAndRight.svg';
import cardiorespiratoryFitnessIcon from '@/assets/cardiorespiratory_fitness.svg';
import muscleStrengthAndMuscleEnduranceIcon from '@/assets/muscle_strength_and_muscle_endurance.svg';
import softnessIcon from '@/assets/softness.svg';
import balanceIcon from '@/assets/balance.svg';

// components
import StudentInfoBar from '@/components/StudentInfoBar';
import PageTitleBar from '@/components/PageTitleBar';
import Dialog from '@/components/Dialog';

const TrainingSchedule: React.FC = () => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const storeGetters = useAppSelector((state) => {
        return {
            studentId: state.studentInfo.studentId
        };
    });

    const trainingTypeTextArray = [
        { text: '心肺適能', icon: cardiorespiratoryFitnessIcon },
        { text: '肌力與肌耐力', icon: muscleStrengthAndMuscleEnduranceIcon },
        { text: '柔軟度', icon: softnessIcon },
        { text: '平衡', icon: balanceIcon }
    ];
    const minYear = 1911;
    const maxYear = 9999;
    const week = ['日', '一', '二', '三', '四', '五', '六'];

    const [today, setToday] = useState<TodayType>({
        year: 0,
        month: 0,
        date: 0,
        day: 0
    });

    const [calendar, setCalendar] = useState<CalendarType>({
        year: 0,
        month: 0,
        date: 0,
        day: 0
    });

    // 儲存觀看模式或排程模式
    const [mode, setMode] = useState<CalendarMode>('watch');

    // 訓練類型
    const [trainingType, setTrainingType] = useState<number>(0);

    const [responseThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequency, setResponseThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequency] = useState<GetThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequencyDataType>();
    const [responseThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequency, setResponseThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequency] = useState<GetThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequencyDataType>();
    const [responseThisWeekTrainingScheduleSoftnessFrequency, setResponseThisWeekTrainingScheduleSoftnessFrequency] = useState<GetThisWeekTrainingScheduleSoftnessRemainingFrequencyDataType>();
    const [responseThisWeekTrainingScheduleBalanceFrequency, setResponseThisWeekTrainingScheduleBalanceFrequency] = useState<GetThisWeekTrainingScheduleBalanceRemainingFrequencyDataType>();

    const [exerciseFrequency, setExerciseFrequency] = useState<number>(0);
    const [remainingFrequency, setRemainingFrequency] = useState<number>(0);
    const [scheduleDateList, setScheduleDateList] = useState<string[]>([]);

    const [visibleTrainingScheduleDialog, setVisibleTrainingScheduleDialog] = useState<boolean>(false);
    const [visibleTrainingScheduleMessage, setVisibleTrainingScheduleMessage] = useState<string>('');

    const [visibleInfoDialog, setVisibleInfoDialog] = useState<boolean>(false);
    const [visibleInfoDialogTitle, setVisibleInfoDialogTitle] = useState<string>('');
    const [visibleInfoDialogMessage, setVisibleInfoDialogMessage] = useState<string>('');

    // 初始化今天日期
    const initToday = () => {
        const date = new Date();
        today.year = calendar.year = date.getFullYear();
        today.month = calendar.month = date.getMonth();
        today.date = calendar.date = date.getDate();
        today.day = calendar.day = date.getDay();

        setToday(state => {
            return { ...state, ...today };
        });

        setCalendar(state => {
            return { ...state, ...calendar };
        });
    };

    // 加或減西元年
    const adjustYear = (fix: number) => {
        calendar.year += fix;

        if (calendar.year <= minYear) {
            calendar.year = minYear;
        }
        else if (calendar.year >= maxYear) {
            calendar.year = maxYear;
        }

        setCalendar(state => {
            return { ...state, ...calendar };
        });
    };

    // 加或減月年
    const adjustMonth = (fix: number) => {
        const month = calendar.month + fix;

        if (month > 11) {
            adjustYear(1);
            calendar.month = 0;
        }
        else if (month < 0) {
            adjustYear(-1);
            calendar.month = 11;
        }
        else {
            calendar.month += fix;
        }

        setCalendar(state => {
            return { ...state, ...calendar };
        });
    };

    // 計算上個月
    const calendarFirstDay = useMemo(() => {
        const mDate = new Date(calendar.year, calendar.month, 1);
        const date = new Date(calendar.year, calendar.month, 1 - mDate.getDay());

        return {
            year: date.getFullYear(),
            month: date.getMonth(),
            date: date.getDate(),
            day: date.getDay()
        };
    }, [calendar, initToday]);

    // 計算上下個月 42 天
    const calendarMonth = useMemo(() => {
        const data = [];
        let date;

        for (let i = 0; i < 42; i++) {
            date = new Date(calendarFirstDay.year, calendarFirstDay.month, calendarFirstDay.date + i);
            data.push({
                year: date.getFullYear(),
                month: date.getMonth(),
                date: date.getDate(),
                day: date.getDay()
            });
        }

        return data;
    }, [calendarFirstDay]);

    const handleCalendarStyle = (i: number, j: number) => {
        const calendarMonthYear = calendarMonth[(i - 1) * 7 + j - 1].year;
        const calendarMonthMonth = calendarMonth[(i - 1) * 7 + j - 1].month;
        const calendarMonthDate = calendarMonth[(i - 1) * 7 + j - 1].date;

        if (mode === 'watch') {
            if (calendarMonthYear === today.year && calendarMonthMonth === today.month && calendarMonthDate === today.date && calendarMonthMonth === calendar.month) {
                return 'today';
            }
            else if (calendarMonthYear === today.year && calendarMonthMonth === today.month && calendarMonthDate === today.date && calendarMonthMonth !== calendar.month) {
                return 'today';
            }
            else if (calendarMonthMonth !== calendar.month) {
                return 'other';
            }
            else {
                return '';
            }
        }
        else {
            if (calendarMonthMonth !== calendar.month) {
                return 'other';
            }

            return '';
        }
    };

    /**
     * 關閉資訊彈窗
     */
    const closeVisibleDialog = () => {
        setVisibleInfoDialog(false);
        setVisibleInfoDialogTitle('');
        setVisibleInfoDialogMessage('');
    };

    /**
     * 處理訓練排程排程天數是否有誤差發放通知
     */
    const handleTrainingScheduleNotify = () => {
        trainingScheduleNotify(storeGetters.studentId).then((res) => {
            const { data } = res.data;

            if (data.isNotify) {
                setVisibleTrainingScheduleDialog(true);
                setVisibleTrainingScheduleMessage(res.data.message);
            }
        }).catch((error) => {
            const errorCode = axiosErrorCode<TrainingScheduleNotifyErrorCodeType>(error);

            switch (errorCode) {
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                default:
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    // 重新導向運動處方路由
    const redirectExercisePrescription = () => {
        navigate('/exercisePrescription/');
    };

    const changeTrainingType = (type: number) => {
        setMode('choose');

        switch (type) {
            case 1:
                handleGetThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequency();
                setTrainingType(type);
                break;
            case 2:
                handleGetThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequency();
                setTrainingType(type);
                break;
            case 3:
                handleGetThisWeekTrainingScheduleSoftnessRemainingFrequency();
                setTrainingType(type);
                break;
            case 4:
                handleGetThisWeekTrainingScheduleBalanceRemainingFrequency();
                setTrainingType(type);
                break;
            default:
                break;
        }
    };

    /**
     * 清除訓練類型預設
     */
    const closeTrainingTypeActive = () => {
        setTrainingType(0);
        setMode('watch');
        setResponseThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequency(undefined);
        setResponseThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequency(undefined);
        setResponseThisWeekTrainingScheduleSoftnessFrequency(undefined);
        setResponseThisWeekTrainingScheduleBalanceFrequency(undefined);
        setScheduleDateList([]);
    };

    /**
     * 取得訓練排程-心肺適能這週剩餘排程天數
     */
    const handleGetThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequency = () => {
        getThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequency(storeGetters.studentId).then((res) => {
            const { data } = res.data;

            setResponseThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequency(data);
            setExerciseFrequency(data.exerciseFrequency);
            setRemainingFrequency(data.remainingFrequency);

            // 如果這週已有安排訓練排程
            if (data.scheduleDate.length > 0) {
                setScheduleDateList(data.scheduleDate);
            }
            else {
                // 新增預設訓練排程
                setScheduleDateList(data.defaultTrainingScheduleDate);
            }
        }).catch((error) => {
            const errorCode = axiosErrorCode<GetThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequencyErrorCodeType>(error);

            switch (errorCode) {
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                case 'StudentDoesNotExist':
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 'NotSetUpAnyExercisePrescriptionCardiorespiratoryFitness':
                    setVisibleTrainingScheduleDialog(true);
                    setVisibleTrainingScheduleMessage('尚未安排任何運動處方-心肺適能');
                    break;
                default:
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    /**
     * 取得訓練排程-肌力與肌耐力這週剩餘排程天數
     */
    const handleGetThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequency = () => {
        getThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequency(storeGetters.studentId).then((res) => {
            const { data } = res.data;

            setResponseThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequency(data);
            setExerciseFrequency(data.exerciseFrequency);
            setRemainingFrequency(data.remainingFrequency);

            if (data.scheduleDate.length > 0) {
                setScheduleDateList(data.scheduleDate);
            }
            else {
                // 新增預設訓練排程
                setScheduleDateList(data.defaultTrainingScheduleDate);
            }
        }).catch((error) => {
            const errorCode = axiosErrorCode<GetThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequencyErrorCodeType>(error);

            switch (errorCode) {
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                case 'StudentDoesNotExist':
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 'NotSetUpAnyExercisePrescriptionMuscleStrengthAndMuscleEndurance':
                    setVisibleTrainingScheduleDialog(true);
                    setVisibleTrainingScheduleMessage('尚未安排任何運動處方-肌力與肌耐力');
                    break;
                default:
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    /**
     * 取得訓練排程-柔軟度這週剩餘排程天數
     */
    const handleGetThisWeekTrainingScheduleSoftnessRemainingFrequency = () => {
        getThisWeekTrainingScheduleSoftnessRemainingFrequency(storeGetters.studentId).then((res) => {
            const { data } = res.data;

            setResponseThisWeekTrainingScheduleSoftnessFrequency(data);
            setExerciseFrequency(data.exerciseFrequency);
            setRemainingFrequency(data.remainingFrequency);

            if (data.scheduleDate.length > 0) {
                setScheduleDateList(data.scheduleDate);
            }
            else {
                setScheduleDateList(data.defaultTrainingScheduleDate);
            }
        }).catch((error) => {
            const errorCode = axiosErrorCode<GetThisWeekTrainingScheduleSoftnessRemainingFrequencyErrorCodeType>(error);

            switch (errorCode) {
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                case 'StudentDoesNotExist':
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 'NotSetUpAnyExercisePrescriptionSoftness':
                    setVisibleTrainingScheduleDialog(true);
                    setVisibleTrainingScheduleMessage('尚未安排任何運動處方-柔軟度');
                    break;
                default:
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    /**
     * 取得訓練排程-平衡這週剩餘排程天數
     */
    const handleGetThisWeekTrainingScheduleBalanceRemainingFrequency = () => {
        getThisWeekTrainingScheduleBalanceRemainingFrequency(storeGetters.studentId).then((res) => {
            const { data } = res.data;

            setResponseThisWeekTrainingScheduleBalanceFrequency(data);
            setExerciseFrequency(data.exerciseFrequency);
            setRemainingFrequency(data.remainingFrequency);

            if (data.scheduleDate.length > 0) {
                setScheduleDateList(data.scheduleDate);
            }
            else {
                setScheduleDateList(data.defaultTrainingScheduleDate);
            }
        }).catch((error) => {
            const errorCode = axiosErrorCode<GetThisWeekTrainingScheduleBalanceRemainingFrequencyErrorCodeType>(error);

            switch (errorCode) {
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                case 'StudentDoesNotExist':
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 'NotSetUpAnyExercisePrescriptionBalance':
                    setVisibleTrainingScheduleDialog(true);
                    setVisibleTrainingScheduleMessage('尚未安排任何運動處方-平衡');
                    break;
                default:
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    /**
     * 新增排程清單
     */
    const addTrainingScheduleList = (i: number, j: number) => {
        const calendarMonthYear = calendarMonth[(i - 1) * 7 + j - 1].year;
        const calendarMonthMonth = (calendarMonth[(i - 1) * 7 + j - 1].month + 1).toString().padStart(2, '0');
        const calendarMonthDate = calendarMonth[(i - 1) * 7 + j - 1].date.toString().padStart(2, '0');
        const calendarMonthFullDate = `${calendarMonthYear}-${calendarMonthMonth}-${calendarMonthDate}`;

        // 檢查選擇日期是否在這週日期範圍裡面(心肺適能)
        const cardiorespiratoryFitnessResult = responseThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequency?.thisWeekDateRemainingDayRange.includes(calendarMonthFullDate);

        // 檢查選擇日期是否在這週日期範圍裡面(肌力與肌耐力)
        const muscleStrengthAndMuscleEnduranceResult = responseThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequency?.thisWeekDateRemainingDayRange.includes(calendarMonthFullDate);

        // 檢查選擇日期是否在這週日期範圍裡面(柔軟度)
        const softnessResult = responseThisWeekTrainingScheduleSoftnessFrequency?.thisWeekDateRemainingDayRange.includes(calendarMonthFullDate);

        // 檢查選擇日期是否在這週日期範圍裡面(平衡)
        const balanceResult = responseThisWeekTrainingScheduleBalanceFrequency?.thisWeekDateRemainingDayRange.includes(calendarMonthFullDate);

        if (cardiorespiratoryFitnessResult || muscleStrengthAndMuscleEnduranceResult || softnessResult || balanceResult) {
            const index = scheduleDateList.indexOf(calendarMonthFullDate);

            if (index === -1) {
                if (responseThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequency?.exerciseFrequency && remainingFrequency !== 0) {
                    scheduleDateList.push(`${calendarMonthYear}-${calendarMonthMonth}-${calendarMonthDate}`);
                    setScheduleDateList(scheduleDateList);
                    setRemainingFrequency(remainingFrequency - 1);
                }
                else if (responseThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequency?.exerciseFrequency && remainingFrequency !== 0) {
                    scheduleDateList.push(`${calendarMonthYear}-${calendarMonthMonth}-${calendarMonthDate}`);
                    setScheduleDateList(scheduleDateList);
                    setRemainingFrequency(remainingFrequency - 1);
                }
                else if (responseThisWeekTrainingScheduleSoftnessFrequency?.exerciseFrequency && remainingFrequency !== 0) {
                    scheduleDateList.push(`${calendarMonthYear}-${calendarMonthMonth}-${calendarMonthDate}`);
                    setScheduleDateList(scheduleDateList);
                    setRemainingFrequency(remainingFrequency - 1);
                }
                else if (responseThisWeekTrainingScheduleBalanceFrequency?.exerciseFrequency && remainingFrequency !== 0) {
                    scheduleDateList.push(`${calendarMonthYear}-${calendarMonthMonth}-${calendarMonthDate}`);
                    setScheduleDateList(scheduleDateList);
                    setRemainingFrequency(remainingFrequency - 1);
                }
            }
            else {
                scheduleDateList.splice(index, 1);
                setScheduleDateList([...scheduleDateList]);
                setRemainingFrequency(remainingFrequency + 1);
            }
        }
    };

    /**
     * 處理呼叫 api 新增訓練排程
     */
    const handleAddTrainingSchedule = () => {
        const data: AddTrainingScheduleWeekRequest = {
            trainingType: trainingType,
            scheduleDate: scheduleDateList
        };

        addTrainingScheduleWeek(storeGetters.studentId, data).then(() => {
            setVisibleInfoDialog(true);
            setVisibleInfoDialogTitle('資訊');
            setVisibleInfoDialogMessage('新增訓練排程成功');

            closeTrainingTypeActive();
        }).catch((error) => {
            const errorCode = axiosErrorCode<AddTrainingScheduleWeekErrorCodeType>(error);

            switch (errorCode) {
                case 'BadRequest':
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('格式錯誤');
                    break;
                case 'SelectDateNotThisWeekRange':
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('選擇日期不存在這週範圍');
                    break;
                case 401:
                    dispatch({ type: 'user/resetToken' });
                    navigate('/home');
                    break;
                case 'StudentDoesNotExist':
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('查無學員');
                    break;
                case 'TrainingScheduleDayCountLargerThanExercisePrescriptionDayCount':
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('訓練排程天數超出運動處方天數');
                    break;
                case 'InsertTrainingScheduleWeekFail':
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('新增訓練排程失敗');
                    break;
                default:
                    setVisibleInfoDialog(true);
                    setVisibleInfoDialogTitle('錯誤');
                    setVisibleInfoDialogMessage('伺服器錯誤');
                    break;
            }
        });
    };

    useEffect(() => {
        initToday();
        handleTrainingScheduleNotify();
    }, []);

    return (
        <div className='trainingSchedule'>
            <StudentInfoBar></StudentInfoBar>

            <PageTitleBar title='訓練排程'></PageTitleBar>

            <div className='training_schedule_info'>
                <div className='flex flex-wrap'>
                    <div className='w-full sm:w-4/12 md:w-4/12 lg:w-4/12 xl:w-4/12 xxl:w-4/12'>
                        <div className='training_schedule_type'>
                            {
                                trainingTypeTextArray.map((item, index: number) => {
                                    return (
                                        <div className={`trainingType ${trainingType === index + 1 ? 'orange' : ''}`} key={item.text} onClick={() => changeTrainingType(index + 1)}>
                                            <img className='trainingTypeIcon' src={item.icon} />
                                            <span className='trainingTypeText'>{item.text}</span>
                                        </div>
                                    );
                                })
                            }

                            {
                                trainingType !== 0 && (
                                    <h2 className='remainingFrequency'>{remainingFrequency}</h2>
                                )
                            }

                            {
                                trainingType !== 0 && (remainingFrequency === 0 || scheduleDateList.length > 0 || scheduleDateList.length <= exerciseFrequency) && (
                                    <button className='btnConfirm' onClick={handleAddTrainingSchedule}>確定</button>
                                )
                            }

                            {
                                trainingType !== 0 && (
                                    <button className='btnClose' onClick={closeTrainingTypeActive}>取消</button>
                                )
                            }

                            {
                                trainingType === 0 && (
                                    <h3 className='description'>請選擇編輯項目</h3>
                                )
                            }
                        </div>
                    </div>

                    <div className='w-full sm:w-8/12 md:w-8/12 lg:w-8/12 xl:w-8/12 xxl:w-8/12'>
                        <div className='card'>
                            <div className={`calendar ${mode === 'watch' ? 'watch' : 'choose'}`}>
                                <div className='dayInfo'>
                                    <div className='info'>
                                        <img className='arrowLeft' src={arrowLeftAndRight} onClick={() => adjustMonth(-1)} />
                                    </div>

                                    <div className='info'>
                                        <h3>{`${calendar.year} 年 ${calendar.month + 1} 月`}</h3>
                                    </div>

                                    <div className='info'>
                                        <img className='arrowRight' src={arrowLeftAndRight} onClick={() => adjustMonth(1)} />
                                    </div>
                                </div>

                                <div className='weekDayRow'>
                                    {
                                        week.map((item, index) => {
                                            return (
                                                <div className='weekDay' key={index}>{item}</div>
                                            );
                                        })
                                    }
                                </div>

                                <div className='dayRow'>
                                    {
                                        [1, 2, 3, 4, 5, 6].map((i) => {
                                            return [1, 2, 3, 4, 5, 6, 7].map((j) => {
                                                return (
                                                    <div
                                                        className={`
                                                            day
                                                            ${handleCalendarStyle(i, j)}
                                                            ${responseThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequency?.thisWeekDateRemainingDayRange.includes(`${calendarMonth[(i - 1) * 7 + j - 1].year}-${(calendarMonth[(i - 1) * 7 + j - 1].month + 1).toString().padStart(2, '0')}-${calendarMonth[(i - 1) * 7 + j - 1].date.toString().padStart(2, '0')}`) ? 'box' : ''}
                                                            ${responseThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequency?.thisWeekDateRemainingDayRange.includes(`${calendarMonth[(i - 1) * 7 + j - 1].year}-${(calendarMonth[(i - 1) * 7 + j - 1].month + 1).toString().padStart(2, '0')}-${calendarMonth[(i - 1) * 7 + j - 1].date.toString().padStart(2, '0')}`) ? 'box' : ''}
                                                            ${responseThisWeekTrainingScheduleSoftnessFrequency?.thisWeekDateRemainingDayRange.includes(`${calendarMonth[(i - 1) * 7 + j - 1].year}-${(calendarMonth[(i - 1) * 7 + j - 1].month + 1).toString().padStart(2, '0')}-${calendarMonth[(i - 1) * 7 + j - 1].date.toString().padStart(2, '0')}`) ? 'box' : ''}
                                                            ${responseThisWeekTrainingScheduleBalanceFrequency?.thisWeekDateRemainingDayRange.includes(`${calendarMonth[(i - 1) * 7 + j - 1].year}-${(calendarMonth[(i - 1) * 7 + j - 1].month + 1).toString().padStart(2, '0')}-${calendarMonth[(i - 1) * 7 + j - 1].date.toString().padStart(2, '0')}`) ? 'box' : ''}
                                                            ${scheduleDateList.includes(`${calendarMonth[(i - 1) * 7 + j - 1].year}-${(calendarMonth[(i - 1) * 7 + j - 1].month + 1).toString().padStart(2, '0')}-${calendarMonth[(i - 1) * 7 + j - 1].date.toString().padStart(2, '0')}`) ? 'default' : ''}
                                                        `
                                                        }
                                                        key={j}
                                                        onClick={() => addTrainingScheduleList(i, j)}
                                                    >
                                                        <span className={`
                                                            
                                                        `}>
                                                            {calendarMonth[(i - 1) * 7 + j - 1].date}
                                                        </span>
                                                    </div>
                                                );
                                            });
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {
                visibleTrainingScheduleDialog && (
                    <Dialog className='visibleTrainingSchedule'>
                        <Dialog.Title title='錯誤'></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <span>{visibleTrainingScheduleMessage}</span>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnConfirm' onClick={redirectExercisePrescription}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }

            {
                visibleInfoDialog && (
                    <Dialog className='visibleInfoDialog'>
                        <Dialog.Title title={visibleInfoDialogTitle}></Dialog.Title>

                        <Dialog.Body>
                            <div className='content'>
                                <span>{visibleInfoDialogMessage}</span>
                            </div>
                        </Dialog.Body>

                        <Dialog.Footer>
                            <button className='dialogBtnConfirm' onClick={closeVisibleDialog}>確定</button>
                        </Dialog.Footer>
                    </Dialog>
                )
            }
        </div>
    );
};

export default TrainingSchedule;