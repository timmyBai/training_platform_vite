import React, { ReactNode } from 'react';
import { useNavigate } from 'react-router-dom';

// css
import './index.sass';

// icon
import homeArrowIcon from '@/assets/icon/home_arrow.svg';
import emg_logo from '@/assets/emg_logo.svg';

const Home: React.FC = (): ReactNode => {
    const navigate = useNavigate();

    const redirectLogin = (): void => {
        navigate('/login/');
    };

    return (
        <div className='home'>
            <div className='home_container'>
                <div className='container mx-auto'>
                    <div className='w-full sm:w-12/12 md:w-12/12 lg:w-12/12 xl:w-12/12 xxl:w-12/12'>
                        <div className='card'>
                            <h1 className='title'>等速肌力訓練後台管理</h1>
                            <h3 className='sub_title'>銀髮族專用智慧型運動器材</h3>
                            <img className='homeArrow' src={homeArrowIcon} onClick={redirectLogin} />
                        </div>
                    </div>
                </div>
            </div>

            <div className='footer'>
                <img className='emg_logo' src={emg_logo} />
                <span className='company_name'>上岳科技股份有限公司</span>
            </div>
        </div>
    );
};

export default Home;