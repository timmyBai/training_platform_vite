import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

// api
import { authLogin, memberInfo } from '@/api/auth';

// utils
import { getToken, setToken, removeToken } from '@/utils/auth';

const initialState: UserStateType = {
    token: getToken(),
    id: '',
    account: '',
    name: '',
    imageUrl: '',
    gender: '',
    birthday: '',
    age: '',
    nbf: '',
    exp: '',
    iss: '',
    aud: '',
    role: '',
    identity: ''
};

const reducers = {
    changeToken: (state = initialState, action: PayloadAction<any>) => {
        state.token = action.payload;
        setToken(action.payload?.accessToken, action.payload?.expires_in);
    },
    changeUserInfo: (state = initialState, action: PayloadAction<ApiUserInfoResponseDateType>) => {
        state.id = action.payload.id;
        state.account = action.payload.account;
        state.name = action.payload.name;
        state.imageUrl = action.payload.imageUrl;
        state.gender = action.payload._gender;
        state.birthday = action.payload.birthday;
        state.age = action.payload.age;
        state.nbf = action.payload.nbf;
        state.exp = action.payload.exp;
        state.iss = action.payload.iss;
        state.aud = action.payload.aud;
        state.role = action.payload._role;
        state.identity = action.payload.identity;
    },
    resetToken: (state = initialState) => {
        removeToken();
        state.token = '';
        state.id = '';
        state.account = '';
        state.name = '';
        state.imageUrl = '';
        state.gender = '';
        state.birthday = '';
        state.age = '';
        state.nbf = '';
        state.exp = '';
        state.iss = '';
        state.aud = '';
        state.role = '';
        state.identity = '';
    }
};

const userReducer = createSlice({
    name: 'user',
    initialState,
    reducers
});

const user = userReducer.reducer;

export const login = createAsyncThunk('user/login', async(data: ApiAuthLoginRequest) => {
    const response = await authLogin(data);

    return response.data;
});

export const getUserInfo = createAsyncThunk('user/introduction', async() => {
    const response = await memberInfo();

    return response.data;
});

export default {
    user
};
