import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState: StudentInfoStateType = {
    student_logo: '',
    studentId: 0,
    name: '',
    gender: '',
    age: 0,
    posture: '',
    medicalHistory: '',
    dangerGrading: '',
    email: '',
    phone: '',
    address: '',
    mark: ''
};

const reducers = {
    changeStudentInfo: (state = initialState, action: PayloadAction<GetStudentListDateType | GetOnlyStudentBaseInfoDateType>) => {
        state.studentId = action.payload.userAuthorizedId;
        state.student_logo = action.payload.imageUrl;
        state.name = action.payload.name;
        state.gender = action.payload.gender;
        state.age = action.payload.age;
        state.posture = action.payload.posture;
        state.medicalHistory = action.payload.medicalHistory;
        state.dangerGrading = action.payload.dangerGrading;
        state.phone = action.payload.phone;
        state.email = action.payload.email;
        state.address = action.payload.address;
        state.mark = action.payload.mark;
    },
    clearStudentInfo: (state = initialState) => {
        state.studentId = 0;
        state.student_logo = '';
        state.name = '';
        state.gender = '';
        state.age = 0;
        state.posture = '';
        state.medicalHistory = '';
        state.dangerGrading = '';
        state.phone = '';
        state.email = '';
        state.address = '';
        state.mark = '';
    }
};

const studentInfoReducer = createSlice({
    name: 'studentInfo',
    initialState,
    reducers
});

const studentInfo = studentInfoReducer.reducer;

export default {
    studentInfo
};
