import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const initialState: InspectionReportStateType = {
    totalPowerListPage: true,
    summaryPage: false,
    summaryListPage: false,
    singleDayPage: false,
    showBtnSummary: false,
    showBtnSingleDay: false,
    btnSummaryText: '清單',
    btnSingleDayText: '單日報表',
    showBtnDeleteSummarySingle: false,
    visibleDeleteMarkDialog: false,
    machinaryCode: 0,
    machinaryName: ''
};

const reducers = {
    visibleTotalPowerListPage: (state = initialState, action: PayloadAction<boolean>) => {
        state.totalPowerListPage = action.payload;
    },
    visibleSummaryPage: (state = initialState, action: PayloadAction<boolean>) => {
        state.summaryPage = action.payload;
    },
    visibleSummaryListPage: (state = initialState, action: PayloadAction<boolean>) => {
        state.summaryListPage = action.payload;
    },
    visibleSingleDayPage: (state = initialState, action: PayloadAction<boolean>) => {
        state.singleDayPage = action.payload;
    },
    changeMachinaryCode: (state = initialState, action: PayloadAction<number>) => {
        state.machinaryCode = action.payload;

        switch(action.payload) {
            case 1:
                state.machinaryName = '屈腿伸腿訓練機';
                break;
            case 2:
                state.machinaryName = '推胸拉背訓練機 ';
                break;
            case 3:
                state.machinaryName = '髖關節外展內收訓練機';
                break;
            case 4:
                state.machinaryName = '蹬腿訓練機';
                break;
            case 5:
                state.machinaryName = '上斜推拉訓練機';
                break;
            case 6:
                state.machinaryName = '蝴蝶訓練機';
                break;
            case 7:
                state.machinaryName = '坐式上臂訓練機';
                break;
            case 8:
                state.machinaryName = '坐式轉體訓練機';
                break;
            case 9:
                state.machinaryName = '深蹲訓練機';
                break;
            case 10:
                state.machinaryName = '坐式腹背訓練機';
                break;
            default:
                break;
        }
    },
    // 顯示或隱藏總表按鈕
    visibleBtnSummary: (state = initialState, action: PayloadAction<boolean>) => {
        state.showBtnSummary = action.payload;
    },
    // 顯示或隱藏單日報表按鈕
    visibleBtnSingleDay: (state = initialState, action: PayloadAction<boolean>) => {
        state.showBtnSingleDay = action.payload;
    },
    // 顯示或隱藏總表刪除按鈕
    visibleBtnDeleteSummarySingle: (state = initialState, action: PayloadAction<boolean>) => {
        state.showBtnDeleteSummarySingle = action.payload;
    },
    // 顯示或隱藏刪除備註彈窗
    visibleDeleteMarkDialog: (state = initialState, action: PayloadAction<boolean>) => {
        state.visibleDeleteMarkDialog = action.payload;
    },
    resetPage: (state = initialState) => {
        state.totalPowerListPage = false;
        state.summaryPage = false;
        state.summaryListPage = false;
        state.singleDayPage = false;
        state.showBtnDeleteSummarySingle = false;
    },
    toggleBtnSummaryText: (state = initialState) => {
        if (state.btnSummaryText === '清單') {
            state.btnSummaryText = '總表';
        }
        else {
            state.btnSummaryText = '清單';
        }
    },
    resetSummaryText: (state = initialState) => {
        state.btnSummaryText = '清單';
    }
};

const inspectionReportReducer = createSlice({
    name: 'inspectionReport',
    initialState,
    reducers
});

const inspectionReport = inspectionReportReducer.reducer;

export default {
    inspectionReport
};
