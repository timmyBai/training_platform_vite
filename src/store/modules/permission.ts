import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// router
import { constantRoutes, asyncRoutes, RoutesTypeNew } from '@/router/index';

const hasPermission = (roles: string, route: RoutesTypeNew): boolean | undefined => {
    if (route.meta && route.meta.roles) {
        const result = route.meta.roles.filter((d: string) => d.indexOf(roles) !== -1);
        
        if (result.length > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return true;
    }
};

const filterAsyncRoutes = (routes: RoutesTypeNew[], roles: string) => {
    const res: any = [];

    routes.forEach(route => {
        const tmp = { ...route };
        
        if (hasPermission(roles, route)) {
            if (tmp.children) {
                tmp.children = filterAsyncRoutes(tmp.children, roles);
            }
            
            res.push(tmp);
        }
    });

    return res;
};

const hasRouteVisable = (route: RoutesTypeNew) => {
    if (route.meta && route.meta.visableRoutes) {
        if (route.meta.visableRoutes) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return true;
    }
};

/**
 * 檢查現在 routes 是否有 visableRoutes key，如果 visableRoutes 為 true 改回 false
 */
const visableRoutes = (routes: RoutesTypeNew[], visable: boolean) => {
    const res: any = [];

    routes.forEach(route => {
        const tmp = { ...route };
        
        if (hasRouteVisable(route)) {
            if (tmp.children) {
                tmp.children = visableRoutes(tmp.children, visable);
            }
            
            res.push(tmp);
        }

        if (tmp.meta && tmp.meta.visableRoutes !== undefined) {
            tmp.meta.visableRoutes = visable;
        }
    });

    return res;
};

const initialState: PermissionStateType = {
    routes: constantRoutes,
    addRoutes: [],
    tempRoutes: constantRoutes
};

const reducers = {
    changeVisableRoutes: (state = initialState, action: PayloadAction<boolean>) => {
        let accessRoutes: any = [];
        
        accessRoutes = visableRoutes(state.routes, action.payload);
        state.routes = accessRoutes;
    },
    generateRoutes: (state = initialState, action: PayloadAction<string>) => {
        let accessRoutes: any = [];

        if (action.payload.includes('業主') || action.payload.includes('物理治療師') || action.payload.includes('復健師') || action.payload.includes('體適能指導員')) {
            accessRoutes = filterAsyncRoutes(asyncRoutes, action.payload);
            
            // react 版 ts bug 最後修復
            const result = accessRoutes.filter((item: any) => {
                return item.children.length !== 0;
            });

            state.addRoutes = result;
            state.routes = [];
            state.routes = state.tempRoutes.concat(state.addRoutes);
        }

        
        action.payload = state.addRoutes;
    }
};

const permissionReducer = createSlice({
    name: 'permission',
    initialState,
    reducers
});

const permission = permissionReducer.reducer;

export default {
    permission
};
