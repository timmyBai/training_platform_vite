type PrintTrainingReportRequestType = {
    isPrintBodyCompositionAndPhysicalFitness: boolean,
    isPrintExercisePrescription: boolean,
    isPrintInstrumentSetting: boolean,
    printInspectionReportCardiorespiratoryFitness: string,
    printInspectionReportMachinaryOne: string,
    printInspectionReportMachinaryTwo: string,
    printInspectionReportMachinaryThree: string,
    printInspectionReportMachinaryFour: string,
    printInspectionReportMachinaryFive: string,
    printInspectionReportMachinarySix: string,
    printInspectionReportMachinarySeven: string,
    printInspectionReportMachinaryEight: string,
    printInspectionReportMachinaryNine: string,
    printInspectionReportMachinaryTen: string,
    printInspectionReportSoftness: string,
    printInspectionReportBalance: string
};

type PrintTrainingReportResponseType = {
    data: Blob
}

type PrintTrainingReportDataType = {
    size: number;
    type: string;
}

type PrintTrainingReportErrorCodeType =
    | 'StudentDoesNotExist'
    | 401;