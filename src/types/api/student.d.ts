type GetStudentListResponseType = {
    isSuccess: boolean;
    message: string;
    data: GetStudentListDateType[]
}

type GetStudentListDateType = {
    userAuthorizedId: number;
    name: string;
    gender: string;
    genderMequence: number;
    posture: string;
    age: number;
    email: string;
    phone: string;
    address: string;
    dangerGrading: string;
    dangerGradingMequence: number;
    medicalHistory: string;
    medicalHistoryMequence: number;
    imageUrl: string;
    updateDate: string;
    updateTime: string;
    mark: string;
}

type DisabledStudentResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type ApiDisabledStudentErrorCode =
    | 'NoSuchStudentFound'
    | 'DeleteStudentFail';

type StudentBaseInfoRequestType = {
    name: string;
    posture: string;
    phone: string;
    address: string
}

type StudentBaseInfoResponseType = {
    isSuccess: boolean;
    message: string;
    data: object
}

type StudentBaseInfoErrorCodeType =
    | 400
    | 'BadRequest'
    | 401
    | 403
    | 'StudentDoesNotExist'
    | 'UserPhoneAlreadyRegistered'

type GetOnlyStudentBaseInfoResponseType = {
    isSuccess: boolean;
    message: string;
    data: GetOnlyStudentBaseInfoDateType
}

type GetOnlyStudentBaseInfoDateType = {
    userAuthorizedId: number;
    phone: string;
    name: string;
    gender: string;
    age: number;
    email: string;
    address: string;
    imageUrl: string;
    posture: string
    dangerGrading: string
    medicalHistory: string,
    mark: string
};

type GetOnlyStudentBaseInfoErrorCodeType =
    | 400
    | 'BadRequest'
    | 403
    | 'StudentDoesNotExist'
    | 401