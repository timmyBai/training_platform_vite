type GetInspectionReportPhysicalFitnessResponseType = {
    isSuccess: boolean;
    message: string;
    data: GetInspectionReportPhysicalFitnessDataType[];
};

type GetInspectionReportPhysicalFitnessDataType = {
    userAuthorizedId: number;
    scheduleDate: string;
    trainingUnitMinute: number;
    actualCompletion: number;
}

type GetInspectionReportPhysicalFitnessErrorCodeType =
    | 401
    | 'StudentDoesNotExist';

type GetStudentEachMachinaryTotalPowerListResponseType = {
    isSuccess: boolean;
    message: string;
    data: GetStudentEachMachinaryTotalPowerListDataType[];
};

type GetStudentEachMachinaryTotalPowerListDataType = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryName: string;
    power: number;
};

type GetStudentEachMachinaryTotalPowerListErrorCodeType =
    | 'StudentDoesNotExist'
    | 401;

type GetStudentMachinaryTotalPowerResponseType = {
    isSuccess: boolean;
    message: string;
    data: GetStudentMachinaryTotalPowerDataType;
};

type GetStudentMachinaryTotalPowerDataType = {
    totalPower: number;
}

type GetStudentFormalTrainingStatusReportResponseType = {
    isSuccess: boolean;
    message: string;
    data: GetStudentFormalTrainingStatusReportDataType[];
};

type GetStudentFormalTrainingStatusReportDataType = {
    userAuthorizedId: number;
    level: number;
    finsh: number;
    mistake: number;
    trainingDate: string;
    trainingDateText: string;
};

type GetStudentTrainingPowerReportResponseType = {
    isSuccess: boolean;
    message: string;
    data: GetStudentTrainingPowerReportDataType[];
};

type GetStudentTrainingPowerReportDataType = {
    userAuthorizedId: number;
    dayTotalPower: number;
    trainingDate: string;
    trainingDateText: string;
};

type GetStudentFormalTrainingSummaryErrorCodeType =
    | 'StudentDoesNotExist'
    | 401;

type GetStudentSingleDayReportRequestType = {
    trainingDate: string;
}

type GetStudentSingleDayReportResponseType = {
    isSuccess: boolean;
    message: string;
    data: GetStudentSingleDayReportDataType;
};

type GetStudentSingleDayReportDataType = {
    singleDayReport: SingleDayReportType[];
    prevDay: string;
    nextDay: string;
}

type SingleDayReportType = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    finish: number;
    mistake: number;
    power: number;
    speed: number[];
    maxSpeed: number;
    speedPercentage: number[];
    consciousEffort: int;
    trainingDate: string;
}

type GetStudentSingleDayReportErrorType =
    | 'BadRequest'
    | 'StudentDoesNotExist'
    | 'NotAnyDayTraining'
    | 401;

type GetStudentTrainingMachinarySummaryListResponseType = {
    isSuccess: boolean;
    message: string;
    data: GetStudentTrainingMachinarySummaryListDataType[]
};

type GetStudentTrainingMachinarySummaryListDataType = {
    userAuthorizedId: number;
    level: number;
    finsh: number;
    mistake: number;
    power: number;
    trainingDate: string;
    mark: string;
};

type ReplaceStudentTrainingMachinarySummaryRequestType = {
    trainingDate: string;
    mark: string;
};

type ReplaceStudentTrainingMachinarySummaryResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type ReplaceStudentTrainingMachinarySummaryErrorCodeType =
    | 'BadRequest'
    | 'StudentDoesNotExist'
    | 'NotAnyFormalTrainingInfo'
    | 401;

type DeleteStudentTrainingMachinarySummaryRequestType = {
    trainingDate: string
};

type DeleteStudentTrainingMachinarySummaryResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type DeleteStudentTrainingMachinarySummaryErrorCodeType =
    | 'BadRequest'
    | 'StudentDoesNotExist'
    | 401;