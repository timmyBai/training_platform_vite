type TrainingScheduleNotifyResponseType = {
    isSuccess: boolean;
    message: string;
    data: TrainingScheduleNotifyDataType
};

type TrainingScheduleNotifyDataType = {
    isNotify: boolean
}

type TrainingScheduleNotifyErrorCodeType =
    | 401
    | 'StudentDoesNotExist'

type GetThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequencyResponseType = {
    isSuccess: boolean;
    message: string;
    data: GetThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequencyDataType
};

type GetThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequencyDataType = {
    remainingFrequency: number;
    exerciseIntensity: string;
    heartRate: string;
    exerciseFrequency: number;
    exerciseFrequencyText: string;
    trainingUnitMinute: string;
    sports: string[];
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    scheduleDate: string[];
    thisWeekDateRemainingDayRange: string[];
    defaultTrainingScheduleDate: string[];
};

type GetThisWeekTrainingScheduleCardiorespiratoryFitnessRemainingFrequencyErrorCodeType =
    | 'StudentDoesNotExist'
    | 'NotSetUpAnyExercisePrescriptionCardiorespiratoryFitness'
    | 401

type GetThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequencyResponseType = {
    isSuccess: boolean;
    message: string;
    data: GetThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequencyDataType;
}

type GetThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequencyDataType = {
    remainingFrequency: number;
    exerciseIntensity: string;
    heartRate: string;
    exerciseFrequency: number;
    exerciseFrequencyText: string;
    trainingUnitGroup: string;
    trainingUnitNumber: string;
    updateRecordId: string;
    updateRecordDate: string;
    updateRecordTime: string;
    scheduleDate: string[];
    thisWeekDateRemainingDayRange: string[];
    defaultTrainingScheduleDate: string[];
}

type GetThisWeekTrainingScheduleMuscleStrengthAndMuscleEnduranceRemainingFrequencyErrorCodeType =
    | 'StudentDoesNotExist'
    | 'NotSetUpAnyExercisePrescriptionMuscleStrengthAndMuscleEndurance'
    | 401

type GetThisWeekTrainingScheduleSoftnessRemainingFrequencyResponseType = {
    isSuccess: boolean;
    message: string;
    data: GetThisWeekTrainingScheduleSoftnessRemainingFrequencyDataType
}

type GetThisWeekTrainingScheduleSoftnessRemainingFrequencyDataType = {
    remainingFrequency: number;
    exerciseIntensity: string;
    heartRate: string;
    exerciseFrequency: number;
    exerciseFrequencyText: string;
    trainingUnitMinute: string;
    sports: string[];
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    scheduleDate: string[];
    thisWeekDateRemainingDayRange: string[]
    defaultTrainingScheduleDate: string[]
}

type GetThisWeekTrainingScheduleSoftnessRemainingFrequencyErrorCodeType =
    | 'StudentDoesNotExist'
    | 'NotSetUpAnyExercisePrescriptionSoftness'
    | 401;

type GetThisWeekTrainingScheduleBalanceRemainingFrequencyResponseType = {
    isSuccess: boolean;
    message: string;
    data: GetThisWeekTrainingScheduleBalanceRemainingFrequencyDataType;
}

type GetThisWeekTrainingScheduleBalanceRemainingFrequencyDataType = {
    remainingFrequency: number;
    exerciseIntensity: string;
    heartRate: string;
    exerciseFrequency: number;
    exerciseFrequencyText: string;
    trainingUnitMinute: string;
    sports: string[];
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    scheduleDate: string[];
    thisWeekDateRemainingDayRange: string[];
    defaultTrainingScheduleDate: string[];
}

type GetThisWeekTrainingScheduleBalanceRemainingFrequencyErrorCodeType =
    | 'StudentDoesNotExist'
    | 'NotSetUpAnyExercisePrescriptionBalance'
    | 401;

type AddTrainingScheduleWeekRequest = {
    trainingType: number;
    scheduleDate: string[]
}

type AddTrainingScheduleWeekResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type AddTrainingScheduleWeekErrorCodeType =
    | 'BadRequest'
    | 'SelectDateNotThisWeekRange'
    | 'StudentDoesNotExist'
    | 'TrainingScheduleDayCountLargerThanExercisePrescriptionDayCount'
    | 'InsertTrainingScheduleWeekFail'

type UpdateTrainingScheduleRequestType = {
    trainingType: number;
    scheduleDate: string;
    actualCompletion: number;
};

type UpdateTrainingScheduleResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type UpdateTrainingScheduleErrorCodeType =
    | 401
    | 'BadRequest'
    | 'StudentDoesNotExist'
    | 'TrainingScheduleDataExist'
    | 'NotModifyBeforeSixtyTrainingSchedule'
    | 'UpdateTrainingScheduleCardiorespiratoryFitnessFail';