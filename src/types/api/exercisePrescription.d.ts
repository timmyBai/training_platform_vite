type ExercisePrescriptionListResponseType = {
    isSuccess: boolean;
    message: string;
    data: ExercisePrescriptionListDataResponseType
}

type ExercisePrescriptionListDataResponseType = {
    exercisePrescriptionCardiorespiratoryFitnessGroup: ExercisePrescriptionCardiorespiratoryFitnessGroupType;
    exercisePrescriptionMuscleStrengthAndMuscleEnduranceGroup: ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupType;
    exercisePrescriptionSoftnessGroup: ExercisePrescriptionSoftnessGroupType;
    exercisePrescriptionBalanceGroup: ExercisePrescriptionBalanceGroupType;
}

type ExercisePrescriptionCardiorespiratoryFitnessGroupType = {
    userAuthorizedId: number;
    exerciseIntensity: string;
    exerciseFrequency: string;
    trainingUnitMinute: number;
    heartRate: string;
    sports: string[];
    trainingFrequency: string;
    mark: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    children: ExercisePrescriptionCardiorespiratoryFitnessGroupChildrenType[];
}

type ExercisePrescriptionCardiorespiratoryFitnessGroupChildrenType = {
    userAuthorizedId: number;
    exerciseIntensity: string;
    exerciseFrequency: string;
    heartRate: string;
    sports: string[];
    trainingFrequency: string;
    mark: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
}

type ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupType = {
    userAuthorizedId: number;
    exerciseIntensity: string;
    heartRate: string;
    trainingFrequency: string;
    trainingArea: string[];
    mark: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    children: ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupChildrenType[]
}

type ExercisePrescriptionMuscleStrengthAndMuscleEnduranceGroupChildrenType = {
    userAuthorizedId: number;
    exerciseIntensity: string;
    heartRate: string;
    trainingFrequency: string;
    trainingArea: string[];
    mark: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
}

type ExercisePrescriptionSoftnessGroupType = {
    userAuthorizedId: number;
    exerciseIntensity: string;
    heartRate: string;
    sports: string[];
    trainingFrequency: string;
    mark: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    children: ExercisePrescriptionSoftnessGroupType[]
};

type ExercisePrescriptionSoftnessGroupChildrenType = {
    userAuthorizedId: number;
    exerciseIntensity: string;
    heartRate: string;
    sports: string[];
    trainingFrequency: string;
    mark: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
}

type ExercisePrescriptionBalanceGroupType = {
    userAuthorizedId: number;
    exerciseIntensity: string;
    heartRate: string;
    sports: string[];
    trainingFrequency: string;
    mark: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    children: ExercisePrescriptionBalanceGroupChildrenType[]
}

type ExercisePrescriptionBalanceGroupChildrenType = {
    userAuthorizedId: number;
    exerciseIntensity: string;
    heartRate: string;
    sports: string[];
    trainingFrequency: string;
    mark: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
}

type ExercisePrescriptionListErrorCodeType =
    | 'StudentDoesNotExist'
    | 401;

type ExercisePrescriptionTrainingStageResponseType = {
    isSuccess: boolean;
    message: string;
    data: ExercisePrescriptionTrainingStageDataType;
};
    
type ExercisePrescriptionTrainingStageDataType = {
    userAuthorizedId: number;
    firstStage: number;
    middleStage: number;
    startDate: string;
    weekText: string;
    week: number;
}
    
type ExercisePrescriptionTrainingStageErrorCodeType =
    | 401;

type ExercisePrescriptionTrainingStageReplaceRequestType = {
    firstStage: string | undefined;
    middleStage: string | undefined;
    startDate: string | undefined;
}

type ExercisePrescriptionTrainingStageReplaceResponseType = {
    isSuccess: boolean;
    message: string;
    data: ExercisePrescriptionTrainingStageReplaceDataType
};

type ExercisePrescriptionTrainingStageReplaceDataType = {
    userAuthorizedId: number;
    firstStage: number;
    middleStage: number;
    startDate: string;
    week: number;
    weekText: string;
}

type ExercisePrescriptionTrainingStageReplaceErrorCodeType =
    | 400
    | 'BadRequest'
    | 'LargerToday'
    | 'StudentDoesNotExist'
    | 401;





type ExercisePrescriptionCardiorespiratoryFitnessRequestType = {
    exerciseIntensity: string | undefined;
    exerciseFrequency: string | undefined;
    trainingUnitMinute: string | undefined;
    sports: string[] | undefined;
    mark: string | undefined
};

type ExercisePrescriptionCardiorespiratoryFitnessResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type ExercisePrescriptionCardiorespiratoryFitnessErrorCodeType =
    | 400
    | 'BadRequest'
    | 'IllegalSports'
    | 'StudentDoesNotExist'
    | 401

type ExercisePrescriptionMuscleStrengthAndMuscleEnduranceRequestType = {
    exerciseIntensity: string;
    exerciseFrequency: string;
    trainingUnitGroup: string;
    trainingUnitNumber: string;
    trainingArea: string[];
    mark: string;
}

type ExercisePrescriptionMuscleStrengthAndMuscleEnduranceResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
}

type ExercisePrescriptionMuscleStrengthAndMuscleEnduranceErrorCodeType =
    | 400
    | 'BadRequest'
    | 'IllegalTrainingArea'
    | 'StudentDoesNotExist'
    | 401

type ExercisePrescriptionSoftnessRequestType = {
    exerciseIntensity: string;
    exerciseFrequency: string;
    trainingUnitMinute: string;
    sports: string[];
    mark: string;
};

type ExercisePrescriptionSoftnessResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type ExercisePrescriptionSoftnessErrorCodeType =
    | 400
    | 'BadRequest'
    | 'IllegalSports'
    | 'StudentDoesNotExist'
    | 401

type ExercisePrescriptionBalanceRequestType = {
    exerciseIntensity: string;
    exerciseFrequency: string;
    trainingUnitMinute: string;
    sports: string[];
    mark: string;
};

type ExercisePrescriptionBalanceResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
}

type ExercisePrescriptionBalanceErrorCodeType =
    | 400
    | 'BadRequest'
    | 'IllegalSports'
    | 'StudentDoesNotExist'
    | 401