type InstrumentSettingsListResponseType = {
    isSuccess: boolean;
    message: string;
    data: InstrumentSettingsListResponseDataType;
};

type InstrumentSettingsListResponseDataType = {
    machinaryCodeOneGroup: InstrumentSettingsMachinaryCodeOneGroup;
    machinaryCodeTwoGroup: InstrumentSettingsMachinaryCodeTwoGroup;
    machinaryCodeThreeGroup: InstrumentSettingsMachinaryCodeThreeGroup;
    machinaryCodeFourGroup: InstrumentSettingsMachinaryCodeFourGroup;
    machinaryCodeFiveGroup: InstrumentSettingsMachinaryCodeFiveGroup;
    machinaryCodeSixGroup: InstrumentSettingsMachinaryCodeSixGroup;
    machinaryCodeSevenGroup: InstrumentSettingsMachinaryCodeSevenGroup;
    machinaryCodeEightGroup: InstrumentSettingsMachinaryCodeEightGroup;
    machinaryCodeNineGroup: InstrumentSettingsMachinaryCodeNineGroup;
    machinaryCodeTenGroup: InstrumentSettingsMachinaryCodeTenGroup;
}

type InstrumentSettingsMachinaryCodeOneGroup = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
    children: InstrumentSettingsMachinaryCodeOneGroupChildren[]
}

type InstrumentSettingsMachinaryCodeOneGroupChildren = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
}

type InstrumentSettingsMachinaryCodeTwoGroup = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    trainingDate: string;
    trainingTime: string;
    mark: string;
    children: InstrumentSettingsMachinaryCodeTwoGroupChildren[]
}

type InstrumentSettingsMachinaryCodeTwoGroupChildren = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
}

type InstrumentSettingsMachinaryCodeThreeGroup = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    trainingDate: string;
    trainingTime: string;
    mark: string;
    children: InstrumentSettingsMachinaryCodeThreeGroupChildren[]
}

type InstrumentSettingsMachinaryCodeThreeGroupChildren = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
}

type InstrumentSettingsMachinaryCodeFourGroup = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
    children: InstrumentSettingsMachinaryCodeFourGroupChildren[]
}

type InstrumentSettingsMachinaryCodeFourGroupChildren = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
}

type InstrumentSettingsMachinaryCodeFiveGroup = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
    children: InstrumentSettingsMachinaryCodeFiveGroupChildren[]
}

type InstrumentSettingsMachinaryCodeFiveGroupChildren = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
}

type InstrumentSettingsMachinaryCodeSixGroup = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
    children: InstrumentSettingsMachinaryCodeSixGroupChildren[]
}

type InstrumentSettingsMachinaryCodeSixGroupChildren = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
}

type InstrumentSettingsMachinaryCodeSevenGroup = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
    children: InstrumentSettingsMachinaryCodeSevenGroupChildren[]
}

type InstrumentSettingsMachinaryCodeSevenGroupChildren = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
}

type InstrumentSettingsMachinaryCodeEightGroup = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
    children: InstrumentSettingsMachinaryCodeEightGroupChildren[]
}

type InstrumentSettingsMachinaryCodeEightGroupChildren = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
}

type InstrumentSettingsMachinaryCodeNineGroup = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
    children: InstrumentSettingsMachinaryCodeNineGroupChildren[]
}

type InstrumentSettingsMachinaryCodeNineGroupChildren = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
}

type InstrumentSettingsMachinaryCodeTenGroup = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
    children: InstrumentSettingsMachinaryCodeTenGroupChildren[];
}

type InstrumentSettingsMachinaryCodeTenGroupChildren = {
    userAuthorizedId: number;
    machinaryCode: number;
    machinaryCodeName: string;
    level: number;
    maxRange: string;
    power: string;
    mark: string;
    trainingDate: string;
    trainingTime: string;
}

type InstrumentSettingsListErrorCodeType =
    | 401
    | 'StudentDoesNotExist';

type InstrumentSettingsSportsPerformanceRequestType = {
    mark: string | undefined;
    trainingDate: string | undefined;
}

type InstrumentSettingsSportsPerformanceResponseType = {
    isSuccess: boolean;
    message: string;
    data: object
};

type InstrumentSettingsSportsPerformanceErrorCodeType =
    | 400
    | 'BadRequest'
    | 'StudentDoesNotExist'
    | 'DataDoesNotExist'
    | 401;
