type BodyMassAadPhysicalFitnessListResponseType = {
    isSuccess: boolean;
    message: string;
    data: BodyMassAadPhysicalFitnessListDataResponseType;
};

type BodyMassAadPhysicalFitnessListDataResponseType = {
    bmiGroup: BmiGroupResponseType;
    bodyFatGroup: BodyFatGroupResponseType;
    visceralFatGroup: VisceralFatGroupResponseType;
    skeletalMuscleRateGroup: SkeletalMuscleRateGroupResponseType;
    physicalFitnessCardiorespiratoryFitnessGroup: PhysicalFitnessCardiorespiratoryFitnessGroupResponseType;
    physicalFitnessMuscleStrengthAndMuscleEnduranceGroup: PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupResponseType;
    physicalFitnessSoftnessGroup: PhysicalFitnessSoftnessGroupResponseType;
    physicalFitnessBalanceGroup: PhysicalFitnessBalanceGroupResponseType;
}

type BmiGroupResponseType = {
    bmi: number;
    bmiCommentLevel: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
    children: ResponseBmiGroupChildrenType[];
};

type BmiGroupChildrenResponseType = {
    bmi: number;
    bmiCommentLevel: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
}

type BodyFatGroupResponseType = {
    bodyFat: number;
    bodyFatCommentLevel: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
    children: BodyFatGroupChildrenResponseType[];
}

type BodyFatGroupChildrenResponseType = {
    bodyFat: number;
    bodyFatCommentLevel: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
}

type VisceralFatGroupResponseType = {
    visceralFat: number;
    visceralFatCommentLevel: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
    children: VisceralFatGroupChildrenResponseType[];
};

type VisceralFatGroupChildrenResponseType = {
    visceralFat: number;
    visceralFatCommentLevel: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
}

type SkeletalMuscleRateGroupResponseType = {
    skeletalMuscleRate: number;
    skeletalMuscleRateCommentLevel: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
    children: SkeletalMuscleRateGroupChildrenResponseType[]
}

type SkeletalMuscleRateGroupChildrenResponseType = {
    skeletalMuscleRate: number;
    skeletalMuscleRateCommentLevel: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
}

type PhysicalFitnessCardiorespiratoryFitnessGroupResponseType = {
    cardiorespiratoryFitnessFraction: number;
    cardiorespiratoryFitnessComment: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
    children: PhysicalFitnessCardiorespiratoryFitnessGroupChildrenResponseType[]
}

type PhysicalFitnessCardiorespiratoryFitnessGroupChildrenResponseType = {
    cardiorespiratoryFitnessFraction: number;
    cardiorespiratoryFitnessComment: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
}

type PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupResponseType = {
    muscleStrengthAndMuscleEnduranceFitness: number;
    muscleStrengthAndMuscleEnduranceComment: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
    children: PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupChildrenResponseType[]
}

type PhysicalFitnessMuscleStrengthAndMuscleEnduranceGroupChildrenResponseType = {
    muscleStrengthAndMuscleEnduranceFitness: number;
    muscleStrengthAndMuscleEnduranceComment: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
}

type PhysicalFitnessSoftnessGroupResponseType = {
    softnessFitness: number;
    softnessComment: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
    children: PhysicalFitnessSoftnessGroupChildrenResponseType[]
}

type PhysicalFitnessSoftnessGroupChildrenResponseType = {
    softnessFitness: number;
    softnessComment: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
};

type PhysicalFitnessBalanceGroupResponseType = {
    balanceFitness: number;
    balanceComment: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
    children: PhysicalFitnessBalanceGroupChildrenResponseType[]
}

type PhysicalFitnessBalanceGroupChildrenResponseType = {
    balanceFitness: number;
    balanceComment: string;
    updateRecordId: number;
    updateRecordDate: string;
    updateRecordTime: string;
    mark: string;
}

type BodyMassAndPhysicalFitnessListErrorCodeType =
    | 401