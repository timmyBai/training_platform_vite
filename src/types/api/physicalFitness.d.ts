// type PhysicalFitnessCardiorespiratoryFitnessMarkRequestType = {
//     mark: string | undefined;
// }

// type PhysicalFitnessCardiorespiratoryFitnessMarkResponseType = {
//     isSuccess: boolean;
//     message: string;
//     data: object
// }



// type PhysicalFitnessMuscleStrengthAndMuscleEnduranceMarkRequestType = {
//     mark: string | undefined;
// }

// type PhysicalFitnessMuscleStrengthAndMuscleEnduranceMarkResponseType = {
//     isSuccess: boolean;
//     message: string;
//     data: object;
// }

// type PhysicalFitnessMuscleStrengthAndMuscleEnduranceMarkErrorCodeType =
//     | 400
//     | 'BadRequest'
//     | 401

// type PhysicalFitnessSoftnessMarkRequestType = {
//     mark: string | undefined
// }

// type PhysicalFitnessSoftnessMarkResponseType = {
//     isSuccess: boolean;
//     message: string;
//     data: object;
// }

// type PhysicalFitnessSoftnessMarkErrorCodeType =
//     | 400
//     | 'BadRequest'
//     | 401

// type PhysicalFitnessBalanceMarkRequestType = {
//     mark: string | undefined
// }

// type PhysicalFitnessBalanceMarkResponseType = {
//     isSuccess: boolean;
//     message: string;
//     data: object;
// }

// type PhysicalFitnessBalanceMarkErrorCodeType =
//     | 400
//     | 'BadRequest'
//     | 401

type ReplacePhysicalFitnessCardiorespiratoryFitnessStepTestAndMarkRequestType = {
    pulseRateOne: number | undefined;
    pulseRateTwo: number | undefined;
    pulseRateThree: number | undefined;
    mark: string | undefined;
};

type ReplacePhysicalFitnessCardiorespiratoryFitnessStepTestAndMarkResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type ReplacePhysicalFitnessCardiorespiratoryFitnessStandingKneeRaiseRequestType = {
    kneeLiftFrequency: number | undefined;
    mark: string | undefined;
};

type ReplacePhysicalFitnessCardiorespiratoryFitnessStandingKneeRaiseResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type PhysicalFitnessCardiorespiratoryFitnessMarkErrorCodeType =
    | 400
    | 'BadRequest'
    | 'StudentDoesNotExist'
    | 'UnderTheAgeOf21To65'
    | 'UnderTheAgeOf65'
    | 401

type ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyRequestType = {
    kneeCrunchesFrequency: number | undefined;
    mark: string | undefined;
};

type ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceKneeCrunchesFrequencyResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
}

type ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyRequestType = {
    armCurlUpperBodyFrequency: number | undefined;
    mark: string | undefined;
};

type ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlUpperBodyFrequencyResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
}

type ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyRequestType = {
    armCurlLowerBodyFrequency: number | undefined;
    mark: string | undefined;
};

type ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceArmCurlLowerBodyFrequencyResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
}

type ReplacePhysicalFitnessMuscleStrengthAndMuscleEnduranceErrorCodeType =
    | 400    
    | 'BadRequest'
    | 'StudentDoesNotExist'
    | 'UnderTheAgeOf21To65'
    | 'UnderTheAgeOf65'
    | 401

type ReplacePhysicalFitnessSoftnessSeatedForwardBendRequestType = {
    seatedForwardBendOne: number | undefined;
    seatedForwardBendTwo: number | undefined;
    seatedForwardBendThree: number | undefined;
    mark: string | undefined;
};

type ReplacePhysicalFitnessSoftnessSeatedForwardBendResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type ReplacePhysicalFitnessSoftnessChairSeatedForwardBendRequestType = {
    chairSeatedForwardBendOne: number | undefined;
    chairSeatedForwardBendTwo: number | undefined;
    chairSeatedForwardBendThree: number | undefined;
    mark: string | undefined
};

type ReplacePhysicalFitnessSoftnessChairSeatedForwardBendResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
}

type ReplacePhysicalFitnessSoftnessPullBackTestRequestType = {
    pullBackTestOne: number | undefined;
    pullBackTestTwo: number | undefined;
    mark: string | undefined;
};

type ReplacePhysicalFitnessSoftnessPullBackTestResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type ReplacePhysicalFitnessSoftnessErrorCodeType =
    | 400
    | 'BadRequest'
    | 'StudentDoesNotExist'
    | 'UnderTheAgeOf21To65'
    | 'UnderTheAgeOf65'
    | 401

type ReplacePhysicalFitnessBalanceAdultEyeOpeningMonopodSecondRequestType = {
    adultEyeOpeningMonopodSecond: number | undefined;
    mark: string | undefined;
};

type ReplacePhysicalFitnessBalanceAdultEyeOpeningMonopodSecondResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type ReplacePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondRequestType = {
    elderlyEyeOpeningMonopodSecond: number | undefined;
    mark: string | undefined;
};

type ReplacePhysicalFitnessBalanceElderlyEyeOpeningMonopodSecondResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type ReplacePhysicalFitnessBalanceSittingAroundOneSecondRequestType = {
    sittingAroundOneSecond: number | undefined;
    mark: string | undefined;
};

type ReplacePhysicalFitnessBalanceSittingAroundOneSecondResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
}

type ReplacePhysicalFitnessBalanceSittingAroundTwoSecondRequestType = {
    sittingAroundTwoSecond: number | undefined;
    mark: string | undefined;
};

type ReplacePhysicalFitnessBalanceSittingAroundTwoSecondResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
};

type ReplacePhysicalFitnessBalanceErrorCodeType =
    | 400
    | 'BadRequest'
    | 'StudentDoesNotExist'
    | 'UnderTheAgeOf21To65'
    | 'UnderTheAgeOf65';