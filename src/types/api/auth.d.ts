type ApiAuthLoginRequest = {
    account: string;
    password: string;
}

type ApiAuthLoginResponse = {
    isSuccess: boolean;
    message: string;
    data: ApiAuthLoginResponseDataType
}

type ApiAuthLoginResponseDataType = {
    accessToken: string,
    expires_in: number
}

type ApiAuthLoginErrorCode =
    | '400'
    | 'BadRequest'
    | '403'
    | 'NoSuchUser'
    | '410'
    | 'AccountDisabled'

type ApiUserInfoResponse = {
    isSuccess: boolean;
    message: string;
    data: ApiUserInfoResponseDateType
}

type ApiUserInfoResponseDateType = {
    id: string;
    account: string;
    name: string;
    imageUrl: string;
    _gender: string;
    birthday: string;
    age: string;
    _role: string;
    identity: string;
    nbf: string;
    exp: string;
    iss: string;
    aud: string;
}

type AuthRegisterRequestType = {
    photo: string;
    account: string;
    password: string;
    name: string;
    gender: string;
    birthday: string;
    height: number | undefined;
    weight: number | undefined;
    phone: string;
    address: string;
    email: string;
    questionnaireFirstPart: AuthRegisterQuestionnaireFirstPartDataRequestType[];
    questionnaireTwoPart: AuthRegisterQuestionnaireTwoPartDataRequestType[];
    questionnaire_aha_and_acsm_part: Auth_Register_Questionnaire_AHA_AND_ACSM_Part_Data_Request_Type[];

    pulseRateOne?: number;
    pulseRateTwo?: number;
    pulseRateThree?: number;
    kneeCrunchesFrequency?: number;
    seatedForwardBendOne?: number;
    seatedForwardBendTwo?: number;
    seatedForwardBendThree?: number;
    adultEyeOpeningMonopodSecond?: number;

    kneeLiftFrequency?: number;
    armCurlUpperBodyFrequency?: number;
    armCurlLowerBodyFrequency?: number;
    chairSeatedForwardBendOne?: number;
    chairSeatedForwardBendTwo?: number;
    chairSeatedForwardBendThree?: number;
    pullBackTestOne?: number;
    pullBackTestTwo?: number;
    elderlyEyeOpeningMonopodSecond?: number;
    sittingAroundOneSecond?: number;
    sittingAroundTwoSecond?: number;
}

type AuthRegisterQuestionnaireFirstPartDataRequestType = {
    questionNumber: number;
    answer: boolean;
}

type AuthRegisterQuestionnaireTwoPartDataRequestType = {
    questionNumber: number;
    subject: string;
    answer: boolean;
};

type Auth_Register_Questionnaire_AHA_AND_ACSM_Part_Data_Request_Type = {
    part: number;
    subject: number
};

type ApiAuthRegisterResponseType = {
    isSuccess: boolean;
    message: string;
    data: object
};

type AuthRegisterErrorCodeType =
    | 'BadRequest'
    | 'AgeNotSuitable'
    | 'UserAccountAlreadyRegistered'
    | 'UserPhoneAlreadyRegistered'
    | 'InvalidFileExtension'
    | 'FileTooLarge'
    | 'UnfilledQuestionnaireTwoPart'
    | 'ApplyRegisterFail'
    | 'ImageSaveFail'
    | 401;