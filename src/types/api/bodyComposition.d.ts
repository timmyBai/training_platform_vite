type BodyCompositionBmiAndMarkRequestType = {
    bmi: number | string | undefined;
    mark: string | undefined;
};

type BodyCompositionBmiAndMarkResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
}

type BodyCompositionBmiAndMarkErrorCodeType =
    | 400
    | 'BadRequest'
    | 401;

type BodyCompositionBodyFatAndMarkRequestType = {
    bodyFat: number | string | undefined;
    mark: string | undefined;
}

type BodyCompositionBodyFatAndMarkResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
}

type BodyCompositionBodyFatAndMarkErrorCodeType =
    | 400
    | 'BadRequest'
    | 401;

type BodyCompositionVisceralFatAndMarkRequestType = {
    visceralFat: number | string | undefined;
    mark: string | undefined;
}

type BodyCompositionVisceralFatAndMarkResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
}

type BodyCompositionVisceralFatAndMarkErrorCodeType =
    | 400
    | 'BadRequest'
    | 401;

type BodyCompositionSkeletalMuscleRateAndMarkRequestType = {
    skeletalMuscleRate: number | string | undefined;
    mark: string | undefined;
}

type BodyCompositionSkeletalMuscleRateAndMarkResponseType = {
    isSuccess: boolean;
    message: string;
    data: object;
}

type BodyCompositionSkeletalMuscleRateAndMarkErrorCodeType =
    | 400
    | 'BadRequest'
    | 401;