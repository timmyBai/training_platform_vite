type UserStateType = {
    token: string | undefined;
    id: string;
    account: string;
    name: string;
    imageUrl: string;
    gender: string;
    birthday: string;
    age: string;
    role: string;
    identity: string;
    nbf: string;
    exp: string;
    iss: string;
    aud: string;
    identity: string;
};