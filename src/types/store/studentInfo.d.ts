type StudentInfoStateType = {
    studentId: number,
    student_logo: string;
    name: string;
    gender: string;
    age: number;
    email: string;
    posture: string;
    medicalHistory: string;
    dangerGrading: string;
    phone: string;
    address: string;
    mark: string
}