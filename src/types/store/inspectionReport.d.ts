type InspectionReportStateType = {
    summaryPage: boolean;
    summaryListPage: boolean;
    singleDayPage: boolean;
    totalPowerListPage: boolean;
    showBtnSummary: boolean;
    showBtnSingleDay: boolean;
    btnSummaryText: BtnSummaryTextType;
    btnSingleDayText: string;
    machinaryCode: number;
    machinaryName: string;
    showBtnDeleteSummarySingle: boolean;
    visibleDeleteMarkDialog: boolean;
};

type BtnSummaryTextType =
    | '清單'
    | '總表';
