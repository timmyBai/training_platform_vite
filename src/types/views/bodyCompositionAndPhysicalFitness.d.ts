type EditBodyCompositionKeyType =
    | 'bmi'
    | 'bodyFat'
    | 'visceralFat'
    | 'skeletalMuscleRate';

type EditPhysicalFitnessKeyType =
    | 'physical_fitness_cardiorespiratory_fitness'
    | 'physical_fitness_muscle_strength_and_muscle_endurance'
    | 'physical_fitness_softness'
    | 'physical_fitness_balance';