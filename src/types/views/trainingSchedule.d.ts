type TodayType = {
    year: number;
    month: number;
    date: number;
    day: number;
};

type CalendarType = {
    year: number;
    month: number;
    date: number;
    day: number;
}

type CalendarMode = 'watch' | 'choose'