const express = require('express');

const app = express();
const port = 5000;

app.use('/DEV/TrainingBackstage', express.static('build'));

app.use('/', (req, res) => {
    res.redirect('/DEV/TrainingBackstage');
});

app.listen(port);
console.log('Server started at http://localhost:' + port);